#pragma once

#include "types_common.h"
#include "str.h"

// Possible evaluation statuses of File_T functions.
typedef enum FileStatus
{
    FILE_END = 1,
	FILE_OK = 0,
    FILE_ARG_ERROR = -1,
    FILE_WRITE_ERROR = -3,
    FILE_READ_ERROR = -4,
    FILE_OBJ_NULLPTR = -5,
    FILE_DATA_NULLPTR = -6
	
} FileStatus_T;

// Possible file opening modes. Choose values that can be masked individually.
typedef enum FileMode
{
	FILEMODE_READ		= 0x01,
	FILEMODE_WRITE		= 0x02,
	FILEMODE_PLUS       = 0x04,
	FILEMODE_BINARY	    = 0x08

} FileMode_T;

// Declaration of private file data structure.
typedef struct FilePrivate_S FilePrivate_T;

// Declaration of file structure.
typedef struct File File_T;

// Definition of file structure.
struct File
{
    // Private data structure for file
	FilePrivate_T *_private;
    // PUBLIC MEMBER FUNCTIONS VIA FUNCTION POINTERS
    Boolean_T(*ReadLine)(const File_T *const self, String_T *const string, FileStatus_T *const status);
    void (*WriteLine)(File_T *const self, const String_T *const string, FileStatus_T *const status);
};

// Constructor for File_T object.
File_T *FileConstruct(const String_T *const name, const FileMode_T mode, FileStatus_T *const status);
// Destructor for File_T object.
void FileDestruct(File_T *file, FileStatus_T *const status);
// Tells if file exists.
Boolean_T ExistFile(const String_T *const name, FileStatus_T *const status);
