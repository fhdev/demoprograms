#pragma once

#include "types_common.h"
#include "str.h"

// Possible evaluation statuses.
typedef enum EvaluatorStatus_E
{
	EVALUATOR_ARG_ERROR = -3,
	EVALUATOR_PARSER_ERROR = -2,
	EVALUATOR_STACK_ERROR = -1,
	EVALUATOR_OK = 0
} EvaluatorStatus_T;

// PUBLIC FUNCTIONS
// Evaluate infix equation
Float64_T EvaluateInfix(const String_T *const infix, EvaluatorStatus_T *const status);
// Initialize the array of accepted operators.
void InitializeOperators();
// Finalize the array of accepted operators.
void FinalizeOperators();
// Test parser functionality.
void ParserTest();