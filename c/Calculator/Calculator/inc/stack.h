#pragma once

// Include common tpyes header
#include "types_common.h"

// Possible evaluation statuses of Stack_T public functions.
typedef enum StackStatus
{
	STACK_OK			=  0,
	STACK_OVERFLOW		= -1,
	STACK_EMPTY			= -2,
    STACK_OBJ_NULLPTR	= -3,
	STACK_DATA_NULLPTR	= -4
} StackStatus_T;

// Declaration of private data type for stack.
typedef struct StackPrivate StackPrivate_T;

// Declaration of public stack tpye.
typedef struct Stack Stack_T;

// Definition of public stack structure.
struct Stack
{
	// Private data structure.
	StackPrivate_T *_private;
    // PUBLIC MEMBER FUNCTIONS VIA FUNCTION POINTERS
	// Get the number of objects in stack.
	Uint64_T (*GetCount)(const Stack_T *const self, StackStatus_T *const status);
	// Get the size of the stack.
	Uint64_T (*GetSize)(const Stack_T *const self, StackStatus_T *const status);
	// Reset stack to empty.
	void (*Reset)(Stack_T *const self, StackStatus_T *const status);
	// Push object to the top of the stack.
	void* (*Push)(Stack_T *const self, const void *const object, StackStatus_T *const status);
	// Pop object from the top of the stack.
	void* (*Pop)(Stack_T *const self, void *const object, StackStatus_T *const status);
	// Return object from the top of the stack without removing it.
	void* (*Peek)(Stack_T *const self, void *const object, StackStatus_T *const status);
};

// PUBLIC FUNCTION DECLARATIONS
// Constructor for Stack_T object.
Stack_T *NewStack(Uint64_T size, Uint64_T elemSize, StackStatus_T *const status);
// Destructor for Stack_T object.
void DeleteStack(Stack_T *stack, StackStatus_T *const status);
// Test Stack_T functionality.
StackStatus_T StackTest(Uint64_T size, Boolean_T overwrite, Boolean_T overread);