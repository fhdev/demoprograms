#pragma once

// Include common types header
#include "types_common.h"

// Constant return values
#define STRING_RETURN_INVALID_CHAR		((Char_T)-1)
#define STRING_RETURN_INVALID_LENGHT	((Uint64_T)0)
// Specify this value as length when creating String_T based on C-style string constant
#define STRING_LENGTH_CONSTANT          ((Uint64_T)0xFFFFFFFF)   

// Possible evaluation statuses of String_T public functions.
typedef enum StringStatus
{
    STRING_OK				=  0,
    STRING_NEW_ARGERROR		= -1,
    STRING_BAD_INDEX		= -2,
    STRING_BAD_CHAR			= -3,
    STRING_OBJ_NULLPTR		= -4,
    STRING_DATA_NULLPTR		= -5
} StringStatus_T;

// Possible character types to search with IsCharOf function.
typedef enum CharacterType
{
    // Give values that have a specific bit set to be able to mask
    CHARACTER_SPACE		= 0x01,
    CHARACTER_DIGIT		= 0x02,
    CHARACTER_DOT		= 0x04
} CharacterType_T;

// Declaration of private data type for string.
typedef struct StringPrivate StringPrivate_T;

// Declaration of public string tpye.
typedef struct String String_T;

// Definition of string type.
struct String
{
    // Private data structure.
    StringPrivate_T *_private;
    // PUBLIC MEMBER FUNCTIONS VIA FUNCTION POINTERS
    // Get the length of the string
    Uint64_T (*GetLength)(const String_T *const self, StringStatus_T *const	status);
    // Get the size of the allocated memory for the string
    Uint64_T (*GetSize)(const String_T *const self, StringStatus_T *const status);
    // Get a character of the string
    Char_T (*GetChar)(const String_T *const	self,
                      const Uint64_T		index,
                      StringStatus_T *const	status);
    // Set a character of the string
    void (*SetChar)(String_T *const			self,
                    const Char_T			character,
                    const Uint64_T			index,
                    StringStatus_T *const	status);
    // Get C-style string
    void (*GetCString)(const String_T *const self,
                       Char_T *const cString,
                       const Uint64_T size,
                       StringStatus_T *const status);
    // Print the string
    void (*Print)(const String_T *const self, StringStatus_T *const	status);
    // Print the string with newline
    void (*PrintLine)(const String_T *const self, StringStatus_T *const status);
    // Copy the string
    String_T *(*Copy)(const String_T *const self, StringStatus_T *const status);
    // Concatenate another string to the string
    String_T *(*CatString)(String_T *const		self,
                           const String_T *const other,
                           StringStatus_T *const status);
    // Concatenate C-style string to the string
    String_T *(*CatCString)(String_T *const			self,
                            const Char_T *const		cString,
                            const Uint64_T			size,
                            StringStatus_T *const	status);
    // Concatenate character to the string
    String_T *(*CatChar)(String_T *const		self, 
                         const Char_T			character,
                         StringStatus_T *const	status);
    // Determine if character is of a specified kind
    Boolean_T (*IsCharOf)(const String_T *const	self,
                          const Uint64_T		index,
                          const CharacterType_T type,
                          StringStatus_T *const	status);
    // Convert to Float64_T value
    Float64_T (*ToFloat64)(const String_T *const self, StringStatus_T *const status);
    // Determine if substring begining from index contains Float64_T value at the begining
    Boolean_T(*GetFloat64At)(const String_T *const self,
                             String_T *const numStr,
                             Uint64_T *const index,
                             StringStatus_T *const status);
    // Determine if substring begining from index contains a substring at the begining
    Boolean_T(*GetStringAt)(const String_T *const self,
                            String_T *const subStr,
                            Uint64_T *const index,
                            StringStatus_T *const status);
    // Clear content
    void(*Clear)(const String_T *const self, StringStatus_T *const status);

};

// PUBLIC FUNCTION DECLARATIONS
// Constructor for String_T object.
String_T *StringConstruct(const Char_T *const cString, const Uint64_T size, StringStatus_T *const status);
// Destructor for String_T object.
void StringDestruct(String_T *string, StringStatus_T *const status);
// Test String_T functionality.
void StringTest();