#pragma once

#ifdef FALSE
	#undef FALSE
#endif // !FALSE

#ifdef TRUE
	#undef TRUE
#endif // !TRUE

// Boolean type definition
typedef enum Boolean_E
{
	FALSE	= 0,
	TRUE	= 1
} Boolean_T;

typedef char					Char_T;
typedef int						Int32_T;
typedef unsigned int			Uint32_T;
typedef long long int			Int64_T;
typedef unsigned long long int	Uint64_T;
typedef float					Float32_T;
typedef double					Float64_T;