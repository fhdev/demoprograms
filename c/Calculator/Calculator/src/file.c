#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file.h"

// Default line length when creating String_T object.
#define DEFAULT_LINE_LENGTH 1024
// Length of fopen mode string.
#define MODE_STRING_LENGTH 10

// Definition of private data structure for File_T
struct FilePrivate_S
{
    // File pointer
	FILE *filePtr;
    // File open mode
    FileMode_T mode;
};

// LOCAL FUNCTION DECLARATIONS
static Boolean_T readLine(const File_T *const self, String_T *const string, FileStatus_T *const status);
static void writeLine(File_T *const self, const String_T *const string, FileStatus_T *const status);
// PUBLIC FUNCTION DEFINITIONS
// Constructor.
File_T *FileConstruct(const String_T *const name, const FileMode_T mode, FileStatus_T *const status)
{
    FileStatus_T _status = FILE_OBJ_NULLPTR;
    // Allocate memory for public data structure
    File_T *file = (File_T*)malloc(sizeof(File_T));
    if ((file != NULL) && (name != NULL))
    {
        // Allocate memory for private data structure
        file->_private = (FilePrivate_T*)malloc(sizeof(FilePrivate_T));
        if (file->_private != NULL)
        {
            // Construct fopen mode string based on FileMode
            Char_T modeCStr[MODE_STRING_LENGTH] = "";
            _status = FILE_DATA_NULLPTR;
            // If either file read or file write is required
            if ((((mode & FILEMODE_READ) > 0) ^ ((mode & FILEMODE_WRITE) > 0)) > 0)
            {
                // Set mode
                file->_private->mode = mode;
                // Construct fopen mode string
                if ((mode & FILEMODE_WRITE) > 0)
                {
                    strcpy(modeCStr, "w");
                }
                else if ((mode & FILEMODE_READ) > 0)
                {
                    strcpy(modeCStr, "r");
                }
                if ((mode & FILEMODE_PLUS) > 0)
                {
                    strcat(modeCStr, "+");
                }
                if ((mode & FILEMODE_BINARY) > 0)
                {
                    strcat(modeCStr, "b");
                }
                // Copy file name from String_T to C-style string
                Uint64_T size = name->GetLength(name, NULL) + 1;
                Char_T *nameCStr = (Char_T*)malloc(size * sizeof(Char_T));
                if (nameCStr != NULL)
                {
                    name->GetCString(name, nameCStr, size, NULL);
                    // Open file
                    file->_private->filePtr = fopen(nameCStr, modeCStr);
                    if (file->_private->filePtr != NULL)
                    {
                        _status = FILE_OK;
                    }
                    // Release buffer for C-style string 
                    free(nameCStr);
                }
                // Assign public function pointers
                file->ReadLine = readLine;
                file->WriteLine = writeLine;
            }
            // Both file read and file write are required which is nonsense.
            else
            {
                _status = FILE_ARG_ERROR;
            }
        }
    }
    // If something went wrong
    if (_status != FILE_OK)
    {
        // Reset file, return NULL
        FileDestruct(file, NULL);
        file = NULL;
    }
    // Set status if required
    if (status != NULL)
    {
        *status = _status;
    }
    return file;
}

// Destructor.
void FileDestruct(File_T *file, FileStatus_T *const status)
{
    FileStatus_T _status = FILE_OK;
    if (file != NULL)
    {
        if (file->_private != NULL)
        {
            if (file->_private->filePtr != NULL)
            {
                // Close file.
                fclose(file->_private->filePtr);
            }
            free(file->_private);
        }
        free(file);
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
}

// Examine file existence
Boolean_T ExistFile(const String_T *const name, FileStatus_T *const status)
{
    Boolean_T result = FALSE;
    FileStatus_T _status = FILE_OBJ_NULLPTR;
    if (name != NULL)
    {
        // Create C-style string based on String_T object for fopen
        Uint64_T size = name->GetLength(name, NULL) + 1;
        Char_T *nameCStr = (Char_T*)malloc(size * sizeof(Char_T));
        _status = FILE_DATA_NULLPTR;
        if (nameCStr != NULL)
        {
            name->GetCString(name, nameCStr, size, NULL);
            // Try to open for read.
            FILE *fp = fopen(nameCStr, "r");
            if (fp != NULL)
            {
                // If file could be opened for read, it exists.
                fclose(fp);
                result = TRUE;
            }
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return result;
}

// LOCAL FUNCTION DEFINITIONS
// Read a line from text file.
static Boolean_T readLine(const File_T *const self, String_T *const string, FileStatus_T *const status)
{
    Boolean_T result = FALSE;
    FileStatus_T _status = FILE_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL) && (string != NULL))
    {
        _status = FILE_DATA_NULLPTR;
        // Delete content of read line
        string->Clear(string, NULL);
        if (self->_private->filePtr != NULL)
        {
            _status = FILE_ARG_ERROR;
            // Check if mode is matching.
            if (((self->_private->mode & FILEMODE_READ) > 0) || ((self->_private->mode & FILEMODE_PLUS) > 0))
            {
                Int32_T fstatus = 0;
                _status = FILE_READ_ERROR;
                // Check if end of stream is reached.
                if ((fstatus = feof(self->_private->filePtr)) > 0)
                {
                    result = TRUE;          
                }
                else
                {
                    Char_T c = '\0';
                    // Read characters of a line, drop newline at the end.
                    while (((c = fgetc(self->_private->filePtr)) != EOF) && (c != '\n'))
                    {
                        string->CatChar(string, c, NULL);
                    }
                }    
                // Check if error flag was set.
                if ((fstatus = ferror(self->_private->filePtr)) == 0)
                {
                    _status = FILE_OK;
                }
            }
        }
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
    return result;
}

// Write a line to text file
static void writeLine(File_T *const self, const String_T *const string, FileStatus_T *const status)
{
    FileStatus_T _status = FILE_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL) && (string != NULL))
    {
        _status = FILE_DATA_NULLPTR;
        if (self->_private->filePtr != NULL)
        {
            _status = FILE_ARG_ERROR;
            // Check if mode is matching. 
            if (((self->_private->mode & FILEMODE_WRITE) > 0) || ((self->_private->mode & FILEMODE_PLUS) > 0))
            {
                Int32_T fstatus = 0;
                Uint32_T i = 0;
                Uint64_T length = string->GetLength(string, NULL);
                _status = FILE_WRITE_ERROR;
                // Write entire string in file.
                for (i = 0; i < length; i++)
                {
                    Char_T c = string->GetChar(string, i, NULL);
                    fputc(c, self->_private->filePtr);
                }
                // Begin new line.
                fputc('\n', self->_private->filePtr);
                // Check if error flag was set.
                if ((fstatus = ferror(self->_private->filePtr)) == 0)
                {
                    _status = FILE_OK;
                }
            }
        }
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
}