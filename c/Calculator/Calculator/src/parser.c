// Includes
#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "types_common.h"
#include "parser.h"
#include "stack.h"

// MACROS
// Default length when creating a new String_T instance.
#define DEFAULT_STRING_LENGTH 512
// Default stack size when creating a new Stack_T instance.
#define DEFAULT_STACK_SIZE 512
// Number of possible operators. Size of static extern varaible Operators.
#define NUMBER_OF_OPERATORS 9

// TYPE DEFINITIONS
// Possible operator precedence levels
typedef enum OperatorPrecedence
{
    PRECEDENCE_UNDEF    = 0,
    PRECEDENCE_1        = 1,
    PRECEDENCE_2        = 2,
    PRECEDENCE_3        = 3
} OperatorPrecedence_T;

// Possible operator assiciativity types
typedef enum OperatorAssociativity
{
    ASSOCIATIVITY_UNDEF     = 0,
    ASSOCIATIVITY_LEFT      = 1,
    ASSOCIATIVITY_RIGHT     = 2
} OperatorAssociativity_T;

// Possible found token types for function getToken.
typedef enum TokenType
{
    TOKEN_ARG_ERROR = -3,
    TOKEN_INVALID = -2,
    TOKEN_UNDEF = -1,
    TOKEN_END = 0,
    TOKEN_OPERAND = 1,
    TOKEN_ARITHMETIC_OPERATOR = 2,
    TOKEN_PARENTHESIS_LEFT = 3,
    TOKEN_PARENTHESIS_RIGHT = 4
} TokenType_T;

// Type that represents an operator
typedef struct Operator
{
    String_T *symbol;
    String_T *unarySymbol;
    TokenType_T tokenType;
    OperatorPrecedence_T precedence;
    OperatorAssociativity_T associativity;
    Uint32_T numOperands;
    Float64_T (*evalFcn)(Float64_T opLeft, Float64_T opRight);
} Operator_T;

// Possible evaluation statuses for function precedenceParser.
typedef enum ParserStatus
{
	PARSER_ARG_ERROR                = -3,
	PARSER_UNBALANCED_PARENTHESIS   = -2,
	PARSER_TOKEN_ERROR              = -1,
	PARSER_OK                       =  0,
} ParserStatus_T;

// PRIVATE EXTERNAL VARIABLE DECLARATIONS
// Undefined operator
static Operator_T OperatorUndefined;

// Array of existing operators
static Operator_T *Operators;

// PRIVATE FUNCTION DECLARATIONS
// Operator actions
static Float64_T add(Float64_T opLeft, Float64_T opRight);
static Float64_T subtract(Float64_T opLeft, Float64_T opRight);
static Float64_T multiply(Float64_T opLeft, Float64_T opRight);
static Float64_T divide(Float64_T opLeft, Float64_T opRight);
static Float64_T power(Float64_T opLeft, Float64_T opRight);
static Float64_T unaryPlus(Float64_T opLeft, Float64_T opRight);
static Float64_T unaryMinus(Float64_T opLeft, Float64_T opRight);
// Functions for evaluation
static Boolean_T getOperatorAt(const String_T *const expr, Uint64_T *const pointer, Operator_T *const op);
static TokenType_T getToken(const String_T *const expr, String_T *const token, Uint64_T *const pointer);
static ParserStatus_T precedenceParser(const String_T *const infix, String_T *const rpn);
static Float64_T evaluateRpn(const String_T *const rpn, EvaluatorStatus_T *const status);

// PRIVATE FUNCTION DEFINITIONS
// Addition
static Float64_T add(Float64_T opLeft, Float64_T opRight)
{
	return opLeft + opRight;
}

// Subtraction
static Float64_T subtract(Float64_T opLeft, Float64_T opRight)
{
	return opLeft - opRight;
}

// Multiplication
static Float64_T multiply(Float64_T opLeft, Float64_T opRight)
{
	return opLeft * opRight;
}

// Division
static Float64_T divide(Float64_T opLeft, Float64_T opRight)
{
	return opLeft / opRight;
}

//Exponentiation
static Float64_T power(Float64_T opLeft, Float64_T opRight)
{
	return pow(opLeft, opRight);
}

// Unary plus sign
static Float64_T unaryPlus(Float64_T opLeft, Float64_T opRight)
{
	return opRight;
}

// Unary minus sign
static Float64_T unaryMinus(Float64_T opLeft, Float64_T opRight)
{
	return -opRight;
}

// Decide if expr contains a valid operator at pointer, and copy it to op if so.
static Boolean_T getOperatorAt(const String_T *const expr,
                               Uint64_T *const pointer,
                               Operator_T *const op)
{
    Uint32_T i = 0;
    Uint64_T _pointer = 0;
    Boolean_T result = FALSE;
    if (pointer != NULL)
    {
        _pointer = *pointer;
    }
    for (i = 0; i < NUMBER_OF_OPERATORS; i++)
    {
        if (result = expr->GetStringAt(expr, Operators[i].symbol, &_pointer, NULL))
        {
            *op = Operators[i];
            break;
        }
    }
    if (pointer != NULL)
    {
        *pointer = _pointer;
    }
    return result;
}

// Get tokens (operators or operands) from expression string.
static TokenType_T getToken(const String_T *const expr,
                            String_T *const token,
                            Uint64_T *const pointer)
{
	// Default return value is signaling an error
	TokenType_T result = TOKEN_ARG_ERROR;
	// Check for valid arguments
	if ((expr != NULL) && (token != NULL) && (pointer != NULL))
	{
		Char_T  chr = STRING_RETURN_INVALID_CHAR;
		Uint64_T _pointer = *pointer;
		Uint64_T length = expr->GetLength(expr, NULL);
		// Clear token
		token->Clear(token, NULL);
		// Skip all whitespace characters at the begining of the expression's remaining part.
		while(_pointer < length)
		{
			chr = expr->GetChar(expr, _pointer, NULL);
			Boolean_T isSpace = !!isspace(chr);
			if (isSpace)
			{
				_pointer++;
			}
			else
			{
				break;
			}
		}
		// Detect the end of the expression.
		if (_pointer >= length)
		{
			// Return end of expression.
			result = TOKEN_END;
		}
		else
		{
            // Test if the substring of expr pointed to by _pointer contains a valid operator 
            // and get it as an Operator_T object if so.
            Operator_T op = OperatorUndefined;
            Boolean_T isOp = getOperatorAt(expr, &_pointer, &op);
            if (isOp)
            {
                token->CatString(token, op.symbol, NULL);
                result = op.tokenType;
            }
            // If the current substring of expr pointed to by _pointer contains no valid operator. 
            else
			{
                // Test if the substring of expr pointed to by _pointer contains a valid
                // floating point number and copy it into token if so. 
                Boolean_T isFloat64 = expr->GetFloat64At(expr, token, &_pointer, NULL);
                if(isFloat64)
                {
                    result = TOKEN_OPERAND;
                }
                // If the current substring contains neither operator nor floating point number.
                else
                {
                    // Copy invalid character to token, increment pointer manually by 1, signal
                    // invalid result.
                    Char_T invalidChar = expr->GetChar(expr, _pointer, NULL);
                    token->CatChar(token, invalidChar, NULL);
                    _pointer++;
                    result = TOKEN_INVALID;
                }
			}
		}
        // Set the new value for the pointer.
		*pointer = _pointer;
	}
    // Return token type. 
	return result;
}

// Parse infix expression to Reverse Polish Notation expression with implementing the 
// Shunting-yard Algorithm
static ParserStatus_T precedenceParser(const String_T *const infix, String_T *const rpn)
{
	ParserStatus_T result = PARSER_ARG_ERROR;
	if ((infix != NULL) && (rpn != NULL))
	{
		// Terminator character for rpn expression.
		const Char_T ter = ' ';
		Boolean_T operandFlag = FALSE;
		Uint64_T pointer = 0;
		Uint64_T stackCount	= 0;
		Operator_T opPrev = OperatorUndefined;
		Operator_T opCurr = OperatorUndefined;
		TokenType_T	type = TOKEN_UNDEF;
		String_T *token	= StringConstruct(NULL, DEFAULT_STRING_LENGTH, NULL);
		Stack_T *stack = NewStack(DEFAULT_STACK_SIZE, sizeof(Operator_T), NULL);
		rpn->Clear(rpn, NULL);
		result = PARSER_OK;
		while ((type = getToken(infix, token, &pointer)) != TOKEN_END)
		{
			// Token is an operand, copy it to the output stream.
			if (type == TOKEN_OPERAND)
			{
				rpn->CatString(rpn, token, NULL);
				rpn->CatChar(rpn, ter, NULL);
				operandFlag = TRUE;
			}
			// Token is an arithmetic operator.
			else if (type == TOKEN_ARITHMETIC_OPERATOR)
			{
                getOperatorAt(token, NULL, &opCurr);
                // If the previous token was not an operand, and the current operator has a unary version
				if (!operandFlag && (opCurr.unarySymbol != NULL))
				{
                    getOperatorAt(opCurr.unarySymbol, NULL, &opCurr);
				}
				// While there is a previous operator at the top of the operator stack with greater
				// than or equal to precedence, and the current read operator is left associative.
				while ((stackCount = stack->GetCount(stack, NULL)) > 0)
				{
					stack->Peek(stack, &opPrev, NULL);
					if ((opPrev.precedence >= opCurr.precedence) 
						&& 
						(opCurr.associativity == ASSOCIATIVITY_LEFT))
					{
						// Pop previous operator from operator stack and copy it to the output 
						// stream. 
						stack->Pop(stack, &opPrev, NULL);
						rpn->CatString(rpn, opPrev.symbol, NULL);
						rpn->CatChar(rpn, ter, NULL);
					}
					else
					{
						break;
					}
				}
				// Push the current read operator to the operator stack.
				stack->Push(stack, &opCurr, NULL);
				operandFlag = FALSE;
			}
			// Token is left parenthesis.
			else if (type == TOKEN_PARENTHESIS_LEFT)
			{
				// Push the read left parenthesis to the opertor stack.
                getOperatorAt(token, NULL, &opCurr);
				stack->Push(stack, &opCurr, NULL);
				operandFlag = FALSE;
			}
			// Token is right parenthesis.
			else if (type == TOKEN_PARENTHESIS_RIGHT)
			{
				// Flag to indicate if left paranthesis is found in operator stack. 
				Boolean_T parenthesisLeftFlag = FALSE;
				// While the operator at the top of operator stack is not a left paranthesis.
				while ((stackCount = stack->GetCount(stack, NULL)) > 0)
				{
					stack->Pop(stack, &opPrev, NULL);
					// Pop previous operator from operator stack and copy it to the output stream.
					if (opPrev.tokenType != TOKEN_PARENTHESIS_LEFT)
					{
						rpn->CatString(rpn, opPrev.symbol, NULL);
						rpn->CatChar(rpn, ter, NULL);
					}
					else
					{
						parenthesisLeftFlag = TRUE;
						break;
					}
				}
				// If no left parenthesis is found, there are unbalanced parentheses in the
				// infix expression.
				if (!parenthesisLeftFlag)
				{
					result = PARSER_UNBALANCED_PARENTHESIS;
					break;
				}
				operandFlag = FALSE;
			}
			// Token is of invalid character or digit-dot sequence, signal error. 
			else
			{
				operandFlag = FALSE;
				result = PARSER_TOKEN_ERROR;
				break;
			}
		}
		// Everything is ok so far.
		if (result == PARSER_OK)
		{
			// While there are operators in the operator stack
			while ((stackCount = stack->GetCount(stack, NULL)) > 0)
			{
				// Pop previous operator from operator stack and copy it to the output stream.
				stack->Pop(stack, &opPrev, NULL);
				if ((opPrev.tokenType != TOKEN_PARENTHESIS_LEFT)
					&&
					(opPrev.tokenType != TOKEN_PARENTHESIS_RIGHT))
				{
					rpn->CatString(rpn, opPrev.symbol, NULL);
					rpn->CatChar(rpn, ter, NULL);
				}
				// There are unbalanced parantheses in the infix expression because there is a
				// paranthesis in operator stack. 
				else
				{
					result = PARSER_UNBALANCED_PARENTHESIS;
					break;
				}
			}
		}
		// Delete token object
		StringDestruct(token, NULL);
		// Delete stack object
		DeleteStack(stack, NULL);
	}
	return result;
}

// Evaluate Reverse Polish Notation expression
static Float64_T evaluateRpn(const String_T *const rpn, EvaluatorStatus_T *const status)
{
	Float64_T result = 0.0;
	EvaluatorStatus_T _status = EVALUATOR_ARG_ERROR;
	if (rpn != NULL)
	{
		// Create variables for expression processing
		String_T *token = StringConstruct(NULL, DEFAULT_STRING_LENGTH, NULL);
		TokenType_T type = TOKEN_UNDEF;
		Uint64_T pointer = 0;
		// Create operand stack
		Stack_T *stack = NewStack(DEFAULT_STACK_SIZE, sizeof(Float64_T), NULL);
        Uint64_T stackCount = 0;
		// Assume that evaluation will be OK
		_status = EVALUATOR_OK;
		// Process RPN expression
		while ((type = getToken(rpn, token, &pointer)) != TOKEN_END)
		{
			// If token is operand, push it to the stack
			if (type == TOKEN_OPERAND)
			{
				Float64_T operand = token->ToFloat64(token, NULL);
				stack->Push(stack, &operand, NULL);
			}
			// If token is an arithmetic operator
			else if (type == TOKEN_ARITHMETIC_OPERATOR)
			{
				// Get Operator_T operator
                Operator_T op = OperatorUndefined;
                getOperatorAt(token, NULL, &op);
				// Variables for operands and result of operation
				Float64_T numLeft = 0.0, numRight = 0.0, res = 0.0;
				// If operator is unary, pop one operand from stack if there is any
				if ((op.numOperands == 1)
					&&
					((stackCount = stack->GetCount(stack, NULL)) >= 1))
				{
					stack->Pop(stack, &numRight, NULL);
				}
				// If operator is binary, pop two operands from stack if there is at least two
				else if ((op.numOperands == 2)
					&&
					((stackCount = stack->GetCount(stack, NULL)) >= 2))
				{
					stack->Pop(stack, &numRight, NULL);
					stack->Pop(stack, &numLeft, NULL);
				}
				// There are only unary and binary operators, so this case means out of stack error
				else
				{
					_status = EVALUATOR_STACK_ERROR;
					break;
				}
				// Evaluate operator 
				res = op.evalFcn(numLeft, numRight);
				// Push result to stack
				stack->Push(stack, &res, NULL);
			}
			// There should be only operands and arithmetic operators in RPN expression
			else
			{
				_status = EVALUATOR_PARSER_ERROR;
				break;
			}
		}
		// At the end of evaluation the result must be the only item in stack
		if ((_status == EVALUATOR_OK) && ((stackCount = stack->GetCount(stack, NULL)) == 1))
		{
			// Pop result from stack
			stack->Pop(stack, &result, NULL);
		}
		else
		{
			_status = EVALUATOR_STACK_ERROR;
		}
		// Delete token object
		StringDestruct(token, NULL);
		// Delete stack object
		DeleteStack(stack, NULL);
	}
	if (status != NULL)
	{
		*status = _status;
	}
	return result;
}

// PUBLIC FUNCTION DEFINITIONS
// Evaluate infix expression
Float64_T EvaluateInfix(const String_T *const infix, EvaluatorStatus_T *const status)
{
	Float64_T result = 0.0;
	EvaluatorStatus_T _status = EVALUATOR_ARG_ERROR;
	if (infix != NULL)
	{
		// Assume that evaluation will be successful
		_status = EVALUATOR_OK;
		// Convert infix expression to Reverse Polish Notation (RPN)
		String_T *rpn = StringConstruct(NULL, DEFAULT_STRING_LENGTH, NULL);
		ParserStatus_T pStatus = precedenceParser(infix, rpn);
		// Parsing to RPN is successful
		if (pStatus == PARSER_OK)
		{
			result = evaluateRpn(rpn, &_status);
		}
		// Parsing to RPN is not successful
		else
		{
			_status = EVALUATOR_PARSER_ERROR;
		}
		StringDestruct(rpn, NULL);
	}
    // Set status if required
	if (status != NULL)
	{
		*status = _status;
	}
    // Return the result of operations.
	return result;
}

// Initialzie array of possible operators.
void InitializeOperators()
{
    // Undefined operator.
    OperatorUndefined.symbol = NULL;
    OperatorUndefined.unarySymbol = NULL;
    OperatorUndefined.tokenType = TOKEN_UNDEF;
    OperatorUndefined.precedence = PRECEDENCE_UNDEF;
    OperatorUndefined.associativity = ASSOCIATIVITY_UNDEF;
    OperatorUndefined.numOperands = 0;
    OperatorUndefined.evalFcn = NULL;
    // Defined operators
    Operators = (Operator_T*)malloc(NUMBER_OF_OPERATORS * sizeof(Operator_T));
    // PLUS
    Operators[0].symbol = StringConstruct("+", 2, NULL);
    Operators[0].unarySymbol = StringConstruct("#", 2, NULL);
    Operators[0].tokenType = TOKEN_ARITHMETIC_OPERATOR;
    Operators[0].precedence = PRECEDENCE_1;
    Operators[0].associativity = ASSOCIATIVITY_LEFT;
    Operators[0].numOperands = 2;
    Operators[0].evalFcn = add;
    // UNARY PLUS
    Operators[1].symbol = StringConstruct("#", 2, NULL);
    Operators[1].unarySymbol = NULL;
    Operators[1].tokenType = TOKEN_ARITHMETIC_OPERATOR;
    Operators[1].precedence = PRECEDENCE_3;
    Operators[1].associativity = ASSOCIATIVITY_RIGHT;
    Operators[1].numOperands = 1;
    Operators[1].evalFcn = unaryPlus;
    // MINUS
    Operators[2].symbol = StringConstruct("-", 2, NULL);
    Operators[2].unarySymbol = StringConstruct("_", 2, NULL);
    Operators[2].tokenType = TOKEN_ARITHMETIC_OPERATOR;
    Operators[2].precedence = PRECEDENCE_1;
    Operators[2].associativity = ASSOCIATIVITY_LEFT;
    Operators[2].numOperands = 2;
    Operators[2].evalFcn = subtract;
    // UNARY MINUS
    Operators[3].symbol = StringConstruct("_", 2, NULL);
    Operators[3].unarySymbol = NULL;
    Operators[3].tokenType = TOKEN_ARITHMETIC_OPERATOR;
    Operators[3].precedence = PRECEDENCE_3;
    Operators[3].associativity = ASSOCIATIVITY_RIGHT;
    Operators[3].numOperands = 1;
    Operators[3].evalFcn = unaryMinus;
    // MULTIPLICATION SIGN
    Operators[4].symbol = StringConstruct("*", 2, NULL);
    Operators[4].unarySymbol = NULL;
    Operators[4].tokenType = TOKEN_ARITHMETIC_OPERATOR;
    Operators[4].precedence = PRECEDENCE_2;
    Operators[4].associativity = ASSOCIATIVITY_LEFT;
    Operators[4].numOperands = 2;
    Operators[4].evalFcn = multiply;
    // DIVISION SIGN
    Operators[5].symbol = StringConstruct("/", 2, NULL);
    Operators[5].unarySymbol = NULL;
    Operators[5].tokenType = TOKEN_ARITHMETIC_OPERATOR;
    Operators[5].precedence = PRECEDENCE_2;
    Operators[5].associativity = ASSOCIATIVITY_LEFT;
    Operators[5].numOperands = 2;
    Operators[5].evalFcn = divide;
    // POWER SIGN
    Operators[6].symbol = StringConstruct("^", 2, NULL);
    Operators[6].unarySymbol = NULL;
    Operators[6].tokenType = TOKEN_ARITHMETIC_OPERATOR;
    Operators[6].precedence = PRECEDENCE_3;
    Operators[6].associativity = ASSOCIATIVITY_RIGHT;
    Operators[6].numOperands = 2;
    Operators[6].evalFcn = power;
    // LEFT PARENTHESIS
    Operators[7].symbol = StringConstruct("(", 2, NULL);
    Operators[7].unarySymbol = NULL;
    Operators[7].tokenType = TOKEN_PARENTHESIS_LEFT;
    Operators[7].precedence = PRECEDENCE_UNDEF;
    Operators[7].associativity = ASSOCIATIVITY_UNDEF;
    Operators[7].numOperands = 0;
    Operators[7].evalFcn = NULL;
    // RIGHT PARENTHESIS
    Operators[8].symbol = StringConstruct(")", 2, NULL);
    Operators[8].unarySymbol = NULL;
    Operators[8].tokenType = TOKEN_PARENTHESIS_RIGHT;
    Operators[8].precedence = PRECEDENCE_UNDEF;
    Operators[8].associativity = ASSOCIATIVITY_UNDEF;
    Operators[8].numOperands = 0;
    Operators[8].evalFcn = NULL;
}

// Finalize (free) the array of possible operators.
void FinalizeOperators()
{
    Uint32_T i = 0;
    // Destruct every dynamically allocated objects.
    for (i = 0; i < NUMBER_OF_OPERATORS; i++)
    {
        StringDestruct(Operators[i].symbol, NULL);
        StringDestruct(Operators[i].unarySymbol, NULL);
    }
    // Free array.
    free(Operators);
}

// Test parser functionality.
void ParserTest()
{
    // Example infix expression to process.
	String_T *expr = StringConstruct("(32+62)*7.54/8*9-(69.52/8.8-8.96)", 100, NULL);
    // Allocate memory for tokens and rpn expression.
	String_T *token = StringConstruct(NULL, 100, NULL);  
	String_T *rpn = StringConstruct(NULL, 1000, NULL);
    // Pointer for processing the infix expression.
    Uint64_T exprPtr = 0;
    // Counter to prevent infinite loops.
    Uint32_T watchCounter = 0;
    // Type of current token.
	TokenType_T type;
    // Status of inline expression parser.
	ParserStatus_T pStatus;
    // Status of rpn expression evaluator.
	EvaluatorStatus_T eStatus;
    // Print input: infix expression:
	printf("INFIX EXPRESSION = ");
	expr->PrintLine(expr, NULL);
    // Process infix expression and display found token types and strings.
	while ((type = getToken(expr, token, &exprPtr)) != TOKEN_END)
	{
		switch (type)
		{
			case TOKEN_PARENTHESIS_LEFT:
			{
				printf("[left paranthesis] = ");
				break;
			}
			case TOKEN_PARENTHESIS_RIGHT:
			{
				printf("[right paranthesis] = ");
				break;
			}
			case TOKEN_OPERAND:
			{
				printf("[operand] = ");
				break;
			}
			case TOKEN_ARITHMETIC_OPERATOR:
			{
				printf("[operator] = ");
				break;
			}
			case TOKEN_INVALID:
			{
				printf("[invalid] = ");
				break;
			}
			case TOKEN_UNDEF:
			{
				printf("[undefined] = ");
				break; 
			}
			case TOKEN_ARG_ERROR:
			{
				printf("[error] =");
				break;
			}
			default:
			{
				printf("[default]=");
				break;
			}
		}
		token->PrintLine(token, NULL);
		// Prevent infinite loop
		if (watchCounter++ > 100)
		{
            printf("Test aborted, infinite loop broken...\n");
			break;
		}
	}
    // Parse infix expression to rpn expression.
	pStatus = precedenceParser(expr, rpn);
    // Print resulting rpn expression.
	printf("RPN EXPRESSION = ");
	rpn->PrintLine(rpn, NULL);
    // Print status of parser
    printf("[parser status] = %d\n", pStatus);
    // Evaluate rpn expression.
	Float64_T result = EvaluateInfix(expr, &eStatus);
    // Print result.
	printf("RESULT = %lf\n", result);
    // Print status of evaluator.
	printf("[evaluator status] = %d\n", eStatus);
    // Destruct (free) dynamically allocated objects.
	StringDestruct(rpn, NULL);
	StringDestruct(expr, NULL);
	StringDestruct(token, NULL);
}
