// INCLUDES
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include "parser.h"
#include "stack.h"
#include "str.h"
#include "file.h"

// MACROS
#define DEFAULT_STRING_LENGTH   ((Uint32_T)512)
#define PRINT_PRECISION         ((Uint32_T)22)
#define PRINT_WIDTH             ((Uint32_T)30)

// MAIN
// Main function of program Calculator.
int main(int argc, Char_T **argv)
{
    // Initialize the array of possible operators.
    InitializeOperators();
	if ((argc < 2) || (argc > 3))
	{
		printf("Usage:\n\tCalculator expression\n\tCalculator input-file output-file\n");
	}
    // Evaluate single expression from command line.
	else if (argc == 2)
	{
		Float64_T result = 0.0;
        // Create expression as String_T object.
		String_T *expression = StringConstruct(argv[1], STRING_LENGTH_CONSTANT, NULL);
		EvaluatorStatus_T status = EVALUATOR_OK;
        // Evaluate expression.
		result = EvaluateInfix(expression, &status);
        // Display results.
		if (status == EVALUATOR_OK)
		{
            printf("%.*lg\n", PRINT_PRECISION, result);
		}
		else
		{
			printf("Error during evaluation...\n");
		}
        // Free String_T object.
        StringDestruct(expression, NULL);
	}
    // Evaluate text file. Each line should contain one expression. 
	else if (argc == 3)
	{
        // Create input and output file names as String_T objects.
		String_T *inputName = StringConstruct(argv[1], STRING_LENGTH_CONSTANT, NULL);
        String_T *outputName = StringConstruct(argv[2], STRING_LENGTH_CONSTANT, NULL);
        // Input file exists.
		if (ExistFile(inputName, NULL))
		{
            // Open input file for reading.
            File_T *inputFile = FileConstruct(inputName, FILEMODE_READ, NULL);
			Char_T choice = 'Y';
            // Confirm overwrite of output file if exists.
			if (ExistFile(outputName, NULL))
			{
				printf("The output-file '%s' already exists. Overwrite (Y/N)? ", argv[2]);
				choice = getchar();
				choice = toupper(choice);
				printf("\n");
			}
            // Overwrite is confirmed or output file does not exist.
			if (choice == 'Y')
			{
                // Open output file for writing.
                File_T *outputFile = FileConstruct(outputName, FILEMODE_WRITE, NULL);
                // Output file opened successfully.
				if (outputFile != NULL)
				{
					FileStatus_T status = FILE_OK;
                    // Write header for .csv
					String_T *line = StringConstruct("Infix;Result;Status", DEFAULT_STRING_LENGTH, NULL);
                    outputFile->WriteLine(outputFile, line, NULL);
                    // Read every line of input file one by another.
					while (!inputFile->ReadLine(inputFile, line, NULL))
					{
                        // Skip empty lines.
                        Uint64_T length = line->GetLength(line, NULL);
                        if (length > 0)
                        {
                            // Evaluate expression in line.
                            Float64_T result = 0.0;
                            Char_T resultString[PRINT_WIDTH] = "";
                            EvaluatorStatus_T status = EVALUATOR_OK;
                            result = EvaluateInfix(line, &status);
                            // Compose .csv line with results
                            snprintf(resultString, PRINT_WIDTH, "%.*lg", PRINT_PRECISION, result);
                            line->CatChar(line, ';', NULL);
                            line->CatCString(line, resultString, PRINT_WIDTH, NULL);
                            line->CatChar(line, ';', NULL);
                            snprintf(resultString, PRINT_WIDTH, "%0+4d", status);
                            line->CatCString(line, resultString, PRINT_WIDTH, NULL);
                            // Write results to output file.
                            outputFile->WriteLine(outputFile, line, NULL);
                            // Write results to console.
                            if (status == EVALUATOR_OK)
                            {
                                printf("%.*lg\n", PRINT_PRECISION, result);
                            }
                            else
                            {
                                printf("Error during evaluation...\n");
                            }
                        }
					}
                    // Free String_T and File_T objects.
                    StringDestruct(line, NULL);
					FileDestruct(outputFile, NULL);
				}
                // Output file could not be opened.
				else
				{
					printf("Could not create output-file!\n");
				}
			}
            // Close and free File_T object.
            FileDestruct(inputFile, NULL);
		}
        // Input file does not exists.
		else
		{
			printf("Could not open input-file!\n");
		}
        // Free String_T objects.
        StringDestruct(inputName, NULL);
        StringDestruct(outputName, NULL);
	}
    // Finalize the array of possible operators.
    FinalizeOperators();
	return 0;
}