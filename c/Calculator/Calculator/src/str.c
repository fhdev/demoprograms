// For library function printf (testing)
#include <stdio.h>
// For library functions malloc and realloc
#include <stdlib.h>
// For library functions memcpy and memset
#include <string.h>
// For library functions isdigit and isspace
#include <ctype.h>
// Own header with declarations for String_T 
#include "str.h"

// Default size of increasement for container array when resize is needed. 
#define CAT_SIZE_INCREASE ((Uint64_T)32)

// Definition of private data type.
struct StringPrivate
{
    // C-style string that holds character array
    Char_T *cString;
    // Length of actual string value
    Uint64_T length;
    // Size of actual container array
    Uint64_T size;
};

// LOCAL FUNCTION DECLARATIONS (PUBLIC MEMBER FUNCTIONS VIA FUNCTION POINTERS)
static Uint64_T	getLength(const String_T *const self, StringStatus_T *const status);

static Uint64_T	getSize(const String_T *const self, StringStatus_T *const status);

static Char_T getChar(const String_T *const	self,
                      const Uint64_T		index,
                      StringStatus_T *const	status);

static void	setChar(String_T *const			self,
                    const Char_T			character,
                    const Uint64_T			index,
                    StringStatus_T *const	status);

static void getCString(const String_T *const self,
                       Char_T *const cString,
                       const Uint64_T size,
                       StringStatus_T *const status);

static void	print(const String_T *const self, StringStatus_T *const status);

static void printLine(const String_T *const self, StringStatus_T *const status);

static String_T *copy(const String_T *const self, StringStatus_T *const status);

static String_T *catString(String_T *const			self,
                           const String_T *const	other,
                           StringStatus_T *const	status);

static String_T *catCString(String_T *const			self,
                            const Char_T *const		cString,
                            const Uint64_T			size,
                            StringStatus_T *const	status);

static String_T *catChar(String_T *const		self,
                         const Char_T			character,
                         StringStatus_T *const	status);

static Boolean_T isCharOf(const String_T *const	self,
                          const Uint64_T		index,
                          const CharacterType_T	type,
                          StringStatus_T *const	status);

static Float64_T toFloat64(const String_T *const self, StringStatus_T *const status);

static Boolean_T getFloat64At(const String_T *const self,
                              String_T *const numStr,
                              Uint64_T *const index,
                              StringStatus_T *const status);

static Boolean_T getStringAt(const String_T *const self,
                             String_T *const subStr,
                             Uint64_T *const index,
                             StringStatus_T *const status);

static void clear(const String_T *const self, StringStatus_T *const status);

// PRIVATE FUNCTION DECLARATIONS
// Check the validity of a char that is to be set with setChar.
static Boolean_T isValidChar(Char_T character);

// PUBLIC FUNCTION DEFINITIONS
// Constructor
String_T *StringConstruct(const Char_T *const cString, const Uint64_T size, StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    String_T *string = (String_T*)malloc(sizeof(String_T));
    if (string != NULL)
    {
        string->_private = (StringPrivate_T*)malloc(sizeof(StringPrivate_T));
        if (string->_private != NULL)
        {
            // Assume that pointer to data will be NULL. If not, status will be set accordingly.
            _status = STRING_DATA_NULLPTR;
            // Create new string object based on existing C-style string. Argument cString must be
            // an array of characters and argument size must be the size of this array!
            if ((cString != NULL) && (*cString != '\0') && (size > 1))
            {
                // Calculate input length without trailing '\0'.
                Uint64_T length = 0;
                while ((length < size) && (cString[length] != '\0'))
                {
                    length++;
                }
                // Assign calculated length without trailing '\0'.
                string->_private->length = length;
                // Assing calculated memory size
                string->_private->size = length + 1;
                // Allocate memory for C-stlye string with trailing '\0'.
                string->_private->cString = (Char_T*)malloc((length + 1) * sizeof(Char_T));
                if (string->_private->cString != NULL)
                {
                    // Copy valuable characters without trailing '\0'.
                    memcpy(string->_private->cString, cString, length);
                    // Create trailing '\0'.
                    string->_private->cString[length] = '\0';
                    // Set status to OK.
                    _status = STRING_OK;
                }
            }
            // Create an empty string with given size. Size less than 2 makes no sense because
            // trailing '\0' takes a place.
            else if(size > 1)
            {
                // Set length
                string->_private->length = 0;
                // Set size
                string->_private->size = size;
                // Allocate memory for C-stlye string with trailing '\0'.
                string->_private->cString = (Char_T*)malloc(size * sizeof(Char_T));
                if (string->_private->cString != NULL)
                {
                    // Clear allocated memory. 
                    memset(string->_private->cString, 0, size * sizeof(Char_T));
                    // Set status to OK.
                    _status = STRING_OK;
                }
            }
            else
            {
                // Initialize fields because malloc leaves garbage, and garbage in cString will 
                // cause function free to fail. 
                string->_private->cString = NULL;
                string->_private->length = 0;
                string->_private->size = 0;
                _status = STRING_NEW_ARGERROR;
            }
        }
        // Assign public functions
        string->GetLength = getLength;
        string->GetSize = getSize;
        string->GetChar = getChar;
        string->SetChar = setChar;
        string->GetCString = getCString;
        string->Print = print;
        string->PrintLine = printLine;
        string->Copy = copy;
        string->CatString = catString;
        string->CatCString = catCString;
        string->CatChar = catChar;
        string->IsCharOf = isCharOf;
        string->ToFloat64 = toFloat64;
        string->GetFloat64At = getFloat64At;
        string->GetStringAt = getStringAt;
        string->Clear = clear;
    }
    // In case of error clear unfinished new string object and return with NULL.
    if (_status != STRING_OK)
    {
        StringDestruct(string, NULL);
        string = NULL;
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
    return string;
}

// Destructor.
void StringDestruct(String_T *string, StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OK;
    if (string != NULL)
    {
        if (string->_private != NULL)
        {
            if (string->_private->cString != NULL)
            {
                free(string->_private->cString);
            }
            free(string->_private);
        }
        free(string);
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
}

// Test String_T functionality. 
void StringTest()
{
    String_T *s, *t, *u, *v;
    StringStatus_T status;
    printf("Creating new empty string s[0,100]...\n");
    s = StringConstruct(NULL, 100, &status);
    printf("Status: %d...\nPrinting new string s[%llu,%llu]...\n", status, s->GetLength(s, NULL), s->GetSize(s, NULL));
    s->PrintLine(s, &status);
    printf("Status: %d...\nCopying string s to new string t...\n", status);
    t = s->Copy(s, &status);
    printf("Status: %d...\nPrinting new string t[%llu,%llu]...\n", status, t->GetLength(s, NULL), t->GetSize(s, NULL));
    t->PrintLine(t, &status);
    printf("Status: %d...\nSet s[0] to 'A'\n", status);
    s->SetChar(s, 'A', 0, &status);
    printf("Status: %d...\nPrinting result s[NE100]...\n", status);
    s->PrintLine(s, &status);
    printf("Status: %d...\nSet t[1] to 'B' which should not succeed...\n", status);
    t->SetChar(t, 'B', 1, &status);
    printf("Status: %d...\nPrinting result t[E100]...\n", status);
    s->PrintLine(t, &status);
    printf("Status: %d...\nDeleting t[E100]...\n", status);
    StringDestruct(t, &status);
    printf("Status: %d...\nCreate new string t[NE] based on string \"Almafa\"...\n", status);
    t = StringConstruct("Almafa", 7, &status);
    printf("Status: %d...\nPrinting new string t[NE]...\n", status);
    t->PrintLine(t, &status);
    Char_T c[1000] = "alma ";
    u = StringConstruct(c, 1000, &status);
    v = StringConstruct(NULL, 1000, &status);
    v->PrintLine(v, &status);
    v->CatString(v, u, &status);
    v->PrintLine(v, &status);
    v->CatString(v, v, &status);
    v->PrintLine(v, &status);
    v->CatCString(v, c, 1000, &status);
    v->PrintLine(v, &status);
    v->CatCString(v, "korte", 10, &status);
    v->PrintLine(v, &status);
    printf("Status: %d\n", status);
    u->CatChar(u, '*', &status);
    u->PrintLine(u, &status);
    v->CatChar(v, '*', &status);
    v->PrintLine(v, &status);
    u->Clear(u, &status);
    u->CatChar(u, 'A', &status);
    u->PrintLine(u, &status);
    String_T *w = StringConstruct("9.", 3, &status);
    w->PrintLine(w, NULL);
    printf("DigitOrDot(0)=%d DigitOrDot(1)=%d\n", w->IsCharOf(w, 0, CHARACTER_DIGIT | CHARACTER_DOT, &status), w->IsCharOf(w, 1, CHARACTER_DIGIT | CHARACTER_DOT, &status));
    StringDestruct(s, NULL);
    StringDestruct(t, NULL);
    StringDestruct(u, NULL);
    StringDestruct(v, NULL);
}

// LOCAL FUNCTION DEFINITIONS (PUBLIC MEMBER FUNCTIONS VIA FUNCTION POINTERS)
// Get the current length of the string.
static Uint64_T getLength(const String_T *const self, StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    Uint64_T length = STRING_RETURN_INVALID_LENGHT;
    if ((self != NULL) && (self->_private != NULL))
    {
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            length = self->_private->length;
            _status = STRING_OK;
        }
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
    return length;
}

// Get the current size of the string container buffer.
static Uint64_T	getSize(const String_T *const self, StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    Uint64_T size = STRING_RETURN_INVALID_LENGHT;
    if ((self != NULL) && (self->_private != NULL))
    {
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            size = self->_private->size;
            _status = STRING_OK;
        }
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
    return size;
}

// Get character specified by index.
static Char_T getChar(const String_T *const self,
                      const Uint64_T		index,
                      StringStatus_T *const status)
{
    Char_T character = STRING_RETURN_INVALID_CHAR;
    // If one of the dynamically allocated objects does not exist, this "default" will be returned.
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    // String object must exist.
    if ((self != NULL) && (self->_private != NULL))
    {
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        // C-style string object must exist.
        if (self->_private->cString != NULL)
        {
            // Index is only valid if it is within the string.
            if ((0 <= index) && (index < self->_private->length))
            {
                character = self->_private->cString[index];
                _status = STRING_OK;
            }
            else
            {
                _status = STRING_BAD_INDEX;
            }
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return character;
}

// Set character specified by index.
static void setChar(String_T *const			self,
                    const Char_T			character,
                    const Uint64_T			index,
                    StringStatus_T *const	status)
{
    // If one of the dynamically allocated objects does not exist, this "default" will be returned.
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    // If the character to be set is valid.
    // String object must exist.
    if ((self != NULL) && (self->_private != NULL))
    {        
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        // C-style string object must exist.
        if (self->_private->cString != NULL)
        {
            _status = STRING_BAD_CHAR;
            if (isValidChar(character))
            {
                // Assume that index will be bad
                _status = STRING_BAD_INDEX;
                // The last character is always the trailing '\0', so valid indexes are
                // 0 <= index <= (size - 2). 
                if ((0 <= index) && (index < (self->_private->size - 1)))
                {
                    //There should not be a zero character before the character one wants
                    // to set.
                    if ((index == 0) || (self->_private->cString[index - 1] != '\0'))
                    {
                        // Set character.
                        self->_private->cString[index] = character;
                        // Maintain length.
                        if (index + 1 > self->_private->length)
                        {
                            self->_private->length = index + 1;
                        }
                        _status = STRING_OK;
                    }
                }
            }
        }
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
}

// Get copy of C-style string representation.
static void getCString(const String_T *const self,
                       Char_T *const cString,
                       const Uint64_T size,
                       StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL) && (cString != NULL))
    {
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            _status = STRING_OK;
            Uint64_T cpyLen = self->_private->length;
            if (cpyLen > size - 1)
            {
                cpyLen = size - 1;
                _status = STRING_BAD_INDEX;
            }
            memcpy(cString, self->_private->cString, cpyLen);
            cString[cpyLen] = '\0';    
        }
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
}

// Print string to console
static void print(const String_T *const self, StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL))
    {
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            printf("%s", self->_private->cString);
            _status = STRING_OK;
        }
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
}

// Print string to console with newline
static void printLine(const String_T *const self, StringStatus_T *const status)
{
    print(self, status);
    printf("\n");
}

// Copy to other String_T object.
static String_T *copy(const String_T *const self, StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    String_T *result = (String_T*)malloc(sizeof(String_T));
    if ((self != NULL) && (result != NULL))
    {
        result->_private = (StringPrivate_T*)malloc(sizeof(StringPrivate_T));
        if ((self->_private!= NULL) && (result->_private != NULL))
        {	
            // Assume that pointer to data will be NULL. If not, status will be set accordingly.
            _status = STRING_DATA_NULLPTR;
            result->_private->length = self->_private->length;
            result->_private->size = self->_private->size;
            result->_private->cString = (Char_T*)malloc(self->_private->size * sizeof(Char_T));
            if ((self->_private->cString != NULL) && (result->_private->cString != NULL))
            {
                memcpy(result->_private->cString, self->_private->cString, self->_private->size);
                _status = STRING_OK;
            }
        }
        result->GetLength = self->GetLength;
        result->GetSize = self->GetLength;
        result->Copy = self->Copy;
        result->GetChar = self->GetChar;
        result->SetChar = self->SetChar;
        result->GetCString = self->GetCString;
        result->Print = self->Print;
        result->PrintLine = self->PrintLine;
        result->CatString = self->CatString;
        result->CatCString = self->CatCString;
        result->CatChar = self->CatChar;
        result->IsCharOf = self->IsCharOf;
        result->ToFloat64 = self->ToFloat64;
        result->GetFloat64At = self->GetFloat64At;
        result->GetStringAt = self->GetStringAt;
        result->Clear = self->Clear;
    }
    // In case of error clear unfinished new string object and return with NULL.
    if (_status != STRING_OK)
    {
        StringDestruct(result, NULL);
        result = NULL;
    }
    // Return status
    if (status != NULL)
    {
        *status = _status;
    }
    return result;
}

// Concatenate other String_T object.
static String_T *catString(String_T *const			self,
                           const String_T *const	other,
                           StringStatus_T *const	status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (other != NULL) && (self->_private != NULL) && (other->_private != NULL))
    {
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        if ((self->_private->cString != NULL) && (other->_private->cString != NULL))
        {
            // Calculate accumulated length
            Uint64_T newLength = self->_private->length + other->_private->length;
            // Result fits in self.
            if (newLength < self->_private->size)
            {
                memcpy(self->_private->cString + self->_private->length,
                    other->_private->cString,
                    other->_private->length);
                self->_private->cString[newLength] = '\0';
                self->_private->length = newLength;
                _status = STRING_OK;
            }
            // Result does not fit in self.
            else
            {
                // Allocate memory for the result string.
                Uint64_T newSize = newLength + CAT_SIZE_INCREASE + 1;
                Char_T *cString = (Char_T*)malloc(newSize * sizeof(Char_T));
                if (cString != NULL)
                {
                    memset(cString, 0, newSize * sizeof(Char_T));
                    // Copy the content of original C-style string into the new C-style string. 
                    memcpy(cString, self->_private->cString, self->_private->length);
                    // Copy the content of other C-style string into the new C-style string.
                    memcpy(cString + self->_private->length,
                        other->_private->cString,
                        other->_private->length);
                    // Set trailing '\0' character.
                    cString[newLength] = '\0';
                    // Free original C-style string.
                    free(self->_private->cString);
                    // Set poiter to the new result string.
                    self->_private->cString = cString;
                    // Assign new length and size.
                    self->_private->length = newLength;
                    self->_private->size = newSize;
                    // Set status.
                    _status = STRING_OK;
                }
            }
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return self;
}

// Concatenate other C-style string.
static String_T *catCString(String_T *const			self,
                            const Char_T *const		cString,
                            const Uint64_T			size,
                            StringStatus_T *const	status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL))
    {
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            // Create String_T object from C-style string.
            String_T *other = StringConstruct(cString, size, &_status);
            if (_status == STRING_OK)
            {
                // Concatenate other String_T object to self.
                self->CatString(self, other, &_status);
            }
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return self;
}

// Concatenate single character.
static String_T *catChar(String_T *const		self,
                         const Char_T			character,
                         StringStatus_T *const	status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL))
    {
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            if (isValidChar(character))
            {
                // Result fits in self
                if (self->_private->length < (self->_private->size - 1))
                {
                    self->SetChar(self, character, self->_private->length, &_status);
                }
                // Result does not fit in self
                else
                {
                    // Create C-style string from character
                    Char_T cString[2];
                    cString[0] = character;
                    cString[1] = '\0';
                    // Concatenate C-style string to String_T object
                    self->CatCString(self, cString, 2, &_status);
                }
            }
            else
            {
                _status = STRING_BAD_CHAR;
            }
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return self;
}

// Decide if character at index has the specified type.
static Boolean_T isCharOf(const String_T *const	self,
                          const Uint64_T		index,
                          const CharacterType_T	type,
                          StringStatus_T *const	status)
{
    Boolean_T result = FALSE;
    // If one of the dynamically allocated objects does not exist, this "default" will be returned.
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    // String object must exist.
    if ((self != NULL) && (self->_private != NULL))
    {
        // Assume that pointer to data will be NULL. If not, status will be set accordingly.
        _status = STRING_DATA_NULLPTR;
        // C-style string object must exist.
        if (self->_private->cString != NULL)
        {
            // Index is only valid if it is within the string.
            if (index < self->_private->length)
            {
                if(!!(type & CHARACTER_DIGIT))
                {
                    result |= !!isdigit(self->_private->cString[index]);
                }
                if(!!(type & CHARACTER_SPACE))
                {
                    result |= !!isspace(self->_private->cString[index]);
                }
                if(!!(type & CHARACTER_DOT))
                {
                    result |= !!(self->_private->cString[index] == '.');
                }
                _status = STRING_OK;
            }
            else
            {
                _status = STRING_BAD_INDEX;
            }
        }

    }
    if (status != NULL)
    {
        *status = _status;
    }
    return result;
}

// Convert to Float64_T
static Float64_T toFloat64(const String_T *const self, StringStatus_T *const status)
{
    Float64_T result = 0.0;
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL))
    {
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            result = atof(self->_private->cString);
            _status = STRING_OK;
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return result;
}

// Get substring begining from index that contains a valid floating point number and copy
// it to numStr.
static Boolean_T getFloat64At(const String_T *const self,
                              String_T *const numStr,
                              Uint64_T *const index,
                              StringStatus_T *const status)
{
    Boolean_T result = FALSE;
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (numStr != NULL) && (index != NULL) && (self->_private != NULL) && (numStr->_private != NULL))
    {
        _status = STRING_DATA_NULLPTR;
        if ((self->_private->cString != NULL) && (numStr->_private->cString != NULL))
        {
            _status = STRING_BAD_INDEX;
            Uint64_T _index = *index;
            // The last character is always the trailing '\0', so valid indexes are
            // 0 <= index <= (size - 2). 
            if ((0 <= _index) && (_index < (self->_private->size - 1)))
            {
                numStr->Clear(numStr, &_status);
                if (_status == STRING_OK)
                {
                    Char_T *end = NULL;
                    Float64_T num = 0.0;
                    Uint64_T cnt = 0;
                    num = strtod(self->_private->cString + _index, &end);
                    cnt = end - (self->_private->cString + _index);
                    if (cnt > 0)
                    {
                        Char_T *buff = (Char_T *)malloc(cnt + 1);
                        memcpy(buff, self->_private->cString + _index, cnt);
                        buff[cnt] = '\0';
                        numStr->CatCString(numStr, buff, cnt + 1, &_status);
                        *index = _index + cnt;
                        if (_status == STRING_OK)
                        {
                            result = TRUE;
                        }
                    }
                }
            }
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return result;
}

// Get substring begnining from index that contains subStr. 
static Boolean_T getStringAt(const String_T *const self,
                                  String_T *const subStr,
                                  Uint64_T *const index,
                                  StringStatus_T *const status)
{
    Boolean_T result = FALSE;
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (subStr != NULL) && (index != NULL) && (self->_private != NULL) && (subStr->_private != NULL))
    {
        _status = STRING_DATA_NULLPTR;
        if ((self->_private->cString != NULL) && (subStr->_private->cString != NULL))
        {
            _status = STRING_BAD_INDEX;
            Uint64_T _index = *index;
            // The last character is always the trailing '\0', so valid indexes are
            // 0 <= index <= (size - 2). 
            if ((0 <= _index) && (_index < (self->_private->size - 1)))
            {
                Char_T *pointer = strstr(self->_private->cString + _index, subStr->_private->cString);
                if (pointer == self->_private->cString + _index)
                {
                    *index = _index + subStr->_private->length;
                    result = TRUE;
                }
                _status = STRING_OK;
            }
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
    return result;
}

// Clear content
static void clear(const String_T *const self, StringStatus_T *const status)
{
    StringStatus_T _status = STRING_OBJ_NULLPTR;
    if ((self != NULL) && (self->_private != NULL))
    {
        _status = STRING_DATA_NULLPTR;
        if (self->_private->cString != NULL)
        {
            memset(self->_private->cString, 0, self->_private->length * sizeof(Char_T));
            self->_private->length = 0;
            _status = STRING_OK;
        }
    }
    if (status != NULL)
    {
        *status = _status;
    }
}

// PRIVATE FUNCTION DEFINITIONS
// Check if character is in valid range. 
static Boolean_T isValidChar(Char_T character)
{
    return (Boolean_T)((character > '\0') && (character <= '\x7F'));
}