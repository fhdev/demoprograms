#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
// Own header with declarations for Stack_T 
#include "stack.h"

// Definition of private data type.
struct StackPrivate
{
	// Size of the stack measured in object number.
	Uint64_T size;
	// Size of one object in bytes.
	Uint64_T elemSize;
	// Stack pointer.
	Uint64_T pointer;
	// Data.
	void *data;
};

// LOCAL FUNCTION DECLARATIONS (PUBLIC MEMBER FUNCTIONS VIA FUNCTION POINTERS)
static Uint64_T getCount(const Stack_T *const self, StackStatus_T *const status);

static Uint64_T getSize(const Stack_T *const self, StackStatus_T *const status);

static void reset(Stack_T *const self, StackStatus_T *const status);

static void* push(Stack_T *const self, const void *const object, StackStatus_T *const status);

static void* pop(Stack_T *const self,  void *const object, StackStatus_T *const status);

static void* peek(Stack_T *const self, void *const object, StackStatus_T *const status);

// PUBLIC FUNCTION DEFINITIONS
// Constructor.
Stack_T *NewStack(Uint64_T size, Uint64_T elemSize, StackStatus_T *const status)
{
	StackStatus_T _status = STACK_OBJ_NULLPTR;
	Stack_T *stack = (Stack_T*)malloc(sizeof(Stack_T));
	if (stack != NULL)
	{
		stack->_private = (StackPrivate_T*)malloc(sizeof(StackPrivate_T));
		if (stack->_private != NULL)
		{
			// Assume that pointer to data will be NULL. If not, status will be set accordingly.
			_status = STACK_DATA_NULLPTR;
			// Assign sizes.
			stack->_private->size = size;
			stack->_private->elemSize = elemSize;
			// Set pointer to 0.
			stack->_private->pointer = 0;
			// Allocate memory for data.
			stack->_private->data = malloc(size * elemSize);
			// Set status on successful memory allocation.
			if (stack->_private->data != NULL)
			{
				_status = STACK_OK;
			}
		}
        // Assign public member function pointers
		stack->GetCount = getCount;
		stack->GetSize = getSize;
		stack->Reset = reset;
		stack->Push = push;
		stack->Pop = pop;
		stack->Peek = peek;
	}
	// Clear unfinished new stack object and return with null.
	if (_status != STACK_OK)
	{
		DeleteStack(stack, NULL);
		stack = NULL;
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
	return stack;
}

// Destructor.
void DeleteStack(Stack_T *stack, StackStatus_T *const status)
{
	StackStatus_T _status = STACK_OK;
	if (stack != NULL)
	{
		if (stack->_private != NULL)
		{
			if (stack->_private->data != NULL)
			{
				free(stack->_private->data);
			}
			free(stack->_private);
		}
		free(stack);
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
}

// LOCAL FUNCTION DEFINITIONS (PUBLIC MEMBER FUNCTIONS VIA FUNCTION POINTERS)
// Get the number of objects in stack.
static Uint64_T getCount(const Stack_T *const self, StackStatus_T *const status)
{
    StackStatus_T _status = STACK_OBJ_NULLPTR;
    Uint64_T result = 0;
	if ((self != NULL) && (self->_private != NULL))
	{
		// Assume that pointer to data will be NULL. If not, status will be set accordingly.
		_status = STACK_DATA_NULLPTR;
		if (self->_private->data != NULL)
		{
            // The number of elements in stack is the current value of stack pointer
			result = self->_private->pointer;
			_status = STACK_OK;
		}
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
	return result;
}

// Get the size of the stack.
static Uint64_T getSize(const Stack_T *const self, StackStatus_T *const status)
{
    StackStatus_T _status = STACK_OBJ_NULLPTR;
    Uint64_T result = 0;
	if ((self != NULL) && (self->_private != NULL))
	{
		// Assume that pointer to data will be NULL. If not, status will be set accordingly.
		_status = STACK_DATA_NULLPTR;
		if (self->_private->data != NULL)
		{
            // Size of the stack is directly stored.
			result = self->_private->size;
			_status = STACK_OK;
		}
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
	return result;
}

// Reset stack to empty.
static void reset(Stack_T *const self, StackStatus_T *const status)
{
	StackStatus_T _status = STACK_OBJ_NULLPTR;
	if ((self != NULL) && (self->_private != NULL))
	{
		// Assume that pointer to data will be NULL. If not, status will be set accordingly.
		_status = STACK_DATA_NULLPTR;
		if (self->_private->data != NULL)
		{
            // Reset pointer to 0, data will be overwritten by next push.
			self->_private->pointer = 0;
			_status = STACK_OK;
		}
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
}

// Push object to the top of the stack.
static void* push(Stack_T *const self, const void *const object, StackStatus_T *const status)
{
	StackStatus_T _status = STACK_OBJ_NULLPTR;
	void* result = NULL;
	if ((self != NULL) && (self->_private != NULL)  && (object != NULL))
	{
		// Assume that pointer to data will be NULL. If not, status will be set accordingly.
		_status = STACK_DATA_NULLPTR;
		if (self->_private->data != NULL)
		{
            // There is no overflow.
			if (self->_private->pointer < self->_private->size)
			{
                // Evaluate where the data block of current element begins.
				void *curDataP = (void*)((Char_T*)(self->_private->data) +
					self->_private->pointer * self->_private->elemSize);
                // Copy current data block to output buffer
				memcpy(curDataP, object, self->_private->elemSize);
                // Increment stack pointer.
				(self->_private->pointer)++;
                // Set return value to the same pointer as output buffer.
				result = (void*)object;
				_status = STACK_OK;
			}
            // There is overflow.
			else
			{
                // Reset stack.
				reset(self, &_status);
				_status = STACK_OVERFLOW;
			}
		}
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
	return result;
}

// Pop object from the top of the stack.
static void* pop(Stack_T *const self, void *const object, StackStatus_T *const status)
{
	StackStatus_T _status = STACK_OBJ_NULLPTR;
	void *result = NULL;
	if ((self != NULL) && (self->_private != NULL) && (object != NULL))
	{
		// Assume that pointer to data will be NULL. If not, status will be set accordingly.
		_status = STACK_DATA_NULLPTR;
		if (self->_private->data != NULL)
		{
            // There is at least one object in stack.
			if (self->_private->pointer > 0)
			{
                // Decrement stack pointer.
				(self->_private->pointer)--;
                // Evaluate where the data block of current element begins.
				void *curDataP = (void*)((Char_T*)(self->_private->data) +
					self->_private->pointer * self->_private->elemSize);
                // Copy current data block to output buffer
				memcpy(object, curDataP, self->_private->elemSize);
                // Set return value to the same pointer as output buffer.
				result = (void*)object;
				_status = STACK_OK;
			}
            // The stack is empty
			else
			{
                // Reset stack.
				reset(self, &_status);
				_status = STACK_EMPTY;
			}
		}
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
	return result;
}

// Return object from the top of the stack without removing it.
static void* peek(Stack_T *const self, void *const object, StackStatus_T *const status)
{
	StackStatus_T _status = STACK_OBJ_NULLPTR;
	void *result = NULL;
	if ((self != NULL) && (self->_private != NULL) && (object != NULL))
	{
		// Assume that pointer to data will be NULL. If not, status will be set accordingly.
		_status = STACK_DATA_NULLPTR;
		if (self->_private->data != NULL)
		{
            // There is at least one object in stack.
			if (self->_private->pointer > 0)
			{
                // Evaluate where the data block of current element begins.
				void *curDataP = (void*)((Char_T*)(self->_private->data) +
					(self->_private->pointer - 1) * self->_private->elemSize);
                // Copy current data block to output buffer
				memcpy(object, curDataP, self->_private->elemSize);
                // Set return value to the same pointer as output buffer.
				result = (void*)object;
				_status = STACK_OK;
			}
            // The stack is empty.
			else
			{
                // Reset stack.
				reset(self, &_status);
				_status = STACK_EMPTY;
			}
		}
	}
    // Set status if required.
	if (status != NULL)
	{
		*status = _status;
	}
	return result;
}

// Test stack functionality.
StackStatus_T StackTest(Uint64_T size,  Boolean_T overwrite, Boolean_T overread)
{
	Uint64_T i;
	StackStatus_T status;
	Float64_T ms;
	Stack_T *s;
	clock_t start, stop;
	start = clock();
	printf("Allocating stack for %llu doubles...\n", size);
	s = NewStack(size, sizeof(Float64_T), &status);
	if (status == STACK_OK)
	{
		printf("Stack allocation successful...\nFill stack with %llu doubles...\n", size);
		for (i = 0; i < size; i++)
		{
			Float64_T value = (Float64_T)(i + 1);
			s->Push(s, &value, &status);
			if (status == STACK_OK)
			{
				printf("Push(%llu): successful... %lf\n", i, value);
			}
			else
			{
				printf("Push(%llu): unsuccessful...\n", i);
				break;
			}
		}
		if (overwrite)
		{
			Float64_T value = (Float64_T)(i + 1);
			s->Push(s, &value, &status);
			printf("Status after overwrite: %d\n", status);
		}
		for (i = 0; i < size; i++)
		{
			Float64_T value;
			s->Pop(s, &value, &status);
			if (status == STACK_OK)
			{
				printf("Pop(%llu): successful... %lf\n", i, value);
			}
			else
			{
				printf("Pop(%llu): unsuccessful...\n", i);
				break;
			}
		}
		if (overread)
		{
			Float64_T value;
			s->Pop(s, &value, &status);
			printf("Status after overread: %d\n", status);
		}
	}
	else
	{
		printf("Stack allocation unsuccessful...\n");
	}
	DeleteStack(s, &status);
	if (status == STACK_OK)
	{
		printf("TEST SUCCEED!\n");
	}
	else
	{
		printf("TEST FAILED!\n");
	}
	stop = clock();
	ms = (Float64_T)(stop - start) / CLOCKS_PER_SEC * 1000.0;
	printf("Test time: %lf ms...\n", ms);
	return status;
}