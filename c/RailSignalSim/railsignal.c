/********************************************************************************************************************/
/*																													*/
/* Description:	Simulation of railway signalling of Hungarian railways.												*/
/*																													*/
/* Date: 2015.04.25.																								*/
/*																													*/
/* Author: Ferenc Heged�s																							*/
/*																													*/
/********************************************************************************************************************/


/********************************************************************************************************************/
/* Include Files																									*/
/********************************************************************************************************************/

#include "railsignal_defs.h"

/********************************************************************************************************************/
/* Defines																											*/
/********************************************************************************************************************/


/********************************************************************************************************************/
/* Constants																										*/
/********************************************************************************************************************/


/********************************************************************************************************************/
/* External Variables																								*/
/********************************************************************************************************************/


/********************************************************************************************************************/
/* Global Variables																									*/
/********************************************************************************************************************/

/* Timer0 tick counter */
volatile tUInt8 timer0_tick = 0;
/********************************************************************/
/* Lamp state									 					*/
/*																	*/
/* GREEN YELLOWN RED YELLOWA CALL STR1 STR2							*/
/*   0      0     0     0      0    0    0        					*/
/*																	*/
/* Cristal oscillator mode, 22.11 MHz frequency.					*/
/********************************************************************/
volatile tUInt8 lamp_state = 0;
/* UART1 received and transmitted temp */
volatile tUInt8 uart1_rx_temp;
volatile tUInt8 uart1_tx_temp;
/* UART sending busy flags */
volatile tBool uart0_busy = FALSE;
volatile tBool uart1_busy = FALSE;
/* UART1 buffers */
volatile tBufferUInt8 uart1_rx_buffer;
volatile tBufferUInt8 uart1_tx_buffer;
/* Flash timing */
volatile tUInt8 green_flash = FLASH_NOT_ENABLED;
volatile tUInt8 yellow_flash = FLASH_NOT_ENABLED;
volatile tUInt8 white_flash = FLASH_NOT_ENABLED;
/* Actuation arrays for act and next signals */
volatile todo set_next[5]; 
volatile todo set_act[6];

/********************************************************************************************************************/
/* Local Function Declarations																						*/
/********************************************************************************************************************/

/********************************************************************************************************************/
/* MAIN																												*/
/********************************************************************************************************************/

void main()
{
	/* Initialization */
	interrupt_disable_all();					/* Disabling all interrupts */
	clock_init();								/* Clock source selection */		
	watchdog_disable();							/* Disabling watchdog timer */
	timer_init();								/* Timer initialization */
	crossbar_init();							/* Port assignment initialization */
	port_init();								/* GPIO initialization */
	uart1_init();								/* UART1 initialization */
	buffer_init(&uart1_rx_buffer,BUFFER_SIZE);	/* RX buffer initialization */
	buffer_init(&uart1_tx_buffer,BUFFER_SIZE);	/* TX buffer initialization */
	signal_array_init();						/* Signal setting functuin pointer array initialization */
	interrupt_enable_all();						/* Enabling all interrupts */
	/* Infinite loop */
	while(TRUE)
	{
		/* Processig receive buffer */
		if(buffer_get(&uart1_rx_buffer, &uart1_rx_temp))
		{
			tUInt8 act_req, next_req;
			next_req = GET_HIGH_4(uart1_rx_temp);
			act_req = GET_LOW_4(uart1_rx_temp);
			if((next_req < 5) && (act_req < 6))
			{
				/* Setting modes. Order of function calls is important. */
				set_next[next_req]();
				set_act[act_req]();
			}
		}
		/* Processing transmit buffer */
		if(buffer_get(&uart1_tx_buffer, &uart1_tx_temp))
		{
			uart1_send_byte(uart1_tx_temp);
		}
		
	}	
}

/********************************************************************************************************************/
/* Local Function Definitions																						*/
/********************************************************************************************************************/

/********************************************************************************************************************/
/* Global Function Definitions																						*/
/********************************************************************************************************************/

/* Clock source initialization. External crystal oscillator selection. */
void clock_init()
{
	/********************************************************************/
	/* OSXCN - External Oscillator Control Register 					*/
	/*																	*/
	/* XTLVLD - Crystal Oscillator Valid Flag							*/
	/* XOSCMD - External Oscillator Modes								*/
	/*																	*/
	/* XTLVLD XOSCMD2 XOSCMD1 XOSCMD0 - XFCN2 XFCN1 XFCN0				*/
	/*   0      1        1       0    0   1     1     1					*/
	/*																	*/
	/* Cristal oscillator mode, 22.11 MHz frequency.					*/
	/********************************************************************/
	OSCXCN = BS(_XOSCMD2) | BS(_XOSCMD1) | BS(_XFCN2) | BS(_XFCN1) | BS(_XFCN0);
	/* Wait for crystal oscillator valid flag */
	while(0 == (OSCXCN & BS(_XTLVLD)))	{}
	/********************************************************************/
	/* OSCICN - Internal Oscillator Control Register					*/
	/*																	*/
	/* MSCLKE - - IFRDY CLKSL IOSCEN IFCN1 IFCN0						*/
	/*	 0    0 0   0     1      0     0     0							*/
	/*																	*/
	/* Internal oscillator disabled, external oscillator enabled.		*/
	/********************************************************************/
	OSCICN = BS(_CLKSL);
}
/* Disabling watchdog timer according to controller's datasheet. */
void watchdog_disable()
{
	WDTCN = 0xDE;
	WDTCN = 0xAD;
}
/* Disabling all interrupts. */
void interrupt_disable_all()
{
	CLRBIT(EA);
}
/* Enabling all interrupts. */
void interrupt_enable_all()
{
	SETBIT(EA);
}
/* Timer initialization */
void timer_init()
{
	/********************************************************************/
	/* CKCON - Timer Clock Source Control Register						*/
	/*																	*/
	/* 0 - System Clock / 12											*/
	/* 1 - System Clock													*/
	/*																	*/
	/* - T4M T2M T1M T0M - - -											*/
	/* 0  0   0   1   0  0 0 0 											*/
	/*																	*/
	/* Timer 1 uses system clock										*/
	/* All other timer use system clock divided by 12					*/
	/********************************************************************/
	CKCON = BS(_T1M);
	/********************************************************************/
	/* TMOD - Timer 0&1 Mode Control Register							*/
	/*																	*/
	/* GATE1 C/T1 T1M1 T1M0 GATE0 C/T0 T0M1 T0M0						*/
	/*  0     0    1    0    0     0    0    1							*/
	/*																	*/
	/* Timer 0 on 16 bit mode.											*/
	/* Timer 1 on 8 bit auto-reload mode (baud rate generator for UART)	*/
	/********************************************************************/
	TMOD = BS(_T0M0) | BS(_T1M1);
	/********************************************************************/
	/* Description: Timer 0 Interrupt frequency is to be set at			*/ 
	/* 22118400 / 12 / 18425 = 100 Hz									*/
	/* 2^16 = 65536 65536 - 18425 = 28686 = 0xB807 : TH0=0xB8, TL0=0x07	*/
	/********************************************************************/
	TH0 = 0xB8;
	TL0 = 0x07;
	/********************************************************************/
	/* Description: UART 0 & 1 baud rate is to be set at 19200 Hz		*/ 
	/* 22118400 / 32  / 19200 = 36										*/
	/* 256 - 220 = 36 : TH1 = 220 = 0xDC								*/
	/********************************************************************/
	TH1 = 0xDC;
	/********************************************************************/
	/* TCON - Timer 0&1 Control Register								*/
	/*																	*/
	/* TF - Overflow flag												*/
	/* TR - Enable bit													*/
	/*																	*/
	/* TF1 TR1 TF0 TR0 IE1 IT1 IE0 IT0									*/
	/*  0   1   0   1   0   0   0   0									*/
	/*																	*/
	/* Timer 0 & 1 enabled												*/
	/********************************************************************/
	TCON = BS(_TR0) | BS(_TR1);
	/********************************************************************/
	/* IE - Interrupt Enable Register 									*/
	/*																	*/
	/* EA IEGF0 ET2 ES0 ET1 EX1 ET0 EX0									*/
	/*  x   x    x   x   x   x   1   x									*/
	/*																	*/
	/* Timer 0 interrupt enable.										*/
	/********************************************************************/	
	IE |= BS(_ET0);	
}
/* Port Initialization */
void port_init()
{
	/********************************************************************/
	/* P74OUT - Ports 7-4 Output Mode Register							*/
	/*																	*/
	/* 0 - Open-Drain	(in)											*/
	/* 1 - Push-Pull	(out)											*/
	/*																	*/
	/* P7H P7L P6H P6L P5H P5L P4H P4L									*/
	/*  0   0   0   0   1   0   0   0									*/
	/*																	*/
	/* Port 5 High (LEDs) push-pull selected.							*/
	/********************************************************************/
	P74OUT = BS(_P5H);
	/* Upper 4 bits: LEDs | Lower 4 bits: Buttons (need to be pulled up) */
	P5 = ~(LED_1_MASK | LED_2_MASK | LED_3_MASK | LED_4_MASK);
	/* P0.4 is TX for UART1 */
	P0MDOUT = BS(4);
}
/* UART 0 initialization */
void uart0_init()
{
	/********************************************************************/
	/* PCON - Power Control Register									*/
	/*																	*/
	/* SMOD0 SSTAT0 - SMOD1 SSTAT1 - STOP IDLE							*/
	/*   0     0    x   x     x    x   x    x							*/
	/*																	*/
	/* UART0 baud rate divided by 2 Enabled								*/
	/* SM20-SM00 access UART0  mode setting								*/
	/********************************************************************/	
	PCON &= ~(BS(_SMOD0) | BS(_SSTAT0));
	/********************************************************************/
	/* SCON0 - UART0 Control Register									*/
	/*																	*/
	/* SM00 SM10 SM20 REN0 TB80 RB80 TI0 RI0							*/
	/*   1     1   0    1    0    0   0   0								*/
	/*																	*/
	/* UART0 Mode 3 selected (9 bit, variable baud rate)				*/
	/* Logic value of 9th bit is ignored								*/
	/* UART0 reception enabled											*/
	/********************************************************************/
	SCON0 = BS(_SM00) | BS(_SM10) | BS(_REN0);
	/* Re-enable SCON0 status bits. */
	PCON |= BS(_SSTAT0);
	/********************************************************************/
	/* IE - Interrupt Enable Register 									*/
	/*																	*/
	/* EA IEGF0 ET2 ES0 ET1 EX1 ET0 EX0									*/
	/*  x   x    x   1   x   x   x   x									*/
	/*																	*/
	/* UART0 interrupt enable.											*/
	/********************************************************************/
	IE |= BS(_ES0);
}
/* UART 1 initialization */
void uart1_init()
{
	/********************************************************************/
	/* PCON - Power Control Register									*/
	/*																	*/
	/* SMOD0 SSTAT0 - SMOD1 SSTAT1 - STOP IDLE							*/
	/*   x     x    x   0     0    x   x    x							*/
	/*																	*/
	/* UART1 baud rate divided by 2 Enabled								*/
	/* SM21-SM01 access UART1 mode setting								*/
	/********************************************************************/	
	PCON &= ~(BS(_SMOD1) | BS(_SSTAT1));
	/********************************************************************/
	/* SCON1 - UART1 Control Register									*/
	/*																	*/
	/* SM01 SM11 SM21 REN1 TB81 RB81 TI1 RI1							*/
	/*   1     1    0   1     0    0   0    0							*/
	/*																	*/
	/* UART1 Mode 3 selected (9 bit, variable baud rate)				*/
	/* Logic value of 9th bit is ignored								*/
	/* UART1 reception enabled											*/
	/********************************************************************/
	SCON1 = BS(_SM01) | BS(_SM11) | BS(_REN1);
	/* Re-enable SCON1 status bits. */
	PCON |= BS(_SSTAT1);
	/********************************************************************/
	/* EIE2 - Extended Interrupt Enable 2 Register 						*/
	/*																	*/
	/* EXVLD ES1 EX7 EX6 EADC1 ET4 EADC0 ET3							*/
	/*   x    1   x   x    x    x    x    x								*/
	/*																	*/
	/* UART1 interrupt enable.											*/
	/********************************************************************/
	EIE2 = BS(_ES1);
}
/* Initialization of port assignment for P0-P3 */
void crossbar_init()
{
	/********************************************************************/
	/* XBR0 - Port I/O Crossbar Register 0		 						*/
	/*																	*/
	/* CP0E ECI0E PCA0ME UART0EN SPI0EN SMB0EN							*/
	/*   0    0   0 0 0     1       0     1								*/
	/*																	*/
	/* UART0 & SMB0 enable.												*/
	/********************************************************************/
	XBR0 = BS(_UART0EN) | BS(_SMB0EN);
	/********************************************************************/
	/* XBR2 - Port I/O Crossbar Register 2		 						*/
	/*																	*/
	/* WEAKPUD XBARE - T4EXE - UART1E EMIFLE CNVSTE						*/
	/*    0      1   0   0        1      0     0						*/
	/*																	*/
	/* UART1 & Crossbar enable.											*/
	/********************************************************************/
	XBR2 = BS(_UART1EN) | BS(_XBARE);
}
/* Initialize FIFO buffer */
void buffer_init(tBufferUInt8 *buffer, tUInt8 buffer_size)
{
	buffer->size = buffer_size;
	buffer->dat = (tUInt8*)malloc((buffer->size)*sizeof(tUInt8));
	memset(buffer->dat, 0, (buffer->size)*sizeof(tUInt8));
	buffer->stored = 0;
	buffer->index_last = 0;
	buffer->index_first = 0;
	buffer->overflow = FALSE;
}
/* Put value to FIFO buffer */
void buffer_put(tBufferUInt8 *buffer, tUInt8 value)
{
	interrupt_disable_all();
	/* If there is available space in buffer */
	if(buffer->stored < buffer->size)
	{
		buffer->overflow = FALSE;
		buffer->dat[buffer->index_last] = value;
		buffer->index_last++;
		if(buffer->index_last >= buffer->size)
		{
			buffer->index_last = 0;
		}
		buffer->stored++;
	}
	else
	{
		buffer->overflow = TRUE;
	}
	interrupt_enable_all();
}
/* Get value from FIFO buffer */
tBool buffer_get(tBufferUInt8 *buffer, tUInt8 *value)
{
	interrupt_disable_all();
	/* If there are available data */
	if(buffer->stored > 0)
	{
		*value = buffer->dat[buffer->index_first];
		buffer->index_first++;
		if(buffer->index_first >= buffer->size)
		{
			buffer->index_first = 0;
		}
		buffer->stored--;
		interrupt_enable_all();
		return TRUE;
	}
	else
	{
		interrupt_enable_all();
		return FALSE;
	}	
}
/* Set Vmax for next signal */
void set_next_vmax()
{
	P5 |= LAMP_GREEN_MASK;
	P5 &= ~LAMP_YELLOWN_MASK;
	green_flash = FLASH_NOT_ENABLED;
	yellow_flash = FLASH_NOT_ENABLED;
	lamp_state |= BS(LAMP_GREEN);
	lamp_state &= ~BS(LAMP_YELLOWN);
}
/* Set 120 for next signal */
void set_next_120()
{
	P5 |= LAMP_GREEN_MASK;
	P5 &= ~LAMP_YELLOWN_MASK;
	green_flash = FLASH_FAST_ENABLED;
	yellow_flash = FLASH_NOT_ENABLED;
	lamp_state |= LAMP_GREEN_MASK;
	lamp_state &= ~LAMP_YELLOWN_MASK;
}
/* Set 80 for next signal */
void set_next_80()
{
	P5 |= LAMP_GREEN_MASK;
	P5 &= ~LAMP_YELLOWN_MASK;
	green_flash = FLASH_SLOW_ENABLED;
	yellow_flash = FLASH_NOT_ENABLED;
	lamp_state |= LAMP_GREEN_MASK;
	lamp_state &= ~LAMP_YELLOWN_MASK;
}
/* Set 40 for next signal */
void set_next_40()
{
	P5 &= ~LAMP_GREEN_MASK;
	P5 |= LAMP_YELLOWN_MASK;
	green_flash = FLASH_NOT_ENABLED;
	yellow_flash = FLASH_SLOW_ENABLED;
	lamp_state &= ~LAMP_GREEN_MASK;
	lamp_state |= LAMP_YELLOWN_MASK;
}
/* Set STOP for next signal */
void set_next_0()
{
	P5 &= ~LAMP_GREEN_MASK;
	P5 |= LAMP_YELLOWN_MASK;
	green_flash = FLASH_NOT_ENABLED;
	yellow_flash = FLASH_NOT_ENABLED;
	lamp_state &= ~LAMP_GREEN_MASK;
	lamp_state |= LAMP_YELLOWN_MASK;
}
/* Set Vmax for actual signal */
void set_act_vmax()
{
	P5 &= ~(LAMP_YELLOWA_MASK | LAMP_RED_MASK);
	white_flash = FLASH_NOT_ENABLED;
	lamp_state &= ~(LAMP_YELLOWA_MASK | LAMP_RED_MASK | LAMP_CALL_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
}
/* Set 120 for actual signal */
void set_act_120()
{
	P5 |= LAMP_YELLOWA_MASK;
	P5 &= ~LAMP_RED_MASK;
	white_flash = FLASH_NOT_ENABLED;
	lamp_state |= (LAMP_YELLOWA_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
	lamp_state &= ~(LAMP_RED_MASK | LAMP_CALL_MASK);
}
/* Set 80 for actual signal */
void set_act_80()
{
	P5 |= LAMP_YELLOWA_MASK;
	P5 &= ~LAMP_RED_MASK;
	white_flash = FLASH_NOT_ENABLED;
	lamp_state |= (LAMP_YELLOWA_MASK | LAMP_STR1_MASK);
	lamp_state &= ~(LAMP_RED_MASK | LAMP_CALL_MASK | LAMP_STR2_MASK);
}
/* Set 40 for actual signal */
void set_act_40()
{
	P5 |= LAMP_YELLOWA_MASK;
	P5 &= ~LAMP_RED_MASK;
	white_flash = FLASH_NOT_ENABLED;
	lamp_state |= LAMP_YELLOWA_MASK;
	lamp_state &= ~(LAMP_RED_MASK | LAMP_CALL_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
}
/* Set STOP for actual signal */
void set_act_0()
{
	P5 &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK);
	P5 |= LAMP_RED_MASK;
	green_flash = FLASH_NOT_ENABLED;
	yellow_flash = FLASH_NOT_ENABLED;
	white_flash = FLASH_NOT_ENABLED;
	lamp_state |= LAMP_RED_MASK;
	lamp_state &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK | LAMP_CALL_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
}
/* Set Call signal for actual signal */
void set_act_call()
{
	P5 &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK);
	P5 |= LAMP_RED_MASK;
	green_flash = FLASH_NOT_ENABLED;
	yellow_flash = FLASH_NOT_ENABLED;
	white_flash = FLASH_SLOW_ENABLED;
	lamp_state |= (LAMP_RED_MASK | LAMP_CALL_MASK);
	lamp_state &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
}
/* Initialize signal set array */
void signal_array_init()
{
	set_next[STOP] = set_next_0;
	set_next[V40] = set_next_40;
	set_next[V80] = set_next_80;
	set_next[V120] = set_next_120;
	set_next[VMAX] = set_next_vmax;
	set_act[STOP] = set_act_0;
	set_act[V40] = set_act_40;
	set_act[V80] = set_act_80;
	set_act[V120] = set_act_120;
	set_act[VMAX] = set_act_vmax;
	set_act[CALL] = set_act_call;
}
/* Send a byte via UART0 */
void uart0_send_byte(tUInt8 byte)
{
	/* Transmission */
	if(!uart0_busy)
	{
		SBUF0 = byte;
		uart0_busy = TRUE;
	}
}
/* Send a byte via UART1 */
void uart1_send_byte(tUInt8 byte)
{
	/* Transmission */
	if(!uart1_busy)
	{
		SBUF1 = byte;
		SCON1 &= ~BS(_RB81);
		uart1_busy = TRUE;
	}
}

/********************************************************************************************************************/
/* Interrupt Service Routines																						*/
/********************************************************************************************************************/

/* Timer0 Interrupt Routine	*/
void timer0_it() interrupt INTERRUPT_TIMER0
{
	/* Reset counter accumulator init values */
	TH0 = 0xB8;
	TL0 = 0x07;
	/* Increasing the number of ticks */
	timer0_tick++;
	/* Green blinking task */
	if(green_flash != FLASH_NOT_ENABLED)
	{
		if(0 == (timer0_tick % green_flash))
		{
			P5 ^= LAMP_GREEN_MASK;
			lamp_state ^= LAMP_GREEN_MASK;
		}
	}
	/* Yellow blinking task */
	if(yellow_flash != FLASH_NOT_ENABLED)
	{
		if(0 == (timer0_tick % yellow_flash))
		{
			P5 ^= LAMP_YELLOWN_MASK;
			lamp_state ^= LAMP_YELLOWN_MASK;
		}
	}
	if(white_flash != FLASH_NOT_ENABLED)
	{
		if(0 == (timer0_tick % white_flash))
		{
			lamp_state ^= LAMP_CALL_MASK;
		}
	}
	/* 250 ms task */
	if(timer0_tick % 10 == 0)
	{
		uart1_send_byte(lamp_state);
	}
	/* Reset the number of ticks to avoid overflow */
	if(timer0_tick > 200)
	{
		timer0_tick = 1;
	}
}

/* UART0 Interrupt Routine */
void uart0_it() interrupt INTERRUPT_UART0 
{
	/* Reception */
	if(BS(_RI0) == (SCON0 & (BS(_RI0))))
	{
		SCON0 &= ~BS(_RI0);
	}
	/* Transmission */
	else if(BS(_TI0) == (SCON0 & (BS(_TI0))))
	{
		uart0_busy = FALSE;
		SCON0 &= ~BS(_TI0);
	}	
}

/* UART1 Interrupt Routine */
void uart1_it() interrupt INTERRUPT_UART1
{
	/* Reception */
	if(BS(_RI1) == (SCON1 & (BS(_RI1))))
	{
		buffer_put(&uart1_rx_buffer,SBUF1);	
		SCON1 &= ~BS(_RI1);
	}
	/* Transmission */
	else if(BS(_TI1) == (SCON1 & (BS(_TI1))))
	{
		uart1_busy = FALSE;
		SCON1 &= ~BS(_TI1);
	}
}

