;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 2.9.0 #5416 (Mar 22 2009) (MINGW32)
; This file was generated Mon May 18 13:35:19 2015
;--------------------------------------------------------
	.module railsignal
	.optsdcc -mmcs51 --model-small
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _uart1_it
	.globl _uart0_it
	.globl _timer0_it
	.globl _main
	.globl _SPIEN
	.globl _MSTEN
	.globl _SLVSEL
	.globl _TXBSY
	.globl _RXOVRN
	.globl _MODF
	.globl _WCOL
	.globl _SPIF
	.globl _AD0LJST
	.globl _AD0WINT
	.globl _AD0CM0
	.globl _AD0CM1
	.globl _AD0BUSY
	.globl _AD0INT
	.globl _AD0TM
	.globl _AD0EN
	.globl _CCF0
	.globl _CCF1
	.globl _CCF2
	.globl _CCF3
	.globl _CCF4
	.globl _CR
	.globl _CF
	.globl _P
	.globl _F1
	.globl _OV
	.globl _RS0
	.globl _RS1
	.globl _F0
	.globl _AC
	.globl _CY
	.globl _CPRL2
	.globl _CT2
	.globl _TR2
	.globl _EXEN2
	.globl _TCLK0
	.globl _RCLK0
	.globl _EXF2
	.globl _TF2
	.globl _SMBTOE
	.globl _SMBFTE
	.globl _AA
	.globl _SI
	.globl _STO
	.globl _STA
	.globl _ENSMB
	.globl _BUSY
	.globl _PX0
	.globl _PT0
	.globl _PX1
	.globl _PT1
	.globl _PS
	.globl _PT2
	.globl _EX0
	.globl _ET0
	.globl _EX1
	.globl _ET1
	.globl _ES0
	.globl _ET2
	.globl _IEGF0
	.globl _EA
	.globl _RI0
	.globl _TI0
	.globl _RB80
	.globl _TB80
	.globl _REN0
	.globl _SM20
	.globl _SM10
	.globl _SM00
	.globl _IT0
	.globl _IE0
	.globl _IT1
	.globl _IE1
	.globl _TR0
	.globl _TF0
	.globl _TR1
	.globl _TF1
	.globl __XPAGE
	.globl _DAC1
	.globl _DAC0
	.globl _TMR4
	.globl _TMR4RL
	.globl _T4
	.globl _RCAP4
	.globl _TMR2
	.globl _TMR2RL
	.globl _T2
	.globl _RCAP2
	.globl _ADC0LT
	.globl _ADC0GT
	.globl _ADC0
	.globl _TMR3
	.globl _TMR3RL
	.globl _DP
	.globl _WDTCN
	.globl _PCA0CPH4
	.globl _PCA0CPH3
	.globl _PCA0CPH2
	.globl _PCA0CPH1
	.globl _PCA0CPH0
	.globl _PCA0H
	.globl _SPI0CN
	.globl _EIP2
	.globl _EIP1
	.globl _TH4
	.globl _TL4
	.globl _SADDR1
	.globl _SBUF1
	.globl _SCON1
	.globl _B
	.globl _RSTSRC
	.globl _PCA0CPL4
	.globl _PCA0CPL3
	.globl _PCA0CPL2
	.globl _PCA0CPL1
	.globl _PCA0CPL0
	.globl _PCA0L
	.globl _ADC0CN
	.globl _EIE2
	.globl _EIE1
	.globl _RCAP4H
	.globl _RCAP4L
	.globl _XBR2
	.globl _XBR1
	.globl _XBR0
	.globl _ACC
	.globl _PCA0CPM4
	.globl _PCA0CPM3
	.globl _PCA0CPM2
	.globl _PCA0CPM1
	.globl _PCA0CPM0
	.globl _PCA0MD
	.globl _PCA0CN
	.globl _DAC1CN
	.globl _DAC1H
	.globl _DAC1L
	.globl _DAC0CN
	.globl _DAC0H
	.globl _DAC0L
	.globl _REF0CN
	.globl _PSW
	.globl _SMB0CR
	.globl _TH2
	.globl _TL2
	.globl _RCAP2H
	.globl _RCAP2L
	.globl _T4CON
	.globl _T2CON
	.globl _ADC0LTH
	.globl _ADC0LTL
	.globl _ADC0GTH
	.globl _ADC0GTL
	.globl _SMB0ADR
	.globl _SMB0DAT
	.globl _SMB0STA
	.globl _SMB0CN
	.globl _ADC0H
	.globl _ADC0L
	.globl _P1MDIN
	.globl _ADC0CF
	.globl _AMX0SL
	.globl _AMX0CF
	.globl _SADEN0
	.globl _IP
	.globl _FLACL
	.globl _FLSCL
	.globl _P74OUT
	.globl _OSCICN
	.globl _OSCXCN
	.globl _P3
	.globl _EMI0CN
	.globl _SADEN1
	.globl _P3IF
	.globl _AMX1SL
	.globl _ADC1CF
	.globl _ADC1CN
	.globl _SADDR0
	.globl _IE
	.globl _P3MDOUT
	.globl _P2MDOUT
	.globl _P1MDOUT
	.globl _P0MDOUT
	.globl _EMI0CF
	.globl _EMI0TC
	.globl _P2
	.globl _CPT1CN
	.globl _CPT0CN
	.globl _SPI0CKR
	.globl _ADC1
	.globl _SPI0DAT
	.globl _SPI0CFG
	.globl _SBUF0
	.globl _SCON0
	.globl _P7
	.globl _TMR3H
	.globl _TMR3L
	.globl _TMR3RLH
	.globl _TMR3RLL
	.globl _TMR3CN
	.globl _P1
	.globl _PSCTL
	.globl _CKCON
	.globl _TH1
	.globl _TH0
	.globl _TL1
	.globl _TL0
	.globl _TMOD
	.globl _TCON
	.globl _PCON
	.globl _P6
	.globl _P5
	.globl _P4
	.globl _DPH
	.globl _DPL
	.globl _SP
	.globl _P0
	.globl _buffer_get_PARM_2
	.globl _buffer_put_PARM_2
	.globl _buffer_init_PARM_2
	.globl _set_act
	.globl _set_next
	.globl _white_flash
	.globl _yellow_flash
	.globl _green_flash
	.globl _uart1_tx_buffer
	.globl _uart1_rx_buffer
	.globl _uart1_busy
	.globl _uart0_busy
	.globl _uart1_tx_temp
	.globl _uart1_rx_temp
	.globl _lamp_state
	.globl _timer0_tick
	.globl _clock_init
	.globl _watchdog_disable
	.globl _interrupt_disable_all
	.globl _interrupt_enable_all
	.globl _timer_init
	.globl _port_init
	.globl _uart0_init
	.globl _uart1_init
	.globl _crossbar_init
	.globl _buffer_init
	.globl _buffer_put
	.globl _buffer_get
	.globl _set_next_vmax
	.globl _set_next_120
	.globl _set_next_80
	.globl _set_next_40
	.globl _set_next_0
	.globl _set_act_vmax
	.globl _set_act_120
	.globl _set_act_80
	.globl _set_act_40
	.globl _set_act_0
	.globl _set_act_call
	.globl _signal_array_init
	.globl _uart0_send_byte
	.globl _uart1_send_byte
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
	.area RSEG    (DATA)
G$P0$0$0 == 0x0080
_P0	=	0x0080
G$SP$0$0 == 0x0081
_SP	=	0x0081
G$DPL$0$0 == 0x0082
_DPL	=	0x0082
G$DPH$0$0 == 0x0083
_DPH	=	0x0083
G$P4$0$0 == 0x0084
_P4	=	0x0084
G$P5$0$0 == 0x0085
_P5	=	0x0085
G$P6$0$0 == 0x0086
_P6	=	0x0086
G$PCON$0$0 == 0x0087
_PCON	=	0x0087
G$TCON$0$0 == 0x0088
_TCON	=	0x0088
G$TMOD$0$0 == 0x0089
_TMOD	=	0x0089
G$TL0$0$0 == 0x008a
_TL0	=	0x008a
G$TL1$0$0 == 0x008b
_TL1	=	0x008b
G$TH0$0$0 == 0x008c
_TH0	=	0x008c
G$TH1$0$0 == 0x008d
_TH1	=	0x008d
G$CKCON$0$0 == 0x008e
_CKCON	=	0x008e
G$PSCTL$0$0 == 0x008f
_PSCTL	=	0x008f
G$P1$0$0 == 0x0090
_P1	=	0x0090
G$TMR3CN$0$0 == 0x0091
_TMR3CN	=	0x0091
G$TMR3RLL$0$0 == 0x0092
_TMR3RLL	=	0x0092
G$TMR3RLH$0$0 == 0x0093
_TMR3RLH	=	0x0093
G$TMR3L$0$0 == 0x0094
_TMR3L	=	0x0094
G$TMR3H$0$0 == 0x0095
_TMR3H	=	0x0095
G$P7$0$0 == 0x0096
_P7	=	0x0096
G$SCON0$0$0 == 0x0098
_SCON0	=	0x0098
G$SBUF0$0$0 == 0x0099
_SBUF0	=	0x0099
G$SPI0CFG$0$0 == 0x009a
_SPI0CFG	=	0x009a
G$SPI0DAT$0$0 == 0x009b
_SPI0DAT	=	0x009b
G$ADC1$0$0 == 0x009c
_ADC1	=	0x009c
G$SPI0CKR$0$0 == 0x009d
_SPI0CKR	=	0x009d
G$CPT0CN$0$0 == 0x009e
_CPT0CN	=	0x009e
G$CPT1CN$0$0 == 0x009f
_CPT1CN	=	0x009f
G$P2$0$0 == 0x00a0
_P2	=	0x00a0
G$EMI0TC$0$0 == 0x00a1
_EMI0TC	=	0x00a1
G$EMI0CF$0$0 == 0x00a3
_EMI0CF	=	0x00a3
G$P0MDOUT$0$0 == 0x00a4
_P0MDOUT	=	0x00a4
G$P1MDOUT$0$0 == 0x00a5
_P1MDOUT	=	0x00a5
G$P2MDOUT$0$0 == 0x00a6
_P2MDOUT	=	0x00a6
G$P3MDOUT$0$0 == 0x00a7
_P3MDOUT	=	0x00a7
G$IE$0$0 == 0x00a8
_IE	=	0x00a8
G$SADDR0$0$0 == 0x00a9
_SADDR0	=	0x00a9
G$ADC1CN$0$0 == 0x00aa
_ADC1CN	=	0x00aa
G$ADC1CF$0$0 == 0x00ab
_ADC1CF	=	0x00ab
G$AMX1SL$0$0 == 0x00ac
_AMX1SL	=	0x00ac
G$P3IF$0$0 == 0x00ad
_P3IF	=	0x00ad
G$SADEN1$0$0 == 0x00ae
_SADEN1	=	0x00ae
G$EMI0CN$0$0 == 0x00af
_EMI0CN	=	0x00af
G$P3$0$0 == 0x00b0
_P3	=	0x00b0
G$OSCXCN$0$0 == 0x00b1
_OSCXCN	=	0x00b1
G$OSCICN$0$0 == 0x00b2
_OSCICN	=	0x00b2
G$P74OUT$0$0 == 0x00b5
_P74OUT	=	0x00b5
G$FLSCL$0$0 == 0x00b6
_FLSCL	=	0x00b6
G$FLACL$0$0 == 0x00b7
_FLACL	=	0x00b7
G$IP$0$0 == 0x00b8
_IP	=	0x00b8
G$SADEN0$0$0 == 0x00b9
_SADEN0	=	0x00b9
G$AMX0CF$0$0 == 0x00ba
_AMX0CF	=	0x00ba
G$AMX0SL$0$0 == 0x00bb
_AMX0SL	=	0x00bb
G$ADC0CF$0$0 == 0x00bc
_ADC0CF	=	0x00bc
G$P1MDIN$0$0 == 0x00bd
_P1MDIN	=	0x00bd
G$ADC0L$0$0 == 0x00be
_ADC0L	=	0x00be
G$ADC0H$0$0 == 0x00bf
_ADC0H	=	0x00bf
G$SMB0CN$0$0 == 0x00c0
_SMB0CN	=	0x00c0
G$SMB0STA$0$0 == 0x00c1
_SMB0STA	=	0x00c1
G$SMB0DAT$0$0 == 0x00c2
_SMB0DAT	=	0x00c2
G$SMB0ADR$0$0 == 0x00c3
_SMB0ADR	=	0x00c3
G$ADC0GTL$0$0 == 0x00c4
_ADC0GTL	=	0x00c4
G$ADC0GTH$0$0 == 0x00c5
_ADC0GTH	=	0x00c5
G$ADC0LTL$0$0 == 0x00c6
_ADC0LTL	=	0x00c6
G$ADC0LTH$0$0 == 0x00c7
_ADC0LTH	=	0x00c7
G$T2CON$0$0 == 0x00c8
_T2CON	=	0x00c8
G$T4CON$0$0 == 0x00c9
_T4CON	=	0x00c9
G$RCAP2L$0$0 == 0x00ca
_RCAP2L	=	0x00ca
G$RCAP2H$0$0 == 0x00cb
_RCAP2H	=	0x00cb
G$TL2$0$0 == 0x00cc
_TL2	=	0x00cc
G$TH2$0$0 == 0x00cd
_TH2	=	0x00cd
G$SMB0CR$0$0 == 0x00cf
_SMB0CR	=	0x00cf
G$PSW$0$0 == 0x00d0
_PSW	=	0x00d0
G$REF0CN$0$0 == 0x00d1
_REF0CN	=	0x00d1
G$DAC0L$0$0 == 0x00d2
_DAC0L	=	0x00d2
G$DAC0H$0$0 == 0x00d3
_DAC0H	=	0x00d3
G$DAC0CN$0$0 == 0x00d4
_DAC0CN	=	0x00d4
G$DAC1L$0$0 == 0x00d5
_DAC1L	=	0x00d5
G$DAC1H$0$0 == 0x00d6
_DAC1H	=	0x00d6
G$DAC1CN$0$0 == 0x00d7
_DAC1CN	=	0x00d7
G$PCA0CN$0$0 == 0x00d8
_PCA0CN	=	0x00d8
G$PCA0MD$0$0 == 0x00d9
_PCA0MD	=	0x00d9
G$PCA0CPM0$0$0 == 0x00da
_PCA0CPM0	=	0x00da
G$PCA0CPM1$0$0 == 0x00db
_PCA0CPM1	=	0x00db
G$PCA0CPM2$0$0 == 0x00dc
_PCA0CPM2	=	0x00dc
G$PCA0CPM3$0$0 == 0x00dd
_PCA0CPM3	=	0x00dd
G$PCA0CPM4$0$0 == 0x00de
_PCA0CPM4	=	0x00de
G$ACC$0$0 == 0x00e0
_ACC	=	0x00e0
G$XBR0$0$0 == 0x00e1
_XBR0	=	0x00e1
G$XBR1$0$0 == 0x00e2
_XBR1	=	0x00e2
G$XBR2$0$0 == 0x00e3
_XBR2	=	0x00e3
G$RCAP4L$0$0 == 0x00e4
_RCAP4L	=	0x00e4
G$RCAP4H$0$0 == 0x00e5
_RCAP4H	=	0x00e5
G$EIE1$0$0 == 0x00e6
_EIE1	=	0x00e6
G$EIE2$0$0 == 0x00e7
_EIE2	=	0x00e7
G$ADC0CN$0$0 == 0x00e8
_ADC0CN	=	0x00e8
G$PCA0L$0$0 == 0x00e9
_PCA0L	=	0x00e9
G$PCA0CPL0$0$0 == 0x00ea
_PCA0CPL0	=	0x00ea
G$PCA0CPL1$0$0 == 0x00eb
_PCA0CPL1	=	0x00eb
G$PCA0CPL2$0$0 == 0x00ec
_PCA0CPL2	=	0x00ec
G$PCA0CPL3$0$0 == 0x00ed
_PCA0CPL3	=	0x00ed
G$PCA0CPL4$0$0 == 0x00ee
_PCA0CPL4	=	0x00ee
G$RSTSRC$0$0 == 0x00ef
_RSTSRC	=	0x00ef
G$B$0$0 == 0x00f0
_B	=	0x00f0
G$SCON1$0$0 == 0x00f1
_SCON1	=	0x00f1
G$SBUF1$0$0 == 0x00f2
_SBUF1	=	0x00f2
G$SADDR1$0$0 == 0x00f3
_SADDR1	=	0x00f3
G$TL4$0$0 == 0x00f4
_TL4	=	0x00f4
G$TH4$0$0 == 0x00f5
_TH4	=	0x00f5
G$EIP1$0$0 == 0x00f6
_EIP1	=	0x00f6
G$EIP2$0$0 == 0x00f7
_EIP2	=	0x00f7
G$SPI0CN$0$0 == 0x00f8
_SPI0CN	=	0x00f8
G$PCA0H$0$0 == 0x00f9
_PCA0H	=	0x00f9
G$PCA0CPH0$0$0 == 0x00fa
_PCA0CPH0	=	0x00fa
G$PCA0CPH1$0$0 == 0x00fb
_PCA0CPH1	=	0x00fb
G$PCA0CPH2$0$0 == 0x00fc
_PCA0CPH2	=	0x00fc
G$PCA0CPH3$0$0 == 0x00fd
_PCA0CPH3	=	0x00fd
G$PCA0CPH4$0$0 == 0x00fe
_PCA0CPH4	=	0x00fe
G$WDTCN$0$0 == 0x00ff
_WDTCN	=	0x00ff
G$DP$0$0 == 0x8382
_DP	=	0x8382
G$TMR3RL$0$0 == 0x9392
_TMR3RL	=	0x9392
G$TMR3$0$0 == 0x9594
_TMR3	=	0x9594
G$ADC0$0$0 == 0xbfbe
_ADC0	=	0xbfbe
G$ADC0GT$0$0 == 0xc5c4
_ADC0GT	=	0xc5c4
G$ADC0LT$0$0 == 0xc7c6
_ADC0LT	=	0xc7c6
G$RCAP2$0$0 == 0xcbca
_RCAP2	=	0xcbca
G$T2$0$0 == 0xcdcc
_T2	=	0xcdcc
G$TMR2RL$0$0 == 0xcbca
_TMR2RL	=	0xcbca
G$TMR2$0$0 == 0xcdcc
_TMR2	=	0xcdcc
G$RCAP4$0$0 == 0xe5e4
_RCAP4	=	0xe5e4
G$T4$0$0 == 0xf5f4
_T4	=	0xf5f4
G$TMR4RL$0$0 == 0xe5e4
_TMR4RL	=	0xe5e4
G$TMR4$0$0 == 0xf5f4
_TMR4	=	0xf5f4
G$DAC0$0$0 == 0xd3d2
_DAC0	=	0xd3d2
G$DAC1$0$0 == 0xd6d5
_DAC1	=	0xd6d5
G$_XPAGE$0$0 == 0x00af
__XPAGE	=	0x00af
;--------------------------------------------------------
; special function bits
;--------------------------------------------------------
	.area RSEG    (DATA)
G$TF1$0$0 == 0x008f
_TF1	=	0x008f
G$TR1$0$0 == 0x008e
_TR1	=	0x008e
G$TF0$0$0 == 0x008d
_TF0	=	0x008d
G$TR0$0$0 == 0x008c
_TR0	=	0x008c
G$IE1$0$0 == 0x008b
_IE1	=	0x008b
G$IT1$0$0 == 0x008a
_IT1	=	0x008a
G$IE0$0$0 == 0x0089
_IE0	=	0x0089
G$IT0$0$0 == 0x0088
_IT0	=	0x0088
G$SM00$0$0 == 0x009f
_SM00	=	0x009f
G$SM10$0$0 == 0x009e
_SM10	=	0x009e
G$SM20$0$0 == 0x009d
_SM20	=	0x009d
G$REN0$0$0 == 0x009c
_REN0	=	0x009c
G$TB80$0$0 == 0x009b
_TB80	=	0x009b
G$RB80$0$0 == 0x009a
_RB80	=	0x009a
G$TI0$0$0 == 0x0099
_TI0	=	0x0099
G$RI0$0$0 == 0x0098
_RI0	=	0x0098
G$EA$0$0 == 0x00af
_EA	=	0x00af
G$IEGF0$0$0 == 0x00ae
_IEGF0	=	0x00ae
G$ET2$0$0 == 0x00ad
_ET2	=	0x00ad
G$ES0$0$0 == 0x00ac
_ES0	=	0x00ac
G$ET1$0$0 == 0x00ab
_ET1	=	0x00ab
G$EX1$0$0 == 0x00aa
_EX1	=	0x00aa
G$ET0$0$0 == 0x00a9
_ET0	=	0x00a9
G$EX0$0$0 == 0x00a8
_EX0	=	0x00a8
G$PT2$0$0 == 0x00bd
_PT2	=	0x00bd
G$PS$0$0 == 0x00bc
_PS	=	0x00bc
G$PT1$0$0 == 0x00bb
_PT1	=	0x00bb
G$PX1$0$0 == 0x00ba
_PX1	=	0x00ba
G$PT0$0$0 == 0x00b9
_PT0	=	0x00b9
G$PX0$0$0 == 0x00b8
_PX0	=	0x00b8
G$BUSY$0$0 == 0x00c7
_BUSY	=	0x00c7
G$ENSMB$0$0 == 0x00c6
_ENSMB	=	0x00c6
G$STA$0$0 == 0x00c5
_STA	=	0x00c5
G$STO$0$0 == 0x00c4
_STO	=	0x00c4
G$SI$0$0 == 0x00c3
_SI	=	0x00c3
G$AA$0$0 == 0x00c2
_AA	=	0x00c2
G$SMBFTE$0$0 == 0x00c1
_SMBFTE	=	0x00c1
G$SMBTOE$0$0 == 0x00c0
_SMBTOE	=	0x00c0
G$TF2$0$0 == 0x00cf
_TF2	=	0x00cf
G$EXF2$0$0 == 0x00ce
_EXF2	=	0x00ce
G$RCLK0$0$0 == 0x00cd
_RCLK0	=	0x00cd
G$TCLK0$0$0 == 0x00cc
_TCLK0	=	0x00cc
G$EXEN2$0$0 == 0x00cb
_EXEN2	=	0x00cb
G$TR2$0$0 == 0x00ca
_TR2	=	0x00ca
G$CT2$0$0 == 0x00c9
_CT2	=	0x00c9
G$CPRL2$0$0 == 0x00c8
_CPRL2	=	0x00c8
G$CY$0$0 == 0x00d7
_CY	=	0x00d7
G$AC$0$0 == 0x00d6
_AC	=	0x00d6
G$F0$0$0 == 0x00d5
_F0	=	0x00d5
G$RS1$0$0 == 0x00d4
_RS1	=	0x00d4
G$RS0$0$0 == 0x00d3
_RS0	=	0x00d3
G$OV$0$0 == 0x00d2
_OV	=	0x00d2
G$F1$0$0 == 0x00d1
_F1	=	0x00d1
G$P$0$0 == 0x00d0
_P	=	0x00d0
G$CF$0$0 == 0x00df
_CF	=	0x00df
G$CR$0$0 == 0x00de
_CR	=	0x00de
G$CCF4$0$0 == 0x00dc
_CCF4	=	0x00dc
G$CCF3$0$0 == 0x00db
_CCF3	=	0x00db
G$CCF2$0$0 == 0x00da
_CCF2	=	0x00da
G$CCF1$0$0 == 0x00d9
_CCF1	=	0x00d9
G$CCF0$0$0 == 0x00d8
_CCF0	=	0x00d8
G$AD0EN$0$0 == 0x00ef
_AD0EN	=	0x00ef
G$AD0TM$0$0 == 0x00ee
_AD0TM	=	0x00ee
G$AD0INT$0$0 == 0x00ed
_AD0INT	=	0x00ed
G$AD0BUSY$0$0 == 0x00ec
_AD0BUSY	=	0x00ec
G$AD0CM1$0$0 == 0x00eb
_AD0CM1	=	0x00eb
G$AD0CM0$0$0 == 0x00ea
_AD0CM0	=	0x00ea
G$AD0WINT$0$0 == 0x00e9
_AD0WINT	=	0x00e9
G$AD0LJST$0$0 == 0x00e8
_AD0LJST	=	0x00e8
G$SPIF$0$0 == 0x00ff
_SPIF	=	0x00ff
G$WCOL$0$0 == 0x00fe
_WCOL	=	0x00fe
G$MODF$0$0 == 0x00fd
_MODF	=	0x00fd
G$RXOVRN$0$0 == 0x00fc
_RXOVRN	=	0x00fc
G$TXBSY$0$0 == 0x00fb
_TXBSY	=	0x00fb
G$SLVSEL$0$0 == 0x00fa
_SLVSEL	=	0x00fa
G$MSTEN$0$0 == 0x00f9
_MSTEN	=	0x00f9
G$SPIEN$0$0 == 0x00f8
_SPIEN	=	0x00f8
;--------------------------------------------------------
; overlayable register banks
;--------------------------------------------------------
	.area REG_BANK_0	(REL,OVR,DATA)
	.ds 8
;--------------------------------------------------------
; overlayable bit register bank
;--------------------------------------------------------
	.area BIT_BANK	(REL,OVR,DATA)
bits:
	.ds 1
	b0 = bits[0]
	b1 = bits[1]
	b2 = bits[2]
	b3 = bits[3]
	b4 = bits[4]
	b5 = bits[5]
	b6 = bits[6]
	b7 = bits[7]
;--------------------------------------------------------
; internal ram data
;--------------------------------------------------------
	.area DSEG    (DATA)
G$timer0_tick$0$0==.
_timer0_tick::
	.ds 1
G$lamp_state$0$0==.
_lamp_state::
	.ds 1
G$uart1_rx_temp$0$0==.
_uart1_rx_temp::
	.ds 1
G$uart1_tx_temp$0$0==.
_uart1_tx_temp::
	.ds 1
G$uart0_busy$0$0==.
_uart0_busy::
	.ds 1
G$uart1_busy$0$0==.
_uart1_busy::
	.ds 1
G$uart1_rx_buffer$0$0==.
_uart1_rx_buffer::
	.ds 8
G$uart1_tx_buffer$0$0==.
_uart1_tx_buffer::
	.ds 8
G$green_flash$0$0==.
_green_flash::
	.ds 1
G$yellow_flash$0$0==.
_yellow_flash::
	.ds 1
G$white_flash$0$0==.
_white_flash::
	.ds 1
G$set_next$0$0==.
_set_next::
	.ds 10
G$set_act$0$0==.
_set_act::
	.ds 12
Lbuffer_init$buffer_size$1$1==.
_buffer_init_PARM_2:
	.ds 1
Lbuffer_init$buffer$1$1==.
_buffer_init_buffer_1_1:
	.ds 3
Lbuffer_put$value$1$1==.
_buffer_put_PARM_2:
	.ds 1
Lbuffer_put$buffer$1$1==.
_buffer_put_buffer_1_1:
	.ds 3
Lbuffer_put$sloc0$1$0==.
_buffer_put_sloc0_1_0:
	.ds 3
Lbuffer_get$value$1$1==.
_buffer_get_PARM_2:
	.ds 3
Lbuffer_get$buffer$1$1==.
_buffer_get_buffer_1_1:
	.ds 3
Lbuffer_get$sloc0$1$0==.
_buffer_get_sloc0_1_0:
	.ds 3
Lbuffer_get$sloc1$1$0==.
_buffer_get_sloc1_1_0:
	.ds 3
;--------------------------------------------------------
; overlayable items in internal ram 
;--------------------------------------------------------
	.area	OSEG    (OVR,DATA)
	.area	OSEG    (OVR,DATA)
;--------------------------------------------------------
; Stack segment in internal ram 
;--------------------------------------------------------
	.area	SSEG	(DATA)
__start__stack:
	.ds	1

;--------------------------------------------------------
; indirectly addressable internal ram data
;--------------------------------------------------------
	.area ISEG    (DATA)
;--------------------------------------------------------
; absolute internal ram data
;--------------------------------------------------------
	.area IABS    (ABS,DATA)
	.area IABS    (ABS,DATA)
;--------------------------------------------------------
; bit data
;--------------------------------------------------------
	.area BSEG    (BIT)
;--------------------------------------------------------
; paged external ram data
;--------------------------------------------------------
	.area PSEG    (PAG,XDATA)
;--------------------------------------------------------
; external ram data
;--------------------------------------------------------
	.area XSEG    (XDATA)
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area XABS    (ABS,XDATA)
;--------------------------------------------------------
; external initialized ram data
;--------------------------------------------------------
	.area XISEG   (XDATA)
	.area HOME    (CODE)
	.area GSINIT0 (CODE)
	.area GSINIT1 (CODE)
	.area GSINIT2 (CODE)
	.area GSINIT3 (CODE)
	.area GSINIT4 (CODE)
	.area GSINIT5 (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area CSEG    (CODE)
;--------------------------------------------------------
; interrupt vector 
;--------------------------------------------------------
	.area HOME    (CODE)
__interrupt_vect:
	ljmp	__sdcc_gsinit_startup
	reti
	.ds	7
	ljmp	_timer0_it
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_uart0_it
	.ds	5
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	reti
	.ds	7
	ljmp	_uart1_it
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area HOME    (CODE)
	.area GSINIT  (CODE)
	.area GSFINAL (CODE)
	.area GSINIT  (CODE)
	.globl __sdcc_gsinit_startup
	.globl __sdcc_program_startup
	.globl __start__stack
	.globl __mcs51_genXINIT
	.globl __mcs51_genXRAMCLEAR
	.globl __mcs51_genRAMCLEAR
	G$uart1_it$0$0 ==.
	C$railsignal.c$38$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:38: volatile tUInt8 timer0_tick = 0;
	mov	_timer0_tick,#0x00
	G$uart1_it$0$0 ==.
	C$railsignal.c$47$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:47: volatile tUInt8 lamp_state = 0;
	mov	_lamp_state,#0x00
	G$uart1_it$0$0 ==.
	C$railsignal.c$52$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:52: volatile tBool uart0_busy = FALSE;
	mov	_uart0_busy,#0x00
	G$uart1_it$0$0 ==.
	C$railsignal.c$53$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:53: volatile tBool uart1_busy = FALSE;
	mov	_uart1_busy,#0x00
	G$uart1_it$0$0 ==.
	C$railsignal.c$58$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:58: volatile tUInt8 green_flash = FLASH_NOT_ENABLED;
	mov	_green_flash,#0xFF
	G$uart1_it$0$0 ==.
	C$railsignal.c$59$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:59: volatile tUInt8 yellow_flash = FLASH_NOT_ENABLED;
	mov	_yellow_flash,#0xFF
	G$uart1_it$0$0 ==.
	C$railsignal.c$60$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:60: volatile tUInt8 white_flash = FLASH_NOT_ENABLED;
	mov	_white_flash,#0xFF
	.area GSFINAL (CODE)
	ljmp	__sdcc_program_startup
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area HOME    (CODE)
	.area HOME    (CODE)
__sdcc_program_startup:
	lcall	_main
;	return from main will lock up
	sjmp .
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area CSEG    (CODE)
;------------------------------------------------------------
;Allocation info for local variables in function 'main'
;------------------------------------------------------------
;act_req                   Allocated to registers r3 
;next_req                  Allocated to registers r2 
;------------------------------------------------------------
	G$main$0$0 ==.
	C$railsignal.c$73$0$0 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:73: void main()
;	-----------------------------------------
;	 function main
;	-----------------------------------------
_main:
	ar2 = 0x02
	ar3 = 0x03
	ar4 = 0x04
	ar5 = 0x05
	ar6 = 0x06
	ar7 = 0x07
	ar0 = 0x00
	ar1 = 0x01
	C$railsignal.c$76$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:76: interrupt_disable_all();					/* Disabling all interrupts */
	lcall	_interrupt_disable_all
	C$railsignal.c$77$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:77: clock_init();								/* Clock source selection */		
	lcall	_clock_init
	C$railsignal.c$78$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:78: watchdog_disable();							/* Disabling watchdog timer */
	lcall	_watchdog_disable
	C$railsignal.c$79$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:79: timer_init();								/* Timer initialization */
	lcall	_timer_init
	C$railsignal.c$80$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:80: crossbar_init();							/* Port assignment initialization */
	lcall	_crossbar_init
	C$railsignal.c$81$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:81: port_init();								/* GPIO initialization */
	lcall	_port_init
	C$railsignal.c$82$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:82: uart1_init();								/* UART1 initialization */
	lcall	_uart1_init
	C$railsignal.c$83$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:83: buffer_init(&uart1_rx_buffer,BUFFER_SIZE);	/* RX buffer initialization */
	mov	_buffer_init_PARM_2,#0x14
	mov	dptr,#_uart1_rx_buffer
	mov	b,#0x40
	lcall	_buffer_init
	C$railsignal.c$84$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:84: buffer_init(&uart1_tx_buffer,BUFFER_SIZE);	/* TX buffer initialization */
	mov	_buffer_init_PARM_2,#0x14
	mov	dptr,#_uart1_tx_buffer
	mov	b,#0x40
	lcall	_buffer_init
	C$railsignal.c$85$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:85: signal_array_init();						/* Signal setting functuin pointer array initialization */
	lcall	_signal_array_init
	C$railsignal.c$86$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:86: interrupt_enable_all();						/* Enabling all interrupts */
	lcall	_interrupt_enable_all
	C$railsignal.c$88$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:88: while(TRUE)
00109$:
	C$railsignal.c$91$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:91: if(buffer_get(&uart1_rx_buffer, &uart1_rx_temp))
	mov	_buffer_get_PARM_2,#_uart1_rx_temp
	mov	(_buffer_get_PARM_2 + 1),#0x00
	mov	(_buffer_get_PARM_2 + 2),#0x40
	mov	dptr,#_uart1_rx_buffer
	mov	b,#0x40
	lcall	_buffer_get
	mov	a,dpl
	jz	00105$
	C$railsignal.c$94$3$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:94: next_req = GET_HIGH_4(uart1_rx_temp);
	mov	a,_uart1_rx_temp
	swap	a
	anl	a,#0x0f
	mov	r2,a
	C$railsignal.c$95$3$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:95: act_req = GET_LOW_4(uart1_rx_temp);
	mov	a,#0x0F
	anl	a,_uart1_rx_temp
	mov	r3,a
	C$railsignal.c$96$3$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:96: if((next_req < 5) && (act_req < 6))
	cjne	r2,#0x05,00119$
00119$:
	jnc	00105$
	cjne	r3,#0x06,00121$
00121$:
	jnc	00105$
	C$railsignal.c$99$4$4 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:99: set_next[next_req]();
	mov	a,r2
	add	a,r2
	add	a,#_set_next
	mov	r0,a
	mov	ar2,@r0
	inc	r0
	mov	ar4,@r0
	dec	r0
	push	ar2
	push	ar3
	push	ar4
	mov	dpl,r2
	mov	dph,r4
	lcall	__sdcc_call_dptr
	pop	ar4
	pop	ar3
	pop	ar2
	C$railsignal.c$100$4$4 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:100: set_act[act_req]();
	mov	a,r3
	add	a,r3
	add	a,#_set_act
	mov	r0,a
	mov	ar2,@r0
	inc	r0
	mov	ar3,@r0
	dec	r0
	push	ar2
	push	ar3
	mov	dpl,r2
	mov	dph,r3
	lcall	__sdcc_call_dptr
	pop	ar3
	pop	ar2
00105$:
	C$railsignal.c$104$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:104: if(buffer_get(&uart1_tx_buffer, &uart1_tx_temp))
	mov	_buffer_get_PARM_2,#_uart1_tx_temp
	mov	(_buffer_get_PARM_2 + 1),#0x00
	mov	(_buffer_get_PARM_2 + 2),#0x40
	mov	dptr,#_uart1_tx_buffer
	mov	b,#0x40
	lcall	_buffer_get
	mov	a,dpl
	jnz	00125$
	ljmp	00109$
00125$:
	C$railsignal.c$106$3$5 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:106: uart1_send_byte(uart1_tx_temp);
	mov	dpl,_uart1_tx_temp
	lcall	_uart1_send_byte
	C$railsignal.c$110$1$1 ==.
	XG$main$0$0 ==.
	ljmp	00109$
;------------------------------------------------------------
;Allocation info for local variables in function 'clock_init'
;------------------------------------------------------------
;------------------------------------------------------------
	G$clock_init$0$0 ==.
	C$railsignal.c$121$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:121: void clock_init()
;	-----------------------------------------
;	 function clock_init
;	-----------------------------------------
_clock_init:
	C$railsignal.c$134$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:134: OSCXCN = BS(_XOSCMD2) | BS(_XOSCMD1) | BS(_XFCN2) | BS(_XFCN1) | BS(_XFCN0);
	mov	_OSCXCN,#0x67
	C$railsignal.c$136$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:136: while(0 == (OSCXCN & BS(_XTLVLD)))	{}
00101$:
	mov	a,#0x80
	anl	a,_OSCXCN
	mov	r2,a
	jz	00101$
	C$railsignal.c$145$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:145: OSCICN = BS(_CLKSL);
	mov	_OSCICN,#0x08
	C$railsignal.c$146$1$1 ==.
	XG$clock_init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'watchdog_disable'
;------------------------------------------------------------
;------------------------------------------------------------
	G$watchdog_disable$0$0 ==.
	C$railsignal.c$148$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:148: void watchdog_disable()
;	-----------------------------------------
;	 function watchdog_disable
;	-----------------------------------------
_watchdog_disable:
	C$railsignal.c$150$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:150: WDTCN = 0xDE;
	mov	_WDTCN,#0xDE
	C$railsignal.c$151$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:151: WDTCN = 0xAD;
	mov	_WDTCN,#0xAD
	C$railsignal.c$152$1$1 ==.
	XG$watchdog_disable$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'interrupt_disable_all'
;------------------------------------------------------------
;------------------------------------------------------------
	G$interrupt_disable_all$0$0 ==.
	C$railsignal.c$154$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:154: void interrupt_disable_all()
;	-----------------------------------------
;	 function interrupt_disable_all
;	-----------------------------------------
_interrupt_disable_all:
	C$railsignal.c$156$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:156: CLRBIT(EA);
	clr	_EA
	C$railsignal.c$157$1$1 ==.
	XG$interrupt_disable_all$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'interrupt_enable_all'
;------------------------------------------------------------
;------------------------------------------------------------
	G$interrupt_enable_all$0$0 ==.
	C$railsignal.c$159$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:159: void interrupt_enable_all()
;	-----------------------------------------
;	 function interrupt_enable_all
;	-----------------------------------------
_interrupt_enable_all:
	C$railsignal.c$161$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:161: SETBIT(EA);
	setb	_EA
	C$railsignal.c$162$1$1 ==.
	XG$interrupt_enable_all$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'timer_init'
;------------------------------------------------------------
;------------------------------------------------------------
	G$timer_init$0$0 ==.
	C$railsignal.c$164$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:164: void timer_init()
;	-----------------------------------------
;	 function timer_init
;	-----------------------------------------
_timer_init:
	C$railsignal.c$178$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:178: CKCON = BS(_T1M);
	mov	_CKCON,#0x10
	C$railsignal.c$188$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:188: TMOD = BS(_T0M0) | BS(_T1M1);
	mov	_TMOD,#0x21
	C$railsignal.c$194$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:194: TH0 = 0xB8;
	mov	_TH0,#0xB8
	C$railsignal.c$195$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:195: TL0 = 0x07;
	mov	_TL0,#0x07
	C$railsignal.c$201$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:201: TH1 = 0xDC;
	mov	_TH1,#0xDC
	C$railsignal.c$213$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:213: TCON = BS(_TR0) | BS(_TR1);
	mov	_TCON,#0x50
	C$railsignal.c$222$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:222: IE |= BS(_ET0);	
	orl	_IE,#0x02
	C$railsignal.c$223$1$1 ==.
	XG$timer_init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'port_init'
;------------------------------------------------------------
;------------------------------------------------------------
	G$port_init$0$0 ==.
	C$railsignal.c$225$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:225: void port_init()
;	-----------------------------------------
;	 function port_init
;	-----------------------------------------
_port_init:
	C$railsignal.c$238$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:238: P74OUT = BS(_P5H);
	mov	_P74OUT,#0x08
	C$railsignal.c$240$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:240: P5 = ~(LED_1_MASK | LED_2_MASK | LED_3_MASK | LED_4_MASK);
	mov	_P5,#0x0F
	C$railsignal.c$242$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:242: P0MDOUT = BS(4);
	mov	_P0MDOUT,#0x10
	C$railsignal.c$243$1$1 ==.
	XG$port_init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'uart0_init'
;------------------------------------------------------------
;------------------------------------------------------------
	G$uart0_init$0$0 ==.
	C$railsignal.c$245$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:245: void uart0_init()
;	-----------------------------------------
;	 function uart0_init
;	-----------------------------------------
_uart0_init:
	C$railsignal.c$256$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:256: PCON &= ~(BS(_SMOD0) | BS(_SSTAT0));
	anl	_PCON,#0x3F
	C$railsignal.c$267$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:267: SCON0 = BS(_SM00) | BS(_SM10) | BS(_REN0);
	mov	_SCON0,#0xD0
	C$railsignal.c$269$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:269: PCON |= BS(_SSTAT0);
	orl	_PCON,#0x40
	C$railsignal.c$278$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:278: IE |= BS(_ES0);
	orl	_IE,#0x10
	C$railsignal.c$279$1$1 ==.
	XG$uart0_init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'uart1_init'
;------------------------------------------------------------
;------------------------------------------------------------
	G$uart1_init$0$0 ==.
	C$railsignal.c$281$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:281: void uart1_init()
;	-----------------------------------------
;	 function uart1_init
;	-----------------------------------------
_uart1_init:
	C$railsignal.c$292$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:292: PCON &= ~(BS(_SMOD1) | BS(_SSTAT1));
	anl	_PCON,#0xE7
	C$railsignal.c$303$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:303: SCON1 = BS(_SM01) | BS(_SM11) | BS(_REN1);
	mov	_SCON1,#0xD0
	C$railsignal.c$305$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:305: PCON |= BS(_SSTAT1);
	orl	_PCON,#0x08
	C$railsignal.c$314$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:314: EIE2 = BS(_ES1);
	mov	_EIE2,#0x40
	C$railsignal.c$315$1$1 ==.
	XG$uart1_init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'crossbar_init'
;------------------------------------------------------------
;------------------------------------------------------------
	G$crossbar_init$0$0 ==.
	C$railsignal.c$317$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:317: void crossbar_init()
;	-----------------------------------------
;	 function crossbar_init
;	-----------------------------------------
_crossbar_init:
	C$railsignal.c$327$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:327: XBR0 = BS(_UART0EN) | BS(_SMB0EN);
	mov	_XBR0,#0x05
	C$railsignal.c$336$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:336: XBR2 = BS(_UART1EN) | BS(_XBARE);
	mov	_XBR2,#0x44
	C$railsignal.c$337$1$1 ==.
	XG$crossbar_init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'buffer_init'
;------------------------------------------------------------
;buffer_size               Allocated with name '_buffer_init_PARM_2'
;buffer                    Allocated with name '_buffer_init_buffer_1_1'
;------------------------------------------------------------
	G$buffer_init$0$0 ==.
	C$railsignal.c$339$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:339: void buffer_init(tBufferUInt8 *buffer, tUInt8 buffer_size)
;	-----------------------------------------
;	 function buffer_init
;	-----------------------------------------
_buffer_init:
	mov	_buffer_init_buffer_1_1,dpl
	mov	(_buffer_init_buffer_1_1 + 1),dph
	mov	(_buffer_init_buffer_1_1 + 2),b
	C$railsignal.c$341$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:341: buffer->size = buffer_size;
	mov	a,#0x03
	add	a,_buffer_init_buffer_1_1
	mov	r5,a
	clr	a
	addc	a,(_buffer_init_buffer_1_1 + 1)
	mov	r6,a
	mov	r7,(_buffer_init_buffer_1_1 + 2)
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,_buffer_init_PARM_2
	lcall	__gptrput
	C$railsignal.c$342$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:342: buffer->dat = (tUInt8*)malloc((buffer->size)*sizeof(tUInt8));
	mov	r0,_buffer_init_PARM_2
	mov	r1,#0x00
	mov	dpl,r0
	mov	dph,r1
	push	ar5
	push	ar6
	push	ar7
	lcall	_malloc
	mov	r0,dpl
	mov	r1,dph
	pop	ar7
	pop	ar6
	pop	ar5
	mov	r2,#0x00
	mov	dpl,_buffer_init_buffer_1_1
	mov	dph,(_buffer_init_buffer_1_1 + 1)
	mov	b,(_buffer_init_buffer_1_1 + 2)
	mov	a,r0
	lcall	__gptrput
	inc	dptr
	mov	a,r1
	lcall	__gptrput
	inc	dptr
	mov	a,r2
	lcall	__gptrput
	C$railsignal.c$343$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:343: memset(buffer->dat, 0, (buffer->size)*sizeof(tUInt8));
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r5,a
	mov	_memset_PARM_3,r5
	mov	(_memset_PARM_3 + 1),#0x00
	mov	_memset_PARM_2,#0x00
	mov	dpl,r0
	mov	dph,r1
	mov	b,r2
	lcall	_memset
	C$railsignal.c$344$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:344: buffer->stored = 0;
	mov	a,#0x04
	add	a,_buffer_init_buffer_1_1
	mov	r2,a
	clr	a
	addc	a,(_buffer_init_buffer_1_1 + 1)
	mov	r3,a
	mov	r4,(_buffer_init_buffer_1_1 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	clr	a
	lcall	__gptrput
	C$railsignal.c$345$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:345: buffer->index_last = 0;
	mov	a,#0x05
	add	a,_buffer_init_buffer_1_1
	mov	r2,a
	clr	a
	addc	a,(_buffer_init_buffer_1_1 + 1)
	mov	r3,a
	mov	r4,(_buffer_init_buffer_1_1 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	clr	a
	lcall	__gptrput
	C$railsignal.c$346$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:346: buffer->index_first = 0;
	mov	a,#0x06
	add	a,_buffer_init_buffer_1_1
	mov	r2,a
	clr	a
	addc	a,(_buffer_init_buffer_1_1 + 1)
	mov	r3,a
	mov	r4,(_buffer_init_buffer_1_1 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	clr	a
	lcall	__gptrput
	C$railsignal.c$347$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:347: buffer->overflow = FALSE;
	mov	a,#0x07
	add	a,_buffer_init_buffer_1_1
	mov	r2,a
	clr	a
	addc	a,(_buffer_init_buffer_1_1 + 1)
	mov	r3,a
	mov	r4,(_buffer_init_buffer_1_1 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	clr	a
	C$railsignal.c$348$1$1 ==.
	XG$buffer_init$0$0 ==.
	ljmp	__gptrput
;------------------------------------------------------------
;Allocation info for local variables in function 'buffer_put'
;------------------------------------------------------------
;value                     Allocated with name '_buffer_put_PARM_2'
;buffer                    Allocated with name '_buffer_put_buffer_1_1'
;sloc0                     Allocated with name '_buffer_put_sloc0_1_0'
;------------------------------------------------------------
	G$buffer_put$0$0 ==.
	C$railsignal.c$350$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:350: void buffer_put(tBufferUInt8 *buffer, tUInt8 value)
;	-----------------------------------------
;	 function buffer_put
;	-----------------------------------------
_buffer_put:
	mov	_buffer_put_buffer_1_1,dpl
	mov	(_buffer_put_buffer_1_1 + 1),dph
	mov	(_buffer_put_buffer_1_1 + 2),b
	C$railsignal.c$352$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:352: interrupt_disable_all();
	lcall	_interrupt_disable_all
	C$railsignal.c$354$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:354: if(buffer->stored < buffer->size)
	mov	a,#0x04
	add	a,_buffer_put_buffer_1_1
	mov	r5,a
	clr	a
	addc	a,(_buffer_put_buffer_1_1 + 1)
	mov	r6,a
	mov	r7,(_buffer_put_buffer_1_1 + 2)
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r0,a
	mov	a,#0x03
	add	a,_buffer_put_buffer_1_1
	mov	r1,a
	clr	a
	addc	a,(_buffer_put_buffer_1_1 + 1)
	mov	r2,a
	mov	r3,(_buffer_put_buffer_1_1 + 2)
	mov	dpl,r1
	mov	dph,r2
	mov	b,r3
	lcall	__gptrget
	mov	r2,a
	clr	c
	mov	a,r0
	subb	a,r2
	jc	00110$
	ljmp	00104$
00110$:
	C$railsignal.c$356$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:356: buffer->overflow = FALSE;
	push	ar0
	mov	a,#0x07
	add	a,_buffer_put_buffer_1_1
	mov	r3,a
	clr	a
	addc	a,(_buffer_put_buffer_1_1 + 1)
	mov	r4,a
	mov	r1,(_buffer_put_buffer_1_1 + 2)
	mov	dpl,r3
	mov	dph,r4
	mov	b,r1
	clr	a
	lcall	__gptrput
	C$railsignal.c$357$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:357: buffer->dat[buffer->index_last] = value;
	mov	dpl,_buffer_put_buffer_1_1
	mov	dph,(_buffer_put_buffer_1_1 + 1)
	mov	b,(_buffer_put_buffer_1_1 + 2)
	lcall	__gptrget
	mov	r3,a
	inc	dptr
	lcall	__gptrget
	mov	r4,a
	inc	dptr
	lcall	__gptrget
	mov	r1,a
	mov	a,#0x05
	add	a,_buffer_put_buffer_1_1
	mov	_buffer_put_sloc0_1_0,a
	clr	a
	addc	a,(_buffer_put_buffer_1_1 + 1)
	mov	(_buffer_put_sloc0_1_0 + 1),a
	mov	(_buffer_put_sloc0_1_0 + 2),(_buffer_put_buffer_1_1 + 2)
	mov	dpl,_buffer_put_sloc0_1_0
	mov	dph,(_buffer_put_sloc0_1_0 + 1)
	mov	b,(_buffer_put_sloc0_1_0 + 2)
	lcall	__gptrget
	mov	r0,a
	add	a,r3
	mov	r3,a
	clr	a
	addc	a,r4
	mov	r4,a
	mov	dpl,r3
	mov	dph,r4
	mov	b,r1
	mov	a,_buffer_put_PARM_2
	lcall	__gptrput
	C$railsignal.c$358$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:358: buffer->index_last++;
	inc	r0
	mov	dpl,_buffer_put_sloc0_1_0
	mov	dph,(_buffer_put_sloc0_1_0 + 1)
	mov	b,(_buffer_put_sloc0_1_0 + 2)
	mov	a,r0
	lcall	__gptrput
	C$railsignal.c$359$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:359: if(buffer->index_last >= buffer->size)
	clr	c
	mov	a,r0
	subb	a,r2
	pop	ar0
	jc	00102$
	C$railsignal.c$361$3$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:361: buffer->index_last = 0;
	mov	dpl,_buffer_put_sloc0_1_0
	mov	dph,(_buffer_put_sloc0_1_0 + 1)
	mov	b,(_buffer_put_sloc0_1_0 + 2)
	clr	a
	lcall	__gptrput
00102$:
	C$railsignal.c$363$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:363: buffer->stored++;
	inc	r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r0
	lcall	__gptrput
	sjmp	00105$
00104$:
	C$railsignal.c$367$2$4 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:367: buffer->overflow = TRUE;
	mov	a,#0x07
	add	a,_buffer_put_buffer_1_1
	mov	r2,a
	clr	a
	addc	a,(_buffer_put_buffer_1_1 + 1)
	mov	r3,a
	mov	r4,(_buffer_put_buffer_1_1 + 2)
	mov	dpl,r2
	mov	dph,r3
	mov	b,r4
	mov	a,#0x01
	lcall	__gptrput
00105$:
	C$railsignal.c$369$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:369: interrupt_enable_all();
	C$railsignal.c$370$1$1 ==.
	XG$buffer_put$0$0 ==.
	ljmp	_interrupt_enable_all
;------------------------------------------------------------
;Allocation info for local variables in function 'buffer_get'
;------------------------------------------------------------
;value                     Allocated with name '_buffer_get_PARM_2'
;buffer                    Allocated with name '_buffer_get_buffer_1_1'
;sloc0                     Allocated with name '_buffer_get_sloc0_1_0'
;sloc1                     Allocated with name '_buffer_get_sloc1_1_0'
;------------------------------------------------------------
	G$buffer_get$0$0 ==.
	C$railsignal.c$372$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:372: tBool buffer_get(tBufferUInt8 *buffer, tUInt8 *value)
;	-----------------------------------------
;	 function buffer_get
;	-----------------------------------------
_buffer_get:
	mov	_buffer_get_buffer_1_1,dpl
	mov	(_buffer_get_buffer_1_1 + 1),dph
	mov	(_buffer_get_buffer_1_1 + 2),b
	C$railsignal.c$374$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:374: interrupt_disable_all();
	lcall	_interrupt_disable_all
	C$railsignal.c$376$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:376: if(buffer->stored > 0)
	mov	a,#0x04
	add	a,_buffer_get_buffer_1_1
	mov	r5,a
	clr	a
	addc	a,(_buffer_get_buffer_1_1 + 1)
	mov	r6,a
	mov	r7,(_buffer_get_buffer_1_1 + 2)
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	lcall	__gptrget
	mov	r0,a
	jnz	00110$
	ljmp	00104$
00110$:
	C$railsignal.c$378$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:378: *value = buffer->dat[buffer->index_first];
	push	ar0
	mov	_buffer_get_sloc0_1_0,_buffer_get_PARM_2
	mov	(_buffer_get_sloc0_1_0 + 1),(_buffer_get_PARM_2 + 1)
	mov	(_buffer_get_sloc0_1_0 + 2),(_buffer_get_PARM_2 + 2)
	mov	dpl,_buffer_get_buffer_1_1
	mov	dph,(_buffer_get_buffer_1_1 + 1)
	mov	b,(_buffer_get_buffer_1_1 + 2)
	lcall	__gptrget
	mov	r4,a
	inc	dptr
	lcall	__gptrget
	mov	r0,a
	inc	dptr
	lcall	__gptrget
	mov	r2,a
	mov	a,#0x06
	add	a,_buffer_get_buffer_1_1
	mov	_buffer_get_sloc1_1_0,a
	clr	a
	addc	a,(_buffer_get_buffer_1_1 + 1)
	mov	(_buffer_get_sloc1_1_0 + 1),a
	mov	(_buffer_get_sloc1_1_0 + 2),(_buffer_get_buffer_1_1 + 2)
	mov	dpl,_buffer_get_sloc1_1_0
	mov	dph,(_buffer_get_sloc1_1_0 + 1)
	mov	b,(_buffer_get_sloc1_1_0 + 2)
	lcall	__gptrget
	mov	r3,a
	add	a,r4
	mov	r4,a
	clr	a
	addc	a,r0
	mov	r0,a
	mov	dpl,r4
	mov	dph,r0
	mov	b,r2
	lcall	__gptrget
	mov	dpl,_buffer_get_sloc0_1_0
	mov	dph,(_buffer_get_sloc0_1_0 + 1)
	mov	b,(_buffer_get_sloc0_1_0 + 2)
	lcall	__gptrput
	C$railsignal.c$379$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:379: buffer->index_first++;
	inc	r3
	mov	dpl,_buffer_get_sloc1_1_0
	mov	dph,(_buffer_get_sloc1_1_0 + 1)
	mov	b,(_buffer_get_sloc1_1_0 + 2)
	mov	a,r3
	lcall	__gptrput
	C$railsignal.c$380$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:380: if(buffer->index_first >= buffer->size)
	mov	a,#0x03
	add	a,_buffer_get_buffer_1_1
	mov	r2,a
	clr	a
	addc	a,(_buffer_get_buffer_1_1 + 1)
	mov	r4,a
	mov	r0,(_buffer_get_buffer_1_1 + 2)
	mov	dpl,r2
	mov	dph,r4
	mov	b,r0
	lcall	__gptrget
	mov	r2,a
	clr	c
	mov	a,r3
	subb	a,r2
	pop	ar0
	jc	00102$
	C$railsignal.c$382$3$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:382: buffer->index_first = 0;
	mov	dpl,_buffer_get_sloc1_1_0
	mov	dph,(_buffer_get_sloc1_1_0 + 1)
	mov	b,(_buffer_get_sloc1_1_0 + 2)
	clr	a
	lcall	__gptrput
00102$:
	C$railsignal.c$384$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:384: buffer->stored--;
	dec	r0
	mov	dpl,r5
	mov	dph,r6
	mov	b,r7
	mov	a,r0
	lcall	__gptrput
	C$railsignal.c$385$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:385: interrupt_enable_all();
	lcall	_interrupt_enable_all
	C$railsignal.c$386$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:386: return TRUE;
	mov	dpl,#0x01
	ret
00104$:
	C$railsignal.c$390$2$4 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:390: interrupt_enable_all();
	lcall	_interrupt_enable_all
	C$railsignal.c$391$2$4 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:391: return FALSE;
	mov	dpl,#0x00
	C$railsignal.c$393$1$1 ==.
	XG$buffer_get$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_next_vmax'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_next_vmax$0$0 ==.
	C$railsignal.c$395$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:395: void set_next_vmax()
;	-----------------------------------------
;	 function set_next_vmax
;	-----------------------------------------
_set_next_vmax:
	C$railsignal.c$397$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:397: P5 |= LAMP_GREEN_MASK;
	orl	_P5,#0x80
	C$railsignal.c$398$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:398: P5 &= ~LAMP_YELLOWN_MASK;
	anl	_P5,#0xBF
	C$railsignal.c$399$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:399: green_flash = FLASH_NOT_ENABLED;
	mov	_green_flash,#0xFF
	C$railsignal.c$400$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:400: yellow_flash = FLASH_NOT_ENABLED;
	mov	_yellow_flash,#0xFF
	C$railsignal.c$401$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:401: lamp_state |= BS(LAMP_GREEN);
	orl	_lamp_state,#0x80
	C$railsignal.c$402$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:402: lamp_state &= ~BS(LAMP_YELLOWN);
	anl	_lamp_state,#0xBF
	C$railsignal.c$403$1$1 ==.
	XG$set_next_vmax$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_next_120'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_next_120$0$0 ==.
	C$railsignal.c$405$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:405: void set_next_120()
;	-----------------------------------------
;	 function set_next_120
;	-----------------------------------------
_set_next_120:
	C$railsignal.c$407$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:407: P5 |= LAMP_GREEN_MASK;
	orl	_P5,#0x80
	C$railsignal.c$408$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:408: P5 &= ~LAMP_YELLOWN_MASK;
	anl	_P5,#0xBF
	C$railsignal.c$409$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:409: green_flash = FLASH_FAST_ENABLED;
	mov	_green_flash,#0x32
	C$railsignal.c$410$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:410: yellow_flash = FLASH_NOT_ENABLED;
	mov	_yellow_flash,#0xFF
	C$railsignal.c$411$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:411: lamp_state |= LAMP_GREEN_MASK;
	orl	_lamp_state,#0x80
	C$railsignal.c$412$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:412: lamp_state &= ~LAMP_YELLOWN_MASK;
	anl	_lamp_state,#0xBF
	C$railsignal.c$413$1$1 ==.
	XG$set_next_120$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_next_80'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_next_80$0$0 ==.
	C$railsignal.c$415$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:415: void set_next_80()
;	-----------------------------------------
;	 function set_next_80
;	-----------------------------------------
_set_next_80:
	C$railsignal.c$417$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:417: P5 |= LAMP_GREEN_MASK;
	orl	_P5,#0x80
	C$railsignal.c$418$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:418: P5 &= ~LAMP_YELLOWN_MASK;
	anl	_P5,#0xBF
	C$railsignal.c$419$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:419: green_flash = FLASH_SLOW_ENABLED;
	mov	_green_flash,#0x64
	C$railsignal.c$420$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:420: yellow_flash = FLASH_NOT_ENABLED;
	mov	_yellow_flash,#0xFF
	C$railsignal.c$421$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:421: lamp_state |= LAMP_GREEN_MASK;
	orl	_lamp_state,#0x80
	C$railsignal.c$422$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:422: lamp_state &= ~LAMP_YELLOWN_MASK;
	anl	_lamp_state,#0xBF
	C$railsignal.c$423$1$1 ==.
	XG$set_next_80$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_next_40'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_next_40$0$0 ==.
	C$railsignal.c$425$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:425: void set_next_40()
;	-----------------------------------------
;	 function set_next_40
;	-----------------------------------------
_set_next_40:
	C$railsignal.c$427$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:427: P5 &= ~LAMP_GREEN_MASK;
	anl	_P5,#0x7F
	C$railsignal.c$428$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:428: P5 |= LAMP_YELLOWN_MASK;
	orl	_P5,#0x40
	C$railsignal.c$429$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:429: green_flash = FLASH_NOT_ENABLED;
	mov	_green_flash,#0xFF
	C$railsignal.c$430$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:430: yellow_flash = FLASH_SLOW_ENABLED;
	mov	_yellow_flash,#0x64
	C$railsignal.c$431$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:431: lamp_state &= ~LAMP_GREEN_MASK;
	anl	_lamp_state,#0x7F
	C$railsignal.c$432$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:432: lamp_state |= LAMP_YELLOWN_MASK;
	orl	_lamp_state,#0x40
	C$railsignal.c$433$1$1 ==.
	XG$set_next_40$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_next_0'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_next_0$0$0 ==.
	C$railsignal.c$435$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:435: void set_next_0()
;	-----------------------------------------
;	 function set_next_0
;	-----------------------------------------
_set_next_0:
	C$railsignal.c$437$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:437: P5 &= ~LAMP_GREEN_MASK;
	anl	_P5,#0x7F
	C$railsignal.c$438$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:438: P5 |= LAMP_YELLOWN_MASK;
	orl	_P5,#0x40
	C$railsignal.c$439$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:439: green_flash = FLASH_NOT_ENABLED;
	mov	_green_flash,#0xFF
	C$railsignal.c$440$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:440: yellow_flash = FLASH_NOT_ENABLED;
	mov	_yellow_flash,#0xFF
	C$railsignal.c$441$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:441: lamp_state &= ~LAMP_GREEN_MASK;
	anl	_lamp_state,#0x7F
	C$railsignal.c$442$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:442: lamp_state |= LAMP_YELLOWN_MASK;
	orl	_lamp_state,#0x40
	C$railsignal.c$443$1$1 ==.
	XG$set_next_0$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_act_vmax'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_act_vmax$0$0 ==.
	C$railsignal.c$445$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:445: void set_act_vmax()
;	-----------------------------------------
;	 function set_act_vmax
;	-----------------------------------------
_set_act_vmax:
	C$railsignal.c$447$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:447: P5 &= ~(LAMP_YELLOWA_MASK | LAMP_RED_MASK);
	anl	_P5,#0xCF
	C$railsignal.c$448$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:448: white_flash = FLASH_NOT_ENABLED;
	mov	_white_flash,#0xFF
	C$railsignal.c$449$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:449: lamp_state &= ~(LAMP_YELLOWA_MASK | LAMP_RED_MASK | LAMP_CALL_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
	anl	_lamp_state,#0xC1
	C$railsignal.c$450$1$1 ==.
	XG$set_act_vmax$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_act_120'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_act_120$0$0 ==.
	C$railsignal.c$452$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:452: void set_act_120()
;	-----------------------------------------
;	 function set_act_120
;	-----------------------------------------
_set_act_120:
	C$railsignal.c$454$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:454: P5 |= LAMP_YELLOWA_MASK;
	orl	_P5,#0x10
	C$railsignal.c$455$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:455: P5 &= ~LAMP_RED_MASK;
	anl	_P5,#0xDF
	C$railsignal.c$456$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:456: white_flash = FLASH_NOT_ENABLED;
	mov	_white_flash,#0xFF
	C$railsignal.c$457$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:457: lamp_state |= (LAMP_YELLOWA_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
	orl	_lamp_state,#0x16
	C$railsignal.c$458$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:458: lamp_state &= ~(LAMP_RED_MASK | LAMP_CALL_MASK);
	anl	_lamp_state,#0xD7
	C$railsignal.c$459$1$1 ==.
	XG$set_act_120$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_act_80'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_act_80$0$0 ==.
	C$railsignal.c$461$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:461: void set_act_80()
;	-----------------------------------------
;	 function set_act_80
;	-----------------------------------------
_set_act_80:
	C$railsignal.c$463$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:463: P5 |= LAMP_YELLOWA_MASK;
	orl	_P5,#0x10
	C$railsignal.c$464$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:464: P5 &= ~LAMP_RED_MASK;
	anl	_P5,#0xDF
	C$railsignal.c$465$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:465: white_flash = FLASH_NOT_ENABLED;
	mov	_white_flash,#0xFF
	C$railsignal.c$466$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:466: lamp_state |= (LAMP_YELLOWA_MASK | LAMP_STR1_MASK);
	orl	_lamp_state,#0x14
	C$railsignal.c$467$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:467: lamp_state &= ~(LAMP_RED_MASK | LAMP_CALL_MASK | LAMP_STR2_MASK);
	anl	_lamp_state,#0xD5
	C$railsignal.c$468$1$1 ==.
	XG$set_act_80$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_act_40'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_act_40$0$0 ==.
	C$railsignal.c$470$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:470: void set_act_40()
;	-----------------------------------------
;	 function set_act_40
;	-----------------------------------------
_set_act_40:
	C$railsignal.c$472$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:472: P5 |= LAMP_YELLOWA_MASK;
	orl	_P5,#0x10
	C$railsignal.c$473$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:473: P5 &= ~LAMP_RED_MASK;
	anl	_P5,#0xDF
	C$railsignal.c$474$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:474: white_flash = FLASH_NOT_ENABLED;
	mov	_white_flash,#0xFF
	C$railsignal.c$475$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:475: lamp_state |= LAMP_YELLOWA_MASK;
	orl	_lamp_state,#0x10
	C$railsignal.c$476$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:476: lamp_state &= ~(LAMP_RED_MASK | LAMP_CALL_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
	anl	_lamp_state,#0xD1
	C$railsignal.c$477$1$1 ==.
	XG$set_act_40$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_act_0'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_act_0$0$0 ==.
	C$railsignal.c$479$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:479: void set_act_0()
;	-----------------------------------------
;	 function set_act_0
;	-----------------------------------------
_set_act_0:
	C$railsignal.c$481$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:481: P5 &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK);
	anl	_P5,#0x2F
	C$railsignal.c$482$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:482: P5 |= LAMP_RED_MASK;
	orl	_P5,#0x20
	C$railsignal.c$483$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:483: green_flash = FLASH_NOT_ENABLED;
	mov	_green_flash,#0xFF
	C$railsignal.c$484$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:484: yellow_flash = FLASH_NOT_ENABLED;
	mov	_yellow_flash,#0xFF
	C$railsignal.c$485$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:485: white_flash = FLASH_NOT_ENABLED;
	mov	_white_flash,#0xFF
	C$railsignal.c$486$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:486: lamp_state |= LAMP_RED_MASK;
	orl	_lamp_state,#0x20
	C$railsignal.c$487$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:487: lamp_state &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK | LAMP_CALL_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
	anl	_lamp_state,#0x21
	C$railsignal.c$488$1$1 ==.
	XG$set_act_0$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'set_act_call'
;------------------------------------------------------------
;------------------------------------------------------------
	G$set_act_call$0$0 ==.
	C$railsignal.c$490$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:490: void set_act_call()
;	-----------------------------------------
;	 function set_act_call
;	-----------------------------------------
_set_act_call:
	C$railsignal.c$492$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:492: P5 &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK);
	anl	_P5,#0x2F
	C$railsignal.c$493$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:493: P5 |= LAMP_RED_MASK;
	orl	_P5,#0x20
	C$railsignal.c$494$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:494: green_flash = FLASH_NOT_ENABLED;
	mov	_green_flash,#0xFF
	C$railsignal.c$495$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:495: yellow_flash = FLASH_NOT_ENABLED;
	mov	_yellow_flash,#0xFF
	C$railsignal.c$496$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:496: white_flash = FLASH_SLOW_ENABLED;
	mov	_white_flash,#0x64
	C$railsignal.c$497$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:497: lamp_state |= (LAMP_RED_MASK | LAMP_CALL_MASK);
	orl	_lamp_state,#0x28
	C$railsignal.c$498$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:498: lamp_state &= ~(LAMP_GREEN_MASK | LAMP_YELLOWN_MASK | LAMP_YELLOWA_MASK | LAMP_STR1_MASK | LAMP_STR2_MASK);
	anl	_lamp_state,#0x29
	C$railsignal.c$499$1$1 ==.
	XG$set_act_call$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'signal_array_init'
;------------------------------------------------------------
;------------------------------------------------------------
	G$signal_array_init$0$0 ==.
	C$railsignal.c$501$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:501: void signal_array_init()
;	-----------------------------------------
;	 function signal_array_init
;	-----------------------------------------
_signal_array_init:
	C$railsignal.c$503$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:503: set_next[STOP] = set_next_0;
	mov	_set_next,#_set_next_0
	mov	(_set_next + 1),#(_set_next_0 >> 8)
	C$railsignal.c$504$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:504: set_next[V40] = set_next_40;
	mov	(_set_next + 0x0002),#_set_next_40
	mov	((_set_next + 0x0002) + 1),#(_set_next_40 >> 8)
	C$railsignal.c$505$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:505: set_next[V80] = set_next_80;
	mov	(_set_next + 0x0004),#_set_next_80
	mov	((_set_next + 0x0004) + 1),#(_set_next_80 >> 8)
	C$railsignal.c$506$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:506: set_next[V120] = set_next_120;
	mov	(_set_next + 0x0006),#_set_next_120
	mov	((_set_next + 0x0006) + 1),#(_set_next_120 >> 8)
	C$railsignal.c$507$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:507: set_next[VMAX] = set_next_vmax;
	mov	(_set_next + 0x0008),#_set_next_vmax
	mov	((_set_next + 0x0008) + 1),#(_set_next_vmax >> 8)
	C$railsignal.c$508$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:508: set_act[STOP] = set_act_0;
	mov	_set_act,#_set_act_0
	mov	(_set_act + 1),#(_set_act_0 >> 8)
	C$railsignal.c$509$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:509: set_act[V40] = set_act_40;
	mov	(_set_act + 0x0002),#_set_act_40
	mov	((_set_act + 0x0002) + 1),#(_set_act_40 >> 8)
	C$railsignal.c$510$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:510: set_act[V80] = set_act_80;
	mov	(_set_act + 0x0004),#_set_act_80
	mov	((_set_act + 0x0004) + 1),#(_set_act_80 >> 8)
	C$railsignal.c$511$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:511: set_act[V120] = set_act_120;
	mov	(_set_act + 0x0006),#_set_act_120
	mov	((_set_act + 0x0006) + 1),#(_set_act_120 >> 8)
	C$railsignal.c$512$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:512: set_act[VMAX] = set_act_vmax;
	mov	(_set_act + 0x0008),#_set_act_vmax
	mov	((_set_act + 0x0008) + 1),#(_set_act_vmax >> 8)
	C$railsignal.c$513$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:513: set_act[CALL] = set_act_call;
	mov	(_set_act + 0x000a),#_set_act_call
	mov	((_set_act + 0x000a) + 1),#(_set_act_call >> 8)
	C$railsignal.c$514$1$1 ==.
	XG$signal_array_init$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'uart0_send_byte'
;------------------------------------------------------------
;byte                      Allocated to registers r2 
;------------------------------------------------------------
	G$uart0_send_byte$0$0 ==.
	C$railsignal.c$516$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:516: void uart0_send_byte(tUInt8 byte)
;	-----------------------------------------
;	 function uart0_send_byte
;	-----------------------------------------
_uart0_send_byte:
	mov	r2,dpl
	C$railsignal.c$519$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:519: if(!uart0_busy)
	mov	a,_uart0_busy
	jnz	00103$
	C$railsignal.c$521$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:521: SBUF0 = byte;
	mov	_SBUF0,r2
	C$railsignal.c$522$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:522: uart0_busy = TRUE;
	mov	_uart0_busy,#0x01
00103$:
	C$railsignal.c$524$2$1 ==.
	XG$uart0_send_byte$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'uart1_send_byte'
;------------------------------------------------------------
;byte                      Allocated to registers r2 
;------------------------------------------------------------
	G$uart1_send_byte$0$0 ==.
	C$railsignal.c$526$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:526: void uart1_send_byte(tUInt8 byte)
;	-----------------------------------------
;	 function uart1_send_byte
;	-----------------------------------------
_uart1_send_byte:
	mov	r2,dpl
	C$railsignal.c$529$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:529: if(!uart1_busy)
	mov	a,_uart1_busy
	jnz	00103$
	C$railsignal.c$531$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:531: SBUF1 = byte;
	mov	_SBUF1,r2
	C$railsignal.c$532$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:532: SCON1 &= ~BS(_RB81);
	anl	_SCON1,#0xFB
	C$railsignal.c$533$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:533: uart1_busy = TRUE;
	mov	_uart1_busy,#0x01
00103$:
	C$railsignal.c$535$2$1 ==.
	XG$uart1_send_byte$0$0 ==.
	ret
;------------------------------------------------------------
;Allocation info for local variables in function 'timer0_it'
;------------------------------------------------------------
;------------------------------------------------------------
	G$timer0_it$0$0 ==.
	C$railsignal.c$542$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:542: void timer0_it() interrupt INTERRUPT_TIMER0
;	-----------------------------------------
;	 function timer0_it
;	-----------------------------------------
_timer0_it:
	push	bits
	push	acc
	push	b
	push	dpl
	push	dph
	push	(0+2)
	push	(0+3)
	push	(0+4)
	push	(0+5)
	push	(0+6)
	push	(0+7)
	push	(0+0)
	push	(0+1)
	push	psw
	mov	psw,#0x00
	C$railsignal.c$545$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:545: TH0 = 0xB8;
	mov	_TH0,#0xB8
	C$railsignal.c$546$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:546: TL0 = 0x07;
	mov	_TL0,#0x07
	C$railsignal.c$548$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:548: timer0_tick++;
	inc	_timer0_tick
	C$railsignal.c$550$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:550: if(green_flash != FLASH_NOT_ENABLED)
	mov	a,#0xFF
	cjne	a,_green_flash,00127$
	sjmp	00104$
00127$:
	C$railsignal.c$552$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:552: if(0 == (timer0_tick % green_flash))
	mov	b,_green_flash
	mov	a,_timer0_tick
	div	ab
	mov	r2,b
	cjne	r2,#0x00,00104$
	C$railsignal.c$554$3$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:554: P5 ^= LAMP_GREEN_MASK;
	xrl	_P5,#0x80
	C$railsignal.c$555$3$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:555: lamp_state ^= LAMP_GREEN_MASK;
	xrl	_lamp_state,#0x80
00104$:
	C$railsignal.c$559$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:559: if(yellow_flash != FLASH_NOT_ENABLED)
	mov	a,#0xFF
	cjne	a,_yellow_flash,00130$
	sjmp	00108$
00130$:
	C$railsignal.c$561$2$4 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:561: if(0 == (timer0_tick % yellow_flash))
	mov	b,_yellow_flash
	mov	a,_timer0_tick
	div	ab
	mov	r2,b
	cjne	r2,#0x00,00108$
	C$railsignal.c$563$3$5 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:563: P5 ^= LAMP_YELLOWN_MASK;
	xrl	_P5,#0x40
	C$railsignal.c$564$3$5 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:564: lamp_state ^= LAMP_YELLOWN_MASK;
	xrl	_lamp_state,#0x40
00108$:
	C$railsignal.c$567$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:567: if(white_flash != FLASH_NOT_ENABLED)
	mov	a,#0xFF
	cjne	a,_white_flash,00133$
	sjmp	00112$
00133$:
	C$railsignal.c$569$2$6 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:569: if(0 == (timer0_tick % white_flash))
	mov	b,_white_flash
	mov	a,_timer0_tick
	div	ab
	mov	r2,b
	cjne	r2,#0x00,00112$
	C$railsignal.c$571$3$7 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:571: lamp_state ^= LAMP_CALL_MASK;
	xrl	_lamp_state,#0x08
00112$:
	C$railsignal.c$575$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:575: if(timer0_tick % 10 == 0)
	mov	b,#0x0A
	mov	a,_timer0_tick
	div	ab
	mov	a,b
	jnz	00114$
	C$railsignal.c$577$2$8 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:577: uart1_send_byte(lamp_state);
	mov	dpl,_lamp_state
	lcall	_uart1_send_byte
00114$:
	C$railsignal.c$580$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:580: if(timer0_tick > 200)
	mov	a,_timer0_tick
	add	a,#0xff - 0xC8
	jnc	00117$
	C$railsignal.c$582$2$9 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:582: timer0_tick = 1;
	mov	_timer0_tick,#0x01
00117$:
	pop	psw
	pop	(0+1)
	pop	(0+0)
	pop	(0+7)
	pop	(0+6)
	pop	(0+5)
	pop	(0+4)
	pop	(0+3)
	pop	(0+2)
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	pop	bits
	C$railsignal.c$584$2$1 ==.
	XG$timer0_it$0$0 ==.
	reti
;------------------------------------------------------------
;Allocation info for local variables in function 'uart0_it'
;------------------------------------------------------------
;------------------------------------------------------------
	G$uart0_it$0$0 ==.
	C$railsignal.c$587$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:587: void uart0_it() interrupt INTERRUPT_UART0 
;	-----------------------------------------
;	 function uart0_it
;	-----------------------------------------
_uart0_it:
	push	acc
	push	ar2
	push	psw
	mov	psw,#0x00
	C$railsignal.c$590$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:590: if(BS(_RI0) == (SCON0 & (BS(_RI0))))
	mov	a,#0x01
	anl	a,_SCON0
	mov	r2,a
	cjne	r2,#0x01,00104$
	C$railsignal.c$592$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:592: SCON0 &= ~BS(_RI0);
	anl	_SCON0,#0xFE
	sjmp	00106$
00104$:
	C$railsignal.c$595$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:595: else if(BS(_TI0) == (SCON0 & (BS(_TI0))))
	mov	a,#0x02
	anl	a,_SCON0
	mov	r2,a
	cjne	r2,#0x02,00106$
	C$railsignal.c$597$2$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:597: uart0_busy = FALSE;
	mov	_uart0_busy,#0x00
	C$railsignal.c$598$2$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:598: SCON0 &= ~BS(_TI0);
	anl	_SCON0,#0xFD
00106$:
	pop	psw
	pop	ar2
	pop	acc
	C$railsignal.c$600$2$1 ==.
	XG$uart0_it$0$0 ==.
	reti
;	eliminated unneeded push/pop dpl
;	eliminated unneeded push/pop dph
;	eliminated unneeded push/pop b
;------------------------------------------------------------
;Allocation info for local variables in function 'uart1_it'
;------------------------------------------------------------
;------------------------------------------------------------
	G$uart1_it$0$0 ==.
	C$railsignal.c$603$2$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:603: void uart1_it() interrupt INTERRUPT_UART1
;	-----------------------------------------
;	 function uart1_it
;	-----------------------------------------
_uart1_it:
	push	bits
	push	acc
	push	b
	push	dpl
	push	dph
	push	(0+2)
	push	(0+3)
	push	(0+4)
	push	(0+5)
	push	(0+6)
	push	(0+7)
	push	(0+0)
	push	(0+1)
	push	psw
	mov	psw,#0x00
	C$railsignal.c$606$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:606: if(BS(_RI1) == (SCON1 & (BS(_RI1))))
	mov	a,#0x01
	anl	a,_SCON1
	mov	r2,a
	cjne	r2,#0x01,00104$
	C$railsignal.c$608$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:608: buffer_put(&uart1_rx_buffer,SBUF1);	
	mov	_buffer_put_PARM_2,_SBUF1
	mov	dptr,#_uart1_rx_buffer
	mov	b,#0x40
	lcall	_buffer_put
	C$railsignal.c$609$2$2 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:609: SCON1 &= ~BS(_RI1);
	anl	_SCON1,#0xFE
	sjmp	00106$
00104$:
	C$railsignal.c$612$1$1 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:612: else if(BS(_TI1) == (SCON1 & (BS(_TI1))))
	mov	a,#0x02
	anl	a,_SCON1
	mov	r2,a
	cjne	r2,#0x02,00106$
	C$railsignal.c$614$2$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:614: uart1_busy = FALSE;
	mov	_uart1_busy,#0x00
	C$railsignal.c$615$2$3 ==.
;	D:\_WORK\PROG\i8051\C\RailSignalSIM\railsignal.c:615: SCON1 &= ~BS(_TI1);
	anl	_SCON1,#0xFD
00106$:
	pop	psw
	pop	(0+1)
	pop	(0+0)
	pop	(0+7)
	pop	(0+6)
	pop	(0+5)
	pop	(0+4)
	pop	(0+3)
	pop	(0+2)
	pop	dph
	pop	dpl
	pop	b
	pop	acc
	pop	bits
	C$railsignal.c$617$2$1 ==.
	XG$uart1_it$0$0 ==.
	reti
	.area CSEG    (CODE)
	.area CONST   (CODE)
	.area XINIT   (CODE)
	.area CABS    (ABS,CODE)
