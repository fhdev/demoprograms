/********************************************************************************************************************/
/*																													*/
/* Description:	Simulation of railway signalling of Hungarian railways.												*/
/*																													*/
/* Date: 2015.04.05.																								*/
/*																													*/
/* Author: Ferenc Heged�s																							*/
/*																													*/
/********************************************************************************************************************/

#ifndef _SERIAL_COM_DEFS_H
#define _SERIAL_COM_DEFS_H

/********************************************************************************************************************/
/* Include Files																									*/
/********************************************************************************************************************/

#include "compiler_defs.h"
#include "C8051F020_defs.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

/********************************************************************************************************************/
/* Defines																											*/
/********************************************************************************************************************/
/* BUFFER SIZE FOR UART */
#define BUFFER_SIZE (20)

/* LOGICAL VALUES */
#define TRUE (1)
#define FALSE (0)

/* LED MASKS */
#define LED_1_MASK (0x10)
#define LED_2_MASK (0x20)
#define LED_3_MASK (0x40)
#define LED_4_MASK (0x80)
#define BUTTONS_PULLUP (0x0F)

/* LAMP MASKS */
#define LAMP_GREEN_MASK		(0x80)
#define LAMP_YELLOWN_MASK	(0x40)
#define LAMP_RED_MASK		(0x20)
#define LAMP_YELLOWA_MASK	(0x10)
#define LAMP_CALL_MASK		(0x08)
#define LAMP_STR1_MASK		(0x04)
#define LAMP_STR2_MASK		(0x02)

#define LAMP_GREEN			(7)
#define LAMP_YELLOWN		(6)
#define LAMP_RED			(5)
#define LAMP_YELLOWA		(4)
#define LAMP_CALL			(3)
#define LAMP_STR1			(2)
#define LAMP_STR2			(1)

/* LED FLASH TIMING */
#define FLASH_FAST_ENABLED (50)
#define FLASH_SLOW_ENABLED (100)
#define FLASH_NOT_ENABLED (255)

/* SFR BIT DEFINITIONS */ 

/* IE - Interrupt Enable Register						*/
#define _EA		(7)		/* Enable All Interrupt 		*/						
#define _IEGF0	(6) 	/* General Purpose Flag 0		*/
#define _ET2	(5)		/* Enable Timer 2 Interrupt 	*/
#define _ES0	(4)		/* Enable UART0 Interrupt		*/
#define _ET1	(3)		/* Enable Timer 1 Interrupt		*/
#define _EX1	(2)		/* Enable External Interrupt 1	*/
#define _ET0	(1)		/* Enable Timer 0 Interrupt		*/
#define _EX0	(0)		/* Ebable External Interrupt 2	*/
 
/* EIE2 - Extended Interrupt Enable Register 2 */
#define _EXVLD	(7)
#define _ES1	(6)
#define _EX7 	(5)
#define _EX6	(4)
#define _EADC1	(3)
#define _ET4	(2)
#define _EADC0	(1)
#define _ET3	(0)

/* OSXCN - External Oscillator Control Register */
#define _XTLVLD		(7)
#define _XOSCMD2	(6)
#define _XOSCMD1	(5)
#define _XOSCMD0	(4)
#define _XFCN2		(2)
#define _XFCN1		(1)
#define _XFCN0		(0)

/* OSCICN Internal Oscillator Control Register */
#define _MSCLKE	(7)
#define _IFRDY 	(4) 
#define _CLKSL	(3)
#define _IOSCEN	(2)
#define _IFCN1	(1)
#define _IFCN0	(0)

/* CKCON - Clock Control Register 				*/
#define _T4M	(6)		/* Timer 4 Clock Select */
#define _T2M	(5)		/* Timer 2 Clock Select */
#define _T1M	(4)		/* Timer 1 Clock Select */
#define _T0M	(3)		/* Timer 0 Clock Select */	

/* TMOD - Timer Mode Register */
#define _GATE1	(7)
#define _C_T1	(6)
#define _T1M1	(5)
#define _T1M0	(4)
#define _GATE0	(3)
#define _C_T0	(2)
#define	_T0M1	(1)
#define _T0M0	(0)

/* TCON - Timer 0&1 Control Register */
#define _TF1	(7)
#define _TR1	(6)
#define _TF0	(5)
#define _TR0	(4)
#define _IE1	(3)
#define _IT1	(2)
#define _IE0	(1)
#define _IT0	(0)

/* T2CON - Timer 2 Control Register */
#define _TF2	(7)
#define _EXF2	(6)
#define _RCLK0	(5)
#define _TCLK0	(4)
#define _EXEN2	(3)
#define _TR2	(2)
#define _C_T2	(1)
#define _CP_RL2	(0)

/* T4CON - Timer 4 Control Register */
#define _TF4	(7)
#define _EXF4	(6)
#define _RCLK1	(5)
#define _TCLK1	(4)
#define _EXEN4	(3)
#define _TR4	(2)
#define _C_T4	(1)
#define _CP_RL4	(0)

/* P74OUT Ports 7-4 Output Mode Register 						*/
#define _P7H	(7)		/* Port 7 Output Mode High Nibble Bit 	*/
#define _P7L	(6)		/* Port 7 Output Mode Low Nibble Bit 	*/
#define _P6H	(5)		/* Port 6 Output Mode High Nibble Bit 	*/
#define _P6L	(4)		/* Port 6 Output Mode Low Nibble Bit 	*/
#define _P5H	(3)		/* Port 5 Output Mode High Nibble Bit 	*/
#define _P5L	(2)		/* Port 5 Output Mode Low Nibble Bit 	*/
#define _P4H	(1)		/* Port 4 Output Mode High Nibble Bit 	*/
#define _P4L	(0)		/* Port 4 Output Mode Low Nibble Bit 	*/

/* PCON - Power Control Register */
#define _SMOD0	(7)
#define _SSTAT0	(6)
#define _SMOD1	(4)
#define _SSTAT1	(3)
#define _STOP	(1)
#define _IDLE	(0)

/* SCON0 - UART0 Control Register */
#define _SM00	(7)
#define _FE0	(7)
#define _SM10	(6)
#define _RXOV0	(6)
#define _SM20	(5)
#define _TXCOL0	(5)
#define _REN0	(4)
#define _TB80	(3)
#define _RB80	(2)
#define _TI0	(1)
#define _RI0	(0)

/* SCON1 - UART1 Control Register */
#define _SM01	(7)
#define _FE1	(7)
#define _SM11	(6)
#define _RXOV1	(6)
#define _SM21	(5)
#define _TXCOL1	(5)
#define _REN1	(4)
#define _TB81	(3)
#define _RB81	(2)
#define _TI1	(1)
#define _RI1	(0)

/* XBR0 - Port I/O Crossbar Register 0 */
#define _CP0E		(7)
#define _ECI0E		(6)
#define _PCA0ME_1	(5)
#define _PCA0ME_2	(4)
#define _PCA0ME_3	(3)
#define _UART0EN	(2)
#define _SPI0EN		(1)
#define _SMB0EN		(0)

/* XBR2 - Port I/O Crossbar Register 2 */
#define _WEAKPUD	(7)
#define _XBARE		(6)
#define _T4EXE		(4)
#define	_T4E		(3)
#define _UART1EN	(2)
#define _EMIFLE		(1)
#define _CNVSTE		(0)

/* MACROS */

/* BIT SET / CLEAR */
#define SETBIT(x) ((x)=(1))
#define CLRBIT(x) ((x)=(0))

/* BIT SHIFT */
#define BS(x) ((1)<<(x))

/* GET LOW 4 BIT */
#define GET_LOW_4(x) ((x) & (0x0F))
/* GET HIGH 4 BIT */
#define GET_HIGH_4(x) ((x)>>(4))

/********************************************************************************************************************/
/* Types																											*/
/********************************************************************************************************************/

/* Basic types */
typedef unsigned char tUInt8;
typedef unsigned char tBool;
typedef unsigned int tUInt16;
typedef unsigned long tUInt32;
typedef signed char tInt8;
typedef signed int tInt16;
typedef signed long tInt32;


/* FIFO buffer structure */
typedef struct stBufferUInt8
{
	tUInt8 *dat;
	tUInt8 size;
	tUInt8 stored;
	tUInt8 index_last;
	tUInt8 index_first;
	tUInt8 overflow;
} tBufferUInt8;

/* Fuction pointer type for signal array */
typedef void activity();
typedef activity* todo;

/********************************************************************************************************************/
/* Constants																										*/
/********************************************************************************************************************/

enum light_states{STOP = 0, V40 = 1, V80 = 2, V120 = 3, VMAX = 4, CALL = 5};

/********************************************************************************************************************/
/* External Variable Declarations																					*/
/********************************************************************************************************************/


/********************************************************************************************************************/
/* Global Function Declarations																						*/
/********************************************************************************************************************/

/* Initialization of pheripherals */
void clock_init();
void watchdog_disable();
void interrupt_disable_all();
void interrupt_enable_all();
void timer_init();
void port_init();
void crossbar_init();
void uart0_init();
void uart1_init();

/* FIFO Buffer handling */
void buffer_init(tBufferUInt8 *buffer, tUInt8 buffer_size);
void buffer_put(tBufferUInt8 *buffer, tUInt8 value); 
tBool buffer_get(tBufferUInt8 *buffer, tUInt8 *value);

/* UART0 send byte */
void uart0_send_byte(tUInt8 byte);
/* UART1 send byte */
void uart1_send_byte(tUInt8 byte);

/* Signal settings */
void set_act_0();
void set_act_40();
void set_act_80();
void set_act_120();
void set_act_vmax();
void set_next_0();
void set_next_40();
void set_next_80();
void set_next_120();
void set_next_vmax();
void set_act_call();

/* Signal set array initialization */
void signal_array_init();

#endif