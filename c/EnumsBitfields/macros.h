#ifndef MACROS_INCLUDED
#define MACROS_INCLUDED

#include <stdint.h>

#define MACRO_NUMBER_OF_OBJECTS (100u)

// these macros are defined before the type t_ObjectCategory, so presumably they are the valid categories
// but this is not unambiguous
#define MACRO_CATEGORY_1 (1u)
#define MACRO_CATEGORY_2 (2u)
#define MACRO_CATEGORY_3 (4u)
#define MACRO_CATEGORY_4 (8u)
#define MACRO_CATEGORY_5 (16u)
#define MACRO_CATEGORY_6 (32u)
#define MACRO_CATEGORY_7 (64u)
#define MACRO_CATEGORY_8 (128u)

typedef struct {
    // object category is of type uint16_t: can not tell how to interpret the values and what the valid values are
    uint16_t a_macroObjectCategory[MACRO_NUMBER_OF_OBJECTS];
} t_MacroObjectCategory;

#endif // MACROS_INCLUDED