#ifndef BITFIELD_INCLUDED
#define BITFIELD_INCLUDED

#include <stdint.h>

#define BITFIELD_NUMBER_OF_OBJECTS (100u)

// a bitfield type is defined so it is unambiguous what the valid values can be
typedef struct {
    unsigned int category1 : 1;
    unsigned int category2 : 1;
    unsigned int category3 : 1;
    unsigned int category4 : 1;
    unsigned int category5 : 1;
    unsigned int category6 : 1;
    unsigned int category7 : 1;
    unsigned int category8 : 1;

} t_BitfieldCategory;

typedef struct {
    // object category is of type t_BitfieldCategory: can tell how to interpret the values and what the valid values are
    t_BitfieldCategory a_bitfieldObjectCategory[BITFIELD_NUMBER_OF_OBJECTS];
} t_BitfieldObjectCategory;

#endif // BITFIELD_INCLUDED