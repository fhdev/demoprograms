#ifndef TYPEDEF_INCLUDED
#define TYPEDEF_INCLUDED

#include <stdint.h>

#define TYPEDEF_NUMBER_OF_OBJECTS (100u)

#define TYPEDEF_SELECT_CATEGORY (1u)

// a type is defined so that it is unambiguous how to interpret categories
typedef uint16_t t_TypedefCategory;

#define TYPEDEF_CATEGORY_1 ((t_TypedefCategory)(1u))
#define TYPEDEF_CATEGORY_2 ((t_TypedefCategory)(2u))
#define TYPEDEF_CATEGORY_3 ((t_TypedefCategory)(4u))
#define TYPEDEF_CATEGORY_4 ((t_TypedefCategory)(8u))
#define TYPEDEF_CATEGORY_5 ((t_TypedefCategory)(16u))
#define TYPEDEF_CATEGORY_6 ((t_TypedefCategory)(32u))
#define TYPEDEF_CATEGORY_7 ((t_TypedefCategory)(64u))
#define TYPEDEF_CATEGORY_8 ((t_TypedefCategory)(128u))

typedef struct {
    // object category is of type t_TypedefCategory: can tell how to interpret the values and what the valid values are
    t_TypedefCategory a_typedefObjectCategory[TYPEDEF_NUMBER_OF_OBJECTS];
} t_TypedefObjectCategory;

#endif // TYPEDEF_INCLUDED