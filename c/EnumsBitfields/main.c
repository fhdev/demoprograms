#include <stdio.h>
#include <string.h>
#include "macros.h"
#include "typedef.h"
#include "bitfield.h"
#include "enum.h"

#define INVALID_CATEGORY (99u)
#define INVALID_BITFIELD_VALUE (2u)

int main(void) {
    // Macros and uint: it is not clear what the valid object categories are
    {
        printf("Macros ... \n");

        t_MacroObjectCategory objectCategory;
        memset(&objectCategory, 0, sizeof(objectCategory));
        printf("sizeof(objectCategory)=%d\n", (int)sizeof(objectCategory));

        // this is a valid category assignment
        objectCategory.a_macroObjectCategory[0] = MACRO_CATEGORY_1;
        // this is not a valid category assignment
        objectCategory.a_macroObjectCategory[1] = INVALID_CATEGORY;
    }
    // Typedef and macros and uint: it is already clear what the valid object categories are, but we receive no 
    // warning if an invalid value is assigned
    {
        printf("Typedef and macros ... \n");

        t_TypedefObjectCategory objectCategory;
        memset(&objectCategory, 0, sizeof(objectCategory));
        printf("sizeof(objectCategory)=%d\n", (int)sizeof(objectCategory));

        // this is a valid category assignment
        objectCategory.a_typedefObjectCategory[0] = TYPEDEF_CATEGORY_1;
        // this is not a valid category assignment
        objectCategory.a_typedefObjectCategory[1] = INVALID_CATEGORY;
    }
    // Bitfield: it is clear what the valid object categories are, and we receive a warning if an invalid value is 
    // assigned
    {
        printf("Bitfield ... \n");

        t_BitfieldObjectCategory objectCategory;
        memset(&objectCategory, 0, sizeof(objectCategory));
        printf("sizeof(objectCategory)=%d\n", (int)sizeof(objectCategory));

        // this is a valid category assignment
        objectCategory.a_bitfieldObjectCategory[0].category1 = TYPEDEF_SELECT_CATEGORY;
        // this is not a valid category assignment
        // QAC: Constant: Positive integer value truncated by implicit conversion to a smaller unsigned type.
        objectCategory.a_bitfieldObjectCategory[1].category2 = INVALID_BITFIELD_VALUE;
        
    }
    // Enum: it is clear what the valid object categories are, and we receive a warning if an invalid value is assigned
    {
        printf("Enum ... \n");

        t_EnumObjectCategory objectCategory;
        memset(&objectCategory, 0, sizeof(objectCategory));
        printf("sizeof(objectCategory)=%d\n", (int)sizeof(objectCategory));
        // this is a valid category assignment
        objectCategory.a_enumObjectCategory[0] = ENUM_CATEGORY_1;
        // this is not a valid category assignment -> QAC issues a warning because of 
        // QAC: An integer constant of 'essentially unsigned' type is being converted to enum type on assignment.
        objectCategory.a_enumObjectCategory[1] = INVALID_CATEGORY;

    }

    return 0;
}
