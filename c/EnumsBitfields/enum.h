#ifndef ENUM_INCLUDED
#define ENUM_INCLUDED

#define ENUM_NUMBER_OF_OBJECTS (100u)

// an enumerable type is defined so it is unambiguous what the valid values can be
typedef enum {
    ENUM_CATEGORY_1 = 1u,
    ENUM_CATEGORY_2 = 2u,
    ENUM_CATEGORY_3 = 4u,
    ENUM_CATEGORY_4 = 8u,
    ENUM_CATEGORY_5 = 16u,
    ENUM_CATEGORY_6 = 32u,
    ENUM_CATEGORY_7 = 64u,
    ENUM_CATEGORY_8 = 128u
} t_EnumCategory;

typedef struct {
    // object category is of type t_EnumCategory: can tell how to interpret the values and what the valid values are
    t_EnumCategory a_enumObjectCategory[ENUM_NUMBER_OF_OBJECTS];
} t_EnumObjectCategory;

#endif // ENUM_INCLUDED