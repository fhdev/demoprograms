﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Renamer
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Renamer.PrintUsage();
                return;
            }
            try
            {
                Renamer renamer = new Renamer(args);
                renamer.DoJob();
            }
            catch(ArgumentException argEx)
            {
                Console.WriteLine();
                Renamer.PrintErrorMessage(argEx.Message);
                Renamer.PrintUsage();
            }
            catch(FileNotFoundException fnfEx)
            {
                Console.WriteLine();
                Renamer.PrintErrorMessage(fnfEx.Message);
                Renamer.PrintUsage();
            }
        }        
    }
}
