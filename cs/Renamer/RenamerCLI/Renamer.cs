﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace Renamer
{
    enum RenamerOptions { PrintListOnly = 0x01,
                          KeepBackupFiles = 0x02,
                          MatchCaseSensitive = 0x04,
                          MatchWholeWord = 0x08}
    class Renamer
    {
        public string SrcPath { get; set; }
        public string QueryString { get; set; }
        public string SubstituteString { get; set; }
        public RenamerOptions Options { get; set; }
        public bool PrintListOnly
        {
            get
            {
                return (Options & RenamerOptions.PrintListOnly) != 0;
            }
        }

        public bool KeepBackupFiles
        {
            get
            {
                return (Options & RenamerOptions.KeepBackupFiles) != 0;
            }
        }
        public bool MatchCaseSensitive
        {
            get
            {
                return (Options & RenamerOptions.MatchCaseSensitive) != 0;
            }
        }
        public bool MatchWholeWord
        {
            get
            {
                return (Options & RenamerOptions.MatchWholeWord) != 0;
            }
        }


        public static void PrintErrorMessage(string message)
        {
            ConsoleColor color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Error.WriteLine("ERROR: " + message);
            Console.ForegroundColor = color;

        }
        public static void PrintInfoMessage(string message)
        {
            ConsoleColor color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message);
            Console.ForegroundColor = color;

        }
        public static List<string> GetAllFiles(string dir)
        {
            List<string> files = new List<string>();
            if (!Directory.Exists(dir))
                return files;
            foreach (string subdir in Directory.GetDirectories(dir))
                files.AddRange(GetAllFiles(subdir));
            files.AddRange(Directory.GetFiles(dir));
            files.Sort();
            return files;
        }

        public static void PrintUsage()
        {
            Console.WriteLine();
            Console.WriteLine("================ Usage ================");
            Console.WriteLine("Renamer.exe");
            Console.WriteLine("[-P <path to file or directory>]");
            Console.WriteLine("[-Q <query string>]");
            Console.WriteLine("[-S <substitute string>]");
            Console.WriteLine("[-l]");
            Console.WriteLine("[-k]");
            Console.WriteLine("[-c]");
            Console.WriteLine("[-w]");
            Console.WriteLine("-l: list files only");
            Console.WriteLine("-k: keep backup files");
            Console.WriteLine("-c: case sensitive match");
            Console.WriteLine("-w: match whole word");
            Console.WriteLine();
        }

        private bool containsQueryString(string line)
        {
            string query = QueryString;
            if (MatchWholeWord)
                query = @"\b" + query + @"\b";
            RegexOptions options = RegexOptions.Singleline;
            if (!MatchCaseSensitive)
                options |= RegexOptions.IgnoreCase;
            return Regex.Match(line, query, options).Success;

        }

        private string replaceQueryString(string line)
        {
            string query = QueryString;
            string substitute = SubstituteString;
            if (MatchWholeWord)
                query = @"\b" + query + @"\b";
            RegexOptions options = RegexOptions.Singleline;
            if (!MatchCaseSensitive)
                options |= RegexOptions.IgnoreCase;
            return Regex.Replace(line, query, substitute, options);
        }
        public Renamer(string[] args)
        {
            for (int iArg = 0; iArg < args.Length; iArg++)
            {
                switch (args[iArg])
                {
                    case "-P":
                    {
                        if (((iArg + 1) < args.Length) && (args[iArg + 1][0] != '-'))
                            SrcPath = args[iArg + 1];
                        else
                            throw new ArgumentException("No string was provided following the -P switch.");
                        break;
                    }
                    case "-Q":
                    {
                        if (((iArg + 1) < args.Length) && (args[iArg + 1][0] != '-'))
                            QueryString = args[iArg + 1];
                        else
                            throw new ArgumentException("No string was provided following the -Q switch.");
                        break;
                    }
                    case "-S":
                    {
                        if (((iArg + 1) < args.Length) && (args[iArg + 1][0] != '-'))
                            SubstituteString = args[iArg + 1];
                        else
                            throw new ArgumentException("No string was provided following the -S switch.");
                        break;
                    }
                    case "-l":
                    {
                        Options |= RenamerOptions.PrintListOnly;
                        break;
                    }
                    case "-c":
                    {
                        Options |= RenamerOptions.MatchCaseSensitive;
                        break;
                    }
                    case "-w":
                    {
                        Options |= RenamerOptions.MatchWholeWord;
                        break;
                    }
                    case "-k":
                    {
                        Options |= RenamerOptions.KeepBackupFiles;
                        break;
                    }
                    default:
                    {
                        if (args[iArg][0] == '-')
                            throw new ArgumentException("Invalid switch " + args[iArg] + " was provided.");
                        break;
                    }
                }
            }
            if (!(Directory.Exists(SrcPath) || File.Exists(SrcPath)))
                throw new FileNotFoundException("Specified path " + SrcPath + " can not be found.");
            if (QueryString == null)
                throw new ArgumentException("No query string specified with the -Q option.");
            if (!PrintListOnly && (SubstituteString == null))
                throw new ArgumentException("No substitute string specified with the -S option.");
        }

        public List<string> GetFilesThatContainQueryString(bool printToConsole = false)
        {
            List<string> files = GetAllFiles(SrcPath);
            List<string> filteredFiles = new List<string>();
            foreach (string file in files)
            {
                try
                {
                    using (StreamReader reader = new StreamReader(file))
                    { 
                        while (!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (containsQueryString(line))
                            {
                                filteredFiles.Add(file);
                                if (printToConsole)
                                    PrintInfoMessage(file);
                                break;
                            }
                        }
                    }
                }
                catch (IOException ioEx)
                {
                    PrintErrorMessage("File operation failed. " + ioEx.Message);
                }
            }
            return filteredFiles;
        }

        public void ReplaceInFiles(bool printToConsole = false)
        {
            List<string> files = GetFilesThatContainQueryString();
            foreach (string file in files)
            {
                try
                {
                    string backupFile = Path.ChangeExtension(file, ".original.bak");
                    File.Move(file, backupFile);
                    using (StreamReader reader = new StreamReader(backupFile))
                    using (StreamWriter writer = new StreamWriter(file))
                    {
                        int lineNo = 1;
                        while(!reader.EndOfStream)
                        {
                            string line = reader.ReadLine();
                            if (containsQueryString(line))
                            {
                                line = replaceQueryString(line);
                                if (printToConsole)
                                    PrintInfoMessage(file + ": Line " + lineNo);
                            }
                            writer.WriteLine(line);
                            lineNo++;
                        }
                    }
                    if(!KeepBackupFiles)
                        File.Delete(backupFile);
                }
                catch (IOException ioEx)
                {
                    PrintErrorMessage("File operation failed. " + ioEx.Message);
                }
            }
        }

        public void DoJob()
        {
            if (PrintListOnly)
                GetFilesThatContainQueryString(true);
            else
                ReplaceInFiles(true);
        }

    }
}
