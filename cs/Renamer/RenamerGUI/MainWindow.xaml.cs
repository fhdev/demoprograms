﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace RenamerGUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Process process;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() => { textBoxStdOut.Text = string.Empty; }));
            Dispatcher.BeginInvoke((Action)(() => { textBoxErrOut.Text = string.Empty; }));
            Dispatcher.BeginInvoke((Action)(() => { buttonStart.IsEnabled = false; }));

            process = new Process();         
            process.StartInfo.FileName = "RenamerCLI.exe";
            process.StartInfo.Arguments += " -P " + textBoxPath.Text;
            process.StartInfo.Arguments += " -Q " + textBoxQuery.Text;
            process.StartInfo.Arguments += " -S " + textBoxSubstitute.Text;
            if (checkBoxOnlyList.IsChecked ?? false)
                process.StartInfo.Arguments += " -l";
            if (checkBoxKeepBackupFiles.IsChecked ?? false)
                process.StartInfo.Arguments += " -k";
            if (checkBoxMatchCaseSensitive.IsChecked ?? false)
                process.StartInfo.Arguments += " -c";
            if (checkBoxMatchWholeWorld.IsChecked ?? false)
                process.StartInfo.Arguments += " -w";
            process.StartInfo.StandardOutputEncoding = Encoding.GetEncoding(850);
            process.StartInfo.StandardErrorEncoding = Encoding.GetEncoding(850);
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;

            process.EnableRaisingEvents = true;
            process.OutputDataReceived += new DataReceivedEventHandler(outputDataReceived);
            process.ErrorDataReceived += new DataReceivedEventHandler(errorDataReceived);
            process.Exited += new EventHandler(processExited);

            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
        }

        private void outputDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.DataBind, (Action)(() => { textBoxStdOut.Text += e.Data + Environment.NewLine; }));
        }

        private void errorDataReceived(object sender, DataReceivedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Data))
                Dispatcher.BeginInvoke((Action)(() => { textBoxErrOut.Text += e.Data + Environment.NewLine; }));
        }

        private void processExited(object sender, EventArgs e)
        {
            Dispatcher.BeginInvoke((Action)(() => { buttonStart.IsEnabled = true; }));
            process.Close();
            process.Dispose();
        }

        private void textBoxGotFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox)?.SelectAll();
        }

        private void textBoxGotFocus(object sender, MouseButtonEventArgs e)
        {
            (sender as TextBox)?.SelectAll();
        }

        private void buttonPath_Click(object sender, RoutedEventArgs e)
        {
            using (var dialog = new System.Windows.Forms.FolderBrowserDialog())
            {
                dialog.SelectedPath = Directory.GetCurrentDirectory();
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                    Dispatcher.BeginInvoke((Action)(() => { textBoxPath.Text = dialog.SelectedPath; }));

            }
        }
    }
}
