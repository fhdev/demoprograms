﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;

namespace CalculatorPlus
{
    class Parser
    {
        private static List<Token> Tokenize(string expression)
        {
            List<Token> tokenList = new List<Token>();
            Token lastToken;
            while (expression.Length > 0)
            {
                // Trim whitespaces.
                expression = expression.Trim();
                // Search for new token.
                Token newToken;
                // Get last token Opertor or Operand
                lastToken = tokenList.LastOrDefault(t => ((t is Operator) || (t is Operand)));
                // Try to get operand.
                if (!Operand.TryGet(expression, lastToken, out newToken) &&     
                   // Try to get parentheses.
                   !RightParentesis.TryGet(expression, out newToken) &&
                   !LeftParenthesis.TryGet(expression, out newToken) &&
                   // Try to get operator.
                   !Operator.TryGet(expression,
                                    lastToken,
                                    out newToken))
                {
                    throw new ArgumentException("Expression contains unexpected character \"" + 
                        expression.FirstOrDefault() + "\".", "expression");
                }
                // Add new token if found.
                tokenList.Add(newToken);
                // Cut found token from the begining of expression.
                expression = expression.Substring(newToken.Symbol.Length);
            }
            // Check if there are operators at the end of the expression
            lastToken = tokenList.LastOrDefault();
            if(lastToken is Operator)
            {
                throw new ArgumentException("Expression contains unexpected operator \"" +
                        lastToken.Symbol + "\".", "expression");
            }
            int balance = 0;
            foreach(Token token in tokenList)
            {
                if(token is RightParentesis)
                {
                    balance++;
                }
                else if(token is LeftParenthesis)
                {
                    balance--;
                }
            }
            if(balance != 0)
            {
                throw new ArgumentException("Expression contains unbalanced parentheses.");
            }
            return tokenList;
        }

        private static List<Token> OperatorPrecedenceParser(List<Token> infix)
        {
            // Token list with reverse polish notation.
            List<Token> rpn = new List<Token>();
            // Stack for operators and parentheses.
            Stack<Token> stack = new Stack<Token>();
            // Exception for unbalanced parenthesis.
            ArgumentException unbalancedException = 
                new ArgumentException("Infix token list contains unbalanced parenthesis.", "infix");
            // While there are tokens in token list with infix notation.
            foreach (Token token in infix)
            {
                // If token is operand, add it to the output list.
                if(token is Operand)
                {
                    rpn.Add(token);
                }
                // If token is an operator, then:
                else if(token is Operator)
                {
                    // While there is a prevoius operator in the stack with greater than or equal to
                    // precedence and the current operator is left associative:
                    while(stack.Count > 0)
                    {
                        Token previous = stack.Peek();
                        if ((previous is Operator) && 
                            (previous as Operator).Precedence >= (token as Operator).Precedence &&
                            (token as Operator).Associativity == Operator.Associativities.LeftToRight)
                        {
                            // pop previous operators from stack to the output list.
                            stack.Pop();
                            rpn.Add(previous);
                        }
                        else
                        {
                            break;
                        }
                    }
                    // Push the current operator to the stack.
                    stack.Push(token);
                }
                // If token is a left parenthesis, push it to the stack.
                else if(token is LeftParenthesis)
                {
                    stack.Push(token);
                }
                // If token is a right parenthesis, then:
                else if(token is RightParentesis)
                {
                    bool leftFound = false;
                    // While there is an operator on the stack, pop it from stack to the output
                    // list, until a left parenthesis is found. Then pop left parenthesis.
                    while(stack.Count > 0)
                    {
                        Token previous = stack.Pop();
                        if(!(previous is LeftParenthesis))
                        {
                            rpn.Add(previous);
                        }
                        else
                        {
                            leftFound = true;
                            break;
                        }
                    }
                    // If stack runs out without finfing a left paranethesis, there are unbalanced
                    // parenthesis in infix list.
                    if(!leftFound)
                    {
                        throw unbalancedException;
                    }
                }
            }
            // While there are still operators on the stack, pop them from stack to the output list.
            while(stack.Count > 0)
            {
                Token previous = stack.Pop();
                if(previous is Operator)
                {
                    rpn.Add(previous);
                }
                else
                {
                    throw unbalancedException;   
                }
            }
            return rpn;
        }

        private static double RpnEvaluator(List<Token> rpn)
        {
            // Stack for operands and intermediate results.
            Stack<double> stack = new Stack<double>();
            // While there are tokens still:
            foreach(Token token in rpn)
            {
                // If token is operand, push it to the operand stack.
                if(token is Operand)
                {
                    stack.Push((token as Operand).Value);
                }
                else if(token is Operator)
                {
                    double left = 0;
                    double right = 0;
                    double result = 0;
                    if(((token as Operator).Type == Operator.Types.Unary) && (stack.Count >= 1))
                    {
                        right = stack.Pop();
                    }
                    else if(((token as Operator).Type == Operator.Types.Binary) && (stack.Count >= 2))
                    {
                        right = stack.Pop();
                        left = stack.Pop();
                    }
                    else
                    {
                        throw new ArgumentException("PRN token list contains too many operators.", 
                            "rpn");
                    }
                    result = (token as Operator).EvaluationFunction(left, right);
                    stack.Push(result);
                }
                else
                {
                    throw new ArgumentException("RPN token list contains invalid token type.");
                }
            }
            if(stack.Count == 0)
            {
                throw new ArgumentException("RPN token list is empty.");
            }
            else if(stack.Count > 1)
            {
                throw new ArgumentException("RPN token list contains too few operators.");
            }
            return stack.Pop();
        }

        public static double EvaluateExpression(string infix)
        {
            List<Token> infixList = Tokenize(infix);
            List<Token> postfixList = OperatorPrecedenceParser(infixList);
            double result = RpnEvaluator(postfixList);
            return result;

        }

        public static void EvaluateDlmTextFile(string inFileName, string outFileName, char delimiter)
        {
            if(File.Exists(inFileName))
            {
                string evalContent = string.Empty;
                string[] lines = File.ReadAllLines(inFileName);
                foreach(string line in lines)
                {
                    string[] sections = line.Split(delimiter);
                    string[] evaluations = new string[sections.Length];
                    for(int i = 0; i < sections.Length; i++)
                    {
                        try
                        {
                            double result = EvaluateExpression(sections[i]);
                            evaluations[i] = result.ToString(CultureInfo.InvariantCulture);
                        }
                        catch
                        {
                            evaluations[i] = sections[i];
                        }   
                    }
                    evalContent += string.Join(delimiter.ToString(), evaluations) + Environment.NewLine;
                }
                StreamWriter outFile = new StreamWriter(outFileName);
                outFile.Write(evalContent);
                outFile.Close();
                outFile.Dispose();
            }
            else
            {
                throw new ArgumentException("Dlm input file \"" + inFileName + "\" does not exist.");
            }
        }
    }
}
