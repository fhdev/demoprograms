﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Text.RegularExpressions;

namespace CalculatorPlus
{
    class Operand : Token
    {
        // Regular expression that matches a floating point number without sign (sign is a unary
        // operator)
        private static readonly string atStartRegex = "^[0-9]+([.,]?[0-9]+)?([eE][-+]?[0-9]+)?";
        public Operand(string symbol) : base(symbol)
        {
            
        }
        public double Value
        {
            get
            {
                double value;
                ParseValue(Symbol, out value);
                return value;
            }
        }

        public static bool ParseValue(string text, out double value)
        {
            return double.TryParse(text.Replace(',', '.'), 
                                   NumberStyles.Any,
                                   CultureInfo.InvariantCulture,
                                   out value);
        }
        public static bool TryGet(string expression, Token previous, out Token foundOperand)
        {
            Match operandMatch = Regex.Match(expression, atStartRegex);
            if(operandMatch.Success)
            {
                if(previous is Operand)
                {
                    throw new ArgumentException("Expression contains unexpected operand \"" +
                        operandMatch.Value + "\".");
                }
                else
                {
                    foundOperand = new Operand(operandMatch.Value);
                }
                
            }
            else
            {
                foundOperand = null;
            }
            return operandMatch.Success;
        }
    }
}
