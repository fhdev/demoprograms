﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorPlus
{
    class LeftParenthesis : Token
    {
        public LeftParenthesis()
            : base("(")
        {

        }
        public static bool TryGet(string expression, out Token foundToken)
        {
            bool found = false;
            foundToken = null;
            if (expression[0] == '(')
            {
                foundToken = new LeftParenthesis();
                found = true;
            }
            return found;
        }
    }
    class RightParentesis : Token
    {
        public RightParentesis()
            : base(")")
        {

        }

        public static bool TryGet(string expression, out Token foundToken)
        {
            bool found = false;
            foundToken = null;
            if (expression[0] == ')')
            {
                foundToken = new RightParentesis();
                found = true;
            }
            return found;
        }
    }
}
