﻿namespace CalculatorPlus
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openInputFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openOutputFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.buttonProcessFile = new System.Windows.Forms.Button();
            this.labelOutFile = new System.Windows.Forms.Label();
            this.labelInFile = new System.Windows.Forms.Label();
            this.textBoxOutFileName = new System.Windows.Forms.TextBox();
            this.formMainBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBoxInFileName = new System.Windows.Forms.TextBox();
            this.labelResult = new System.Windows.Forms.Label();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.textBoxExpression = new System.Windows.Forms.TextBox();
            this.buttonEvaluate = new System.Windows.Forms.Button();
            this.labelExpression = new System.Windows.Forms.Label();
            this.openFileDialogIn = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogOut = new System.Windows.Forms.SaveFileDialog();
            this.menuStripMain.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.formMainBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.BackColor = System.Drawing.SystemColors.ControlLight;
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStripMain.Size = new System.Drawing.Size(384, 24);
            this.menuStripMain.TabIndex = 0;
            this.menuStripMain.Text = "menuStripMain";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openInputFileToolStripMenuItem,
            this.openOutputFileToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openInputFileToolStripMenuItem
            // 
            this.openInputFileToolStripMenuItem.Name = "openInputFileToolStripMenuItem";
            this.openInputFileToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.openInputFileToolStripMenuItem.Text = "Open input file";
            this.openInputFileToolStripMenuItem.Click += new System.EventHandler(this.openInputFileToolStripMenuItem_Click);
            // 
            // openOutputFileToolStripMenuItem
            // 
            this.openOutputFileToolStripMenuItem.Name = "openOutputFileToolStripMenuItem";
            this.openOutputFileToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.openOutputFileToolStripMenuItem.Text = "Open output file";
            this.openOutputFileToolStripMenuItem.Click += new System.EventHandler(this.openOutputFileToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.buttonProcessFile, 1, 7);
            this.tableLayoutPanelMain.Controls.Add(this.labelOutFile, 0, 8);
            this.tableLayoutPanelMain.Controls.Add(this.labelInFile, 0, 6);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxOutFileName, 0, 9);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxInFileName, 0, 7);
            this.tableLayoutPanelMain.Controls.Add(this.labelResult, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxResult, 0, 4);
            this.tableLayoutPanelMain.Controls.Add(this.textBoxExpression, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.buttonEvaluate, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.labelExpression, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 24);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 11;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(384, 237);
            this.tableLayoutPanelMain.TabIndex = 1;
            // 
            // buttonProcessFile
            // 
            this.buttonProcessFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonProcessFile.Location = new System.Drawing.Point(287, 143);
            this.buttonProcessFile.Name = "buttonProcessFile";
            this.buttonProcessFile.Size = new System.Drawing.Size(94, 24);
            this.buttonProcessFile.TabIndex = 9;
            this.buttonProcessFile.Text = "Process file";
            this.buttonProcessFile.UseVisualStyleBackColor = true;
            this.buttonProcessFile.Click += new System.EventHandler(this.buttonProcessFile_Click);
            // 
            // labelOutFile
            // 
            this.labelOutFile.AutoSize = true;
            this.labelOutFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelOutFile.Location = new System.Drawing.Point(1, 171);
            this.labelOutFile.Margin = new System.Windows.Forms.Padding(1);
            this.labelOutFile.Name = "labelOutFile";
            this.labelOutFile.Size = new System.Drawing.Size(282, 18);
            this.labelOutFile.TabIndex = 8;
            this.labelOutFile.Text = "Output file";
            this.labelOutFile.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labelInFile
            // 
            this.labelInFile.AutoSize = true;
            this.labelInFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInFile.Location = new System.Drawing.Point(1, 121);
            this.labelInFile.Margin = new System.Windows.Forms.Padding(1);
            this.labelInFile.Name = "labelInFile";
            this.labelInFile.Size = new System.Drawing.Size(282, 18);
            this.labelInFile.TabIndex = 7;
            this.labelInFile.Text = "Input file";
            this.labelInFile.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // textBoxOutFileName
            // 
            this.textBoxOutFileName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.formMainBindingSource, "OutFileName", true));
            this.textBoxOutFileName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxOutFileName.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxOutFileName.Location = new System.Drawing.Point(3, 193);
            this.textBoxOutFileName.Name = "textBoxOutFileName";
            this.textBoxOutFileName.ReadOnly = true;
            this.textBoxOutFileName.Size = new System.Drawing.Size(278, 23);
            this.textBoxOutFileName.TabIndex = 6;
            this.textBoxOutFileName.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.textBoxOutFileName_MouseDoubleClick);
            // 
            // formMainBindingSource
            // 
            this.formMainBindingSource.DataSource = this;
            this.formMainBindingSource.Position = 0;
            // 
            // textBoxInFileName
            // 
            this.textBoxInFileName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.formMainBindingSource, "InFileName", true));
            this.textBoxInFileName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxInFileName.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxInFileName.Location = new System.Drawing.Point(3, 143);
            this.textBoxInFileName.Name = "textBoxInFileName";
            this.textBoxInFileName.ReadOnly = true;
            this.textBoxInFileName.Size = new System.Drawing.Size(278, 23);
            this.textBoxInFileName.TabIndex = 5;
            this.textBoxInFileName.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.textBoxInFileName_MouseDoubleClick);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelResult.Location = new System.Drawing.Point(1, 61);
            this.labelResult.Margin = new System.Windows.Forms.Padding(1);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(282, 18);
            this.labelResult.TabIndex = 4;
            this.labelResult.Text = "Result";
            this.labelResult.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // textBoxResult
            // 
            this.textBoxResult.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.formMainBindingSource, "Result", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxResult.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxResult.Location = new System.Drawing.Point(3, 83);
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.Size = new System.Drawing.Size(278, 23);
            this.textBoxResult.TabIndex = 2;
            this.textBoxResult.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBoxExpression
            // 
            this.textBoxExpression.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.formMainBindingSource, "Expression", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.textBoxExpression.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxExpression.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBoxExpression.Location = new System.Drawing.Point(3, 33);
            this.textBoxExpression.Name = "textBoxExpression";
            this.textBoxExpression.Size = new System.Drawing.Size(278, 23);
            this.textBoxExpression.TabIndex = 0;
            this.textBoxExpression.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxExpression_KeyDown);
            // 
            // buttonEvaluate
            // 
            this.buttonEvaluate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonEvaluate.Location = new System.Drawing.Point(287, 33);
            this.buttonEvaluate.Name = "buttonEvaluate";
            this.buttonEvaluate.Size = new System.Drawing.Size(94, 24);
            this.buttonEvaluate.TabIndex = 1;
            this.buttonEvaluate.Text = "Evaluate";
            this.buttonEvaluate.UseVisualStyleBackColor = true;
            this.buttonEvaluate.Click += new System.EventHandler(this.buttonEvaluate_Click);
            // 
            // labelExpression
            // 
            this.labelExpression.AutoSize = true;
            this.labelExpression.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelExpression.Location = new System.Drawing.Point(1, 11);
            this.labelExpression.Margin = new System.Windows.Forms.Padding(1);
            this.labelExpression.Name = "labelExpression";
            this.labelExpression.Size = new System.Drawing.Size(282, 18);
            this.labelExpression.TabIndex = 3;
            this.labelExpression.Text = "Expression";
            this.labelExpression.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // openFileDialogIn
            // 
            this.openFileDialogIn.FileName = ".csv";
            this.openFileDialogIn.Filter = "Comma separated values|*.csv|All files|*.*";
            // 
            // saveFileDialogOut
            // 
            this.saveFileDialogOut.FileName = ".csv";
            this.saveFileDialogOut.Filter = "Comma separated values|*.csv|All files|*.*";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.menuStripMain);
            this.MainMenuStrip = this.menuStripMain;
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "FormMain";
            this.Text = "Calculator+";
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.formMainBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.TextBox textBoxExpression;
        private System.Windows.Forms.Button buttonEvaluate;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.BindingSource formMainBindingSource;
        private System.Windows.Forms.Label labelExpression;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label labelOutFile;
        private System.Windows.Forms.Label labelInFile;
        private System.Windows.Forms.TextBox textBoxOutFileName;
        private System.Windows.Forms.TextBox textBoxInFileName;
        private System.Windows.Forms.Button buttonProcessFile;
        private System.Windows.Forms.OpenFileDialog openFileDialogIn;
        private System.Windows.Forms.SaveFileDialog saveFileDialogOut;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openInputFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openOutputFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
    }
}

