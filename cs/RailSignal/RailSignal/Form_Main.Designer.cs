﻿namespace RailSignal
{
    partial class Form_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.serialPort = new System.IO.Ports.SerialPort(this.components);
            this.comboBox_Baud = new System.Windows.Forms.ComboBox();
            this.label_Baud = new System.Windows.Forms.Label();
            this.textBox_Command = new System.Windows.Forms.TextBox();
            this.label_Command = new System.Windows.Forms.Label();
            this.button_Command = new System.Windows.Forms.Button();
            this.label_PortName = new System.Windows.Forms.Label();
            this.textBox_PortName = new System.Windows.Forms.TextBox();
            this.button_PortOpen = new System.Windows.Forms.Button();
            this.label_Hex = new System.Windows.Forms.Label();
            this.button_PortClose = new System.Windows.Forms.Button();
            this.listBoxLampState = new System.Windows.Forms.ListBox();
            this.label_LampState = new System.Windows.Forms.Label();
            this.panel_Main = new System.Windows.Forms.Panel();
            this.menuStrip_Main = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.portToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox_Main = new System.Windows.Forms.PictureBox();
            this.comboBox_Next = new System.Windows.Forms.ComboBox();
            this.panel_VSet = new System.Windows.Forms.Panel();
            this.label_NextSignal = new System.Windows.Forms.Label();
            this.label_ActSignal = new System.Windows.Forms.Label();
            this.comboBox_Act = new System.Windows.Forms.ComboBox();
            this.button_Set = new System.Windows.Forms.Button();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel_Main.SuspendLayout();
            this.menuStrip_Main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).BeginInit();
            this.panel_VSet.SuspendLayout();
            this.SuspendLayout();
            // 
            // serialPort
            // 
            this.serialPort.BaudRate = 19200;
            this.serialPort.Parity = System.IO.Ports.Parity.Space;
            this.serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort_DataReceived);
            // 
            // comboBox_Baud
            // 
            this.comboBox_Baud.FormattingEnabled = true;
            this.comboBox_Baud.Items.AddRange(new object[] {
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "57600",
            "115200",
            "230400"});
            this.comboBox_Baud.Location = new System.Drawing.Point(10, 30);
            this.comboBox_Baud.Name = "comboBox_Baud";
            this.comboBox_Baud.Size = new System.Drawing.Size(70, 24);
            this.comboBox_Baud.TabIndex = 2;
            this.comboBox_Baud.SelectedIndexChanged += new System.EventHandler(this.comboBox_Baud_SelectedIndexChanged);
            // 
            // label_Baud
            // 
            this.label_Baud.AutoSize = true;
            this.label_Baud.Location = new System.Drawing.Point(10, 10);
            this.label_Baud.Name = "label_Baud";
            this.label_Baud.Size = new System.Drawing.Size(75, 17);
            this.label_Baud.TabIndex = 3;
            this.label_Baud.Text = "Baud Rate";
            // 
            // textBox_Command
            // 
            this.textBox_Command.Location = new System.Drawing.Point(40, 210);
            this.textBox_Command.Name = "textBox_Command";
            this.textBox_Command.Size = new System.Drawing.Size(40, 22);
            this.textBox_Command.TabIndex = 4;
            this.textBox_Command.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label_Command
            // 
            this.label_Command.AutoSize = true;
            this.label_Command.Location = new System.Drawing.Point(10, 190);
            this.label_Command.Name = "label_Command";
            this.label_Command.Size = new System.Drawing.Size(71, 17);
            this.label_Command.TabIndex = 5;
            this.label_Command.Text = "Command";
            // 
            // button_Command
            // 
            this.button_Command.Location = new System.Drawing.Point(10, 240);
            this.button_Command.Name = "button_Command";
            this.button_Command.Size = new System.Drawing.Size(70, 30);
            this.button_Command.TabIndex = 6;
            this.button_Command.Text = "Send";
            this.button_Command.UseVisualStyleBackColor = true;
            this.button_Command.Click += new System.EventHandler(this.button_Command_Click);
            // 
            // label_PortName
            // 
            this.label_PortName.AutoSize = true;
            this.label_PortName.Location = new System.Drawing.Point(10, 60);
            this.label_PortName.Name = "label_PortName";
            this.label_PortName.Size = new System.Drawing.Size(75, 17);
            this.label_PortName.TabIndex = 7;
            this.label_PortName.Text = "Port Name";
            // 
            // textBox_PortName
            // 
            this.textBox_PortName.Location = new System.Drawing.Point(10, 80);
            this.textBox_PortName.Name = "textBox_PortName";
            this.textBox_PortName.Size = new System.Drawing.Size(70, 22);
            this.textBox_PortName.TabIndex = 8;
            this.textBox_PortName.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button_PortOpen
            // 
            this.button_PortOpen.Location = new System.Drawing.Point(10, 110);
            this.button_PortOpen.Name = "button_PortOpen";
            this.button_PortOpen.Size = new System.Drawing.Size(70, 30);
            this.button_PortOpen.TabIndex = 9;
            this.button_PortOpen.Text = "Open";
            this.button_PortOpen.UseVisualStyleBackColor = true;
            this.button_PortOpen.Click += new System.EventHandler(this.button_PortOpen_Click);
            // 
            // label_Hex
            // 
            this.label_Hex.Location = new System.Drawing.Point(10, 210);
            this.label_Hex.Name = "label_Hex";
            this.label_Hex.Size = new System.Drawing.Size(30, 20);
            this.label_Hex.TabIndex = 10;
            this.label_Hex.Text = "0x";
            this.label_Hex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_PortClose
            // 
            this.button_PortClose.Location = new System.Drawing.Point(10, 150);
            this.button_PortClose.Name = "button_PortClose";
            this.button_PortClose.Size = new System.Drawing.Size(70, 30);
            this.button_PortClose.TabIndex = 11;
            this.button_PortClose.Text = "Close";
            this.button_PortClose.UseVisualStyleBackColor = true;
            this.button_PortClose.Click += new System.EventHandler(this.button_PortClose_Click);
            // 
            // listBoxLampState
            // 
            this.listBoxLampState.FormattingEnabled = true;
            this.listBoxLampState.IntegralHeight = false;
            this.listBoxLampState.ItemHeight = 16;
            this.listBoxLampState.Location = new System.Drawing.Point(10, 300);
            this.listBoxLampState.Name = "listBoxLampState";
            this.listBoxLampState.Size = new System.Drawing.Size(70, 210);
            this.listBoxLampState.TabIndex = 12;
            // 
            // label_LampState
            // 
            this.label_LampState.AutoSize = true;
            this.label_LampState.Location = new System.Drawing.Point(10, 280);
            this.label_LampState.Name = "label_LampState";
            this.label_LampState.Size = new System.Drawing.Size(41, 17);
            this.label_LampState.TabIndex = 13;
            this.label_LampState.Text = "State";
            // 
            // panel_Main
            // 
            this.panel_Main.Controls.Add(this.comboBox_Baud);
            this.panel_Main.Controls.Add(this.label_LampState);
            this.panel_Main.Controls.Add(this.label_Baud);
            this.panel_Main.Controls.Add(this.listBoxLampState);
            this.panel_Main.Controls.Add(this.textBox_Command);
            this.panel_Main.Controls.Add(this.button_PortClose);
            this.panel_Main.Controls.Add(this.label_Command);
            this.panel_Main.Controls.Add(this.label_Hex);
            this.panel_Main.Controls.Add(this.button_Command);
            this.panel_Main.Controls.Add(this.button_PortOpen);
            this.panel_Main.Controls.Add(this.label_PortName);
            this.panel_Main.Controls.Add(this.textBox_PortName);
            this.panel_Main.Location = new System.Drawing.Point(10, 30);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Size = new System.Drawing.Size(90, 510);
            this.panel_Main.TabIndex = 14;
            // 
            // menuStrip_Main
            // 
            this.menuStrip_Main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.portToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip_Main.Location = new System.Drawing.Point(0, 0);
            this.menuStrip_Main.Name = "menuStrip_Main";
            this.menuStrip_Main.Size = new System.Drawing.Size(342, 28);
            this.menuStrip_Main.TabIndex = 15;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // portToolStripMenuItem
            // 
            this.portToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.closeToolStripMenuItem1});
            this.portToolStripMenuItem.Name = "portToolStripMenuItem";
            this.portToolStripMenuItem.Size = new System.Drawing.Size(48, 24);
            this.portToolStripMenuItem.Text = "Port";
            // 
            // pictureBox_Main
            // 
            this.pictureBox_Main.BackColor = System.Drawing.Color.White;
            this.pictureBox_Main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_Main.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_Main.Image")));
            this.pictureBox_Main.Location = new System.Drawing.Point(130, 40);
            this.pictureBox_Main.Name = "pictureBox_Main";
            this.pictureBox_Main.Size = new System.Drawing.Size(80, 500);
            this.pictureBox_Main.TabIndex = 16;
            this.pictureBox_Main.TabStop = false;
            this.pictureBox_Main.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Main_Paint);
            // 
            // comboBox_Next
            // 
            this.comboBox_Next.FormattingEnabled = true;
            this.comboBox_Next.Items.AddRange(new object[] {
            "Stop",
            "40",
            "80",
            "120",
            "Vmax"});
            this.comboBox_Next.Location = new System.Drawing.Point(10, 30);
            this.comboBox_Next.Name = "comboBox_Next";
            this.comboBox_Next.Size = new System.Drawing.Size(70, 24);
            this.comboBox_Next.TabIndex = 17;
            // 
            // panel_VSet
            // 
            this.panel_VSet.Controls.Add(this.button_Set);
            this.panel_VSet.Controls.Add(this.comboBox_Act);
            this.panel_VSet.Controls.Add(this.label_ActSignal);
            this.panel_VSet.Controls.Add(this.label_NextSignal);
            this.panel_VSet.Controls.Add(this.comboBox_Next);
            this.panel_VSet.Location = new System.Drawing.Point(240, 30);
            this.panel_VSet.Name = "panel_VSet";
            this.panel_VSet.Size = new System.Drawing.Size(90, 510);
            this.panel_VSet.TabIndex = 18;
            // 
            // label_NextSignal
            // 
            this.label_NextSignal.AutoSize = true;
            this.label_NextSignal.Location = new System.Drawing.Point(10, 10);
            this.label_NextSignal.Name = "label_NextSignal";
            this.label_NextSignal.Size = new System.Drawing.Size(36, 17);
            this.label_NextSignal.TabIndex = 14;
            this.label_NextSignal.Text = "Next";
            // 
            // label_ActSignal
            // 
            this.label_ActSignal.AutoSize = true;
            this.label_ActSignal.Location = new System.Drawing.Point(10, 60);
            this.label_ActSignal.Name = "label_ActSignal";
            this.label_ActSignal.Size = new System.Drawing.Size(47, 17);
            this.label_ActSignal.TabIndex = 18;
            this.label_ActSignal.Text = "Actual";
            // 
            // comboBox_Act
            // 
            this.comboBox_Act.FormattingEnabled = true;
            this.comboBox_Act.Items.AddRange(new object[] {
            "Stop",
            "40",
            "80",
            "120",
            "Vmax",
            "Call"});
            this.comboBox_Act.Location = new System.Drawing.Point(10, 80);
            this.comboBox_Act.Name = "comboBox_Act";
            this.comboBox_Act.Size = new System.Drawing.Size(70, 24);
            this.comboBox_Act.TabIndex = 19;
            this.comboBox_Act.SelectedIndexChanged += new System.EventHandler(this.comboBox_Act_SelectedIndexChanged);
            // 
            // button_Set
            // 
            this.button_Set.Location = new System.Drawing.Point(10, 110);
            this.button_Set.Name = "button_Set";
            this.button_Set.Size = new System.Drawing.Size(70, 30);
            this.button_Set.TabIndex = 14;
            this.button_Set.Text = "Set";
            this.button_Set.UseVisualStyleBackColor = true;
            this.button_Set.Click += new System.EventHandler(this.button_Set_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.button_PortOpen_Click);
            // 
            // closeToolStripMenuItem1
            // 
            this.closeToolStripMenuItem1.Name = "closeToolStripMenuItem1";
            this.closeToolStripMenuItem1.Size = new System.Drawing.Size(152, 24);
            this.closeToolStripMenuItem1.Text = "Close";
            this.closeToolStripMenuItem1.Click += new System.EventHandler(this.button_PortClose_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(152, 24);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 553);
            this.Controls.Add(this.panel_VSet);
            this.Controls.Add(this.pictureBox_Main);
            this.Controls.Add(this.panel_Main);
            this.Controls.Add(this.menuStrip_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MainMenuStrip = this.menuStrip_Main;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "RailSignal";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_Main.ResumeLayout(false);
            this.panel_Main.PerformLayout();
            this.menuStrip_Main.ResumeLayout(false);
            this.menuStrip_Main.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Main)).EndInit();
            this.panel_VSet.ResumeLayout(false);
            this.panel_VSet.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort;
        private System.Windows.Forms.ComboBox comboBox_Baud;
        private System.Windows.Forms.Label label_Baud;
        private System.Windows.Forms.TextBox textBox_Command;
        private System.Windows.Forms.Label label_Command;
        private System.Windows.Forms.Button button_Command;
        private System.Windows.Forms.Label label_PortName;
        private System.Windows.Forms.TextBox textBox_PortName;
        private System.Windows.Forms.Button button_PortOpen;
        private System.Windows.Forms.Label label_Hex;
        private System.Windows.Forms.Button button_PortClose;
        private System.Windows.Forms.ListBox listBoxLampState;
        private System.Windows.Forms.Label label_LampState;
        private System.Windows.Forms.Panel panel_Main;
        private System.Windows.Forms.MenuStrip menuStrip_Main;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem portToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox_Main;
        private System.Windows.Forms.ComboBox comboBox_Next;
        private System.Windows.Forms.Panel panel_VSet;
        private System.Windows.Forms.Button button_Set;
        private System.Windows.Forms.ComboBox comboBox_Act;
        private System.Windows.Forms.Label label_ActSignal;
        private System.Windows.Forms.Label label_NextSignal;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

