﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace RailSignal
{
    class RSignal
    {
        /*Masks for the lamps & stripes */
        private const byte LAMP_GREEN_MASK = 0x80;
        private const byte LAMP_YELLOWN_MASK =0x40;
        private const byte LAMP_RED_MASK = 0x20;
        private const byte LAMP_YELLOWA_MASK = 0x10;
        private const byte LAMP_CALL_MASK = 0x08;
        private const byte LAMP_STR1_MASK = 0x04;
        private const byte LAMP_STR2_MASK = 0x02;
        /* Geometry */
        private Size lampSize, stripeSize;
        private Color lRed, lGreen, lYellow, lDark;
        private Rectangle lampGreen, lampYellowN, lampYellowA, lampRed, lampCall, stripeGreenUp, stripeGreenDown;
        /* Initialization of signal geometry and colors */
        public RSignal()
        {
            lampSize = new Size(25, 25);
            stripeSize = new Size(45, 8);
            lampGreen = new Rectangle(new Point(25, 30), lampSize);
            lampYellowN = new Rectangle(new Point(25, 70), lampSize);
            lampYellowA = new Rectangle(new Point(25, 170), lampSize);
            lampRed = new Rectangle(new Point(25, 130), lampSize);
            lampCall = new Rectangle(new Point(25, 284), lampSize);
            stripeGreenUp = new Rectangle(new Point(16, 240), stripeSize);
            stripeGreenDown = new Rectangle(new Point(16, 252), stripeSize);
            lRed = Color.FromArgb(220, 40, 50);
            lYellow = Color.FromArgb(220, 200, 0);
            lGreen = Color.FromArgb(0, 200, 80);
            lDark = Color.FromArgb(40, 40, 40);
        }
        /* Drawing signal based on sent signal state */
        public void Draw(byte State, PaintEventArgs E)
        {
            Graphics G = E.Graphics;
            SolidBrush B = new SolidBrush(Color.White);
            /* Green lamp */
            if (LAMP_GREEN_MASK == (byte)(State & LAMP_GREEN_MASK))
            {
                B.Color = lGreen;
            }
            else
            {
                B.Color = lDark;
            }
            G.FillEllipse(B, lampGreen);
            /* Yellow lamp of next signal */
            if (LAMP_YELLOWN_MASK == (byte)(State & LAMP_YELLOWN_MASK))
            {
                B.Color = lYellow;
            }
            else
            {
                B.Color = lDark;
            }
            G.FillEllipse(B, lampYellowN);
            /* Red lamp */
            if (LAMP_RED_MASK == (byte)(State & LAMP_RED_MASK))
            {
                B.Color = lRed;
            }
            else
            {
                B.Color = lDark;
            }
            G.FillEllipse(B, lampRed);
            /* Yellow lamp of actual signal*/
            if (LAMP_YELLOWA_MASK == (byte)(State & LAMP_YELLOWA_MASK))
            {
                B.Color = lYellow;
            }
            else
            {
                B.Color = lDark;
            }
            G.FillEllipse(B, lampYellowA);
            /* Upper green stripe */
            if (LAMP_STR1_MASK == (byte)(State & LAMP_STR1_MASK))
            {
                B.Color = lGreen;
            }
            else
            {
                B.Color = lDark;
            }
            G.FillRectangle(B, stripeGreenUp);
            /* Green stripe below */
            if (LAMP_STR2_MASK == (byte)(State & LAMP_STR2_MASK))
            {
                B.Color = lGreen;
            }
            else
            {
                B.Color = lDark;
            }
            G.FillRectangle(B, stripeGreenDown);
            /* White lamp */
            if (LAMP_CALL_MASK == (byte)(State & LAMP_CALL_MASK))
            {
                B.Color = Color.White;
            }
            else
            {
                B.Color = lDark;
            }
            G.FillEllipse(B, lampCall); 
        }
    }
}
