﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace RailSignal
{
    public partial class Form_Main : Form
    {
        /* The state of railway signal lamps received from uC */
        private byte lampState;
        /* Signal representation */
        private RSignal signal;
        /* Delegates for threadsafe control method calls */
        private delegate void stringCallback(string s);
        private delegate void objectCallback(object item);
        private delegate void voidCallback();
        /* Default serial port name */
        private readonly string defaultPortName = "COM4";
        /* Default constructor of form */
        public Form_Main()
        {
            InitializeComponent();
        }
        /* Initialization */
        private void Form1_Load(object sender, EventArgs e)
        {
            lampState = 0;
            /* 19200 Baud selected */
            comboBox_Baud.SelectedIndex = 6;
            /* Vmax selected for both */
            comboBox_Next.SelectedIndex = 4;
            comboBox_Act.SelectedIndex = 4;
            /* Default port name setting */
            textBox_PortName.Text = defaultPortName;
            signal = new RSignal();
        }
        /* Set selected baud rate */
        private void comboBox_Baud_SelectedIndexChanged(object sender, EventArgs e)
        {
            int Value;
            if(int.TryParse((sender as ComboBox).SelectedItem.ToString(), out Value))
            {
                serialPort.BaudRate = Value;
            }
        }
        /* Opening serial port */
        private void button_PortOpen_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort.PortName = textBox_PortName.Text;
                serialPort.Open();
                if (sender is Button)
                {
                    (sender as Button).Enabled = false;
                }
            }
            catch 
            {
                MessageBox.Show("Port " + textBox_PortName.Text + " is not valid.", "Port setting failure");
            }
        }
        /* Direct command sending */
        private void button_Command_Click(object sender, EventArgs e)
        {
            byte command = Convert.ToByte(textBox_Command.Text, 16);
            send_Command(command);
        }
        /* Set selected signalling according to combo boxes */
        private void button_Set_Click(object sender, EventArgs e)
        {
            byte command = (byte)(((byte)comboBox_Next.SelectedIndex << 4) | (byte)comboBox_Act.SelectedIndex);
            send_Command(command);
        }
        /* Sending one byte command on serial port */
        private void send_Command(byte Command)
        {
            if (serialPort.IsOpen)
            {
                try
                {
                    byte[] SendArray = new byte[] { Command };
                    serialPort.Write(SendArray, 0, 1);
                }
                catch
                {
                    MessageBox.Show("Command is not valid.", "Direct command sending falure");
                }
            }
            else
            {
                MessageBox.Show("Serial port is closed. Please check port name settings.", "Direct command sending falure");
            }
        }
        /* Serial port data reception */
        private void serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            if ((sender as SerialPort).IsOpen)
            {
                byte[] ReceiveArray = new byte[1];
                (sender as SerialPort).Read(ReceiveArray, 0, 1);
                lampState = ReceiveArray[0];
                addListBoxItem(lampState);
                if (listBoxLampState.Items.Count > 11)
                {
                    clearListBox();
                }
                invalidatePictureBox();
            }
        }
        /* Closing serial port */
        private void button_PortClose_Click(object sender, EventArgs e)
        {
            serialPort.Close();
            button_PortOpen.Enabled = true;
        }
        /* Threadsafe Form text setting */
        private void setFormText(string text)
        {
            if (this.InvokeRequired)
            {
                stringCallback d = new stringCallback(setFormText);
                this.BeginInvoke(d, new object[] { text });
            }
            else
            {
                this.Text = text;
            }
        }
        /* Threadsafe listbox item addition */
        private void addListBoxItem(object item)
        {
            if (this.listBoxLampState.InvokeRequired)
            {
                objectCallback a = new objectCallback(addListBoxItem);
                this.listBoxLampState.BeginInvoke(a, new object[] { item });
            }
            else
            {
                this.listBoxLampState.Items.Add(item);
            }
        }
        /* Threadsafe listobox item clearing */
        private void clearListBox()
        {
            if (this.listBoxLampState.InvokeRequired)
            {
                voidCallback c = new voidCallback(clearListBox);
                this.listBoxLampState.BeginInvoke(c);
            }
            else
            {
                this.listBoxLampState.Items.Clear();
            }
        }
        /* Threadsafe picturebox invalidation */
        private void invalidatePictureBox()
        {
            if (this.pictureBox_Main.InvokeRequired)
            {
                voidCallback c = new voidCallback(invalidatePictureBox);
                this.pictureBox_Main.BeginInvoke(c);
            }
            else
            {
                this.pictureBox_Main.Invalidate();
            }
        }
        /* Painting the railroad signal */
        private void pictureBox_Main_Paint(object sender, PaintEventArgs e)
        {
            signal.Draw(lampState, e);
        }
        /* On Form closing close serial port */
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort.Close();
        }
        /* Disabling commands for next signal when STOP or CALL signal is set for actual signal */
        private void comboBox_Act_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == 0)
            {
                comboBox_Next.Enabled = false;
                comboBox_Next.SelectedIndex = 0;
            }
            else if ((sender as ComboBox).SelectedIndex == 5)
            {
                comboBox_Next.Enabled = false;
                comboBox_Next.SelectedIndex = 0;
            }
            else
            {
                comboBox_Next.Enabled = true;
            }
        }
        /* Exit program */
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /* Show about */
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox AB = new AboutBox();
            AB.Show();
        }
    }
}
