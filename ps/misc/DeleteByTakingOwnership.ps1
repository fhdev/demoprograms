using namespace System.IO

param(
	[Parameter(Mandatory=$true)]
	[string] $path
)

function TakeOwnAndRenameRecursive($path) {
	# Set preference variables
	$ErrorActionPreference = 'Stop'
	
	# Get username
	$user = [System.Security.Principal.WindowsIdentity]::GetCurrent().Name
	
	# Set ownership of current item
	[Console]::WriteLine(">>> Processing $path ...")
	$stdOutErr = & takeown.exe /f """${path}""" 2>&1
	[Console]::WriteLine(">>> TAKEOWN: $stdOutErr")
	$stdOutErr = & icacls.exe """${path}""" /grant """${user}:f""" 2>&1
	[Console]::WriteLine(">>> ICACLS: $stdOutErr")
	
	# Rename current item to short name
	$cnt = 0
	$parent = [Path]::GetDirectoryName($path)
	$newPath = [Path]::Combine($parent, $cnt)
	while(Test-Path $newPath) {
		$cnt++
		$newPath = [Path]::Combine($parent, $cnt)
	}
	[Console]::WriteLine(">>> Rename $path to $newPath ...")
	Rename-Item -Path $path -NewName "$cnt" -Force
	# Process children
	if ((Get-Item $newPath -Force) -is [DirectoryInfo]) {
		$children = Get-ChildItem -Path $newPath -Force | Sort-Object -Property Name
		foreach($child in $children) {
			$childPath = "$newPath\$($child.Name)"
			TakeOwnAndRenameRecursive($childPath) | Out-Null
		}
	}
	
	return $newPath
}

$ErrorActionPreference = 'Stop'
if (-not (Test-Path $path)) {
	[Console]::WriteLine(">>> Path $path does not exist!")
	exit 1
}

$newPath = TakeOwnAndRenameRecursive($path)

[Console]::WriteLine("Deleting $newPath ...")

Remove-Item $newPath -Recurse -Force
