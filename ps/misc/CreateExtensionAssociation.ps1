### associate the extension $EXT with the program $EXEPATH ###

# parameters
$EXT = '.my_extension'
$CLASS = 'my_extension_file'
$EXEPATH = '%SystemRoot%\System32\Notepad.exe'

$HKCR = 'registry::HKEY_CLASSES_ROOT'
$HKCU = 'registry::HKEY_CURRENT_USER'
$DEFAULT = '(Default)'
$COMMAND = "$exePath `"%1`" %*"

# create association of the class to the executable
$regKeyClass = "$HKCR\$CLASS"
if(Test-Path $regKeyClass) {
	Remove-Item $regKeyClass -Recurse -Force
}
New-Item -Path $HKCR -Name $CLASS
$regKeyShell = "$regKeyClass\shell"
New-Item -Path $regKeyClass -Name shell
Set-ItemProperty $regKeyShell -Name $DEFAULT -Type String -Value Edit
$regKeyEdit = "$regKeyShell\Edit"
New-Item -Path $regKeyShell -Name Edit
$regKeyCommand = "$regKeyEdit\Command"
New-Item -Path $regKeyEdit -Name Command
Set-ItemProperty $regKeyCommand -Name $DEFAULT -Type ExpandString -Value $COMMAND
# create association of the extension to the class
$regKeyExt = "$HKCR\$EXT"
if(Test-Path $regKeyExt) {
	Remove-Item $regKeyExt -Recurse -Force
}
New-Item -Path $HKCR -Name $EXT
Set-ItemProperty $regKeyExt -Name $DEFAULT -Type String -Value $CLASS
# delete user-defined association of the extension to any program
$regKeyUserChoice = "$HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\FileExts\$EXT\UserChoice"
if (Test-Path $regKeyUserChoice) {
	Remove-Item -Path $regKeyUserChoice -Recurse -Force
}