using namespace System.Management

# This function detects if a smart card is connected to the computer, and warns the user if the screen is locked.
function DetectSmartCard {
    # Find inserted SmartCard among devices on local computer
    $query = "SELECT * From Win32_PnPEntity WHERE Description = 'CardOS API Minidriver'"
    $mos = [ManagementObjectSearcher]::New("\root\cimv2", $query)
    $mob = $mos.Get();
    # Warn user if necessary
    if ($mob.Count -eq 0) {
        Write-Host "SmartCard is not connected."
    }
    else {
        Write-Host "SmartCard is connected."
        if (Get-Process -Name logonui -ErrorAction SilentlyContinue) {
            # Create a new SpVoice objects
            $voice = New-Object -ComObject SAPI.SPVoice
            # Set the speed (positive numbers are faster, negative numbers are slower)
            $voice.rate = 0
            # Set audio output to speaker
            foreach ($output in $voice.GetAudioOutputs()) {
                if ($output.GetDescription().Contains("Speakers")) {
                    $voice.AudioOutput = $output
                    break
                }
            }
            # Warn user
            $voice.speak("Your smart card is still connected.")
        }
    }
}

# Run function if called as script or with call operator &
if ($MyInvocation.InvocationName -ne ".") {
    DetectSmartCard
}