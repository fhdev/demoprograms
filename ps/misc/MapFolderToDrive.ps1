$DIR = 'C:\DRIVES\X'
$DRIVE = 'X:'
$HKLM = 'registry::HKEY_LOCAL_MACHINE'

$regKeyDosDevices = "$HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\DOS Devices"

New-Item -ItemType Directory -Path $DIR
New-ItemProperty -Path $regKeyDosDevices -Name $DRIVE -Value "\??\$DIR" -PropertyType String