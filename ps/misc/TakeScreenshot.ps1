param(
    [Parameter(Mandatory=$true)]
    [string] $path,
    [Parameter(Mandatory=$false)]
    [byte] $screenIndex = 255
)

[Reflection.Assembly]::LoadWithPartialName("System.Drawing") | Out-Null
[Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null

$width = 0;
$height = 0;
$workingAreaX = 0;
$workingAreaY = 0;

$screens= [System.Windows.Forms.Screen]::AllScreens;

if ($screenIndex -eq 255) {
    foreach ($screen in $screens) {
        if($screen.WorkingArea.X -lt $workingAreaX) {
            $workingAreaX = $screen.WorkingArea.X;
        }
        if($screen.WorkingArea.Y -lt $workingAreaY) {
            $workingAreaY = $screen.WorkingArea.Y;
        }
        $width = $width + $screen.Bounds.Width;
        if($screen.Bounds.Height -gt $height) {
            $height = $screen.Bounds.Height;
        }
    }
}
else {
    $workingAreaX = $screens[$screenIndex].WorkingArea.X;
    $workingAreaY = $screens[$screenIndex].WorkingArea.Y;
    $width = $screens[$screenIndex].Bounds.Width;
    $height = $screens[$screenIndex].Bounds.Height;
}

$bounds = [System.Drawing.Rectangle]::New($workingAreaX, $workingAreaY, $width, $height);
$bmp = [System.Drawing.Bitmap]::New($width, $height);
$graphics = [System.Drawing.Graphics]::FromImage($bmp);
$graphics.CopyFromScreen($bounds.Location, [System.Drawing.Point]::Empty, $bounds.size);
$bmp.Save($path);
$graphics.Dispose();
$bmp.Dispose();
