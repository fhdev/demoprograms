﻿function BuildCs {
    param(
        [Parameter(Mandatory=$true)]
        [string]$solutionDir,

        [string]$devComPromptPath = "C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\Common7\Tools\VsDevCmd.bat"
    )
    # Constants
    $projFileExt = ".csproj"
    $resultFileExt = ".exe"
    $errorMsg = ""

    # Check if Developer Command Prompt for Visual Studio exists
    if (-not (Test-Path -Path $devComPromptPath))
    {
        $errorMsg = "Developer Command Prompt does not exist at $devComPromptPath"
        Write-Error $errorMsg
        return $null, $errorMsg
    }

    # Search for C# project file(s) in the solution directory
    Write-Host "Searching for .csproj file(s) in directory $solutionDir ..."
    $projectFiles = Get-ChildItem -Path $solutionDir -Filter  "*$projFileExt" -File -Recurse | ForEach-Object { $_.FullName }
    if (!$projectFiles)
    {
        $errorMsg = "No $projFileExt file found in directory $solutionDir!"
        Write-Host $errorMsg -ForegroundColor Red
        return $null, $errorMsg
    }
    if ($projectFiles -is [Array])
    {
        Write-Host "Multiple project files $projectFiles found in directory $solutionDir."
    }
    else
    {
        Write-Host "Project file $projectFiles found in directory $solutionDir."
    }

    # Delete all executables inside the solution directory (we will treat every .exe after build as the potential target)
    Get-ChildItem -Path $solutionDir -Filter  "*$resultFileExt" -File -Recurse | ForEach-Object { $_.FullName } | Remove-Item -Force

    # Build found C# project file(s)
    $success = $false
    foreach ($projectFile in $projectFiles)
    {
        # Restore NuGet packages (.NET Core Apps)
        Write-Host "Restoring packages for $projectFile ..."
        $msBuildRestoreArgs = """$projectFile"" -nologo -t:restore"
        $restoreResult = & cmd /c """$devComPromptPath"" && MSBuild $msBuildRestoreArgs"
        if ($LASTEXITCODE -eq 0)
        {
            Write-Host "Restoring packages for $projectFile succeeded." -ForegroundColor Green
        }
        else
        {
            Write-Host "Restoring packages for $projectFile failed." -ForegroundColor Red
            Write-Host "Details:"
            Write-Host $restoreResult
            continue
        }

        # Build default target
        Write-Host "Building $projectFile ..."
        $msBuildArgs = """$projectFile"" -nologo"
        $buildResult = & cmd /c """$devComPromptPath"" && MSBuild $msBuildArgs"
        if ($LASTEXITCODE -eq 0)
        {
            Write-Host "Build of $projectFile succeeded." -ForegroundColor Green
            $success = $true
        }
        else
        {
            Write-Host "Build of $projectFile failed!" -ForegroundColor Red
            Write-Host "Details:"
            Write-Host $buildResult
        }
    }
    if (!$success)
    {
        $errorMsg = "No build succeeded in directory $solutionDir!"
        Write-Host $errorMsg -ForegroundColor Red
        return $null, $errorMsg 
    }

    # Find and return build results (.exe files in bin folders)
    Write-Host "Searching for build result(s) ..."
    $exeFiles = Get-ChildItem -Path $solutionDir -Filter  "*$resultFileExt" -File -Recurse | ForEach-Object { $_.FullName } `
        | Select-String "^(.+)\\bin\\(.+)\$resultFileExt$"
    if(!$exeFiles)
    {
        $errorMsg = "No $resultFileExt file found after build in directory $solutionDir!"
        Write-Host $errorMsg -ForegroundColor Red
        return $null, $errorMsg
    }
    if ($exeFiles -is [Array])
    {
        Write-Host "Multiple executables $exeFiles found after build in directory $solutionDir."
    }
    else
    {
        Write-Host "Executable $exeFiles found after build in directory $solutionDir." 
    }
    return $exeFiles

}