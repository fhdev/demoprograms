﻿using namespace System.IO

# Parameters
param(
    [string]$workingFolderPath = $PSScriptRoot,

    [string]$workbookName = "Eredmenyek.xlsx",

    [string]$worksheetName = "kHF",

    [string]$devComPromptPath = "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\Common7\Tools\VsDevCmd.bat",

    [switch]$testMode
)
# Functions
function cnor()
{
    # Create Numeric Output Regex
    param(
        [Parameter(Mandatory=$true)]
        [double[]]$values
    )
    $regex = "(?:^|\n)"
    for ($iValue = 0; $iValue -lt $values.Length; $iValue++)
    {
        if (($values[$iValue] % 1) -eq 0)
        {
            $regex += "[^0-9,.\r\n]*{0:g}[^0-9,.\r\n]*\r?" -f $values[$iValue]; 
        }
        else
        {
            $regex += "[^0-9,.\r\n]*{0:g}[0-9]*[^0-9,.\r\n]*\r?" -f $values[$iValue]; 
        }
        if ($iValue -lt $values.Length - 1)
        {
            $regex += "\n"
        }
        else
        {
            $regex += "(?:\n|\z)"
        }
    }
    return $regex
}
function cnis()
{
    # Create Numeric Input String
    param(
        [Parameter(Mandatory=$true)]
        [double[]]$values
    )
    $string = ""
    for ($iValue = 0; $iValue -lt $values.Length; $iValue++) 
    {
        $string += "{0:g}`r`n" -f $values[$iValue]
    }
    return $string    
}

# Script settings
$ErrorActionPreference = "Stop"
$OutputEncoding = [System.Text.Encoding]::UTF8
chcp 65001 1>$null

# Source functions
. "$PSScriptRoot\BuildCs.ps1"
. "$PSScriptRoot\RunExe.ps1"

$FIRST_ROW = 4
$NID_COL = 2
$HW_FIRST_COL = 6
$HW_FIRST_ID = 4
$TIMEOUT_SEC = 3
$MAX_OUT_LEN = 5000

$TESTINPUTS  =  @( `
# HF04: LARGEST OF 3
@( (cnis((1, 2, 3))), (cnis((10, 22, 3))), (cnis((100, 2002, 345))), (cnis((9.2, 9, 3))), (cnis((1.1, 1.1, 1.1))) ), `
# HF05: BIGGER / SMALLER
@( (cnis((2, 1))), (cnis((10, 3))), (cnis((10.3, 3.7))), (cnis((1.5, 99.34))) ), `
# HF06: RESULTANT RESISTANCE
@( ("S`r`n" + (cnis((1, 1, 1, 0)))), ("S`r`n" + (cnis((10.3, 2.34, -1)))), ("P`r`n" + (cnis((10, 10, -1)))), ("P`r`n" + (cnis((3.3, 3.3, 3.3, 0)))) ), `
# HF07: EULER NUMBER
@( (cnis((10, 1e-6))), (cnis((10, 1e-1))), (cnis((2, 1e-10))) ), `
# HF08: GREATEST AND SMALLEST NUMBERS IN INTEGER ARRAY
@( (cnis((3, 1, 2, 3))), (cnis((4, 120, 54, 99, 32))), (cnis((5, 1, 99, 43, 21, 5))) ), `
# HF09: NUMBER OF ELEMENTS THAT ARE GREATER OR SMALLER THAN AVERAGE IN DOUBLE ARRAY
@( (cnis((3, 1.1, 2.2, 3.3))), (cnis((5, 1.9, 10.23, 8.32, 23.2, 1.11))), (cnis((4, 11.11, 11.11, 11.11, 11.11))) ), `
# HF10: HOW MANY EVEN OR 7-DIVISABLE NUMBER IS IN MATRIX ROWS
@( (cnis((2, 1, 2, 3, 4))), (cnis((3, 12, 50, 67, 22, 70, 14, 9, 8, 11))), (cnis((4, 102, 33, 41, 13, 56, 43, 21, 55, 90, 76, 32, 12, 103, 34, 9993, 2))) ), `
# HF11: HOW MANY CHARS IN MULTI-LINE TEXT THAT THE NEXT CHAR IS ALSO NEXT IN ABC
@( ("abcde`r`nfghij`r`n`r`n"), ("alma`r`nkörte`r`n`r`n"), ("stangli`r`ndefekt`r`nklaviatúra`r`n`r`n") ) `
)

$TESTOUTPUTS =  @( `
# HF04: LARGEST OF 3
@( (cnor(3)), (cnor(22)), (cnor(2002)), (cnor(9.2)), (cnor(1.1)) ), `
# HF05: BIGGER / SMALLER
@( (cnor(2)), (cnor(3.333)), (cnor(2.78)), (cnor((66.226))) ), `
# HF06: RESULTANT RESISTANCE
@( (cnor(3)), (cnor(12.64)), (cnor((5))), (cnor((1.1))) ), `
# HF07: EULER NUMBER
@( (cnor(2.718)), (cnor(2.66)), (cnor(2.5)) ), `
# HF08: GREATEST AND SMALLEST NUMBERS IN INTEGER ARRAY
@( (cnor((3, 2, 1, 0))), (cnor((120, 0, 32, 3))), (cnor((99, 1, 1, 0))) ), `
# HF09: NUMBER OF ELEMENTS THAT ARE GREATER OR SMALLER THAN AVERAGE IN DOUBLE ARRAY
@( (cnor((2, 1))), (cnor((2, 3))), (cnor((0, 0))) ), `
# HF10: HOW MANY EVEN OR 7-DIVISABLE NUMBER IS IN MATRIX ROWS
@( (cnor((1, 1))), (cnor((2, 3, 1))), (cnor((1, 2, 4, 2))) ), `
# HF11: HOW MANY CHARS IN MULTI-LINE TEXT THAT THE NEXT CHAR IS ALSO NEXT IN ABC
@( (cnor(9)), (cnor(1)), (cnor(4)) ) `
)

# Check arguments
# Check if Working Directory exists
if (-not (Test-Path -Path $workingFolderPath))
{
    Write-Error "Working directory does not exist at $workingFolderPath"
    exit(1)
}
# Check if Developer Command Prompt for Visual Studio exists
if (-not (Test-Path -Path $devComPromptPath))
{
    Write-Error "Developer Command Prompt does not exist at $devComPromptPath"
    exit(1)
}
# Check if Workbook exits
if (-not $testMode)
{
    $workbookPath = [Path]::Combine($workingFolderPath, $workbookName)
    if (-not (Test-Path -Path $workbookPath))
    {
        Write-Error "Workbook does not exist at $workbookPath"
        exit(1)
    }
}

# Write Argument info
Write-Host "Checking homeworks in directory $workingFolderPath ..."
Write-Host "Using VS Developer Command Prompt from $devComPromptPath ..."

# No Workbook in case of test mode
if (-not $testMode)
{
    # Open Excel HW worksheet
    $xlApp = New-Object -ComObject Excel.Application
    $xlWorkbook = $xlApp.Workbooks.Open($workbookPath)
    $xlWorksheet = $xlWorkbook.Sheets.Item($worksheetName)

    # Get Neptun IDs
    $lastRow = $xlWorksheet.UsedRange.Rows.Count
    $nIds = $xlWorksheet.Range($xlWorksheet.Cells($FIRST_ROW, $NID_COL), `
    $xlWorksheet.Cells($lastRow, $NID_COL)).Value2
}
else
{
    # Set-up dummy Neptun ID
    $nIds = [string[,]]::new(2, 2)
    $nIds[1, 1] = "999_hf"
}

# Create container for results
$allResults = [string[,]]::new($nIds.GetLength(0), $TESTINPUTS.Length)

# For Each Neptun IDs
for($iNid = 0; $iNid -lt $nIds.Length; $iNid++)
{
    # Somehow this uses 1 based indices (Excel interop is lame)
    $nId = $nIds[($iNid + 1), 1]

    # Skip if Neptun ID is empty
    if ([string]::IsNullOrEmpty($nId)) { continue }

    # Make Neptun ID uppercase
    $nId = $nId.ToUpper()
    # Write separator line to console
    Write-Host "****************************************************************************************************"
    Write-Host "Checking homework of $nId ..."

    # For Each homework
    for($iHw = 0; $iHw -lt $TESTINPUTS.Length; $iHw++)
    {
        $hwId = $iHw + $HW_FIRST_ID
        Write-Host "------------------------------------------------"
        Write-Host "Checking homework $hwId ..."

        # Result variables
        $hwResult = ""
        $hwSuccess = $false

        # Test if folder exists for current homework
        $hwSubFolder = "$nId\hf$("{0:00}" -f $hwId)"
        $hwFolder = [Path]::Combine($workingFolderPath, $hwSubFolder)
        # If homework folder exists
        if (Test-Path $hwFolder)
        {
            # Homework folder found
            # Try to build
            $exePaths, $buildError = BuildCs -solutionDir $hwFolder -devComPromptPath $devComPromptPath

            # Check results if there is any executable succesfully built
            # If homework builds
            if ($exePaths)
            {
                # Successful build - executable found
                # Check if one of the executables produces correct output
                $correctOutput = $false
                # For Each executables
                for($iExePath = 0; $iExePath -lt $exePaths.Length; $iExePath++)
                {
                    Write-Host "Testing executable $($exePaths[$iExePath]) ..."
                    $correctOutput = $true
                    for($iIn = 0; $iIn -lt $TESTINPUTS[$iHw].Length; $iIn++)
                    {
                        # Run executable with timeout TIMEOUT_SEC and limit output to MAX_OUT_LEN
                        ($exitcode, $stdout, $stderr) = RunExe -exePath $exePaths[$iExePath] `
                            -stdIn $TESTINPUTS[$iHw][$iIn] -timeOut $TIMEOUT_SEC -maxOutChars $MAX_OUT_LEN
                        $correctCurrentOutput = $stdout -match $TESTOUTPUTS[$iHw][$iIn]
                        # If output is wrong for one of the inputs
                        if (!$correctCurrentOutput) 
                        {
                            $stdout = $stdout.Replace("`r", "").Replace("`n", " ↵ ");
                            $stdin =  $TESTINPUTS[$iHw][$iIn].Replace("`r", "").Replace("`n", "↵");
                            $hwResult += "| Wrong output <$stdout> for input <$stdin>. "
                        }
                        $correctOutput = $correctOutput -and $correctCurrentOutput
                    }
                    # If one of the executables succeeds, exiting from loop
                    if ($correctOutput) { break }

                } # End For Each executables 
                if ($correctOutput)
                {
                    $hwResult = "Correct output for homework $hwId in $hwFolder. " + $hwResult
                    $hwSuccess = $true
                }
                else
                {
                    $hwResult = "Wrong output for homework $hwId in $hwFolder. " + $hwResult
                    $hwSuccess = $false
                }
            }
            else
            {
                # Build failed - no executable found
                $hwResult = "No build succeeded in $hwFolder. Reason: $buildError "
                $hwSuccess = $false
            } # End If homework builds
        }
        else
        {
            # Homework folder not found
            $hwResult = "No homework found in $hwFolder. "
            $hwSuccess = $false

        } # End If homework folder exists

        # Inform user
        if ($hwSuccess)
        {
            $hwVerdict = "SUCCESS! $hwResult"
            Write-Host $hwVerdict -ForegroundColor Green
        }
        else
        {
            $hwVerdict = "FAILURE! $hwResult"
            Write-Host $hwVerdict -ForegroundColor Red
        }

        # Write results to result table
        $allResults[$iNid, $iHw] = $hwVerdict

    } # End For Every homework

} # End For Every Neptun IDs

# No Workbook in case of test mode
if (-not $testMode)
{
    # Write results to Workbook
    $xlWorksheet.Range($xlWorksheet.Cells($FIRST_ROW, $HW_FIRST_COL), `
        $xlWorksheet.Cells($lastRow, $HW_FIRST_COL + $TESTINPUTS.Length - 1)).Value2 = $allResults

    # Save Workbook
    $xlWorkbook.Save()

    # Close Workbook and Excel
    $xlWorkbook.Close($false)
    $xlApp.Quit()

    # Remove junk
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlWorksheet) | Out-Null
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlWorkbook) | Out-Null
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlApp) | Out-Null
    [System.GC]::Collect()
    [System.GC]::WaitForPendingFinalizers()
    Remove-Variable xlWorksheet, xlWorkbook, xlApp
}
