﻿function RunExe 
{
    param(
        [Parameter(Mandatory=$true)]
        [string]$exePath,

        [string]$workingDir = "",

        [string]$arguments = "",

        [string]$stdIn = "",

        [int]$timeOut = 86400,

        [int]$maxOutChars = [int]::MaxValue,

        [switch]$mirrorStdIo
    )

    # Check arguments
    if (-not (Test-Path $exePath))
    {
        Write-Error "Executable not found at $exePath!"
        return $null, $null, $null
    }
    if ((-not ([string]::IsNullOrEmpty($workingDir))) -and (-not (Test-Path $workingDir)))
    {
        Write-Error "Working directory not found at $workingDir!"
        return $null, $null, $null
    }

    # Create stdin, stdout and stderr temp files
    $timeTag = (Get-Date).ToString("yyyyMMddHHmmssfff")
    $stdInFilePath = [System.IO.Path]::Combine($env:TEMP, "stdInTmp_$timeTag");
    $stdOutFilePath = [System.IO.Path]::Combine($env:TEMP, "stdOutTmp_$timeTag");
    $stdErrFilePath = [System.IO.Path]::Combine($env:TEMP, "stdErrTmp_$timeTag");
    Out-File -FilePath $stdInFilePath -InputObject $stdIn -Encoding utf8 -Force
    New-Item -Path $stdOutFilePath -ItemType File -Force | Out-Null
    New-Item -Path $stdErrFilePath -ItemType File -Force | Out-Null

    # Create job that mirrors stdout.
    $stdOutGetterJob = $null
    if ($mirrorStdIo)
    {
        $stdOutGetterJob = Start-Job -ScriptBlock {param($outFile) Get-Content $outFile -Encoding utf8  -Wait } -ArgumentList $stdOutFilePath
    }

    # Create job that mirrors stderr.
    $stdErrGetterJob = $null
    if ($mirrorStdIo)
    {
        $stdErrGetterJob = Start-Job -ScriptBlock {param($outFile) Get-Content $outFile -Encoding utf8  -Wait } -ArgumentList $stdErrFilePath
    }

    # Create job that runs the executable
    $cmd = ""
    $cmd = "$cmd chcp 65001 1>nul "
    if ($workingDir)
    {
        $cmd = "$cmd && cd ""$workingDir"" 1>nul "
    }
    $cmd = "$cmd && ""$exePath"" $arguments <""$stdInFilePath"" 1>>""$stdOutFilePath"" 2>>""$stdErrFilePath"" "
    $cmdScript = 
    {
        param([string]$command)
        & cmd /c $command
        $LASTEXITCODE
    }
    $runnerJob = Start-Job -ScriptBlock $cmdScript -ArgumentList $cmd

    # Let executable run until timout is reached. Mirror stdout and stderr in the meantime if required.
    $stopWatch = [System.Diagnostics.Stopwatch]::StartNew()
    do
    {
        if ($mirrorStdIo)
        {
            if ($stdOutGetterJob.HasMoreData)
            {
                $currentStdOut = Receive-Job $stdOutGetterJob
                if ($currentStdOut) { Write-Host (Out-String -InputObject $currentStdOut) -NoNewline }
            }
            if ($stdErrGetterJob.HasMoreData)
            {
                $currentStdErr = Receive-Job $stdErrGetterJob
                if ($currentStdErr) { Write-Host (Out-String -InputObject $currentStdErr) -NoNewline }
            }
        }
        # Timeout
        if ($stopWatch.Elapsed.TotalSeconds -gt $timeOut) { break }
    }
    while ($runnerJob.State -eq "Running")

    # Examine if executable exited before timeout (if runner job is completed) and set exit code.
    $success = ($runnerJob.State -eq "Completed")
    if ($success)
    { 
        $exitCode = Receive-Job -Job $runnerJob
    }
    else
    { 
        $exitCode = [int]::MinValue
    }
    # Stop and remove runner job.
    Stop-Job -Job $runnerJob
    Remove-Job -Job $runnerJob
    
    # Mirror last outputs to stdout 
    if ($mirrorStdIo)
    {
        # In theory, Get-Content -Wait will execute after maximally 1s, so that we get the final lines.
        Start-Sleep -Seconds 1
        if ($stdOutGetterJob.HasMoreData)
        {
            $currentStdOut = Receive-Job $stdOutGetterJob
            if ($currentStdOut) { Write-Host (Out-String -InputObject $currentStdOut) -NoNewline }
        }
        Stop-Job -Job $stdOutGetterJob
        Remove-Job -Job $stdOutGetterJob
        if ($stdErrGetterJob.HasMoreData)
        {
            $currentStdErr = Receive-Job $stdErrGetterJob
            if ($currentStdErr) { Write-Host (Out-String -InputObject $currentStdErr) -NoNewline }
        }
        Stop-Job -Job $stdErrGetterJob
        Remove-Job -Job $stdErrGetterJob
    }
    
    # Read stdout from file
    $stdOut = $null
    if (Test-Path $stdOutFilePath)
    {
        $reader = [System.IO.StreamReader]::new($stdOutFilePath)
        $stdOut = [char[]]::new($maxOutChars)
        $outChars = $reader.ReadBlock($stdOut, 0, $maxOutChars)
        [Array]::Resize([ref]$stdOut, $outChars)
        $stdOut = [string]::new($stdOut)
        $reader.Close()
        $reader.Dispose()
        Remove-Item $stdOutFilePath -Force
    }

    # Read stderr from file
    $stdErr = $null
    if (Test-Path $stdErrFilePath)
    {
        $reader = [System.IO.StreamReader]::new($stdErrFilePath)
        $stdErr = [char[]]::new($maxOutChars)
        $outChars = $reader.ReadBlock($stdErr, 0, $maxOutChars)
        [Array]::Resize([ref]$stdErr, $outChars)
        $stdErr = [string]::new($stdErr)
        $reader.Close()
        $reader.Dispose()
        Remove-Item $stdErrFilePath -Force
    }

    # Delete stdin file
    if (Test-Path $stdInFilePath)
    {
        Remove-Item $stdInFilePath -Force
    }
    
    # Return exitcode, stdout and stderr
    return $exitCode, $stdOut, $stdErr
}

function RunExeP
{
    param(
        [Parameter(Mandatory=$true)]
        [string]$exePath,

        [string]$workingDir = "",

        [string]$arguments = "",

        [string]$stdIn = "",

        [int]$timeOut = 86400,

        [int]$maxOutChars = [int]::MaxValue,

        [switch]$mirrorStdIo
    )

    $OutputEncoding = [System.Text.Encoding]::UTF8

    # Create new process
    $process = [System.Diagnostics.Process]::new()

    # Process start information
    $process.StartInfo.FileName = "cmd.exe"
    $process.StartInfo.Arguments = "/c chcp 65001 1>nul && ""$exePath"" $arguments"
    $process.StartInfo.WorkingDirectory = $workingDir
    $process.StartInfo.CreateNoWindow = $true
    $process.StartInfo.UseShellExecute = $false
    $process.StartInfo.RedirectStandardInput = $true
    $process.StartInfo.RedirectStandardOutput = $true
    $process.StartInfo.RedirectStandardError = $true
    $process.StartInfo.StandardOutputEncoding = [System.Text.Encoding]::UTF8
    $process.StartInfo.StandardErrorEncoding = [System.Text.Encoding]::UTF8

    # Create output capturing script block
    $stdOutFwd =
    {
        $sb = $Event.MessageData[0]
        $maxLen = $Event.MessageData[1]
        $str = $Event.SourceEventArgs.Data
        $freeLen = $maxLen - $sb.Length
        if ($freeLen -gt 0)
        {
            if ($str.Length -le $freeLen)
            {
                $sb.AppendLine($str)
            }
            else
            {
                $sb.AppendLine($str.Substring(0, $freeLen))
            }
        }
    }

    # Create string builder and event to capture standard output of process
    $stdOutSb = [System.Text.StringBuilder]::new()
    $stdOutEvent = Register-ObjectEvent -Action $stdOutFwd -InputObject $process `
        -EventName OutputDataReceived -MessageData ($stdOutSb, $maxOutChars)

    # Create string builder and event to capture standard error of process
    $stdErrSb = [System.Text.StringBuilder]::new()
    $stdErrEvent = Register-ObjectEvent -Action $stdOutFwd -InputObject $process `
        -EventName ErrorDataReceived -MessageData ($stdErrSb, $maxOutChars)

    # Start process, begin capturing output
    $process.Start() | Out-Null
    $process.BeginOutputReadLine()
    $process.BeginErrorReadLine()

    # Write input to process standard input with utf8 encoding
    $bytes = [System.Text.Encoding]::UTF8.GetBytes($stdIn)
    $process.StandardInput.BaseStream.Write($bytes, 0, $bytes.Length)
    $process.StandardInput.BaseStream.Close()

    # Wait until process exits and mirror stdout and strerr if necessary. Kill process on timeout.
    $hasExited = $true
    $stdOutStart = 0
    $stdErrStart = 0
    $stopWatch = [System.Diagnostics.Stopwatch]::StartNew()
    do
    {
        # Mirror stdout and stderr
        if ($mirrorStdIo)
        {
            # If there is new data on stdout
            if ($stdOutSb.Length -gt $stdOutStart)
            {
                Write-Host ($stdOutSb.ToString($stdOutStart, $stdOutSb.Length - $stdOutStart).Trim())
                $stdOutStart = $stdOutSb.Length
            }
            # If there is new data on stderr
            if ($stdErrSb.Length -gt $stdErrStart)
            {
                Write-Host ($stdErrSb.ToString($stdErrStart, $stdErrSb.Length - $stdErrStart).Trim())
                $stdErrStart = $stdErrSb.Length
            }
        }
        # Timeout
        if ($stopWatch.Elapsed.TotalSeconds -gt $timeOut)
        {
            $hasExited = $false
            break
        }
    }
    while (!$process.HasExited)

    # Must do this to capture all OutputDataReceived and ErrorDataReceived events!
    $process.WaitForExit(100) | Out-Null

    # Get exit code if process exited successfully. Kill process if it has not exited.
    if ($hasExited) 
    {
        $exitCode = $process.ExitCode
    }
    else
    { 
        $exitCode = [int]::MinValue
        $process.CancelErrorRead()
        $process.CancelOutputRead()
        # Must use taskkill to kill child processes as well
        & taskkill /pid $process.id /f /t | Out-Null
        # Free process resources
        $process.Close()
        $process.Dispose()
    }

    # Mirror output from the events that are fired after $process.HasExited is set to true as well.
    if ($mirrorStdIo)
    {
        # If there is new data on stdout
        if ($stdOutSb.Length -gt $stdOutStart)
        {
            Write-Host ($stdOutSb.ToString($stdOutStart, $stdOutSb.Length - $stdOutStart).Trim())
            $stdOutStart = $stdOutSb.Length
        }
        # If there is new data on stderr
        if ($stdErrSb.Length -gt $stdErrStart)
        {
            Write-Host ($stdErrSb.ToString($stdErrStart, $stdErrSb.Length - $stdErrStart).Trim())
            $stdErrStart = $stdErrSb.Length
        }
    }

    # Unregister output capturing events
    Unregister-Event -SourceIdentifier $stdOutEvent.Name
    Unregister-Event -SourceIdentifier $stdErrEvent.Name

    # Return with exit code, stdout and stderr
    $stdOut = $stdOutSb.ToString().Trim()
    $stdErr = $stdErrSb.ToString().Trim()

    return ($exitCode, $stdOut, $stdErr)
}