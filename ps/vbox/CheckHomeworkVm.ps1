# Using directives
using namespace System.IO

# Source all functions
. "$PSScriptRoot\VirtualBoxFunctions.ps1"
. "$PSScriptRoot\CreateReducedResultWorkbook.ps1"

# Parameters
$sharedFolder = "D:\VM\Shared"
$sharedFolderVM = "S:\"
$snycFolder = "D:\OneDrive\Shared\Programozas_CSharp"
$subjectFolder = "D:\Qsync\Munka\Egyetem\Edu\Programozas_CSharp\2019_2020_01"
$workbookFileName = "Eredmenyek.xlsx"
$checkerScriptName = "CheckHomework.ps1"
$builderScriptName = "BuildCs.ps1"
$runnerScriptName = "RunExe.ps1"
$batchScriptName = "callCheckHomework.bat"
$vmName = "Win_10_VM"
$vmUser = "virtualuser"
$vmPassword = "virtualpassword"
$startTime = 60
$stopTime = 30
$cmdPath = "C:\Windows\System32\cmd.exe"
$logFileNamePrefix = "Ellenorzes" # date comes after
$reducedNameSuffix = "_NK_nelkul"

# Script settings
$ErrorActionPreference = "Stop"

# Copy everything to working folder ################################################################
Write-Host "Copying content of synced homework folder and results workbook to shared folder of VM ..."

# Copy the content of synced homework folder to working folder
$workingFolderName = "HomeWorkCheck_$((Get-Date).ToString("yyyyMMdd_HHmmss"))"
$workingFolder = [Path]::Combine($sharedFolder, $workingFolderName)
if (Test-Path $workingFolder) { Remove-Item $workingFolder -Recurse -Force }
New-Item $workingFolder -ItemType Directory -Force | Out-Null
$syncFolderHomework = [Path]::Combine($snycFolder, "leadas", "*")
Copy-Item -Path $syncFolderHomework -Destination $workingFolder -Recurse -Force

# Create backup from result workbook in subject folder
$workbookPathSubjFolder = [Path]::Combine($subjectFolder, $workbookFileName)
Copy-Item -Path $workbookPathSubjFolder -Destination "$workbookPathSubjFolder.backup" -Force

# Copy result workbook to working folder
$workbookPathWorkingFolder = [Path]::Combine($workingFolder, $workbookFileName)
Copy-Item -Path $workbookPathSubjFolder -Destination $workbookPathWorkingFolder -Force

# Copy the homework checker script to working folder
$src = [Path]::Combine($PSScriptRoot, $checkerScriptName)
$dst = [Path]::Combine($workingFolder, $checkerScriptName)
Copy-Item -Path $src -Destination $dst -Force

# Copy .csproj builder script to working folder
$src = [Path]::Combine($PSScriptRoot, $builderScriptName)
$dst = [Path]::Combine($workingFolder, $builderScriptName)
Copy-Item -Path $src -Destination $dst -Force

# Copy .exe runner script to working folder
$src = [Path]::Combine($PSScriptRoot, $runnerScriptName)
$dst = [Path]::Combine($workingFolder, $runnerScriptName)
Copy-Item -Path $src -Destination $dst -Force

# Copy bat script to working folder
$src = [Path]::Combine($PSScriptRoot, $batchScriptName)
$dst = [Path]::Combine($workingFolder, $batchScriptName)
Copy-Item -Path $src -Destination $dst -Force

# Start VM if necessary ############################################################################
$runningAtStart = IsVmPowered -vmName $vmName
if (-not $runningAtStart)
{
    Write-Host "Starting VM and waiting $startTime seconds ..."
    PowerOnVm -vmName $vmName | Out-Null
    Start-Sleep -Seconds $startTime
}

# Run homework checking ############################################################################
# Run script on VM
Write-Host "Checking homeworks on VM ..."
$workingFolderPathVM = [Path]::Combine($sharedFolderVM, $workingFolderName)
$batchScriptPathVM = [Path]::Combine($workingFolderPathVM, $batchScriptName)
$arguments = "/c $batchScriptPathVM"
($exitcode, $stdout, $stderr) = RunExeOnVm -vmName $vmName -userName $vmUser -password $vmPassword `
    -exePath $cmdPath -arguments $arguments -mirrorStdIo

# Save log
$logPath = [Path]::Combine($workingFolder, "$($logFileNamePrefix)_$((Get-Date).ToString("yyyyMMdd_HHmmss"))")
Out-File -InputObject $stdout -FilePath $logPath -Force

# Stop VM if necessary #############################################################################
if (-not $runningAtStart)
{
    Write-Host "Stopping VM and waiting $stopTime seconds ..."
    PowerOffWinVm -vmName $vmName -userName $vmUser -password $vmPassword | Out-Null
    Start-Sleep -Seconds $stopTime
}

# Copy results back to subject folder and synced homework folder ###################################
Write-Host "Copying content of shared folder of VM back to subject folder and synced homework folder ..."

# Copy result workbook back to original location
Copy-Item -Path $workbookPathWorkingFolder -Destination $workbookPathSubjFolder -Force

# Creating reduced result workbook
$workbookPathSubjFolder = CreateReducedResultWorkbook -workbookPath $workbookPathSubjFolder `
    -reducedNameSuffix $reducedNameSuffix

# Copy reduced result workbook to synced folder
$syncFolderResult = [Path]::Combine($snycFolder, "anyagok")
Copy-Item -Path $workbookPathSubjFolder -Destination $syncFolderResult -Force