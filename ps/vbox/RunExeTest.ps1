﻿. "$PSScriptRoot\RunExe.ps1"

Clear-Host

$ErrorActionPreference = "Stop"

$exePath = "D:\Qsync\Ideiglenes\AccentTester\AccentTester\bin\Debug\AccentTester.exe"
$stdIn = "árvíztűrő tükörfúrógép"
$workingDir = "D:\"
$timeOut = 10
$exitCodeRef = 3

$stopWatchP = New-Object System.Diagnostics.Stopwatch
$stopWatch = New-Object System.Diagnostics.Stopwatch
for($i = 1; $i -le 10; $i++)
{
    $stopWatchP.Start()
    ($exitCodeP, $stdOutP, $stdErrP) = RunExeP -exePath $exePath -stdIn $stdIn -timeOut $timeOut `
        -workingDir $workingDir -mirrorStdIo
    $stopWatchP.Stop()
    <#
    $stopWatch.Start()
    ($exitCode, $stdOut, $stdErr) = RunExe -exePath $exePath -stdIn $stdIn -timeOut $timeOut `
        -workingDir $workingDir -mirrorStdIo
    $stopWatch.Stop()
    #>

    if (-not ($exitCodeP -eq $exitCodeRef)) { Write-Error "Error_P" } else { Write-Host "OK_P $i" }

    if (-not ($exitCode -eq $exitCodeRef)) { Write-Error "Error" } else { Write-Host "OK $i" }
    
}

Write-Host "Time_P $($stopWatchP.Elapsed.TotalSeconds)"
Write-Host "Time $($stopWatch.Elapsed.TotalSeconds)"