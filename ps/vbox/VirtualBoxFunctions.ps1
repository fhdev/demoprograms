﻿. "$PSScriptRoot\RunExe.ps1"

function IsVmPowered {
    param(
        [Parameter(Mandatory=$true)]
        [string]$vmName,

        [string]$vboxManagePath = "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe"
    )

    # This is necessary so that we are moving through the & operator if there is an error.
    # This is the only way we get a useful error message.
    $ErrorActionPreference = "Continue"
    
    # Perform command and redirect standard error to standard output so that we get the content
    # of the error message without being written to the standard error.
    $stdOutErr = & $vboxManagePath list runningvms 2>&1

    if($LASTEXITCODE -ne 0)
    {
        # Write own and original error message and stop execution.
        Write-Error "VBoxManage.exe list runningvms resulted in error! Cause:`n$stdOutErr" `
            -ErrorAction Stop
    }

    $isPowered = $false
    if ($stdOutErr) { $isPowered = $stdOutErr.Contains($vmName) }
    return $isPowered
}

function RunExeOnVm {
    param(
        [Parameter(Mandatory=$true)]
        [string]$vmName,

        [Parameter(Mandatory=$true)]
        [string]$userName,

        [string]$password = "",

        [Parameter(Mandatory=$true)]
        [string]$exePath,

        [string]$arguments = "",

        [int]$timeOut = 86400,

        [switch]$mirrorStdIo,

        [string]$vboxManagePath = "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe"
    )

    $vboxManageArguments = -join("guestcontrol ""$vmName"" run --username ""$userName"" ", 
        "--password ""$password"" --exe ""$exePath"" --wait-stdout --wait-stderr ", 
        "-- ""$exePath"" $arguments")
    ($exitcode, $stdout, $stderr) = RunExeP -exePath $vboxManagePath -arguments $vboxManageArguments `
        -timeOut $timeOut -mirrorStdIo:$mirrorStdIo
    
    return ($exitcode, $stdout, $stderr)
}

function PowerOnVm {
    param(
        [Parameter(Mandatory=$true)]
        [string]$vmName,
        
        [string]$vboxManagePath = "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe"
    )

    # This is necessary so that we are moving through the & operator if there is an error.
    # This is the only way we get a useful error message.
    $ErrorActionPreference = "Continue"

    # Perform command and redirect standard error to standard output so that we get the content
    # of the error message without being written to the standard error.
    $stdOutErr = & $vboxManagePath startvm $vmName --type gui 2>&1

    if($LASTEXITCODE -ne 0)
    {
        # Write own and original error message and stop execution.
        Write-Error "VBoxManage.exe startvm $vmName --type gui resulted in error! Cause:`n$stdOutErr" `
            -ErrorAction Stop
    }
    return $LASTEXITCODE -eq 0
}

function PowerOffWinVm {
    param(
        [Parameter(Mandatory=$true)]
        [string]$vmName,

        [Parameter(Mandatory=$true)]
        [string]$userName,

        [string]$password = "",
        
        [string]$vboxManagePath = "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe"
    )
    # This is necessary so that we are moving through the & operator if there is an error.
    # This is the only way we get a useful error message.
    $ErrorActionPreference = "Continue"

    # Perform command and redirect standard error to standard output so that we get the content
    # of the error message without being written to the standard error.
    $stdOutErr = & $vboxManagePath guestcontrol $vmName run --username $userName --password $password `
        --exe shutdown.exe -- shutdown.exe /s /f /t 0 2>&1

    if($LASTEXITCODE -ne 0)
    {
        # Write own and original error message and stop execution.
        Write-Error (-join("VBoxManage.exe guestcontrol $vmName run --username $userName ",
            "--password $password --exe shutdown.exe -- shutdown.exe /s /f /t 0 resulted in error! ",
            "Cause:`n$stdOutErr")) -ErrorAction Stop
    }
    return $LASTEXITCODE -eq 0
}