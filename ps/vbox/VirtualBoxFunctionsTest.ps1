﻿. "$PSScriptRoot\VirtualBoxFunctions.ps1"

$ErrorActionPreference = "Stop"

$runningAtStart  = $false

$runningAtStart = IsVmPowered -vmName "Win_10_VM"

if (-not $runningAtStart)
{
    $powerOnSuccess = PowerOnVm -vmName "Win_10_VM"
    Start-Sleep -Seconds 30
}

($exitcode, $stdout, $stderr) = RunExeOnVm -vmName "Win_10_VM" -userName "virtualuser" `
    -password "virtualpassword" -exePath "cmd.exe" -arguments "/c echo %path%" -mirrorStdIo $true

if (-not $runningAtStart)
{
    $powerOffSuccess = PowerOffWinVm -vmName "Win_10_VM" -userName "virtualuser" -password "virtualpassword"
    Start-Sleep -Seconds 30
}