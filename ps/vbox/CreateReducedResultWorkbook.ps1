function CreateReducedResultWorkbook
{
    param(
        [Parameter(Mandatory=$true)]
        [string]$workbookPath,

        [string]$reducedNameSuffix = '_reduced',

        [int]$nameColumnNum = 1
    )

    $workbookName = [System.IO.Path]::GetFileNameWithoutExtension($workbookPath)
    $workbookExt = [System.IO.Path]::GetExtension($workbookPath)
    $workbookDir = [System.IO.Path]::GetDirectoryName($workbookPath)

    $reducedWorkbookPath = [System.IO.Path]::Combine($workbookDir, "$workbookName$reducedNameSuffix$workbookExt")

    Copy-Item -Path $workbookPath -Destination $reducedWorkbookPath -Force

    $xlApp = New-Object -ComObject Excel.Application
    $xlWorkbook = $xlApp.Workbooks.Open($reducedWorkbookPath)
    $nWorksheets = $xlWorkbook.Sheets.Count

    for($iWorksheet = 1; $iWorksheet -le $nWorksheets; $iWorksheet++)
    {
        $xlWorksheet = $xlWorkbook.Sheets.Item($iWorksheet)
        $xlWorksheet.Columns($nameColumnNum).EntireColumn.Delete() | Out-Null
    }

    # Save Workbook
    $xlWorkbook.Save()

    # Close Workbook and Excel
    $xlWorkbook.Close($false)
    $xlApp.Quit()

    # Remove junk
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlWorksheet) | Out-Null
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlWorkbook) | Out-Null
    [System.Runtime.Interopservices.Marshal]::ReleaseComObject($xlApp) | Out-Null
    [System.GC]::Collect()
    [System.GC]::WaitForPendingFinalizers()
    Remove-Variable xlWorksheet, xlWorkbook, xlApp

    # Return new path
    return $reducedWorkbookPath
}
