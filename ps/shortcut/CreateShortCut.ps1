﻿param (
    [Parameter(Mandatory=$true)]
    [string] $SrcPath, 
    [Parameter(Mandatory=$true)]
    [string] $DstPath,
    [string] $WorkDir,
    [string] $Arguments
)
Write-Host ("Trying to create shortcut for """ + $SrcPath + """ to """ + $DstPath + """...   ")
Try
{
    $WshShell = New-Object -comObject WScript.Shell
    $Shortcut = $WshShell.CreateShortcut($DstPath)
    $Shortcut.TargetPath = $SrcPath
    $Shortcut.WorkingDirectory = $WorkDir
    $Shortcut.Arguments = $Arguments
    $Shortcut.Save()
    Write-Host ">> Shortcut created successfully."
}
Catch
{
    Write-Host ">> Error happened during shortcut creation."
    Throw
}