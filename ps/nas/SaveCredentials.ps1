﻿$credential = Get-Credential -Message "Please provide your credentials." -Verbose
$content = $credential.UserName
$content = -join($content, ";", (ConvertFrom-SecureString $credential.Password))
Set-Content "$PSScriptRoot\$env:COMPUTERNAME.txt" $content 