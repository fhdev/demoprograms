﻿function ConnectNasDrives {
    # Get credentials previously saved by Save-Credentials function #######################################################
    Write-Host "Specify credentials ..."
    $UserName = Read-Host -Prompt "Username"
    $Password = Read-Host -Prompt "Password" -AsSecureString
    $Credentials = New-Object System.Management.Automation.PsCredential($UserName, $Password)


    # Mapping shares ######################################################################################################
    Write-Host "Mapping NAS drives ..."
    $NasAddressLAN = "\\192.168.5.102"

    # Mapping home share
    $SharedFolder = "home"
    $DriveLetter = "Q"
    # Try to map on LAN
    $Share = "$NasAddressLAN\$SharedFolder"
    $MappedHome = MapDrive -Share $Share -DriveLetter $DriveLetter -Credentials $Credentials

    # Mapping Download share
    $SharedFolder = "Download"
    $DriveLetter = "R"
    # Try to map on LAN
    $Share = "$NasAddressLAN\$SharedFolder"
    $MappedDownload = MapDrive -Share $Share -DriveLetter $DriveLetter -Credentials $Credentials

    # Mapping Multimedia share
    $SharedFolder = "Multimedia"
    $DriveLetter = "S"
    # Try to map on LAN
    $Share = "$NasAddressLAN\$SharedFolder"
    $MappedMultimedia = MapDrive -Share $Share -DriveLetter $DriveLetter -Credentials $Credentials
}

function DisconnectNasDrives {
    # Unmapping shares ######################################################################################################
    Write-Host "Unapping NAS drives ..."
    $NasAddressLAN = "\\192.168.5.102"

    # Unmapping home share
    $SharedFolder = "home"
    $DriveLetter = "Q"
    $UnmappedHome = UnmapDrive -DriveLetter $DriveLetter

    # Unmapping Download share
    $SharedFolder = "Download"
    $DriveLetter = "R"
    $UnmappedDownload = UnmapDrive -DriveLetter $DriveLetter

    # Unmapping Multimedia share
    $SharedFolder = "Multimedia"
    $DriveLetter = "S"
    $UnmappedMultimedia = UnmapDrive -DriveLetter $DriveLetter

}

# This function maps Share to DriveLetter with Credentials
function MapDrive($Share, $DriveLetter, $Credentials)
{
    $NetExecutable = "C:\Windows\System32\net.exe"
    $Success = $false
    $Drive = "${DriveLetter}:"
    # Check if Drive is already mapped and unmap if so.
    if(Test-Path $Drive)
    {
        Write-Host "Drive $Drive is already mapped. Unmapping ..."
        $Success = UnmapDrive $DriveLetter
        if($Success -eq $false) {return $Success}
    }
    # Map Share to Drive
    Write-Host "Mapping shared folder $Share to drive $Drive ..."
    $stdOutErr = & net use $Drive $Share "/user:$($Credentials.GetNetworkCredential().UserName)" $($Credentials.GetNetworkCredential().Password) /persistent:no 2>&1
    if($LASTEXITCODE -eq 0)
    {
        Write-Host "Mapping succeeded." -ForegroundColor Green
        $Success = $true
    }
    else
    {
        Write-Host "Mapping failed. Cause: $stdOutErr" -ForegroundColor Red
    }

    return $Success
}

# This function unmaps drive DriveLetter.
function UnmapDrive($DriveLetter)
{
    $NetExecutable = "C:\Windows\System32\net.exe"
    $Success = $false
    $Drive = "${DriveLetter}:"
    # Check if Drive is already mapped and unmap if so.
    if(Test-Path $Drive)
    {
        Write-Host "Unmapping drive $Drive ..."
        $Process = Start-Process -FilePath $NetExecutable -ArgumentList " use $Drive /delete /yes" -PassThru -Wait -WindowStyle Hidden
        if($Process.ExitCode -eq 0)
        {
            Write-Host "Unmapping succeeded." -ForegroundColor Green
            $Success = $true
        }
        else
        {
            Write-Host "Unmapping failed" -ForegroundColor Red
        }
    }
    else
    {
        Write-Host "Drive $Drive is not connected."
        $Success = $true
    }

    return $Success
}