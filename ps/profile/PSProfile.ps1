Write-Host "Executing profile $PSCommandPath ..."

# LOAD / INSTALL MODULES ###############################################################################################
if (Get-Module -ListAvailable -Name "Posh-Git") {
    Import-Module Posh-Git
}
else {
    PowerShellGet\Install-Module posh-git -Scope CurrentUser -Force
    Import-Module Posh-Git
}

# FUNCTIONS ############################################################################################################
# Prompt
function Prompt {
    # Posh-Git Prompt settings
    $GitPromptSettings.DefaultPromptPath = ""
    $GitPromptSettings.DefaultPromptPrefix.Text = ""
    $GitPromptSettings.DefaultPromptSuffix.Text = ""
    $GitPromptSettings.DefaultPromptDebug.Text = ""
    $GitPromptSettings.DefaultPromptTimingFormat.Text = ""
    $GitPromptSettings.DefaultPromptDebug.ForegroundColor =  $GitPromptSettings.DefaultColor.ForegroundColor
    $GitPromptSettings.PathStatusSeparator = ""
    # Escape character
    $ESC = [char]27
    # Determine location
    $location = $(Get-Location).ToString().Replace($HOME, '~')
    # Assemble prompt
    "`n" + `
    "$ESC[91mPS " + `
    "$ESC[92m$(whoami)$ESC[97m@$ESC[92m$(hostname) " + `
    "$ESC[95m$location " + `
    "$(& $GitPromptScriptBlock)" + `
    "`n" + `
    "$ESC[m|> "
}

# Function to list the date and time when computer was started
function ListStarts {
    param(
        [Parameter(Position=0)]
        [Int32]$days = 5
    )
    $START_EVENT_ID = 12
    $STOP_EVENT_ID = 13
    $now = (Get-Date).Date
    $startTime = $now - (New-TimeSpan -Day $days)
    $filter = @{
        LogName = "System";
        Id = (12, 13);
        ProviderName = "Microsoft-Windows-Kernel-General";
        StartTime = $startTime
    }
    $allStartStopEvents = Get-WinEvent -FilterHashTable $filter -Oldest
    $lastDate = $null
    $lastId = 0
    foreach($event in $allStartStopEvents) {
        if ($lastDate -ne $event.TimeCreated.Date) {
            Write-Host
            Write-Host "_____________________"
            Write-Host "$($event.TimeCreated.Date.ToString('yyyy.MM.dd'))" -NoNewline
        }
        if ($event.Id -eq $START_EVENT_ID) {
            Write-Host
            Write-Host "  $($event.TimeCreated.TimeOfDay.ToString('hh\:mm\:ss')) - " -NoNewline
        }
        if ($event.Id -eq $STOP_EVENT_ID) {
            if ($lastDate -ne $event.TimeCreated.Date) {
                Write-Host
                Write-Host "           - " -NoNewline
            }
            Write-Host "$($event.TimeCreated.TimeOfDay.ToString('hh\:mm\:ss'))" -NoNewline
        }
        $lastDate = $event.TimeCreated.Date
        $lastId = $event.Id
    }
    if ($lastId -ne $STOP_EVENT_ID) {
        Write-Host
    }
}

# Function to mimic Unix wget functionality (used to download jenkins logs)
function DownloadFile {
    param(
        [Parameter(Position=0, Mandatory=$true)]
        [string]$url,

        [Parameter(Position=1)]
        [string]$path = $null
    )
    if (-not $path)
    {
        $path = [System.IO.Path]::Combine((Get-Location).Path, [System.IO.Path]::GetFileName($url))
    }
    $client = New-Object System.Net.WebClient
    $client.DownloadFile($url, $path)
}

# Function to get the current full user name
function GetFullUserName {
    return ([adsi] "WinNT://$env:userdomain/$env:username,user" ).fullname
}

# Function to get the uptime of the computer
function GetUptime {
    $uptime = (Get-Date) - (Get-CimInstance Win32_OperatingSystem).LastBootUpTime
    Write-Host $uptime
}

# Function to create a new empty file
function CreateEmptyFile {
    param(
        [Parameter(Position=0, Mandatory=$true)]
        [string]$path
    )
    if (Test-Path $path) {
        Write-Host "File $path already exists."
    }
    else {
        New-Item -Path $path -Type File | Out-Null
    }
}

function StartVsDevShell {
    param(
        [Parameter(Position=0, Mandatory=$false)]
        [string]$vsInstallPath = "C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional"
    )
    $vsDevShellPath = "$vsInstallPath\Common7\Tools\Microsoft.VisualStudio.DevShell.dll"
    if (Test-Path $vsDevShellPath)
    {
        Import-Module $vsDevShellPath
        Enter-VsDevShell -VsInstallPath $vsInstallPath -SkipAutomaticLocation
    }
    else
    {
        Write-Host "Warning: Visual Studio Developer Shell is not available."
    }

}

function ConvertWindows1252ToUtf8 {
    param(
        [Parameter(Position=0, Mandatory=$true)]
        [string]$filePath
    )
    # Convert to absolute path as System.IO does not handle current directory properly for relative paths
    $filePath = Convert-Path -Path $filePath
    Write-Host "Converting $filePath ..."
    # Save backup
    Copy-Item -Path $filePath -Destination "$filePath.bak" -Force
    # Convert from Windows 1252 to UTF8 without BOM
    $encodingWindows1252 = [System.Text.Encoding]::GetEncoding(1252);
    $encodingUtf8WithoutBom = [System.Text.UTF8Encoding]::new($false)
    $content = [System.IO.File]::ReadAllLines($filePath, $encodingWindows1252)
    [System.IO.File]::WriteAllLines($filePath, $content, $encodingUtf8WithoutBom)
}

# SETTINGS #############################################################################################################
$OutputEncoding = [Console]::InputEncoding = [Console]::OutputEncoding = [System.Text.Encoding]::UTF8

Set-PSReadLineOption -Colors @{
    ContinuationPrompt = 'White'
    Emphasis           = 'White'
    Error              = 'Red'
    Selection          = 'White'
    Default            = 'White'
    Comment            = 'White'
    Keyword            = 'DarkRed'
    String             = 'Green'
    Operator           = 'White'
    Variable           = 'Gray'
    Command            = 'Yellow'
    Parameter          = 'Gray'
    Type               = 'Cyan'
    Number             = 'White'
    Member             = 'White'
#   InlinePrediction   = 'White'
}

Set-PSReadLineKeyHandler -Chord UpArrow     -Function HistorySearchBackward
Set-PSReadLineKeyHandler -Chord DownArrow   -Function HistorySearchForward
Set-PSReadLineKeyHandler -Chord Ctrl+r      -Function ReverseSearchHistory
Set-PSReadLineKeyHandler -Chord Ctrl+s      -Function ForwardSearchHistory

# SOURCES ##############################################################################################################
. ([System.IO.Path]::Combine($PSScriptRoot, "..", "repo", "GitUtils.ps1"))
. ([System.IO.Path]::Combine($PSScriptRoot, "..", "misc", "DetectSmartCard.ps1"))
. ([System.IO.Path]::Combine($PSScriptRoot, "..", "nas",  "MountDriveFcns.ps1"))

# ALIASES ##############################################################################################################
# Functions in profile
if ([System.Environment]::OSVersion.Platform -eq "Win32NT") {
Set-Alias wget      DownloadFile                                    -Option AllScope -Force
Set-Alias touch     CreateEmptyFile                                 -Option AllScope -Force
}
Set-Alias gcu       GetCurrentUser                                  -Option AllScope -Force
Set-Alias lstarts   ListStarts                                      -Option AllScope -Force

# Functions in dot sourced files
Set-Alias card      DetectSmartCard                                 -Option AllScope -Force

Set-Alias nasc      ConnectNasDrives                                -Option AllScope -Force
Set-Alias nasd      DisconnectNasDrives                             -Option AllScope -Force

Set-Alias gits      GitStatusShortUntracked                         -Option AllScope -Force
Set-Alias gith      GitHelp                                         -Option AllScope -Force

# EVENTS ###############################################################################################################
# Save current folder on exit
# Register-EngineEvent PowerShell.Exiting -SupportEvent -Action {
#     Set-Content -Path "${env:USERPROFILE}\.pscwd" -Value (Get-Location).Path
# }

# CURRENT FOLDER #######################################################################################################
# Load current folder
# if (Test-Path -Path "${env:USERPROFILE}\.pscwd") {
#     Set-Location -Path (Get-Content -Path "${env:USERPROFILE}\.pscwd" -Raw).Trim()
# }

Write-Host "Finished executing profile $PSCommandPath."
