﻿param (
    [string]$Date = [DateTime]::Today.ToString('yyyyMMdd'),
    [string]$EmailAddress = ''
)

Add-Type -Assembly 'Microsoft.Office.Interop.Outlook' | Out-Null

$Outlook = New-Object -ComObject Outlook.Application

# Create meeting

$Meeting = $Outlook.CreateItem('olAppointmentItem')

$Meeting.Subject = 'Out of Office'
$Meeting.Body = 'I''m out of office'
$Meeting.Location = 'home'

$Meeting.ReminderSet = $false
$Meeting.ResponseRequested = $false
$Meeting.BusyStatus = [Microsoft.Office.Interop.Outlook.OlBusyStatus]::olFree

$Meeting.Importance = [Microsoft.Office.Interop.Outlook.OlImportance]::olImportanceNormal
$Meeting.MeetingStatus = [Microsoft.Office.Interop.Outlook.OlMeetingStatus]::olMeeting
$Meeting.Start = [DateTime]::ParseExact(-join($Date,'_000000'), 'yyyyMMdd_HHmmss', [System.Globalization.CultureInfo]::InvariantCulture)
$Meeting.AllDayEvent = $true

# Send meeting
if($EmailAddress -ne '')
{
    Write-Host "Sending meeting to $EmailAddress ..."
    $Meeting.Recipients.Add($EmailAddress) | Out-Null
    $Meeting.Send()
}

# Set out of office status and category
$Meeting.BusyStatus = [Microsoft.Office.Interop.Outlook.OlBusyStatus]::olOutOfOffice
$Meeting.Categories = 'Away'

# Save meeting
Write-Host 'Saving meeting ...'
$Meeting.Save()