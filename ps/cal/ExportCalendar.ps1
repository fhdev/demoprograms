﻿param (
    [string]$EmailAddress = "", 
    [string]$Directory = "",
    [int]$NumberOfDays = 7
)

if(($EmailAddress -ne "") -or ($Directory -ne ""))
{

    Add-Type -Assembly "Microsoft.Office.Interop.Outlook" | Out-Null

    $Application = New-Object -ComObject "Outlook.Application"

    $Calendar = $Application.Session.GetDefaultFolder([Microsoft.Office.Interop.Outlook.olDefaultFolders]::olFolderCalendar)

    $Exporter = $Calendar.GetCalendarExporter()
    $Exporter.CalendarDetail = [Microsoft.Office.Interop.Outlook.OlCalendarDetail]::olFullDetails
    $Exporter.IncludeAttachments = $true
    $Exporter.IncludePrivateDetails = $false
    $Exporter.RestrictToWorkingHours = $false
    $Exporter.StartDate = [DateTime]::Today.Date
    $Exporter.EndDate = [DateTime]::Today.Date.AddDays($NumberOfDays)

    if ($Directory -ne "")
    {
        $Tag = [DateTime]::Now.ToString("yyyyMMdd_HHmmss")
        $Exporter.SaveAsICal("$Directory\Calendar_$Tag.ical")
        Write-Host "Calendar saved to $Directory"
    }

    if ($EmailAddress -ne "")
    {
        $Mail = $Exporter.ForwardAsICal([Microsoft.Office.Interop.Outlook.OlCalendarMailFormat]::olCalendarMailFormatDailySchedule)
        $Mail.To = $EmailAddress
        $Mail.Send()
        Write-Host "Calendar sent to $EmailAddress."
    }

}
else
{
    Write-Host "Please specify -EmailAddress or -Directory"
}