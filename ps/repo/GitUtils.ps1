# This function shows git status with all untracked files (-uall) in short format (-s)
function GitStatusShortUntracked {
    git status -uall -s
}

function GitHelp {
    $help = @"
____________________________________________________________________________________________________________________
clone a remote repository                                           | git clone <remote>

show status in short format with all untracked files included       | git status -suall

show last <number> entries from log                                 | git log -n <number>
show last <number> entries from log in one line format              | git log -n <number> --oneline
show branch graph                                                   | git log --all --graph --oneline --decorate

list all branches                                                   | git branch -a
create new branch (do not check out)                                | git branch <branch>
set up current branch to track remote branch                        | git branch -u origin/<branch>
delete branch                                                       | git branch -d <branch>

create tag                                                          | git tag -a <name> -m <message> <commit>
delete tag                                                          | git tag -d <name>

merge another branch into current active branch                     | git merge <other_branch>
abort merge (conflicts)                                             | git merge --abort

rebase current branch to the tip of branch                          | git rebase <branch>

show difference of working copy from index (compare words)          | git diff --color-words
show difference of working copy from last commit                    | git diff HEAD
show difference of index from last commit                           | git diff HEAD --staged
show comit content                                                  | git show <commit_hash>

show repository content after last commit                           | git ls-tree -r HEAD

add changes to index, including new files                           | git add
add modifications to index, but no new files or deletions           | git add -u

delete file from working copy and add deletion to index             | git rm <file>

move file in working copy and add moving to index                   | git mv <file_old> <file_new>

undo changes in index                                               | git reset
undo the last commit (keep working copy) !!!                        | git reset HEAD~1
reset current branch head and index to <commit> !!!                 | git reset <commit>
reset current branch head, index, and working copy to <commit> !!!  | git reset --hard <commit>

reverts changes in <commit> (does not yet commit the revert)        | git revert -n <commit>

save index and working copy to stash (with untracked)               | git stash push -um <message>
apply changes to index and working copy from stash                  | git stash apply
delete item from stash                                              | git stash drop <item>

undo changes in working copy (keeps untracked)                      | git checkout
check out existing branch (overwrite working copy)                  | git checkout <branch>
check out commit (detached head)                                    | git checkout <commit>
create and check out new local branch                               | git checkout -b <branch> <tracking_branch>

commit with message                                                 | git commit -m <message>

upload to remote                                                    | git push
upload new branch to remote                                         | git push -u origin <branch>
delete remote branch                                                | git push -d origin <branch>

load changes from remote to tracking branches (prune deleted)       | git fetch -p

apply changes from remote to local branch (with rebase)             | git pull --rebase

remove all untracked files and folders                              | git clean -fdx
remove all ignored files and folders                                | git clean -fdX
____________________________________________________________________________________________________________________
"@

Write-Host $help

}