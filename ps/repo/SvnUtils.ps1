# Function to update all SVN repositories inside a folder. [svnua]
function SvnUpdateAll
{
	Get-ChildItem -Path . -Directory | ForEach-Object -Process { svn update $_.FullName --force }
}

# Function to show SVN log [svnl]
function SvnShowLog
{
	param(
		[Parameter(Position=0)]
		[string]$url,

		[Parameter(Position=1)]
		[int]$limit = 3

	)
	svn log --stop-on-copy --verbose --limit $limit $url
}

# Function to show SVN log with differences [svnld]
function SvnShowLogDiff
{
	param(
		[Parameter(Position=0)]
		[string]$url,

		[Parameter(Position=1)]
		[int]$limit = 3
	)
	svn log --stop-on-copy --verbose --diff --limit $limit $url
}

# Function to get latest SVN revision of a repository [svnr]
function SvnGetLatestRevision
{
	param(
		[Parameter(Position=0)]
		[string]$url
	)
	svn info --show-item last-changed-revision $url
}

# Function to create a new developer branch [svnb]
function SvnCreateDevBranch
{
	param(
		[Parameter(Position=0)]
		[string]$tag="",

		[Parameter(Position=1)]
		[string]$from="",

		[Parameter(Position=2)]
		[string]$to=""

	)
	$fromRevision = & svn info $from --show-item last-changed-revision
	$fromRevision = $fromRevision.Trim()
	$to = "$to$tag"
	svn copy $from $to -m "New development branch from $from@$fromRevision."
	$folderName =  [System.IO.Path]::GetFileName($to)
	svn checkout $to $folderName
}