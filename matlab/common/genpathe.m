function pathString = genpathe(rootDir, excludeDirs)
%% Check and prepare arguments
% rootDir
if ~(ischar(rootDir) && isvector(rootDir))
    error('ERROR: Root directory name must be a char vector!');
elseif exist(rootDir, 'dir') ~= 7
    error('ERROR: Root directory does not exist!');
end
if (strcmpi(computer, 'pcwin64') && (rootDir(2) ~= ':')) || ...
   (strcmpi(computer, 'glnxa64') && (rootDir(1) ~= filesep))
    rootDir = fullfile(pwd, rootDir);
end
% excludeDirs
if ~iscell(excludeDirs)
    excludeDirs = {excludeDirs};
end
excludeDirs = excludeDirs(:);
for iDir = 1 : length(excludeDirs)
    if ~(ischar(excludeDirs{iDir}) && isvector(excludeDirs{iDir}))
        error('ERROR: Exclude directory names must be char vectors!');
    elseif ((length(excludeDirs{iDir}) > 2) && strcmpi(excludeDirs{iDir}(1:2), ['.', filesep]))
        excludeDirs{iDir} = fullfile(rootDir, excludeDirs{iDir});
    end
end
%% Assemble path string
pathString = [rootDir, pathsep];
% Get subdirectories
dirStruct = dir(rootDir);
childPath = fullfile({dirStruct.folder}, {dirStruct.name});
subDirPath = childPath(([dirStruct.isdir] == 1) & ~matches({dirStruct.name}, {'.', '..'}));
% Filter subdirectories
subDirPathFilt = subDirPath(~endsWith(subDirPath, excludeDirs) & ~contains(subDirPath, {'@'}));
for i = 1 : numel(subDirPathFilt)
    pathString = [pathString, genpathe(subDirPathFilt{i}, excludeDirs)]; %#ok<AGROW>
end
end