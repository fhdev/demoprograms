function tcmd(dir, tab)
%% Default arguments
if nargin < 2
    tab = 'L';
    if nargin < 1
        dir = pwd;
    end
end
%% Check arguments
if ~java.io.File(dir).isDirectory()
    error('ERROR: Directory ''%s'' does not exist!', dir);
end
if ~any(strcmp(tab, {'L', 'R'}))
    error('ERROR: Tab must be ''L'' or ''R''!');
end
%% Find Total Commander
pathes = {'C:\Program Files\Total Commander\totalcmd64.exe';
          'C:\Program Files (x86)\Total Commander\totalcmd64.exe';
          'C:\Program Files\TotalCommander\totalcmd64.exe';
          'C:\Program Files (x86)\TotalCommander\totalcmd64.exe';
          'C:\TotalCommander\totalcmd64.exe'};
found = false;
for i = 1 : length(pathes)
    if java.io.File(pathes{i}).exists()
        found = true;
        break;
    end
end
if ~found
    error('ERROR: totalcmd64.exe is not found at default install locations!');
end
%% Open dir with totalcmd64.exe
command = ['start "" "', pathes{i}, '" /O /T /', tab, '="', dir, '"'];
system(command);
end