function npp(file)
%% Default arguments
if nargin < 1
    file = matlab.desktop.editor.getActive();
    if isempty(file)
        error('ERROR: No file is opened in editor!');
    else
        file = file.Filename;
    end
end
%% Check arguments
if ~java.io.File(file).exists()
    error('ERROR: File ''%s'' does not exist!', file);
end
%% Find notepad++ / kate 
switch computer
    case 'PCWIN64'
        name = 'notepad++.exe';
        pathes = { 'C:\Program Files\Notepad++\notepad++.exe'; ...
                   'C:\Program Files (x86)\Notepad++\notepad++.exe' };
    case 'GLNXA64'
        name = 'kate';
        pathes = { '/usr/bin/kate'};
    otherwise
        error('ERROR: Not implemented for MacOS!');
end
notepad = '';
for iPath = 1 : length(pathes)
    if exist(pathes{iPath}, 'file')
        notepad = pathes{iPath};
        break
    end
end
if isempty(notepad)
    error('ERROR: File %s is not found at default install locations!', name);
end

%% Assemble command for calling notepad++ or kate
switch computer
    case 'PCWIN64'
        command = sprintf('"%s"', notepad);
    case 'GLNXA64'
        command = sprintf('env LD_LIBRARY_PATH="" "%s"', notepad);
    otherwise
        error('ERROR: Not implemented for MacOS!');
end
command = strcat(command, sprintf(' "%s" &', file));

%% Open file with notepad++ or kate 
[exitCode, stdOutErr] = system(command);
if exitCode ~= 0
    error('ERROR: Running %s failed. Reason: %s.', name, stdOutErr);
end
end