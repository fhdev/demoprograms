function flag = isabspath(path)
%% Check argument
if ~(ischar(path) && isvector(path) && (length(path) > 1))
    error('ERROR: path must be a character vector with minimal length of 2.');
end
flag = (path(1) == filesep) || (path(2) == ':');
end