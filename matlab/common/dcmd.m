function dcmd(dir, tab)
%% Default arguments
if nargin < 2
    tab = 'L';
    if nargin < 1
        dir = pwd;
    end
end
%% Check arguments
if ~java.io.File(dir).isDirectory()
    error('ERROR: Directory ''%s'' does not exist!', dir);
end
if ~any(strcmp(tab, {'L', 'R'}))
    error('ERROR: Tab must be ''L'' or ''R''!');
end
%% Find Double Commander
switch computer
    case 'PCWIN64'
        pathes = { 'C:\Program Files\Double Commander\doublecmd.exe'; ...
                   'C:\Program Files (x86)\Double Commander\doublecmd.exe'; ...
                   'C:\HFE2BP\Programs\DoubleCommander\doublecmd.exe'};
    case 'GLNXA64'
        pathes = { '/usr/bin/doublecmd'};
    otherwise
        error('ERROR: Not implemented for MacOS!');
end
dcmd = [];
for iPath = 1 : length(pathes)
    if exist(pathes{iPath}, 'file')
        dcmd = pathes{iPath};
        break
    end
end
if isempty(dcmd)
    error('ERROR: File doublecmd.exe is not found at default install locations!');
end

%% Assemble command for calling doublecmd
switch computer
    case 'PCWIN64'
        command = sprintf('"%s"', dcmd);
        confDir = 'D:\ProgramSettings\DoubleCommander';
    case 'GLNXA64'
        command = sprintf('env LD_LIBRARY_PATH="" "%s"', dcmd);
        confDir = '/home/hferi/ProgramSettings/DoubleCommander';
    otherwise
        error('ERROR: Not implemented for MacOS!');
end
if exist(confDir, 'dir')
    command = strcat(command, sprintf(' --config-dir="%s"', confDir));
end
command = strcat(command, sprintf(' -C -T -P %s "%s"', tab, dir));

%% Call doublecmd
[exitCode, stdOutErr] = system(command);
if exitCode ~= 0
    error('ERROR: Running doublecmd failed. Reason: %s.', stdOutErr);
end
end