% close Simulink models and requirements if there is any open
if ~isempty(license('inuse', 'simulink'))
    slclr();
end
% close current Project if there is one open
rPrj = matlab.project.rootProject();
if ~isempty(rPrj)
    rPrj.close();
end
% switch off diary
diary('off');
% close all figures
close('all', 'force');
% close all files
fclose('all');
% delete slprj folder
if exist(fullfile(pwd, 'slprj'), 'dir')
    rmdir(fullfile(pwd, 'slprj'), 's');
end
% delete .slxc files
slxcFiles = dir('**/*.slxc');
slxcPaths = fullfile({slxcFiles.folder}, {slxcFiles.name});
for slxcPath = slxcPaths
    delete(slxcPath{1});
end
% clear all variables
clear('all'); %#ok<CLALL>
% delete all timers
delete(timerfindall());
% clear console
clc;