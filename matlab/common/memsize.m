function sizeStr = memsize(var) %#ok<INUSD> used by whos
if nargin < 1
    info = evalin('caller', 'whos');
    bytes = sum([info.bytes]);
else
    info = whos('var');
    bytes = info.bytes;
end
units = { 'B', 'KB', 'MB', 'GB'};
level = floor(log(bytes) / log(1024));
bytes = bytes / 1024^level;
if nargout > 0
    sizeStr = sprintf("%g %s", bytes, units{level + 1});
else
    fprintf(1, "%g %s\n", bytes, units{level + 1});
end
end