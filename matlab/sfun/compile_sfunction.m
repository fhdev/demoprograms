%   [function description] compile_sfunction
%
%       Compile C-MEX S-Function for Windows with Microsoft Visual C++ compiler.
%       (Microsoft Visual C++ 2008 2010 2013 2015)
%
%   [syntax]
%
%       e.g.: compile_sfunction('sfun.c', config);
%
%   [input parameters]
%
%       srcFiles        A single string or cellarray of strings that contains the absolute or 
%                       relative path(s) of the source file(s) which need to be compiled. If 
%                       multiple source files are used for the C-MEX S-Function, the source file 
%                       which contains the code of the S-Function callbacks and the #define 
%                       'S_FUNCTION_NAME' must be the first in the list.
%
%       config          A structure that contains compiler configuration. The following can be
%                       configured:
%
%                       config.platform       Target platform. Must be 'win32' or 'win64'.
%                       config.outDir         Output directory for compilation process.
%                       config.matlabDir      Directory of MATLAB installation to use.
%                       config.vcDir          Directory of Microsoft Visual C++ installation.
%                       config.clOpts         Additional command line arguments for cl.exe 
%                                               (overwrites defaults in case of mismatch)
%                       config.linkOpts       Additional command line arguments for link.exe 
%                                               (overwrites defaults in case of mismatch)
%                       config.mexOnly        Keeps only the MEX file after build (true / false).
%                       config.verbose        Performs verbose build (true / false).
%
%   [return parameters]
%
%       status          Value of %ERRORLEVEL% environment variable after processing command. Zero 
%                       if no errors occured.
%
%       result          The content of the Command Prompt
%
%       cmd             The executed build command.
%
%---------------------------------------------------------------------------------------------------
function [status, result, cmd] = CSSim_compile_sfunction(srcFiles, config)
    %% CHECKING ARGUMENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Create cellarray of S-Function files if only one is provided
    if ~iscell(srcFiles)
        srcFiles = {srcFiles};
    end
    % If no config is provided, function will use default settings
    if nargin < 2
        config = [];
    end
    % Automatically set unspecified configuration settings
    % Platform
    % If no setting is provided, obtain it automatically.
    if ~isfield(config, 'platform')
        config.platform = 'win32';
        if strcmp(computer, 'PCWIN64')
            config.platform = 'win64';
        end
    end
    % Output directory
    if ~isfield(config, 'outDir')
        config.outDir = '.';
    end
    % Matlab root directory
    % If no Matlab root directory is provided, obtain it automatically.
    if ~isfield(config, 'matlabDir')
        % If the platform is specified in configuration but does not match current MATLAB version,
        % the obtained root directory would be false.
        if (strcmp(computer, 'PCWIN64') && strcmpi(config.platform, 'win32'))...
                                        ||...
           (strcmp(computer, 'PCWIN') && strcmpi(config.platform, 'win64'))
            error(['The specified platform ''', config.platform,...
                  ''' does not match current MATLAB version. ',...
                  'Specify a matching MATLAB root directory in configuration.']);
        end
        config.matlabDir = matlabroot;
    end
    % Visual Studio Command Prompt root directory
    % If no Visual Studio install directory is provided, search for Visual Studio installation automatically
    if isfield(config, 'vcDir')
        vcInstallDirs = { config.vcDir };
    else
        vcInstallDirs = { 'C:\Program Files (x86)\Microsoft Visual Studio 9.0\VC',...
                          'C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC',...
                          'C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC',...
                          'C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC'      };
    end
    % Command Prompt batch file for 32-bit
    vcBatPaths = { '\bin\vcvars32.bat' };
    % Possible Command Prompt batch files for 64-bit
    if strcmpi(config.platform, 'win64')
        vcBatPaths = { '\bin\amd64\vcvarsamd64.bat', '\bin\amd64\vcvars64.bat', '\bin\x86_amd64\vcvarsx86_amd64.bat' };
    end
    % Flag for valid Visual Studio installation
    vcBatFound = false;
    % Check the given installation directory or all possible installation directories
    for i=1:length(vcInstallDirs)
        % Check all possible Command Prompt batch files
        for j=1:length(vcBatPaths)
            vcBatPath = [vcInstallDirs{i}, vcBatPaths{j}];
            if exist(vcBatPath, 'file') == 2
                vcBatFound = true;
                break;
            end
        end
        if vcBatFound
            break;
        end
    end
    % Command Prompt batch file is not found in given installation directory or all possible installation directories
    if ~vcBatFound
        error('Microsoft Visual Studio is not installed properly in the given directory, or not found automatically!');
    end
    % Custom compiler options
    if ~isfield(config, 'clOpts')
        config.clOpts = '';
    end
    % Custom linker options
    if ~isfield(config, 'linkOpts')
        config.linkOpts = '';
    end
    % Keep only mex files
    if ~isfield(config, 'mexOnly')
        config.mexOnly = true;
    end
    % Verbose mode
    if ~isfield(config, 'verbose')
        config.verbose = false;
    end
    % Check validity of arguments
    % Check if specified S-Function source files are existing
    for i=1:length(srcFiles)
        if exist(srcFiles{i}, 'file') ~= 2
            error(['The S-Function source file ''', srcFiles{i}, ''' does not exist.']);
        end
    end
    % Platform
    % Check validity of platform setting
    if ~strcmpi(config.platform, 'win32') && ~strcmpi(config.platform, 'win64')
        error('The configuration platform should be either ''win32'' or ''win64''.');
    end
    % Check validity of output directory
    if exist(config.outDir, 'dir') ~= 7
        error(['The output directory ''', config.outDir, ''' does not exist.']);
    end
    % Check validity of Matlab root directory
    if exist(config.matlabDir, 'dir') ~= 7
        error(['The MATLAB root directory ''', config.matlabDir, ''' does not exist.']);
    end
    % Check validity of Visual Studio Command Prompt batch file
    % If the Visual Studio Command prompt batch file not exists, throw error
    if exist(vcBatPath, 'file') ~= 2
        error(['The Visual Studio Command Prompt batch file ''', vcBatPath, ''' does not exist.']);
    end
    % Check keep only mex files option
    if ~islogical(config.mexOnly)
        error('The option to generate MEX-file only should be a logical value.');
    end
    % Check verbosity option
    if ~islogical(config.verbose)
        error('The verbosity option should be a logical value.');
    end
    %% CONFIGURATION DEPENDENT SETTINGS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Additional include directories for compiler
    matlabIncDir = [config.matlabDir, '\extern\include'];
    simulinkIncDir = [config.matlabDir, '\simulink\include'];
    % Additional library directories for linker
    matlabLibDir = [config.matlabDir, '\extern\lib\win32\microsoft'];
    if strcmpi(config.platform, 'win64')
        matlabLibDir = [config.matlabDir, '\extern\lib\win64\microsoft'];
    end
    % Target machine setting for linker
    linkMachine = 'X86';
    if strcmpi(config.platform, 'win64')
        linkMachine = 'X64';
    end
    % Target extension setting for linker
    mexExt = '.mexw32';
    if strcmpi(config.platform, 'win64')
        mexExt = '.mexw64';
    end
    % Source list for compiler and object list for linker
    srcList = [];
    objList = [];
    for i=1:length(srcFiles)
        srcList = [srcList, srcFiles{i}, ' ']; %#ok<AGROW>
        [~, srcName, ~] = fileparts(srcFiles{i});
        objList = [objList, '"', config.outDir, '\', srcName, '.obj" ']; %#ok<AGROW>
    end
    % Output name for linker
    [~, sFunName, ~] = fileparts(srcFiles{1});
    baseOut = [config.outDir, '\', sFunName];
    mexOut = [baseOut, mexExt];
    pdbOut = [mexOut, '.pdb'];
    implibOut = [mexOut, '.implib'];
    mapOut = [mexOut, '.map'];
    manifestOut = [mexOut, '.manifest'];
    %% MEX COMPILER CONFIGURATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Last -rightmost- option always wins in case of conflict!
    %% Compiler options
    % Compiler executable
    clExe = 'cl';
    % Compiler define options
    clDefOpts = ['/D_CRT_SECURE_NO_DEPRECATE /D_SCL_SECURE_NO_DEPRECATE /D_SECURE_SCL=0 ',...
                 '/DMX_COMPAT_32 /DMATLAB_MEX_FILE'];
    % Compiler additional include options
    clIncOpts = ['/I"', matlabIncDir, '" /I"', simulinkIncDir,'"'];
    % Compiler optimization options
    clOptimOpts = '/O2 /Oy- /DNDEBUG';
    % Compiler other options
    clOtherOpts = ['/c /GR /W3 /EHs /nologo /MD /Fo"', config.outDir, '\\"'];
    %% Linker options
    % Linker executable
    linkExe = 'link';
    % Linker additional library options
    linkLibOpts = ['/LIBPATH:"', matlabLibDir, '" libmx.lib libmex.lib libmat.lib kernel32.lib ',...
                   'user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ',...
                   'ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib'];
    % Linker optimization options
    linkOptimOpts = '';
    % Linker debug options 
    linkDebugOpts = ['/DEBUG /PDB:', pdbOut];
    % Linker other options
    linkOtherOpts = ['/dll /export:mexFunction /implib:"', implibOut, '" /MAP:"', mapOut,...
                     '" /MANIFEST /MANIFESTFILE:"', manifestOut, '" /MACHINE:', linkMachine,...
                     ' /nologo /incremental:NO /out:"', mexOut, '"'];
    %% Manifest tool options
    % Manifest tool executable
    manifestExe = 'mt';
    % Manifest tool options
    manifestOpts = ['-outputresource:"', mexOut, ';2" -manifest "', manifestOut, '"'];
    %% COMPILATION PROCESS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Construction of dos command string for compilation
    % Calling Visual Studio Command Prompt
    cmd = ['"', vcBatPath, '" '];
    % Calling compiler
    cmd = strcat(cmd, [' && ', clExe, ' ', clDefOpts, ' ', clIncOpts, ' ', clOptimOpts, ' ',...
                 clOtherOpts, ' ', config.clOpts, ' ', srcList]);
    % Calling linker
    cmd = strcat(cmd, [' && ', linkExe, ' ', linkLibOpts, ' ', linkOptimOpts, ' ', linkDebugOpts,...
                 ' ', linkOtherOpts, ' ', config.linkOpts, ' ', objList]);
    % Calling manifest tool
    cmd = strcat(cmd, [' && ', manifestExe, ' ', manifestOpts]);
    % Execute command
    if config.verbose
        % Write out dos command
        disp('--------------------');
        disp(cmd);
    end
    [status, result] = dos(cmd);
    % If build process failed
    if status ~= 0 || config.verbose
        if ~config.verbose
            % Write out dos command
            disp('--------------------');
            disp(cmd);
        end
        % Write the content of dos window
        disp(result);
        disp(' ');
        % Write errorlevel
        disp(['Process exited with errorlevel: ', num2str(status)]);
        if status ~= 0
            % Set error
            error(['Error during compilation of S-Function ', sFunName, '.']);
        end
    end
    % If the option to keep only the .mex32/.mex64 file is selected, delete all other outputs
    if config.mexOnly
        genExts = {[mexExt, '.exp'], '.lib', [mexExt, '.manifest'], '.obj', [mexExt, '.pdb'],...
                   [mexExt '.implib'], [mexExt, '.map'], [mexExt, '.pdb']};
        for i=1:length(genExts)
            genPath = [baseOut, genExts{i}];
            if exist(genPath, 'file') == 2
                delete(genPath);
            end
        end
    end
end