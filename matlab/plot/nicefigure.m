function fig = nicefigure(name, widthCm, heightCm)
%% Default values
if nargin < 3
    heightCm = 5;
    if nargin < 2
        widthCm = 8;
        if nargin < 1
            name = 'Figure';
        end
    end
end
%% Check arguments
if ~(ischar(name) && isvector(name))
    error('ERROR: name is not a valid character vector!');
end
if ~(isnumeric(widthCm) && isscalar(widthCm))
    error('ERROR: widthCm is not a valid numeric scalar!');
end
if ~(isnumeric(heightCm) && isscalar(heightCm))
    error('ERROR: heightCm is not a valid numeric scalar!');
end
%% Create figure
paperSize = [widthCm, heightCm];
position = [0, 0, paperSize];
paperPosition = [0, 0, paperSize];
fig = figure( ...
    'Name', name, ...
    'Color', 'White', ...
    'Unit', 'Centimeters', ...
    'PaperUnit', 'Centimeters', ...
    'Position', position, ...
    'PaperPosition', paperPosition, ...
    'PaperSize', paperSize ...
    );
end
