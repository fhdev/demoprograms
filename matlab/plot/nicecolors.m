%% nicecolors **************************************************************************************
% [Summary]
%   This class provides constant fields for color definitions.
%
% [Used in]
%   user
%
% [Subclasses]
%   none
%
% **************************************************************************************************
classdef nicecolors
    properties (Constant)
        % Green colors
        GreenTurquoise    = [ 26, 188, 156] / 255;
        GreenEmerald      = [ 46, 204, 113] / 255;
        GreenSea          = [ 22, 160, 133] / 255;
        GreenNephritis    = [ 39, 174,  96] / 255;
        GreenPureApple    = [106, 176,  76] / 255;
        % Blue colors
        BluePeterRiver    = [ 52, 152, 219] / 255;
        BlueBelizeHole    = [ 41, 128, 185] / 255;
        BlueBlurple       = [ 72,  52, 212] / 255;
        BlueDeepKoamaru   = [ 48,  51, 107] / 255;
        % Purple colors
        PurpleAmethyst    = [155,  89, 182] / 255;
        PurpleWisteria    = [142,  68, 173] / 255;
        PurpleSteelPink   = [190,  46, 221] / 255;
        % Yellow colors
        YellowSunFlower   = [241, 196,  15] / 255;
        YellowTurbo       = [249, 202,  36] / 255;
        % Orange colors
        OrangeCarrot      = [230, 126,  34] / 255;
        OrangePumpkin     = [211,  84,   0] / 255;
        % Red colors
        RedAlizarin       = [231,  76,  60] / 255;
        RedPomegrante     = [192,  57,  43] / 255;
        RedCarmine        = [235,  77,  75] / 255;
    end
end