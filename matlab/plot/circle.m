function axOut = circle(axIn, x, y, r, varargin)
%% Default arguments
if nargin < 1 || isempty(axIn)
    axIn = gca;
end
if nargin < 2 || isempty(x)
    x = 0;
end
if nargin < 3 || isempty(y)
    y = 0;
end
if nargin < 4 || isempty(r)
    r = 1;
end

%% Check arguments
if ~isa(axIn, 'matlab.graphics.axis.Axes')
    error('ERROR: axIn must be a "matlab.graphics.axis.Axes" object.');
end
if ~isnumeric(x) || ~isvector(x)
    error('ERROR: x must be a numeric vector!');
end
x = x(:);
if ~isnumeric(y) || ~isvector(x)
    error('ERROR: y must be a numeric vector!');
end
y = y(:);
if ~isnumeric(r) || ~isvector(x)
    error('ERROR: r must be a numeric vector!');
end
r = r(:);
if (length(x) ~= length(y)) || (length(y) ~= length(r))
    error('ERROR: x, y, and r must have the same length!');
end

t = 0 : 0.01*pi : 2*pi;
xt = r * cos(t) + x;
yt = r * sin(t) + y;
plot(axIn, xt', yt', varargin{:});

%% Give output
if nargout > 0
    axOut = axIn;
end

end