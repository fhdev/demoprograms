function lgd = nicelegend(legends, ax, fontSize)
%% Default arguments
if nargin < 3
    fontSize = 8;
    if nargin < 2
        ax = gca();
    end
end
%% Check arguments
if ~(iscell(legends) && isvector(legends))
    error('ERROR: legends is not a cell array!');
else
    for iLegend = 1 : numel(legends)
        if ~(ischar(legends{iLegend}) && isvector(legends{iLegend}))
            error('ERROR: legends is not a cell array of strings!');
        end
    end
end
if ~isa(ax, 'matlab.graphics.axis.Axes')
    error('ERROR: fig is not a valid matlab.graphics.axis.Axes object!');
end
if ~(isnumeric(fontSize) && isscalar(fontSize))
    error('ERROR: fontSize is not a valid numeric scalar!');
end
%% Create legend
% Prepare
lgd = legend(ax, legends, 'InterPreter', 'Latex', 'FontSize', fontSize, 'Location', 'Best');
end