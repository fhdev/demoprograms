function startup()
% Start measuring time
tStart = tic();
% Add own path items
utilsDir = fileparts(mfilename('fullpath'));
% Add utilities to path
addpath(genpath(fullfile(utilsDir, 'common')));
addpath(genpathe(utilsDir, {'.git'}));
% Set long format
format longG
% Set default figure to docked
% set(0, 'DefaultFigureWindowStyle', 'docked');
warning('off', 'MATLAB:Figure:SetPosition');
% Set Simulink encoding to utf-8
slCharacterEncoding('UTF-8');
% Print startup time
fprintf(1, 'Startup took %g seconds.\n', toc(tStart));
end









