function [X, Y] = ode4(obj, t, X0, U)
%% Check and prepare arguments
if ~ismethod(obj, 'StateEquation')
    error('ERROR: Dynamic system (obj) must have public function StateEquation!');
end

if ~(isnumeric(t) && isvector(t) && issorted(t, 'strictascend'))
    error('ERROR: Time values (t) must be a strictly ascending numeric vector!');
end
t = t(:);
nt = length(t);

if ~(isnumeric(X0) && isvector(X0))
    error('ERROR: Initial state (X0) must be a numeric vector!');
end
X0 = X0(:);
nX = length(X0);

if ~(isnumeric(U) && ismatrix(U) && (size(U, 2) == nt))
    error('ERROR: Input (U) must be a numeric matrix with length(t) columns!');
end

%% Initial run and check of state equation
try
    [dX0, Y0] = obj.StateEquation(t(1), X0, U(:, 1));
catch ex
    disp(ex.message);
    error('ERROR: Error when calling method obj.StateEquation.');
end

if ~(isnumeric(dX0) && isvector(dX0) && (length(dX0) == nX))
    error('ERROR: First return value of obj.StateEquation (dX) must be a numeric vector with length(X0) elements!');
end

if ~((isnumeric(Y0) && isvector(Y0)) || isempty(Y0))
    error('ERROR: Second return value of obj.StateEquation (Y) must be a numeric vector or an empty scalar!');
end
Y0 = Y0(:);
nY = length(Y0);

%% Preallocate memory and set initial values
% Pre-allocate memory for states and set initial value
X = nan(nX, nt);
X(:, 1) = X0;
% Pre-allocate memory for intermediate solution values
K = nan(nX, 4);

if ~isempty(Y0)
    % Pre-allocate memory for outputs if there are any
    Y = nan(nY, nt);
    Y(:, 1) = Y0;
    % Pre-allocate memory for intermediate output values
    L = nan(nY, 4);
else
    Y = [];
end



%% Solve state equation
for i = 1 : nt - 1
    % Current step size
    h = t(i + 1) - t(i);
    % Input at the middle
    Um = U(:, i) + (U(:, i + 1) - U(:, i)) / 2;
    % 4th order Runge-Kutta coefficients
    [K(:, 1), L(:, 1)] = obj.StateEquation(t(i),         X(:, i),                   U(:, i));
    [K(:, 2), L(:, 2)] = obj.StateEquation(t(i) + h / 2, X(:, i) + h / 2 * K(:, 1), Um);
    [K(:, 3), L(:, 3)] = obj.StateEquation(t(i) + h / 2, X(:, i) + h / 2 * K(:, 2), Um);
    [K(:, 4), L(:, 4)] = obj.StateEquation(t(i + 1),     X(:, i) + h * K(:, 3),     U(:, i + 1));
    % Calculate next state
    X(:, i + 1) = X(:, i) + h / 6 * (K(:, 1)  + 2 * K(:, 2)  + 2 * K(:, 3)  + K(:, 4));
    % Store current output as well if there are outputs
    if ~isempty(Y0)
        Y(:, i + 1) = 1 / 6 * (L(:, 1) + 2 * L(:, 2) + 2 * L(:, 3) + L(:, 4));
    end
end

end
