function [X, Y, t] = ode4rt(obj, tmax, X0)

%% Check and prepare arguments

if ~ismethod(obj, 'StateEquation')
    error('ERROR: Dynamic system (obj) must have public function StateEquation!');
end

if ~(isnumeric(X0) && isvector(X0))
    error('ERROR: Initial state (X0) must be a numeric vector!');
end
X0 = X0(:);
nX = length(X0);

%% Initialize
obj.Initialize();

%% Initial run and check of state equation
% Time of simulation
tsim = 0;
try
    U0 = obj.InputProvider(tsim);
    [dX0, Y0] = obj.StateEquation(tsim, X0, U0);
catch ex
    disp(ex.message);
    error('ERROR: Error when calling method obj.StateEquation.');
end

if ~(isnumeric(dX0) && isvector(dX0) && (length(dX0) == nX))
    error('ERROR: First return value of obj.StateEquation (dX) must be a numeric vector with length(X0) elements!');
end

if ~((isnumeric(Y0) && isvector(Y0)) || isempty(Y0))
    error('ERROR: Second return value of obj.StateEquation (Y) must be a numeric vector or an empty scalar!');
end
Y0 = Y0(:);
nY = length(Y0);

%% Preallocate memory and set initial values

% Real-time sample time
dt = obj.SampleTimeRt;

% Maximal number of real-time steps
nRtSteps = tmax / dt;
assert(mod(nRtSteps, 1) == 0, 'Maximal time must be must be a multiple integer of real-time step!');

% Pre-allocate memory for time and set initial value
t = nan(1, nRtSteps + 1);
t(1) = tsim;

% Pre-allocate memory for states and set initial value
X = nan(nX, nRtSteps + 1);
X(:, 1) = X0;
% Pre-allocate memory for intermediate solution values
K = nan(nX, 4);

if ~isempty(Y0)
    % Pre-allocate memory for outputs if there are any
    Y = nan(nY, nRtSteps + 1);
    Y(:, 1) = Y0;
    % Pre-allocate memory for intermediate output values
    L = nan(nY, 4);
else
    Y = [];
end

%% Solve state equation

% Ode solution sample time
h = obj.SampleTime;
% Number of ode solution steps inside one real-time step
nSolSteps = dt / h;
assert(mod(nSolSteps, 1) == 0, 'Real-time step must be a multiple integer of solution step!')

% Start to measure time
t0 = tic;
for iRt = 1 : nRtSteps
    % Input provider
    U = obj.InputProvider(tsim);
    % State at t
    Xminor = X(:, iRt);
    % Do minor steps
    for iSolStep = 1 : nSolSteps
        % 4th order Runge-Kutta coefficients
        [K(:, 1), L(:, 1)] = obj.StateEquation(tsim,         Xminor,                   U);
        [K(:, 2), L(:, 2)] = obj.StateEquation(tsim + h / 2, Xminor + h / 2 * K(:, 1), U);
        [K(:, 3), L(:, 3)] = obj.StateEquation(tsim + h / 2, Xminor + h / 2 * K(:, 2), U);
        [K(:, 4), L(:, 4)] = obj.StateEquation(tsim + h,     Xminor + h * K(:, 3),     U);
        % Calculate next state
        Xminor = Xminor + h / 6 * (K(:, 1)  + 2 * K(:, 2)  + 2 * K(:, 3)  + K(:, 4));
        % Store current output as well if there are outputs
        if ~isempty(Y0) && (iSolStep == nSolSteps)
            Yminor = 1 / 6 * (L(:, 1) + 2 * L(:, 2) + 2 * L(:, 3) + L(:, 4));
        end
        % Increase time
        tsim = tsim + h;
    end
    % Store results
    X(:, iRt + 1) = Xminor;
    if ~isempty(Y0)
        Y(:, iRt + 1) = Yminor;
    end
    % Wait to be real time toc(t0);
    while toc(t0) < tsim
        t(iRt + 1) = toc(t0);
    end
    % Output provider
    obj.OutputProvider(tsim, Xminor, Yminor);
    % Tasks 5x 
    if (mod(iRt, 5) == 0) && ismethod(obj, 'Task5x')
        obj.Task5x(tsim, Xminor, Yminor);
    end
    % Task 10x
    if (mod(iRt, 10) == 0) && ismethod(obj, 'Task10x')
        obj.Task10x(tsim, Xminor, Yminor);
    end
    % Task 20x
    if (mod(iRt, 20) == 0) && ismethod(obj, 'Task20x')
        obj.Task20x(tsim, Xminor, Yminor);
    end
end
fprintf('Simulation finished in %g seconds.\n', toc(t0));

%% Finalize
obj.Finalize();

end
