%% polylines3d
% Calculate point-line and line-line distances.in 3D.
% Author:           Ferenc Hegedus <ferenc.hegedus@outlook.com>
% Date of creation: 2021.05.18.
function polylines3d()
%% initialize
close all;

%% distancePoint2Line
CREATE_PLOTS = false;
inputs = {
    % point, linePoints(1), linePoints(2)
    struct('x',  0, 'y',  0, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y',  0, 'z',  0), 'all points at same place';
    struct('x',  0, 'y',  0, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z',  0), 'point at segment endpoint';
    struct('x',  0, 'y',  5, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z',  0), 'point on line';
    struct('x',  5, 'y',  5, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z',  0), 'point is closest to line segment 1 (x-y)';
    struct('x',  0, 'y',  5, 'z',  5), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z',  0), 'point is closest to line segment 1 (y-z)';
    struct('x', -5, 'y',  5, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z',  0), 'point is closest to line segment 2';
    struct('x',  0, 'y', 10, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x', 10, 'y', 10, 'z',  0), 'point is closest to line segment 3 (x-y)';
    struct('x',  0, 'y',  0, 'z', 10), struct('x', 0, 'y', 0, 'z', 0), struct('x', 10, 'y',  0, 'z', 10), 'point is closest to line segment 3 (x-z)';
    struct('x',  5, 'y',  0, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x', 10, 'y', 10, 'z',  0), 'point is closest to line segment 4';
    struct('x',  5, 'y', -5, 'z',  0), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z',  0), 'point is closest to segment endpoint 1 (x-y)';
    struct('x',  0, 'y',  5, 'z', -5), struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y',  0, 'z', 10), 'point is closest to segment endpoint 1 (y-z)';
    struct('x',  1, 'y',  1, 'z',  0), struct('x', 2, 'y', 4, 'z', 0), struct('x',  6, 'y',  6, 'z',  0), 'point is closest to segment endpoint 2 (x-y)';
    struct('x',  1, 'y',  0, 'z',  1), struct('x', 2, 'y', 0, 'z', 4), struct('x',  6, 'y',  0, 'z',  6), 'point is closest to segment endpoint 2 (x-z)';
    struct('x',  1, 'y',  4, 'z',  0), struct('x', 5, 'y', 2, 'z', 0), struct('x', 11, 'y',  4, 'z',  0), 'point is closest to segment endpoint 3 (x-y)';
};
outputs = {
    % distance(true), closestPoint(true), distance(false), closestPoint(false)
    0,             struct('x',   0, 'y',   0, 'z', 0), 0,             struct('x', 0,   'y',    0, 'z',  0);
    0,             struct('x',   0, 'y',   0, 'z', 0), 0,             struct('x', 0,   'y',    0, 'z',  0);
    0,             struct('x',   0, 'y',   5, 'z', 0), 0,             struct('x', 0,   'y',    5, 'z',  0);
    5,             struct('x',   0, 'y',   5, 'z', 0), 5,             struct('x', 0,   'y',    5, 'z',  0);
    5,             struct('x',   0, 'y',   5, 'z', 0), 5,             struct('x', 0,   'y',    5, 'z',  0);
    5,             struct('x',   0, 'y',   5, 'z', 0), 5,             struct('x', 0,   'y',    5, 'z',  0);
    5 * sqrt(2),   struct('x',   5, 'y',   5, 'z', 0), 5 * sqrt(2),   struct('x', 5,   'y',    5, 'z',  0);
    5 * sqrt(2),   struct('x',   5, 'y',   0, 'z', 5), 5 * sqrt(2),   struct('x', 5,   'y',    0, 'z',  5);
    2.5 * sqrt(2), struct('x', 2.5, 'y', 2.5, 'z', 0), 2.5 * sqrt(2), struct('x', 2.5, 'y',  2.5, 'z',  0);
    5 * sqrt(2),   struct('x',   0, 'y',   0, 'z', 0), 5,             struct('x', 0,   'y',   -5, 'z',  0);
    5 * sqrt(2),   struct('x',   0, 'y',   0, 'z', 0), 5,             struct('x', 0,   'y',    0, 'z', -5);
    sqrt(10),      struct('x',   2, 'y',   4, 'z', 0), sqrt(5),       struct('x', 0,   'y',    3, 'z',  0);
    sqrt(10),      struct('x',   2, 'y',   0, 'z', 4), sqrt(5),       struct('x', 0,   'y',    0, 'z',  3);
    sqrt(20),      struct('x',   5, 'y',   2, 'z', 0), sqrt(10),      struct('x', 2,   'y',    1, 'z',  0);
};

for iInput = 1 : size(inputs, 1)
    %% Test
    point = [inputs{iInput, 1}];
    linePoints = [inputs{iInput, 2:3}];
    type = [inputs{iInput, 4}];
    
    distance_ = [outputs{iInput, 1}];
    closestPoint_ = [outputs{iInput, 2}];
    [distance1, closestPoint1] = distancePoint2Line(point, linePoints, true);
    assert(isequal(distance1, distance_) && isequal(closestPoint1, closestPoint_), ...
        sprintf('Test "%s" failed with clamping.', type));
    
    distance_ = [outputs{iInput, 3}];
    closestPoint_ = [outputs{iInput, 4}];
    [distance2, closestPoint2] = distancePoint2Line(point, linePoints, false);
    assert(isequal(distance2, distance_) && isequal(closestPoint2, closestPoint_), ...
        sprintf('Test "%s" failed without clamping.', type));
    
    fprintf(1, 'Test "%s" passed.\n', type)
    
    %% Plot
    if ~CREATE_PLOTS
        continue
    end
    fig = figure('Color', 'white');
    ax = axes(fig);
    hold(ax, 'on');
    grid(ax, 'on');
    plot3(ax, [linePoints.x], [linePoints.y], [linePoints.z], 'go-', 'LineWidth', 1, 'MarkerSize', 8);
    plot3(ax, point.x, point.y, point.z, 'rs', 'LineWidth', 2, 'MarkerSize', 12);
    plot3(ax, closestPoint1.x, closestPoint1.y, closestPoint1.z, 'md', 'LineWidth', 1, 'MarkerSize', 10);
    plot3(ax, [point.x, closestPoint1.x], [point.y, closestPoint1.y], [point.z, closestPoint1.z], 'y-', 'LineWidth', 1);
    plot3(ax, closestPoint2.x, closestPoint2.y, closestPoint2.z, 'bv', 'LineWidth', 1, 'MarkerSize', 8);
    plot3(ax, [point.x, closestPoint2.x], [point.y, closestPoint2.y], [point.z, closestPoint2.z], 'c-', 'LineWidth', 1); 
    axis(ax, 'equal');
    ax.XTick = ax.XTick(1):ax.XTick(end);
    ax.YTick = ax.YTick(1):ax.YTick(end);
    ax.ZTick = ax.ZTick(1):ax.ZTick(end);
    title(ax, sprintf('%s\nDc=%g, D=%g', type, distance1, distance2));
    legend(ax, {'line', 'point', 'closest point of line (clamp)', 'distance (clamp)', ...
        'closest point of line ( no clamp)', 'distance (no clamp)'}, 'Location', 'Best');
    xlabel(ax, 'x');
    ylabel(ax, 'y');
    zlabel(ax, 'z');
end

%% distanceLine2Line
CREATE_PLOTS = true;
inputs = {
    % point, linePoints(1), linePoints(2)
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x',  0, 'y',   0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), 'overlapping 1';
    struct('x', 0, 'y', 1, 'z', 0), struct('x',  0, 'y',  9, 'z', 0), struct('x',  0, 'y',   0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), 'overlapping 2';
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x',  1, 'y',   0, 'z', 0), struct('x',  1, 'y', 10, 'z', 0), 'parallel 1';
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x',  1, 'y',   1, 'z', 0), struct('x',  1, 'y',  9, 'z', 0), 'parallel 2';
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x',  0, 'y',  11, 'z', 0), struct('x',  0, 'y', 21, 'z', 0), 'colinear';
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x', -5, 'y',   5, 'z', 0), struct('x',  5, 'y',  5, 'z', 0), 'intersecting 1';
    struct('x', 0, 'y', 0, 'z', 0), struct('x', 10, 'y', 10, 'z', 0), struct('x', 10, 'y',   0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), 'intersecting 2';
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x', -5, 'y',   5, 'z', 5), struct('x',  5, 'y',  5, 'z', 5), 'skew 1';
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y',  5, 'z', 0), struct('x',  5, 'y',  10, 'z', 5), struct('x', 15, 'y', 10, 'z', 5), 'skew 2';
    struct('x', 0, 'y', 0, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x',  1, 'y',   1, 'z', 0), struct('x', 10, 'y', 10, 'z', 0), 'general 1';
    struct('x', 3, 'y', 3, 'z', 0), struct('x',  7, 'y',  5, 'z', 0), struct('x',  2, 'y',   1, 'z', 0), struct('x',  5, 'y', -2, 'z', 0), 'general 2';
    struct('x', 1, 'y', 1, 'z', 0), struct('x',  4, 'y',  1, 'z', 0), struct('x',  3, 'y',   2, 'z', 0), struct('x',  3, 'y',  5, 'z', 0), 'general 3';
    struct('x', 0, 'y', 1, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x',  1, 'y',   1, 'z', 1), struct('x', 10, 'y', 10, 'z', 1), 'general 4';
    struct('x', 0, 'y', 1, 'z', 0), struct('x',  0, 'y', 10, 'z', 0), struct('x',  1, 'y', 0.5, 'z', 1), struct('x', 10, 'y',  5, 'z', 1), 'general 5';
};
outputs = {
    % distance(true), closestPoint(true), distance(false), closestPoint(false)
    0,           struct('x', 0, 'y',  0, 'z', 0), struct('x', 0, 'y',   0, 'z', 0), 0, struct('x', 0, 'y',  0, 'z', 0), struct('x', 0, 'y',  0, 'z', 0);
    0,           struct('x', 0, 'y',  1, 'z', 0), struct('x', 0, 'y',   1, 'z', 0), 0, struct('x', 0, 'y',  1, 'z', 0), struct('x', 0, 'y',  1, 'z', 0);
    1,           struct('x', 0, 'y',  0, 'z', 0), struct('x', 1, 'y',   0, 'z', 0), 1, struct('x', 0, 'y',  0, 'z', 0), struct('x', 1, 'y',  0, 'z', 0);
    1,           struct('x', 0, 'y',  1, 'z', 0), struct('x', 1, 'y',   1, 'z', 0), 1, struct('x', 0, 'y',  0, 'z', 0), struct('x', 1, 'y',  0, 'z', 0);
    1,           struct('x', 0, 'y', 10, 'z', 0), struct('x', 0, 'y',  11, 'z', 0), 0, struct('x', 0, 'y',  0, 'z', 0), struct('x', 0, 'y',  0, 'z', 0);
    0,           struct('x', 0, 'y',  5, 'z', 0), struct('x', 0, 'y',   5, 'z', 0), 0, struct('x', 0, 'y',  5, 'z', 0), struct('x', 0, 'y',  5, 'z', 0);
    0,           struct('x', 5, 'y',  5, 'z', 0), struct('x', 5, 'y',   5, 'z', 0), 0, struct('x', 5, 'y',  5, 'z', 0), struct('x', 5, 'y',  5, 'z', 0);
    5,           struct('x', 0, 'y',  5, 'z', 0), struct('x', 0, 'y',   5, 'z', 5), 5, struct('x', 0, 'y',  5, 'z', 0), struct('x', 0, 'y',  5, 'z', 5);
    5 * sqrt(3), struct('x', 0, 'y',  5, 'z', 0), struct('x', 5, 'y',  10, 'z', 5), 5, struct('x', 0, 'y', 10, 'z', 0), struct('x', 0, 'y', 10, 'z', 5);
    1,           struct('x', 0, 'y',  1, 'z', 0), struct('x', 1, 'y',   1, 'z', 0), 0, struct('x', 0, 'y',  0, 'z', 0), struct('x', 0, 'y',  0, 'z', 0);
    sqrt(5),     struct('x', 3, 'y',  3, 'z', 0), struct('x', 2, 'y',   1, 'z', 0), 0, struct('x', 1, 'y',  2, 'z', 0), struct('x', 1, 'y',  2, 'z', 0);
    1,           struct('x', 3, 'y',  1, 'z', 0), struct('x', 3, 'y',   2, 'z', 0), 0, struct('x', 3, 'y',  1, 'z', 0), struct('x', 3, 'y',  1, 'z', 0);
    sqrt(2),     struct('x', 0, 'y',  1, 'z', 0), struct('x', 1, 'y',   1, 'z', 1), 1, struct('x', 0, 'y',  0, 'z', 0), struct('x', 0, 'y',  0, 'z', 1);
    1.5,         struct('x', 0, 'y',  1, 'z', 0), struct('x', 1, 'y', 0.5, 'z', 1), 1, struct('x', 0, 'y',  0, 'z', 0), struct('x', 0, 'y',  0, 'z', 1);
};

for iInput = 1 : size(inputs, 1)
    %% Test
    linePoints1 = [inputs{iInput, 1:2}];
    linePoints2 = [inputs{iInput, 3:4}];
    type = [inputs{iInput, 5}];
    
    distance1_ = [outputs{iInput, 1}];
    closestPoints1_ = [outputs{iInput, 2:3}];
    [distance1, closestPoints1] = distanceLine2Line(linePoints1, linePoints2, true);
    assert((abs(distance1 - distance1_) < 1e-12) && isequal(closestPoints1, closestPoints1_), ...
        sprintf('Test "%s" failed with clamping.', type));
    
    distance2_ = [outputs{iInput, 4}];
    closestPoints2_ = [outputs{iInput, 5:6}];
    [distance2, closestPoints2] = distanceLine2Line(linePoints1, linePoints2, false);
    assert((abs(distance2 - distance2_) < 1e-12) && isequal(closestPoints2, closestPoints2_), ...
        sprintf('Test "%s" failed without clamping.', type));
    
    fprintf(1, 'Test "%s" passed.\n', type)
    
    %% Plot
    if ~CREATE_PLOTS
        continue
    end
    fig = figure('Color', 'white');
    ax = axes(fig);
    hold(ax, 'on');
    grid(ax, 'on');
    plot3(ax, [linePoints1.x], [linePoints1.y], [linePoints1.z], 'go-', 'LineWidth', 2, 'MarkerSize', 8);
    plot3(ax, [linePoints2.x], [linePoints2.y], [linePoints2.z], 'rs-', 'LineWidth', 1, 'MarkerSize', 8);
    plot3(ax, [closestPoints1.x], [closestPoints1.y], [closestPoints1.z], 'md-', 'LineWidth', 1, 'MarkerSize', 10);
    plot3(ax, [closestPoints2.x], [closestPoints2.y], [closestPoints2.z], 'bv-', 'LineWidth', 1, 'MarkerSize', 10);
    axis(ax, 'equal');
    ax.XTick = ax.XTick(1):ax.XTick(end);
    ax.YTick = ax.YTick(1):ax.YTick(end);
    ax.ZTick = ax.ZTick(1):ax.ZTick(end);
    title(ax, sprintf('%s\nDc=%g, D=%g', type, distance1, distance2));
    legend(ax, {'line 1', 'line 2', 'distance (clamp)', 'distance (no clamp)'}, 'Location', 'Best');
    xlabel(ax, 'x');
    ylabel(ax, 'y');
    zlabel(ax, 'z');
end

end

%% distancePoint2Line
% Calculates the distance of a point from a line segment specified by a point pair.
%
% Sources:
%   http://paulbourke.net/geometry/pointlineplane
%
% Principle:
%   We have a line L specified by points l1 = [x1, y1, z1] and l2 = [x2, y2, z2].
%   Points along L from l1 to l2 are given as
%       l = l1 + u (l2 - l1)    [x1, y1, z1] + u [x2 - x1, y2 - y1, z2 - z1]
%   The nearest point on the line is where the segment [l, p] is perpendicular to the line
%       (p - l) . (l2 - l1) = 0
%   which gives
%       [p - l1 - u(l2 - l1)] . (l2 - l1) = 0
%       u = (p - l1) . (l2 - l1) / (l2 - 1) . (l2 - 1)
%       u = ((xp - x1)(x2 - x1) + (yp - y1)(y2 - y1) + (zp - z1)(z2 - z1)) / ((x2 - x1)^2 + (y2 - y1)^2 + (z2 - z1)^2)
function [distance, closestLinePoint] = distancePoint2Line(point, linePoints, clamp)
%% Default arguments
if nargin < 3
    % Clamp by default
    clamp = true;
end
%% Check arguments
if length(linePoints) ~= 2
    error('Error: argument linePoints must contain 2 points.');
end
%% Calculate point to line segment distance
toPointFromLinePoint1 = directionVector(point, linePoints(1));
if isequal(linePoints(1), linePoints(2))
    %% The two points of the line are identical, calculate point to point distance
    distance = sqrt(dotProduct(toPointFromLinePoint1, toPointFromLinePoint1));
    closestLinePoint = linePoints(1);
else
    %% The two points are not identical, calculate point to line distance
    lineDirVect = directionVector(linePoints(2), linePoints(1));
    u = dotProduct(toPointFromLinePoint1, lineDirVect) / dotProduct(lineDirVect, lineDirVect);
    if clamp
        % Restrict to end points
        u = min(max(0, u), 1);
    end
    closestLinePoint = addVectors(linePoints(1), multiplyByScalar(u, lineDirVect));
    toPointFromClosestLinePoint = directionVector(point, closestLinePoint);
    distance = sqrt(dotProduct(toPointFromClosestLinePoint, toPointFromClosestLinePoint));
end
end

%% distanceLine2Line
% Calculates the distance of two line segments, each specified by one point pair.
%
% Sources:
%   https://math.stackexchange.com/questions/846054/closest-points-on-two-line-segments
%   (adaptations are needed because there is a mistake in the solution of the equations)
%
% Principle:
%   We have the first line L1 specified by points l11 = [x11, y11, z11] and l12 = [x12, y12, z12].
%   Points along L1 from l11 to l12 are given as
%       l1 = l11 + u (l12 - l11)    [x11, y11, z11] + u [x12 - x11, y12 - y11, z12 - z11]
%   We have the second line L2 specified by points l21 = [x21, y21] and l22 = [x22, y22].
%   Points along L2 from l21 to l22 are given as
%       l2 = l21 + t (l22 - l21)    [x21, y21, z22] + t [x22 - x21, y22 - y21, z12 - z11]
%   The distance between the two lines is minimal if the line connecting them is perpendicular to
%   both. To find intersection points, the following equation system needs to be solved:
%       (l1(u) - l2(t)) . (l12 - l11) = 0
%       (l1(u) - l2(t)) . (l22 - l21) = 0
%   Closest points are found in terms of values of u and t.
function [distance, closestPoints] = distanceLine2Line(linePoints1, linePoints2, clamp)
%% Default arguments
if nargin < 3
    clamp = true;
end
%% Check arguments
if length(linePoints1) ~= 2
    error('Error: argument linePoints1 must contain 2 points.');
end
if length(linePoints2) ~= 2
    error('Error: argument linePoints2 must contain 2 points.');
end
%% Calculate distance
% Find the
D21 = (linePoints1(2).x - linePoints1(1).x)^2 + (linePoints1(2).y - linePoints1(1).y)^2 + (linePoints1(2).z - linePoints1(1).z)^2;
D43 = (linePoints2(2).x - linePoints2(1).x)^2 + (linePoints2(2).y - linePoints2(1).y)^2 + (linePoints2(2).z - linePoints2(1).z)^2;
D4321 = (linePoints2(2).x - linePoints2(1).x) * (linePoints1(2).x - linePoints1(1).x) + ...
    (linePoints2(2).y - linePoints2(1).y) * (linePoints1(2).y - linePoints1(1).y) + ...
    (linePoints2(2).z - linePoints2(1).z) * (linePoints1(2).z - linePoints1(1).z);
D3121 = (linePoints2(1).x - linePoints1(1).x) * (linePoints1(2).x - linePoints1(1).x) + ...
    (linePoints2(1).y - linePoints1(1).y) * (linePoints1(2).y - linePoints1(1).y) + ...
    (linePoints2(1).z - linePoints1(1).z) * (linePoints1(2).z - linePoints1(1).z);
D4331 = (linePoints2(2).x - linePoints2(1).x) * (linePoints2(1).x - linePoints1(1).x) + ...
    (linePoints2(2).y - linePoints2(1).y) * (linePoints2(1).y - linePoints1(1).y) + ...
    (linePoints2(2).z - linePoints2(1).z) * (linePoints2(1).z - linePoints1(1).z);
u = (D3121 * D43 - D4321 * D4331) / (D21 * D43 - D4321^2);
t = (D4331 * D21 - D4321 * D3121) / (D4321^2 - D21 * D43);
if ((0 <= u) && (u <= 1) && (0 <= t) && (t <= 1)) || ...
        (~clamp && ~isinf(u) && ~isinf(t) && ~isnan(u) && ~isnan(t))
    % [line segments are intersecting] or [lines are intersecting and clamping is switched off]
    lineDirVect1 = directionVector(linePoints1(2), linePoints1(1));
    closestPoints(1) = addVectors(linePoints1(1), multiplyByScalar(u, lineDirVect1));
    lineDirVect2 = directionVector(linePoints2(2), linePoints2(1));
    closestPoints(2) = addVectors(linePoints2(1), multiplyByScalar(t, lineDirVect2));
    betweenClosestPoints = directionVector(closestPoints(1), closestPoints(2));
    distance = sqrt(dotProduct(betweenClosestPoints, betweenClosestPoints));
else
    % [line segments are not intersecting]
    [dists(1), clPts(1)] = distancePoint2Line(linePoints1(1), linePoints2, clamp);
    [dists(2), clPts(2)] = distancePoint2Line(linePoints1(2), linePoints2, clamp);
    [dists(3), clPts(3)] = distancePoint2Line(linePoints2(1), linePoints1, clamp);
    [dists(4), clPts(4)] = distancePoint2Line(linePoints2(2), linePoints1, clamp);
    [distance, iMin] = min(dists);
    if iMin < 3
        % Closest point is found on the second line
        closestPoints(1) = linePoints1(iMin);
        closestPoints(2) = clPts(iMin);
    else
        % Closest point is found on the first line
        closestPoints(1) = clPts(iMin);
        closestPoints(2) = linePoints2(iMin - 2);
    end
end
end

%% dotProduct
% Calculates the dot product of two 2D vectors
function prod = dotProduct(vect1, vect2)
prod = vect1.x * vect2.x + vect1.y * vect2.y + vect1.z * vect2.z;
end

%% directionVector
% Calculates direction vector to point [to] from point [from]
function vect = directionVector(to, from)
vect.x = to.x - from.x;
vect.y = to.y - from.y;
vect.z = to.z - from.z;
end

%% addVectors
% Adds two 2D vectors
function sum = addVectors(vect1, vect2)
sum.x = vect1.x + vect2.x;
sum.y = vect1.y + vect2.y;
sum.z = vect1.z + vect2.z;
end

%% multiplyByScalar
% Multiply vector by scalar number
function prod = multiplyByScalar(scalar, vect)
prod.x = scalar * vect.x;
prod.y = scalar * vect.y;
prod.z = scalar * vect.z;
end