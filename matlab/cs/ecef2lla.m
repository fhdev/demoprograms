%   This function calculates geodetic coordinates (latitude, longitude, altitude) from 
%   Earth-centered Earth-fixed (ECEF) coordinates. Calculation assumes WGS84 ellipsoid.
%   Maximal deviation compared to MATLAB's ecef2lla:
%      [lat, lon, alt] = [4.44089e-16 rad, 4.44089e-16 rad, 4.22038e-09 m]
%
% ECEF coordinate system:
%   x -> 0 latitude, 0 longitude (international reference meridian)
%   y -> right-handed
%   z -> north pole (international reference pole)
%
% [input parameters]
%   x: ECEF x coordinate(s), numeric vector
%   y: ECEF y coordinate(s), numeric vector
%   z: ECEF z coordinate(s), numeric vector
%
% [return parameters]
%   latitude: numeric column vector, latitude, positive north, in range [-pi/2; pi/2]
%   longitude: numeric column vector, longitude, east from Greenwich meridian, in range [-pi; pi]
%   altitude: numeric column vector, altitude above ellipsoid

function [latitude, longitude, altitude] = ecef2lla(x_ecef, y_ecef, z_ecef)
%% Prepare input
% output always column vectors
x_ecef = x_ecef(:);
y_ecef = y_ecef(:);
z_ecef = z_ecef(:);
%% WGS84 ellipsoid constants
% ellipsoidal equatorial radius (a = 6378137 m)
a = 6378137;
% eccentricity of ellipsoid (e^2 = 0.00669437999014)
e = 0.0818191908426215;
e2 = e^2;
%% Calculation
b = a * sqrt(1 - e2);
r = sqrt(x_ecef.^2 + y_ecef.^2);
% Reference
%       Zhu, Jijie. "Conversion of Earth-centered Earth-fixed coordinates to geodetic coordinates." 
%            IEEE Transactions on Aerospace and Electronic Systems 30.3 (1994): 957-961. Page 958.
% Heikekken's formula
longitude = atan2(y_ecef, x_ecef);
ec2 = (a^2 - b^2) / b^2;
F = 54 * b^2 * z_ecef.^2;
G = r.^2 + (1 - e2) * z_ecef.^2 - e2 * (a^2 - b^2);
c = e^4 * F .* r.^2 ./ G.^3;
s = (1 + c + sqrt(c.^2 + 2 * c)).^(1/3);
P = F ./ (3 * (s + 1./s + 1).^2 .* G.^2);
Q = sqrt(1 + 2 * e^4 * P);
r0 = - P * e2 .* r ./ (1 + Q) + real(sqrt(a^2 / 2 * (1 + 1 ./ Q) - P * (1 - e2) .* z_ecef.^2 ./ (Q .* (1 + Q)) - P .* r.^2 / 2));
U = sqrt((r - e2 * r0).^2 + z_ecef.^2);
V = sqrt((r - e2 * r0).^2 + (1 - e2) .* z_ecef.^2);
z0 = b^2 * z_ecef ./ (a * V);
altitude = U .* (1 - b^2 ./ (a * V));
latitude = atan((z_ecef + ec2 * z0) ./ r);
end