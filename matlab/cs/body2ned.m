% This function converts translational quantities (position, velocity, acceleration)
% from BODY to NED coordinate system.
% 
% Note: This function is not suitable to convert angular velocities!
%
% NED coordinate system:
%   x (N) -> north
%   y (E) -> east
%   z (D) -> downwards
%
% BODY coordinate system:
%   x -> forward (driving / flying direction)
%   y -> right
%   z -> downwards
%
% [input parameters]
%   Translational BODY coordinates
%       x_body_forward: longitudinal, positive forward (driving direction), numeric vector
%       y_body_right:   lateral, positiove right, numeric vector
%       z_body_down:    vertical, positive downwards, numeric vector
%   Euler (Tait-Bryan) angles in Z-Y-X (yaw-pitch-roll) rotation sequence
%       roll_x:  roll angle (around x-axis after rotation around NED z-axis and intermediate y-axis), numeric vector
%       pitch_y: pitch angle (around y-axis after rotation around NED z-axis), numeric vector
%       yaw_z:   yaw angle (around NED z-axis), numeric vector
%
% [return parameters]
%   Translational NED coordinates
%       x_ned_north: NED north coordiante, numeric vector
%       y_ned_east:  NED east coordiante, numeric vector
%       z_ned_down:  NED down coordiante, numeric vector

function [x_ned_north, y_ned_east, z_ned_down] = ...
    body2ned(x_body_forward, y_body_right, z_body_down, roll_x, pitch_y, yaw_z)
%% Check and prepare input
if ~(isnumeric(x_body_forward) && isvector(x_body_forward))
    error('ERROR: x_body_forward must be a numeric vector.');
end
n = length(x_body_forward);
if ~(isnumeric(y_body_right) && isvector(y_body_right) && (length(y_body_right) == n))
    error('ERROR: y_body_right must be a numeric vector with same length as x_body_forward.');
end
if ~(isnumeric(z_body_down) && isvector(z_body_down) && (length(z_body_down) == n))
    error('ERROR: z_body_down must be a numeric vector with same length as x_body_forward.');
end
if ~(isnumeric(roll_x) && isvector(roll_x) && (length(roll_x) == n))
    error('ERROR: roll_x must be a numeric vector with same length as x_body_forward.');
end
if ~(isnumeric(pitch_y) && isvector(pitch_y) && (length(pitch_y) == n))
    error('ERROR: pitch_y must be a numeric vector with same length as x_body_forward.');
end
if ~(isnumeric(yaw_z) && isvector(yaw_z) && (length(yaw_z) == n))
    error('ERROR: yaw_z must be a numeric vector with same length as x_body_forward.');
end
%% Transformation
% References
%       Cai, Guowei, Ben M. Chen, and Tong Heng Lee. 
%           Unmanned rotorcraft systems. Springer Science & Business Media, 2011.
%           Equations 2.17-2.19 2.32-2.35
%
%       Schramm, Dieter, Manfred Hiller, and Roberto Bardini. 
%           Vehicle dynamics. Modeling and Simulation. Berlin, Heidelberg (2014): 151.
%           Equation 2.79-2.83 (in 2.83 (3,2) element is erroneous)
%
%       Fossen, Thor I. 
%           Handbook of marine craft hydrodynamics and motion control. John Wiley & Sons, 2011.
%           Equations 2.15-2.18

% Preallocation for speed
x_ned_north = zeros(n, 1);
y_ned_east = zeros(n, 1);
z_ned_down = zeros(n, 1);
Rz = zeros(3,3);
Ry = zeros(3,3);
Rx = zeros(3,3);
tbody = zeros(3,1);
for i = 1 : n
    % Yaw
    sin_rz = sin(yaw_z(i));
    cos_rz = cos(yaw_z(i));
    
    %     Rz = [ cos_rz, -sin_rz, 0; 
    %            sin_rz,  cos_rz, 0; 
    %                 0,       0, 1];
            
    Rz(1,1) =  cos_rz;
    Rz(1,2) = -sin_rz;
    Rz(2,1) =  sin_rz;
    Rz(2,2) =  cos_rz;
    Rz(3,3) =  1;
    
    % Pitch
    sin_ry = sin(pitch_y(i));
    cos_ry = cos(pitch_y(i));
    
    %     Ry = [ cos_ry, 0, sin_ry;
    %                 0, 1,      0;
    %           -sin_ry, 0, cos_ry];

    Ry(1,1) = cos_ry;
    Ry(1,3) = sin_ry;
    Ry(2,2) = 1;
    Ry(3,1) = -sin_ry;
    Ry(3,3) = cos_ry;
    
    % Roll
    sin_rx = sin(roll_x(i));
    cos_rx = cos(roll_x(i));
    
    %     Rx = [ 1,      0,       0;
    %            0, cos_rx, -sin_rx;
    %            0, sin_rx,  cos_rx];
    
    Rx(1,1) =  1;
    Rx(2,2) =  cos_rx;
    Rx(2,3) = -sin_rx;
    Rx(3,2) =  sin_rx;
    Rx(3,3) =  cos_rx;

    %% Conversion
    
    tbody(1) = x_body_forward(i);
    tbody(2) = y_body_right(i);
    tbody(3) = z_body_down(i);
    
    tned = Rz * Ry * Rx * tbody;
    
    x_ned_north(i) = tned(1);
    y_ned_east(i) = tned(2);
    z_ned_down(i) = tned(3);
    
end

end