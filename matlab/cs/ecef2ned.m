% This function converts translational quantities (position, velocity, acceleration)
% from Earth-Centered Earth-Fixed (ECEF) coordinates to local North East Down (NED) coordinates.
%
%  NED coordinate system:
%    x (N) -> north
%    y (E) -> east
%    z (D) -> downwards
%
% ECEF coordinate system:
%   x -> 0 latitude, 0 longitude (international reference meridian)
%   y -> right-handed
%   z -> north pole (international reference pole)
%
%  [input parameters]
%   LLH latitude and longitude of reference plane
%       latituderef:  latitude of reference point, positive north, in range [-pi/2; pi/2], numeric vector
%       longituderef: longitude of reference point, east from Greenwich meridian, in range [-pi; pi], numeric vector
%   ECEF coordinates of quantity
%       x_ecef: ECEF x coordinate(s), numeric vector
%       y_ecef: ECEF y coordinate(s), numeric vector
%       z_ecef: ECEF z coordinate(s), numeric vector
%   ECEF coordinates of reference point
%       xref_ecef: ECEF x coordinate(s) of reference point, numeric vector, only for position transformation
%       yref_ecef: ECEF y coordinate(s) of reference point, numeric vector, only for position transformation
%       zref_ecef: ECEF z coordinate(s) of reference point, numeric vector, only for position transformation
%
%  [return parameters]
%   Translational NED coordinates
%       x_ned_north: NED north coordiante, numeric vector
%       y_ned_east:  NED east coordiante, numeric vector
%       z_ned_down:  NED down coordiante, numeric vector

function [x_ned_north, y_ned_east, z_ned_down] = ...
    ecef2ned(latituderef, longituderef, x_ecef, y_ecef, z_ecef, xref_ecef, yref_ecef, zref_ecef)
%% Check and prepare input
if ~(isnumeric(latituderef) && isvector(latituderef))
    error('ERROR: latituderef must be a numeric vector.');
end
n = length(latituderef);
if ~(isnumeric(longituderef) && isvector(longituderef) && (length(longituderef) == n))
    error('ERROR: longituderef must be a numeric vector with same length as latituderef.');
end
if ~(isnumeric(x_ecef) && isvector(x_ecef) && (length(x_ecef) == n))
    error('ERROR: x_ecef must be a numeric vector with same length as latituderef.');
end
if ~(isnumeric(y_ecef) && isvector(y_ecef) && (length(y_ecef) == n))
    error('ERROR: y_ecef must be a numeric vector with same length as latituderef.');
end
if ~(isnumeric(z_ecef) && isvector(z_ecef) && (length(z_ecef) == n))
    error('ERROR: z_ecef must be a numeric vector with same length as latituderef.');
end
if nargin > 5
    if ~(isnumeric(xref_ecef) && isvector(xref_ecef) && (length(xref_ecef) == n))
        error('ERROR: xref_ecef must be a numeric vector with same length as latituderef.');
    end
    if ~(isnumeric(yref_ecef) && isvector(yref_ecef) && (length(yref_ecef) == n))
        error('ERROR: yref_ecef must be a numeric vector with same length as latituderef.');
    end
    if ~(isnumeric(zref_ecef) && isvector(zref_ecef) && (length(zref_ecef) == n))
        error('ERROR: zref_ecef must be a numeric vector with same length as latituderef.');
    end
end


%% Transformation
% Reference
%       Cai, Guowei, Ben M. Chen, and Tong Heng Lee. 
%           Unmanned rotorcraft systems. Springer Science & Business Media, 2011.
%           Equations 2.23-2.24
%
%       Fossen, Thor I. 
%           Handbook of marine craft hydrodynamics and motion control. John Wiley & Sons, 2011.
%           Equations 2.84

% Preallocation for speed
x_ned_north = zeros(n, 1);
y_ned_east = zeros(n, 1);
z_ned_down = zeros(n, 1);
R = zeros(3, 3);
tecef = zeros(3, 1);
for i = 1 : n

    % Transformation matrix
    sin_latitude = sin(latituderef(i));
    cos_latitude = cos(latituderef(i));
    sin_longitude = sin(longituderef(i));
    cos_longitude = cos(longituderef(i));
    
    
    % [-sin_latitude * cos_longitude, -sin_latitude * sin_longitude,  cos_latitude, 
    %                 -sin_longitude,                 cos_longitude,             0,
    %  -cos_latitude * cos_longitude, -cos_latitude * sin_longitude, -sin_latitude]
    R(1, 1) = -sin_latitude * cos_longitude;
    R(1, 2) = -sin_latitude * sin_longitude;
    R(1, 3) =  cos_latitude;
    R(2, 1) = -sin_longitude;
    R(2, 2) =  cos_longitude;
    R(3, 1) = -cos_latitude * cos_longitude;
    R(3, 2) = -cos_latitude * sin_longitude;
    R(3, 3) = -sin_latitude;
    
    if nargin > 5
        tecef(1) = x_ecef(i) - xref_ecef(i);
        tecef(2) = y_ecef(i) - yref_ecef(i);
        tecef(3) = z_ecef(i) - zref_ecef(i);
    else
        
        tecef(1) = x_ecef(i);
        tecef(2) = y_ecef(i);
        tecef(3) = z_ecef(i);
    end
    
    tned = R * tecef;
    
    x_ned_north(i) = tned(1);
    y_ned_east(i) = tned(2);
    z_ned_down(i) = tned(3);
    
end

end