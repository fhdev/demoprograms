function mps = kph2mps(kph)
mps = kph / 3.6;
end