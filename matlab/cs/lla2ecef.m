%   This function calculates Earth-centered Earth-fixed (ECEF) coordinates from geodetic coordinates 
%   (latitude, longitude, altitude). Calculation assumes WGS84 ellipsoid.
%   Maximal deviation compared to MATLAB's lla2ecef:
%       [x, y, z] = [1.86265e-09 m, 3.25963e-09 m, 1.86265e-09 m]
%
% ECEF coordinate system:
%   x -> 0 latitude, 0 longitude (international reference meridian)
%   y -> right-handed
%   z -> north pole (international reference pole)
%
% [input parameters]
%   latitude:  latitude, positive north, in range [-pi/2; pi/2], numeric vector
%   longitude: longitude, east from Greenwich meridian, in range [-pi; pi], numeric vector
%   altitude:  altitude above ellipsoid, numeric vector
%
% [return parameters]
%   x_ecef: numeric vector, ECEF x coordinate(s)
%   y_ecef: numeric vector, ECEF y coordinate(s)
%   z_ecef: numeric vector, ECEF z coordinate(s)

function [x_ecef, y_ecef, z_ecef] = lla2ecef(latitude, longitude, altitude)
%% Prepare input
% output always column vectors
latitude = latitude(:);
longitude = longitude(:);
altitude = altitude(:);
%% WGS84 ellipsoid constants
% ellipsoidal equatorial radius (a = 6378137 m)
a = 6378137;
% eccentricity of ellipsoid (e^2 = 0.00669437999)
e = 0.0818191908426215;
e2 = e^2;
%% Calculation
% Reference
%       Zhu, Jijie. "Conversion of Earth-centered Earth-fixed coordinates to geodetic coordinates." 
%            IEEE Transactions on Aerospace and Electronic Systems 30.3 (1994): 957-961. Page 957.
R = a ./ sqrt(1 - e2 .* sin(latitude).^2);
x_ecef = (R + altitude) .* cos(latitude) .* cos(longitude);
y_ecef = (R + altitude) .* cos(latitude) .* sin(longitude);
z_ecef = (R + altitude - e2 .* R) .* sin(latitude);
end