% This function converts translational quantities (position, velocity, acceleration)
% from local North East Down (NED) coordinates to Earth-Centered Earth-Fixed (ECEF) coordinates.
%
% NED coordinate system:
%    x (N) -> north
%    y (E) -> east
%    z (D) -> downwards
%
% ECEF coordinate system:
%   x -> 0 latitude, 0 longitude (international reference meridian)
%   y -> right-handed
%   z -> north pole (international reference pole)
%
%  [input parameters]
%   LLH latitude and longitude of reference plane
%       latituderef:  latitude of reference point, positive north, in range [-pi/2; pi/2], numeric vector
%       longituderef: longitude of reference point, east from Greenwich meridian, in range [-pi; pi], numeric vector
%   Translational NED coordinates
%       x_ned_north: NED north coordiante, numeric vector
%       y_ned_east:  NED east coordiante, numeric vector
%       z_ned_down:  NED down coordiante, numeric vector
%   ECEF coordinates of reference point
%       xref_ecef: ECEF x coordinate(s) of reference point, numeric vector, only for position transformation
%       yref_ecef: ECEF y coordinate(s) of reference point, numeric vector, only for position transformation
%       zref_ecef: ECEF z coordinate(s) of reference point, numeric vector, only for position transformation
%
%  [return parameters]
%   ECEF coordinates of quantity
%       x_ecef: ECEF x coordinate(s), numeric vector
%       y_ecef: ECEF y coordinate(s), numeric vector
%       z_ecef: ECEF z coordinate(s), numeric vector

function [x_ecef, y_ecef, z_ecef] = ...
    ned2ecef(latituderef, longituderef, x_ned_north, y_ned_east, z_ned_down, xref_ecef, yref_ecef, zref_ecef)
%% Check and prepare input
if ~(isnumeric(latituderef) && isvector(latituderef))
    error('ERROR: latituderef must be a numeric vector.');
end
n = length(latituderef);
if ~(isnumeric(longituderef) && isvector(longituderef) && (length(longituderef) == n))
    error('ERROR: longituderef must be a numeric vector with same length as latituderef.');
end
if ~(isnumeric(x_ned_north) && isvector(x_ned_north) && (length(x_ned_north) == n))
    error('ERROR: x_ned_north must be a numeric vector with same length as latituderef.');
end
if ~(isnumeric(y_ned_east) && isvector(y_ned_east) && (length(y_ned_east) == n))
    error('ERROR: y_ned_east must be a numeric vector with same length as latituderef.');
end
if ~(isnumeric(z_ned_down) && isvector(z_ned_down) && (length(z_ned_down) == n))
    error('ERROR: z_ned_down must be a numeric vector with same length as latituderef.');
end
if nargin > 5
    if ~(isnumeric(xref_ecef) && isvector(xref_ecef) && (length(xref_ecef) == n))
        error('ERROR: xref_ecef must be a numeric vector with same length as latituderef.');
    end
    if ~(isnumeric(yref_ecef) && isvector(yref_ecef) && (length(yref_ecef) == n))
        error('ERROR: yref_ecef must be a numeric vector with same length as latituderef.');
    end
    if ~(isnumeric(zref_ecef) && isvector(zref_ecef) && (length(zref_ecef) == n))
        error('ERROR: zref_ecef must be a numeric vector with same length as latituderef.');
    end
end


%% Transformation
% Reference
%       Cai, Guowei, Ben M. Chen, and Tong Heng Lee. 
%           Unmanned rotorcraft systems. Springer Science & Business Media, 2011.
%           Equations 2.23-2.24
%
%       Fossen, Thor I. 
%           Handbook of marine craft hydrodynamics and motion control. John Wiley & Sons, 2011.
%           Equations 2.84

% Preallocation for speed
x_ecef = zeros(n, 1);
y_ecef = zeros(n, 1);
z_ecef = zeros(n, 1);
R = zeros(3, 3);
tned = zeros(3, 1);
for i = 1 : n

    % Transformation matrix
    sin_latitude = sin(latituderef(i));
    cos_latitude = cos(latituderef(i));
    sin_longitude = sin(longituderef(i));
    cos_longitude = cos(longituderef(i));
    
    % [-sin_latitude * cos_longitude, -sin_longitude, -cos_latitude * cos_longitude,
    %  -sin_latitude * sin_longitude,  cos_longitude, -cos_latitude * sin_longitude,
    %                   cos_latitude,             0,                  -sin_latitude]
    R(1, 1) = -sin_latitude * cos_longitude;
    R(1, 2) = -sin_longitude;
    R(1, 3) = -cos_latitude * cos_longitude;
    R(2, 1) = -sin_latitude * sin_longitude;
    R(2, 2) =  cos_longitude;
    R(2, 3) = -cos_latitude * sin_longitude;
    R(3, 1) =  cos_latitude;
    R(3, 3) = -sin_latitude;
    
    tned(1) = x_ned_north(i);
    tned(2) = y_ned_east(i);
    tned(3) = z_ned_down(i);
    
    tecef = R * tned;
    
    if nargin > 5
        x_ecef(i) = tecef(1) + xref_ecef(i);
        y_ecef(i) = tecef(2) + yref_ecef(i);
        z_ecef(i) = tecef(3) + zref_ecef(i);
    else
        x_ecef(i) = tecef(1);
        y_ecef(i) = tecef(2);
        z_ecef(i) = tecef(3);
    end
    
end

end