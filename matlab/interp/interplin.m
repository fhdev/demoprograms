function yq = interplin(x, y, xq)

%% Check and prepare arguments
if ~(isnumeric(x) && isvector(x) && issorted(x, 'strictascend'))
    error('ERROR: Knot sites (x) must be specified in a strictly ascending numeric vector.');
end
x = x(:);
n = length(x);
if ~(isnumeric(y) && isvector(y) && (n == length(y)))
    error(['ERROR: Knot values (y) must be specified in a numeric vector with ', ...
        'same number of elements as knot sites (x).']);
end
y = y(:);
if ~(isnumeric(xq) && isvector(xq) && (min(xq) >= min(x)) && (max(xq) <= max(x)))
    error('ERROR: Query sites (xq) must be spefified in a numeric vector in range of knot sites (x).');
end
xq = xq(:);
%% Calculate query values
[~, ~, i] = histcounts(xq, x);
yq = y(i) + (xq - x(i)) .* ((y(i + 1) - y(i)) ./ (x(i + 1) - x(i)));
end