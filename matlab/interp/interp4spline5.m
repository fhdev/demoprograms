function [yq, dyq, ddyq] = interp4spline5(x, y, dyb, ddyb, xq)
% Default arguments
if isempty(dyb)
    dyb = zeros(2, 1);
end
if isempty(ddyb)
    ddyb = zeros(2, 1);
end
% Argument check
if ~isnumeric(x) || ~isvector(x) || (length(x) ~= 4) || ~all(diff(x) > 0)
    error('ERROR: x must be a strictly monotonic numeric vector with 4 elements.');
end
x = x(:);
if ~isnumeric(y) || ~isvector(y) || (length(y) ~= 4)
    error('ERROR: y must be a numeric vector with 4 elements.');
end
y = y(:);
if ~isnumeric(dyb) || ~isvector(dyb) || (length(dyb) ~= 2)
    error('ERROR: dyb must be a numeric vector with 2 elements.');
end
dyb = dyb(:);
if ~isnumeric(ddyb) || ~isvector(ddyb) || (length(ddyb) ~= 2)
    error('ERROR: ddyb must be a numeric vector with 2 elements.');
end
ddyb = ddyb(:);
if ~isnumeric(xq) || ~isvector(xq) || ~all(diff(xq) > 0) || (xq(1) < x(1)) || (xq(end) > x(end))
    error('ERROR: ddyb must be a strictly monotonic numeric vector in the range of x.');
end
xq = xq(:);
% Create exponents
e5 =  (5 : -1 : 0);
e4 = [(4 : -1 : 0), 0];
e3 = [(3 : -1 : 0), 0, 0];
% Create normalized x values so that X does not get numerically close to singular
xs = x / x(end);
xqs = xq / x(end);
% Create powers of x and its derivatives
xv = xs .^ e5;
dxv   = xv(:, [2:end, end]) .* e5;
ddxv  = xv(:, [3:end, end, end]) .* e5 .*e4;
dddxv = xv(:, [4:end, end, end, end]) .* e5 .*e4 .* e3;
% Create 1x6 zero block
z = zeros(1, 6);
% Create vector of y values with restpect to the normalization of x
Y = [y([1, 2, 2, 3, 3, 4]); dyb * x(end); 0; 0; ddyb * x(end)^2; z'];
% Create matrix of interpolation equations
X = [     xv(1,:),            z,           z;      % p1(x1) = y1
          xv(2,:),            z,           z;      % p1(x2) = y2
                z,      xv(2,:),           z;      % p2(x2) = y2
                z,      xv(3,:),           z;      % p2(x3) = y3
                z,            z,     xv(3,:);      % p3(x3) = y3
                z,            z,     xv(4,:);      % p3(x4) = y4
         dxv(1,:),            z,           z;      % p1'(x1) = y1'
                z,            z,    dxv(4,:);      % p3'(x4) = y4'
         dxv(2,:),    -dxv(2,:),           z;      % p1'(x2) - p2'(x2) = 0
                z,     dxv(3,:),   -dxv(3,:);      % p2'(x3) - p3'(x3) = 0
        ddxv(1,:),            z,           z;      % p1''(x1) = y1''
                z,            z,   ddxv(4,:);      % p3''(x4) = y4''
        ddxv(2,:),   -ddxv(2,:),           z;      % p1''(x2) - p2''(x2) = 0
                z,    ddxv(3,:),  -ddxv(3,:);      % p2''(x3) - p3''(x3) = 0
       dddxv(1,:),            z,           z;      % p1'''(x1) = 0
                z,            z,  dddxv(4,:);      % p3'''(x4) = 0
       dddxv(2,:),  -dddxv(2,:),           z;      % p1'''(x2) - p2'''(x2) = 0
                z,   dddxv(3,:), -dddxv(3,:)];     % p2'''(x3) - p3'''(x3) = 0
% interpolate spline parameters
p = X \ Y;
% Create indices that show which x value belongs to which polynomial 
I = xqs <= xs(2:end)';
i = -sum(I,2) + 4;
% Create matrix of spline coefficients
pi = 6 * (i - 1) + (1 : 6);
% Calculate output with respect to normalized x values used for the spline in case of derivatives
xqsv = xqs .^ e5;
yq = sum(xqsv .* p(pi), 2);
dyq = sum(xqsv(:, [2:end, end]) .* e5 .* p(pi), 2) / x(end);
ddyq = sum(xqsv(:, [3:end, end, end]) .* e5 .* e4 .* p(pi), 2) / x(end)^2;
end