%% interppoly
xq = 0:0.01:10;
x = [0, 1, 3, 5, 7, 10];
y = [0,-3, 10, 2, 0, -6];
dyb = [0, 0];
yq1 = interppoly(x, y, xq);
yq2 = interppoly(x, y, xq, dyb);
yq3 = interpspline3(x, y, xq);
yq4 = interpspline3(x, y, xq, dyb);
yq5 = interplin(x, y, xq);
figure('Color', 'White');
plot(x, y, 'bo', xq, yq1, 'r-', xq, yq2, 'g-', xq, yq3, 'm-', xq, yq4, 'c-', xq, yq5, 'y-');
legend({'values', 'poly', 'poly + derivatives', 'spline3', 'spline3 + derivatives', 'linear'});
set(gca, 'XTick', sort(unique(x)), 'YTick', sort(unique(y)));
axis equal;
grid on;
box on;

