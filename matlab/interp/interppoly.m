%% interppoly --------------------------------------------------------------------------------------
% [Summary]
%   This function evaluates the polynomial specified by knot sites x and values y in the
%   query sites specified by x_q. If the parameter dy_b is specified, the initial and final
%   derivatives of the polynomial are adjusted accordingly.
%
%   The degree of the polynomial is equal to the number of knot points by default.
%   If the derivative boundary conditions are specified, the degree of the polynomial is
%   greater than the number of knot points by 2.
%
% [Input]
%   x    - Knot sites.
%   y    - Knot values.
%   xq   - Query sites.
%   dyb  - Optional. Initial dyb(1) and final dyb(2) derivatives of the polynomial.
%
% [Output]
%   yq  - Values of polynomial at query sites xq.
% --------------------------------------------------------------------------------------------------
function yq = interppoly(x, y, xq, dyb)
%% Default arguments
if nargin < 4
    dyb = nan;
end
%% Check and prepare inputs
if ~(isnumeric(x) && isvector(x) && issorted(x, 'strictascend'))
    error('ERROR: Knot sites (x) must be specified in a strictly ascending numeric vector.');
end
x = x(:);
n = length(x);
if ~(isnumeric(y) && isvector(y) && (n == length(y)))
    error(['ERROR: Knot values (y) must be specified in a numeric vector with ', ...
        'same number of elements as knot sites (x).']);
end
y = y(:);
if ~(isnumeric(xq) && isvector(xq) && (min(xq) >= min(x)) && (max(xq) <= max(x)))
    error('ERROR: Query sites (xq) must be spefified in a numeric vector in range of knot sites (x).');
end
xq = xq(:);
if ~(any(isnan(dyb)) || (isnumeric(dyb) && isvector(dyb) && (length(dyb) == 2)))
    error(['ERROR: Derivative boundary conditions (dyb) must be specified in a ', ...
        'vector with 2 elements.']);
end
dyb = dyb(:);
%% Calculate polynomial coefficients
if isnan(dyb)
    % Transformation matrix from polynomial coefficients to knot values
    % Equations for knot substitution values.
    T = vander(x);
    % Evaluate coefficients
    p = T \ y;
else
    % Transformation matrix from polynomial coefficients to knot values
    % Equations for knot point values. Two zeros are added to get space for derivative
    % boundary equations.
    T = vander([x; 0; 0]);
    % Equations for initial and final derivatives
    T(n + 1, :) = min(x) .^ [(n : -1 : 0), 0] .* [((n + 1) : -1 : 1), 0];
    T(n + 2, :) = max(x) .^ [(n : -1 : 0), 0] .* [((n + 1) : -1 : 1), 0];
    % Evaluate coefficients
    p = T \ [y; dyb];
end
%% Evaluate spline points
yq = polyval(p, xq);
end