%% interpspline3 -----------------------------------------------------------------------------------
% [Summary]
%   This function evaluates the cubic spline specified by knot sites x and values y at the query
%   sites specified by xq. If the parameter dyb is specified, the initial and final derivatives
%   of the spline are adjusted accordingly. If dyb is not specified, a narual spline is evaluated
%   (inditial and final second derivatives are chosen 0).
%
% [Input]
%   x    - Knot sites.
%   y    - Knot values.
%   xq   - Query sites.
%   dyb  - Optional. Initial dyb(1) and final dyb(2) derivatives of the polynomial.
%
% [Output]
%   yq   - Values of polynomial at query sites x_q.
% --------------------------------------------------------------------------------------------------
function [yq, dyq, ddyq] = interpspline3(x, y, xq, dyb)
%% Default arguments
if nargin < 4
    dyb = [];
end
%% Check and prepare arguments
if ~(isnumeric(x) && isvector(x) && issorted(x, 'strictascend'))
    error('ERROR: Knot sites (x) must be specified in a strictly ascending numeric vector.');
end
x = x(:);
n = length(x);
if ~(isnumeric(y) && isvector(y) && (n == length(y)))
    error(['ERROR: Knot values (y) must be specified in a numeric vector with ', ...
        'same number of elements as knot sites (x).']);
end
y = y(:);
if ~(isnumeric(xq) && isvector(xq) && (min(xq) >= min(x)) && (max(xq) <= max(x)))
    error('ERROR: Query sites (xq) must be spefified in a numeric vector in range of knot sites (x).');
end
xq = xq(:);
if ~(isempty(dyb) || (isnumeric(dyb) && isvector(dyb) && (length(dyb) == 2)))
    error(['ERROR: Derivative boundary conditions (dyb) must be specified in a ', ...
        'vector with 2 elements.']);
end
dyb = dyb(:);
%% Assemble matrix of transformation T from polynomial coefficietns to knot values Y
% Normalize x values to be in interval 0 .. 1 to avoid matrix that is close to singular
xmax = x(end);
x = x / xmax;
xq = xq / xmax;
% Number of polynomial sections
n = length(x) - 1;
% Preallocate transformation matrix and vector of knot values
T = zeros(4 * n, 4 * n);
Y = zeros(4 * n, 1);
% Loop through the rows of transformation matrix
for ir = 1 : (2 * n)
    %% Substitution values
    % Index of knot point (site and value)
    ik = floor(ir / 2) + 1;
    % Begining column index of current equation
    ic = 4 * floor((ir - 1) / 2) + 1;
    % Substitution equation at the right place
    T(ir, ic)     = x(ik)^3;
    T(ir, ic + 1) = x(ik)^2;
    T(ir, ic + 2) = x(ik);
    T(ir, ic + 3) = 1;
    Y(ir, 1)      = y(ik);
end
for ir = (2 * n + 1) : (4 * n)
    %% Continuous 1st and 2nd derivatives
    % Index of knot point (site and value)
    ik = floor((ir - 2 * n) / 2) + 1;
    if (ir == (2 * n + 1))
        % Start point
        % Begining column index of current equation
        ic = 1;
        if isempty(dyb)
            % Second derivative equation at right place
            T(ir, ic)     = 6 * x(ik);
            T(ir, ic + 1) = 2;
        else
            % First derivative equation at right place
            T(ir, ic)     = 3 * x(ik)^2;
            T(ir, ic + 1) = 2 * x(ik);
            T(ir, ic + 2) = 1;
            Y(ir, 1)      = dyb(1) * xmax;
        end
    elseif (ir == (4 * n))
        % End point
        % Begining column index of current equation
        ic = 4 * (n - 1) + 1;
        if isempty(dyb)
            % Second derivative equation at right place
            T(ir, ic)     = 6 * x(ik);
            T(ir, ic + 1) = 2;
        else
            % First derivative equation at right place
            T(ir, ic)     = 3 * x(ik)^2;
            T(ir, ic + 1) = 2 * x(ik);
            T(ir, ic + 2) = 1;
            Y(ir, 1)       = dyb(2) * xmax;
        end
    else
        % Inner points
        % Begining column index of current equation
        ic = 4 * (floor((ir - 2 * n) / 2) - 1) + 1;
        if mod(ir, 2) == 0
            % First derivative equation at right place
            T(ir, ic)     = 3 * x(ik)^2;
            T(ir, ic + 1) = 2 * x(ik);
            T(ir, ic + 2) = 1;
            T(ir, ic + 4) = -T(ir, ic);
            T(ir, ic + 5) = -T(ir, ic + 1);
            T(ir, ic + 6) = -T(ir, ic + 2);
        else
            % Second derivative equation at right place
            T(ir, ic)     = 6 * x(ik);
            T(ir, ic + 1) = 2;
            T(ir, ic + 4) = -T(ir, ic);
            T(ir, ic + 5) = -T(ir, ic + 1);
        end
        
    end
end

%% Evaluate spline coefficients
p = T \ Y;

%% Evaluate query values
% Find the interval that xq belongs to
[~, ~, iq] = histcounts(xq, x);
% Find the starting index of corresponding polynomial coefficients
ip = 4 * (iq - 1) + 1;
% Evaluate query value
yq   =      p(ip) .* xq.^3 +     p(ip + 1) .* xq.^2 + p(ip + 2) .* xq + p(ip + 3);
dyq  = (3 * p(ip) .* xq.^2 + 2 * p(ip + 1) .* xq    + p(ip + 2)) / xmax;
ddyq = (6 * p(ip) .* xq    + 2 * p(ip + 1)) / xmax^2;
end
