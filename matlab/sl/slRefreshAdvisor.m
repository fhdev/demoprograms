function slRefreshAdvisor()
    sl_refresh_customizations();
    Advisor.Manager.refresh_customizations();
    slprj = fullfile(pwd, 'slprj');
    if exist(slprj, 'dir') == 7
        fclose('all');
        rmdir(slprj, 's');
    end
end