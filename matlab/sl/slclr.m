function slclr()
    bdclose('all');
    Simulink.data.dictionary.closeAll('-discard');
    slreq.clear();
    %slreq.close();
    sltest.testmanager.clear();
    sltest.testmanager.clearResults();
    sltest.testmanager.close();
end