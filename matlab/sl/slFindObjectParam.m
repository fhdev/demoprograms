function matchingValueParamNames = slFindObjectParam(slObj, nameRegex, valueRegex)
    if nargin < 2
        nameRegex = '';
    end
    if nargin < 3
        valueRegex = '';
    end
    allParams = get_param(slObj, 'ObjectParameters');
    allParamNames = fieldnames(allParams);
    if ~isempty(nameRegex)
        matchingParamsInd = cellfun(@(x) ~isempty(x), regexpi(allParamNames, nameRegex, 'once'));
        matchingNameParams = allParamNames(matchingParamsInd);
    else
        matchingNameParams = allParamNames;
    end
    if ~isempty(valueRegex)
        ind = [];
        for i = 1 : numel(matchingNameParams)
            if ~any(strcmp('write-only', allParams.(matchingNameParams{i}).Attributes))
                value = get_param(slObj, matchingNameParams{i});
                if ischar(value) && ~isempty(regexpi(value, valueRegex, 'once'))
                    ind = [ind; i];
                end
            end
        end
        matchingValueParamNames = matchingNameParams(ind);
    else 
        matchingValueParamNames = matchingNameParams;
    end
end