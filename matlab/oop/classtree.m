function classtree(directory)

if nargin < 1
    directory = pwd;
end

directory = fullpath(directory);

if ~(ischar(directory) && isvector(directory))
    error('ERROR: Invalid directory name!');
end

classes = getClassNames(directory);

classes = sort(classes);

matrix = zeros(length(classes));

for iClass = 1 : length(classes)
    try
        metadata = meta.class.fromName(classes{iClass});
        for iSuper = 1 : length(metadata.SuperclassList)
            matrix(strcmp(classes, metadata.SuperclassList(iSuper).Name), iClass) = 1;
        end
    catch exception
        disp(exception.message);
        warning(['WARNING: There is a problem with the definition of class ', classes{iClass}, '.']);
    end
end

view(biograph(matrix, classes));

end
