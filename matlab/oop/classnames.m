function [classes, files] = classnames(directory)
%% Default arguments
if nargin < 1
    directory = pwd;
end
%% Check arguments
if ~(ischar(directory) && isvector(directory))
    error('ERROR: Invalid directory name!');
end
% Make directory name a full path
directory = fullpath(directory);
%% Do job
%#ok<*AGROW>
classes = [];
files = [];
content = dir(directory);
for iItem = 1 : length(content)
    if ~strcmp(content(iItem).name, '.') && ~strcmp(content(iItem).name, '..')
        if content(iItem).isdir
            [c, f] = getClassNames(fullfile(directory, content(iItem).name));
            classes = [classes; c];
            files = [files; f];
        else
            [~, className, classExt] = fileparts(content(iItem).name);
            if strcmpi(classExt, '.m') && exist(className, 'class')
                classes = [classes; {className}];
                files = [files; {fullfile(content(iItem).folder, content(iItem).name)}];
            end
        end
    end
end
end