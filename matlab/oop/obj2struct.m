function structure = obj2struct(object)
if isempty(object)
    %% Empty
    structure = [];
elseif isobject(object) || isstruct(object)
    %% Object or structure
    objectSize = size(object);
    % Get field names of object or structure
    if isobject(object)
        fieldNames = properties(object(1));
    elseif isstruct(object)
        fieldNames = fieldnames(object(1));
    end
    % Preallocate structure
    nFields = length(fieldNames);
    defaults = repmat({[]}, 1, nFields);
    fields = [fieldNames(:)'; defaults];
    structure = repmat(struct(fields{:}), objectSize);
    % Process properties of objects
    for iObject = 1 : prod(objectSize)
        for iProp = 1 : nFields
            structure(iObject).(fieldNames{iProp}) = obj2struct(object(iObject).(fieldNames{iProp}));
        end
    end
elseif iscell(object)
    %% Cell
    objectSize = size(object);
    % Process cell elements
    structure = cell(objectSize);
    for iCell = 1 : prod(objectSize)
        structure{iCell} = obj2struct(object{iCell});
    end
else
    %% Ordinary variable
    structure = object;
end

end