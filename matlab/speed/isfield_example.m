%% Call isfield with cellarray instead of calling it multiple times
clear;

n = 1e6;

structure.sub1.field1 = 1;
structure.sub1.field2 = 2;
structure.sub1.field3 = 3;

% Calling multiple times is slow
tic;
for i = 1 : n
    isfield(structure, 'sub1');
    isfield(structure, 'sub2');
end
toc;

% Calling one time with multiple data is fast
tic;
for i = 1 : n
    isfield(structure, {'sub1', 'sub2'});
end
toc;