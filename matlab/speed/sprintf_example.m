%% Use sprintf to concatenate strings containing formatted output
n = 1e5;

% Using array concatenation and int2str / num2str is slow
tic;
for i = 1 : n
    a = ['alma', int2str(i)];
end
toc;

% Sprintf is faster
tic;
for i = 1 : n
    b = sprintf('alma%d',i);
end
toc;