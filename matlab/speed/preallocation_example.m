%% Preallocation over concatenation

clear;

n = 1e5;

% Concatenation is slow
tic;
a = [];
for i = 1 : n
    a = [a, i];
end
toc;

% Preallocation is fast
tic;
b = zeros(1, n);
for i = 1 : n
    b(i) = i;
end
toc;

assert(isequal(a,b));