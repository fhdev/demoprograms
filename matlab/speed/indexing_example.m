%% Use linear indexing when indexing an array
clear;

n = 1e6;

structure.sub1.field1 = 1;
structure.sub2.field2 = 2;
structure.sub3.field3 = 3;

names = fieldnames(structure);

a = 0;
b = 0; 
c = 0;

% Matrix indexing takes more time
tic;
for i = 1 : n
    a = structure.(names{1, 1}).field1;
    b = structure.(names{2, 1}).field2;
    c = structure.(names{3, 1}).field3;
end
toc;

% Linear indexing takes less time
tic;
for i = 1 : n
    a = structure.(names{1}).field1;
    b = structure.(names{2}).field2;
    c = structure.(names{3}).field3;
end
toc;