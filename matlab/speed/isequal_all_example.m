%% Use isequal when comparing vectors or matrices
clear;

n = 1e6;

u = 1:10;
v = 1:10;

% Compare with all and == is slow
tic;
for i = 1 : n
    all(u == v);
end
toc;

% Compare with isequal is fast
tic;
for i = 1 : n
    isequal(u, v);
end
toc;