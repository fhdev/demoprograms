%% Get substructure if you use it multiple times
clear;

n = 1e6;

structure.sub1.field1 = 1;
structure.sub1.field2 = 2;
structure.sub1.field3 = 3;

a = 0;
b = 0; 
c = 0;

% Reaching the data in a substructure with "two dots" multiple times is slow
tic;
for i = 1 : n
    a = structure.sub1.field1;
    b = structure.sub1.field2;
    c = structure.sub1.field3;
end
toc;

% Geting the substructure first and reach the fields with "one dot" is fast
tic;
for i = 1 : n
    s = structure.sub1;
    a = s.field1;
    b = s.field2;
    c = s.field3;
end
toc;