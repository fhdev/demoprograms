%% Assignment over allocation

clear;

n = 1e6;

% Assigning to matrix elements is fast
tic;
for i = 1 : n
    a(1, 1) = i;
    a(1, 2) = 2 * i;
    a(2, 1) = 3 * i;
    a(2, 2) = 4 * i;
end
toc;

% Allocating a new matrix is slow
tic;
for i = 1 : n
    b = [   i, 2 * i;
        3 * i, 4 * i];
end
toc;