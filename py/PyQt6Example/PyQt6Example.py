from PyQt6.QtCore import Qt
from PyQt6.QtGui import QAction
from PyQt6.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QVBoxLayout, QLabel, QLineEdit, QSizePolicy, QMenu

# Only needed for access to command line arguments
import sys

# Subclass QMainWindow to customize your application's main window
class MainWindow(QMainWindow):
    BUTTON_TEXT_LIST = ["push me", "and then just touch me", "till I can get my", "satisfaction"]

    def __init__(self):
        super().__init__()
        # Create counter
        self._counter = 0

        # Set title
        self.setWindowTitle("PyQt6Example")
        self.resize(640, 480)

        # Create button
        button = QPushButton(self.BUTTON_TEXT_LIST[self._counter])
        # Enable varaible size
        button.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        # Connect button clicked signal (event) to our custom slot ("event handler")
        button.clicked.connect(self.buttonClicked)

        # Create label
        label = QLabel()
        # Enable varaible size
        label.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)

        # Create line edit
        lineEdit = QLineEdit()
        # Enable varaible size
        lineEdit.setSizePolicy(QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding)
        # Connect line edit text changed signal (event) directly to label text setter slot ("event handler")
        lineEdit.textChanged.connect(label.setText)

        # Create layout
        layout = QVBoxLayout()
        # Add all widgets to the layout
        layout.addWidget(button)
        layout.addWidget(lineEdit)
        layout.addWidget(label)

        # Create container widget with layout
        container = QWidget()
        container.setLayout(layout)

        # Set the central widget of the Window.
        self.setCentralWidget(container)

    # Button clicked slot (event handler)
    def buttonClicked(self):
        self._counter += 1
        self.sender().setText(self.BUTTON_TEXT_LIST[self._counter % len(self.BUTTON_TEXT_LIST)])

    # Event handler of MainWindow (self widget) for mouse press
    def mousePressEvent(self, e):
        button = e.button()
        if button == Qt.MouseButton.RightButton:
            self.setWindowTitle(f"mousePressEvent: right")
        if button == Qt.MouseButton.LeftButton:
            self.setWindowTitle(f"mousePressEvent: left")
        if button == Qt.MouseButton.MiddleButton:
            self.setWindowTitle(f"mousePressEvent: middle")

    # Event handler of MainWindow (self widget) for mouse release
    def mouseReleaseEvent(self, e):
        self.setWindowTitle("mouseReleaseEvent")

    # Event handler of MainWindow (self widget) for mouse double click
    def mouseDoubleClickEvent(self, e):
        self.setWindowTitle("mouseDoubleClickEvent")

    # Event handler of MainWindow (self widget) for context menu
    def contextMenuEvent(self, e):
        context = QMenu(self)
        # 1st action
        actionFirst = QAction("set title to 1st", self)
        actionFirst.triggered.connect(self.actionTriggered)
        context.addAction(actionFirst)
        # 2nd action
        actionSecond = QAction("set title to 2nd", self)
        actionSecond.triggered.connect(self.actionTriggered)
        context.addAction(actionSecond)
        # show context menu
        context.exec(e.globalPos())

    # Conect menu action triggered slot (event handler)
    def actionTriggered(self):
        self.setWindowTitle(self.sender().text()[-3:])

# main function
def main():
     # You need one (and only one) QApplication instance per application.
    # Pass in sys.argv to allow command line arguments for your app.
    # If you know you won't use command line arguments QApplication([]) works too.
    app = QApplication(sys.argv)

    # Create a Qt widget, which will be our window.
    window = MainWindow()
    window.show()  # IMPORTANT!!!!! Windows are hidden by default.

    # Start the event loop.
    app.exec()

    # Your application won't reach here until you exit and the event
    # loop has stopped.

if __name__ == "__main__":
   main()