# pylint: disable = line-too-long, invalid-name

"""
List of files to save on host AQLAAAPW
"""
import os

USERNAME = os.environ["USERNAME"]
APPDATA = os.path.realpath(os.environ["APPDATA"])
LOCALAPPDATA = os.path.realpath(os.environ["LOCALAPPDATA"])
USERPROFILE = os.path.realpath(os.environ["USERPROFILE"])
WSLROOT = R"//wsl.localhost/xde-22"

BACKUP_DIR = f"{USERPROFILE}/OD/BackupSettings"
FILE_LIST = [
    ["BeyondCompare4",         f"{APPDATA}/Scooter Software/Beyond Compare 4",                                "BCState.xml"                     ],
    ["DoubleCommander",        "C:/Programs/DC/settings",                                                     "doublecmd.cfg"                   ],
    ["DoubleCommander",        "C:/Programs/DC/settings",                                                     "doublecmd.xml"                   ],
    ["DoubleCommander",        "C:/Programs/DC/settings",                                                     "colors.json"                     ],
    ["DoubleCommander",        "C:/Programs/DC/settings",                                                     "highlighters.xml"                ],
    ["Git",                    f"{USERPROFILE}",                                                              ".bash_history"                   ],
    ["Git",                    f"{USERPROFILE}",                                                              ".gitconfig"                      ],
    ["Git",                    f"{USERPROFILE}",                                                              ".bashrc"                         ],
    ["Git",                    f"{USERPROFILE}",                                                              ".inputrc"                        ],
    ["Git",                    f"{USERPROFILE}",                                                              ".private_functions.bash"         ],
    ["PowerShell",             f"{APPDATA}/Microsoft/Windows/PowerShell/PSReadLine",                          "ConsoleHost_history.txt"         ],
    ["PowerShell",             f"{USERPROFILE}/OD/Documents/WindowsPowershell",                               "Microsoft.PowerShell_profile.ps1"],
    ["PowerToysAlwaysOnTop",   f"{LOCALAPPDATA}/Microsoft/PowerToys/AlwaysOnTop",                             "settings.json"                   ],
    ["PowerToysColorPicker",   f"{LOCALAPPDATA}/Microsoft/PowerToys/ColorPicker",                             "settings.json"                   ],
    ["PowerToysFancyZones",    f"{LOCALAPPDATA}/Microsoft/PowerToys/FancyZones",                              "settings.json"                   ],
    ["PowerToysFindMyMouse",   f"{LOCALAPPDATA}/Microsoft/PowerToys/FindMyMouse",                             "settings.json"                   ],
    ["PowerToysRun",           f"{LOCALAPPDATA}/Microsoft/PowerToys/PowerToys Run",                           "settings.json"                   ],
    ["PowerToysMeasureTool",   f"{LOCALAPPDATA}/Microsoft/PowerToys/Measure Tool",                            "settings.json"                   ],
    ["PowerToysTextExtractor", f"{LOCALAPPDATA}/Microsoft/PowerToys/TextExtractor",                           "settings.json"                   ],
    ["SSH",                    f"{USERPROFILE}/.ssh",                                                         "id_ed25519"                      ],
    ["SSH",                    f"{USERPROFILE}/.ssh",                                                         "id_ed25519.pub"                  ],
    ["SSH",                    f"{USERPROFILE}/.ssh",                                                         "known_hosts"                     ],
    ["VisualStudio2017",       f"{LOCALAPPDATA}/Microsoft/VisualStudio/15.0_a9587f1e/Settings",               "CurrentSettings.vssettings"      ],
    ["VisualStudio2022",       f"{LOCALAPPDATA}/Microsoft/VisualStudio/17.0_99f9377c/Settings",               "CurrentSettings.vssettings"      ],
    ["VSCode",                 f"{APPDATA}/Code/User",                                                        "settings.json"                   ],
    ["VSCode",                 f"{APPDATA}/Code/User",                                                        "keybindings.json"                ],
    ["WindowsTerminal",        f"{LOCALAPPDATA}/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState", "settings.json"                   ],
    ["WSL",                    f"{WSLROOT}/home/{USERNAME}",                                                  ".bash_history"                   ],
    ["WSL",                    f"{WSLROOT}/home/{USERNAME}",                                                  ".gitconfig"                      ],
    ["WSL",                    f"{WSLROOT}/home/{USERNAME}",                                                  ".bashrc"                         ],
    ["WSL",                    f"{WSLROOT}/home/{USERNAME}",                                                  ".inputrc"                        ],
    ["WSL",                    f"{WSLROOT}/home/{USERNAME}",                                                  ".private_functions.bash"         ]
]
