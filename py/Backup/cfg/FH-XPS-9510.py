# pylint: disable = line-too-long, invalid-name

"""
List of files to save on host FH-XPS-9510
"""

import os

# List files to save
USERNAME = os.environ["USERNAME"]
APPDATA = os.path.realpath(os.environ["APPDATA"])
LOCALAPPDATA = os.path.realpath(os.environ["LOCALAPPDATA"])
USERPROFILE = os.path.realpath(os.environ["USERPROFILE"])
WSLROOT = "//wsl.localhost/Ubuntu-22.04"
NEXTCLOUD = "D:/Nextcloud"

BACKUP_DIR = f"{NEXTCLOUD}/Beallitasok/BackupSettings"
FILE_LIST = [
    ["DoubleCommander",        f"{APPDATA}/doublecmd",                                                        "colors.json"                     ],
    ["DoubleCommander",        f"{APPDATA}/doublecmd",                                                        "doublecmd.cfg"                   ],
    ["DoubleCommander",        f"{APPDATA}/doublecmd",                                                        "doublecmd.xml"                   ],
    ["DoubleCommander",        f"{APPDATA}/doublecmd",                                                        "highlighters.xml"                ],
    ["Git",                    f"{USERPROFILE}",                                                              ".bash_history"                   ],
    ["Git",                    f"{USERPROFILE}",                                                              ".gitconfig"                      ],
    ["Git",                    f"{USERPROFILE}",                                                              ".bashrc"                         ],
    ["Git",                    f"{USERPROFILE}",                                                              ".inputrc"                        ],
    ["Git",                    f"{USERPROFILE}",                                                              ".private_functions.bash"         ],
    ["Matlab",                 f"{APPDATA}/MathWorks/MATLAB/R2022b",                                          "matlab.prf"                      ],
    ["Matlab",                 f"{APPDATA}/MathWorks/MATLAB/R2022b",                                          "MATLABDesktop.xml"               ],
    ["Matlab",                 f"{APPDATA}/MathWorks/MATLAB/R2022b",                                          "FavoriteCommands.xml"            ],
    ["Notepad++",              f"{APPDATA}/Notepad++/themes",                                                 "Dracula.xml"                     ],
    ["Notepad++",              f"{APPDATA}/Notepad++",                                                        "Config.xml"                      ],
    ["Nextcloud",              NEXTCLOUD,                                                                     ".sync-exclude.lst"               ],
    ["PowerShell",             f"{APPDATA}/Microsoft/Windows/PowerShell/PSReadLine",                          "ConsoleHost_history.txt"         ],
    ["PowerShell",             f"{NEXTCLOUD}/Dokumentumok/PowerShell",                                        "Microsoft.PowerShell_profile.ps1"],
    ["PowerToysAlwaysOnTop",   f"{LOCALAPPDATA}/Microsoft/PowerToys/AlwaysOnTop",                             "settings.json"                   ],
    ["PowerToysColorPicker",   f"{LOCALAPPDATA}/Microsoft/PowerToys/ColorPicker",                             "settings.json"                   ],
    ["PowerToysFancyZones",    f"{LOCALAPPDATA}/Microsoft/PowerToys/FancyZones",                              "settings.json"                   ],
    ["PowerToysFindMyMouse",   f"{LOCALAPPDATA}/Microsoft/PowerToys/FindMyMouse",                             "settings.json"                   ],
    ["PowerToysPowerToysRun",  f"{LOCALAPPDATA}/Microsoft/PowerToys/PowerToys Run",                           "settings.json"                   ],
    ["PowerToysMeasureTool",   f"{LOCALAPPDATA}/Microsoft/PowerToys/Measure Tool",                            "settings.json"                   ],
    ["PowerToysTextExtractor", f"{LOCALAPPDATA}/Microsoft/PowerToys/TextExtractor",                           "settings.json"                   ],
    ["SSH",                    f"{USERPROFILE}/.ssh",                                                         "id_ed25519"                      ],
    ["SSH",                    f"{USERPROFILE}/.ssh",                                                         "id_ed25519.pub"                  ],
    ["SSH",                    f"{USERPROFILE}/.ssh",                                                         "known_hosts"                     ],
    ["Syncthing",              f"{LOCALAPPDATA}/Syncthing",                                                   "cert.pem"                        ],
    ["Syncthing",              f"{LOCALAPPDATA}/Syncthing",                                                   "config.xml"                      ],
    ["Syncthing",              f"{LOCALAPPDATA}/Syncthing",                                                   "csrftokens.txt"                  ],
    ["Syncthing",              f"{LOCALAPPDATA}/Syncthing",                                                   "https-cert.pem"                  ],
    ["Syncthing",              f"{LOCALAPPDATA}/Syncthing",                                                   "https-key.pem"                   ],
    ["Syncthing",              f"{LOCALAPPDATA}/Syncthing",                                                   "key.pem"                         ],
    ["Syncthing",              "D:/Syncthing",                                                                ".stglobalignore"                 ],
    ["Syncthing",              "D:/Syncthing",                                                                ".stignore"                       ],
    ["VisualStudio",           f"{LOCALAPPDATA}/Microsoft/VisualStudio/17.0_13683f95/Settings",               "CurrentSettings.vssettings"      ],
    ["VSCode",                 f"{APPDATA}/Code/User",                                                        "settings.json"                   ],
    ["VSCode",                 f"{APPDATA}/Code/User",                                                        "keybindings.json"                ],
    ["WindowsTerminal",        f"{LOCALAPPDATA}/Packages/Microsoft.WindowsTerminal_8wekyb3d8bbwe/LocalState", "settings.json"                   ],
    ["WSL",                    f"{WSLROOT}/home/feri",                                                        ".bash_history"                   ],
    ["WSL",                    f"{WSLROOT}/home/feri",                                                        ".gitconfig"                      ],
    ["WSL",                    f"{WSLROOT}/home/feri",                                                        ".bashrc"                         ],
    ["WSL",                    f"{WSLROOT}/home/feri",                                                        ".inputrc"                        ],
    ["WSL",                    f"{WSLROOT}/home/feri",                                                        ".private_functions.bash"         ],
    ["WSL_Conf",               f"{WSLROOT}/etc",                                                              "wsl.conf"                        ],
    ["WSL_SSH",                f"{WSLROOT}/home/feri/.ssh",                                                   "id_ed25519"                      ],
    ["WSL_SSH",                f"{WSLROOT}/home/feri/.ssh",                                                   "id_ed25519.pub"                  ],
    ["WSL_SSH",                f"{WSLROOT}/home/feri/.ssh",                                                   "known_hosts"                     ],
]
