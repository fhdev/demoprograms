# pylint: disable = subprocess-run-check
"""
Backup and restore specified files
"""
# imports ##############################################################################################################
import argparse
import datetime
import importlib
import logging
import os
import platform
import shutil
import subprocess
import sys

from tkinter import messagebox
from types import ModuleType

# global constants #####################################################################################################
SCRIPT_ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
BACKUP_ROOT_DIR = os.path.join(SCRIPT_ROOT_DIR, "settings")
CURRENT_DIR = os.getcwd()
HOSTNAME = platform.node()
HOME = os.path.expanduser('~')

# functions ############################################################################################################
def setup_logging() -> None:
    """
    Setup logging format
    """
    logging.basicConfig(format="[%(asctime)s.%(msecs)03d] [%(levelname)-8s] %(message)s",
                        datefmt="%Y.%m.%d %H:%M:%S",
                        level=logging.DEBUG)

def load_cfg(config_file:str) -> ModuleType:
    """
    Load configuration python file
    """
    config_dir = os.path.dirname(config_file)
    module_name = os.path.splitext(os.path.basename(config_file))[0]
    sys.path.append(config_dir)
    cfg = importlib.import_module(module_name)
    return cfg

def parse_args() -> argparse.Namespace:
    """
    Process command line arguments
    """
    parser = argparse.ArgumentParser(description="Backup and restore setting files")
    default_config_file = os.path.join(SCRIPT_ROOT_DIR, "cfg", f"{HOSTNAME}.py")
    parser.add_argument(
        "-c", "--config_file", dest="config_file", help="Configuration file", default=default_config_file
    )
    parser.add_argument(
        "-r", "--restore", dest="restore", help="Restore files", default=False, action="store_true"
    )
    # Parse arguments
    return parser.parse_args()

def backup_or_restore(backup_dir:str, file_list:list[list[str]], restore:bool) -> None:
    """
    Backup or restore specified files
    """
    # Inform user
    if restore:
        logging.info("Restoring backup ...")
    else:
        logging.info("Creating backup ...")
    # Copy files
    for file_spec in file_list:
        # get backup file spec
        backup_subdir_name = file_spec[0]
        src_dir_path = file_spec[1]
        file_name = file_spec[2]
        # assemble source and destination paths
        src_path = os.path.join(os.path.realpath(src_dir_path), file_name)
        dst_dir = os.path.join(backup_dir, HOSTNAME, backup_subdir_name)
        dst_path = os.path.join(dst_dir, file_name)
        # swap source and destination for restore
        if restore:
            src_path, dst_path = dst_path, src_path
        # create destination directory if necessary
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir, exist_ok=True)
        # check source file existence
        if not os.path.exists(src_path):
            msg = f"File '{src_path}' does not exist! Proceeding."
            logging.warning(msg)
            # this program is supposed to run automatically without a window, so a pop-up is needed to recognize errors
            messagebox.showwarning("Backup warning", msg)
            continue
        logging.info("Copying: '%s' -> '%s' .", src_path, dst_path)
        try:
            shutil.copy2(src_path, dst_path)
        except Exception as e: # pylint: disable = broad-exception-caught
            msg = f"Failed to copy: '{src_path}' -> '{dst_path}' . Reason: '{e}'. Aborting."
            logging.error(msg)
            # this program is supposed to run automatically without a window, so a pop-up is needed to recognize errors
            messagebox.showerror("Backup error", msg)
            return
    # Commit changes if there are any
    if not restore:
        git_commit_backup(backup_dir)

def git_commit_backup(backup_dir:str) -> None:
    """
    Create git repository in backup directory if necessary. Commit backup files if there are any changes.
    """
    run_kwargs = {
        "stdout": subprocess.PIPE,
        "stderr": subprocess.PIPE,
        "text": True,
        "check": False,
        "creationflags": subprocess.CREATE_NO_WINDOW
    }
    # check if git is working
    args = ["git", "--version"]
    result = subprocess.run(args, **run_kwargs)
    if result.returncode != 0:
        msg = "Can not execute command 'git --version'! Aborting."
        logging.error(msg)
        messagebox.showerror("Backup error", msg)
        return
    # initialize git repository if this was not yet done
    args = ["git", "-C", backup_dir, "status"]
    result = subprocess.run(args, **run_kwargs)
    if result.returncode == 0:
        logging.info("Backup directory '%s' is already a git repository.", backup_dir)
    else:
        # this is not a git repository yet
        logging.info("Initializing git repository in directory '%s' ...", backup_dir)
        args = ["git", "-C", backup_dir, "init"]
        result = subprocess.run(args, **run_kwargs)
        if result.returncode != 0:
            msg = f"Failed to initialize git repository in directory '{backup_dir}'! " + \
                  f"Git output: '{result.stdout} {result.stderr}'. Aborting."
            logging.error(msg)
            # this program is supposed to run automatically without a window, so a pop-up is needed to recognize errors
            messagebox.showerror("Backup error", msg)
            return
    # check for changes
    args = ["git", "-C", backup_dir, "status", "--porcelain"]
    result = subprocess.run(args, **run_kwargs)
    if result.returncode != 0:
        msg = f"Failed to check changes in git repository in directory '{backup_dir}'! " + \
              f"Git output: '{result.stdout} {result.stderr}'. Aborting."
        logging.error(msg)
        # this program is supposed to run automatically without a window, so a pop-up is needed to recognize errors
        messagebox.showerror("Backup error", msg)
        return
    if not result.stdout.strip():
        logging.info("There are no differences to commit in repository '%s'.", backup_dir)
        return
    # add changes
    logging.info("Commiting backup to repository '%s' ...", backup_dir)
    args = ["git", "-C", backup_dir, "add", "--all"]
    result = subprocess.run(args, **run_kwargs)
    if result.returncode != 0:
        msg = f"Failed to add all files to git repository in directory '{backup_dir}'! " + \
              f"Git output: '{result.stdout} {result.stderr}'. Aborting."
        logging.error(msg)
        # this program is supposed to run automatically without a window, so a pop-up is needed to recognize errors
        messagebox.showerror("Backup error", msg)
        return
    # commit changes
    timestamp = datetime.datetime.now().strftime("%Y.%m.%d. %H:%M:%S")
    args = ["git", "-C", backup_dir, "commit", "--message", f"Automatic backup created at {timestamp}"]
    result = subprocess.run(args, **run_kwargs)
    if result.returncode != 0:
        msg = f"Failed to commit changes to git repository in directory '{backup_dir}'! " + \
              f"Git output: '{result.stdout} {result.stderr}'. Aborting."
        logging.error(msg)
        # this program is supposed to run automatically without a window, so a pop-up is needed to recognize errors
        messagebox.showerror("Backup error", msg)
        return

def main() -> int:
    """
    Main function. The program begins here.
    """
    # initialize logging
    setup_logging()
    # load arguments
    args = parse_args()
    # load configuration file
    cfg = load_cfg(args.config_file)
    # print general information
    logging.info("Script root directory is '%s'.", SCRIPT_ROOT_DIR)
    logging.info("Current directory is '%s'.", CURRENT_DIR)
    logging.info("Configuration file is '%s'.", args.config_file)
    logging.info("Backup directory is '%s'.", cfg.BACKUP_DIR)
    # perform backup or restore
    backup_or_restore(cfg.BACKUP_DIR, cfg.FILE_LIST, args.restore)
    return 0

# script body ##########################################################################################################
if __name__ == "__main__":
    exit(main())
