import numpy as np
import time

def primes1(n):
    # candidates for primes
    candidates = np.arange(2, n + 1)
    # logical indices that indicate primes (True = prime)
    indices = np.ones(candidates.shape, dtype=bool)
    for c in candidates:
        indices[2 * c - 2 : : c] = False
    return candidates[indices]

def primes2(n):
    candidates = np.hstack((2, np.arange(3, n + 1, 2)))
    primes = np.zeros(candidates.shape, dtype=int)
    nPrimes = 0
    if n > 1:
        for c in candidates:
            isPrime = True
            for i in np.arange(0, nPrimes):
                if (c % primes[i]) == 0:
                    isPrime = False
                    break
            if isPrime:
                primes[nPrimes] = c
                nPrimes += 1
    return primes[0:nPrimes]


def main():
    # Comparison
    n = 100

    start = time.time()
    p1 = primes1(n)
    end = time.time()
    print(p1)
    print(f"Elapsed time is {1000 * (end - start)} ms.")

    start = time.time()
    p2 = primes2(n)
    end = time.time()
    print(p2)
    print(f"Elapsed time is {1000 * (end - start)} ms.")

    # Tests
    inputs = np.array([0, 1, 2, 5, 40])
    outputs = [np.array([]), np.array([]), np.array([2]), np.array([2, 3, 5]), np.array([2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37])]

    for i in np.arange(0, inputs.size):
        assert np.array_equal(primes1(inputs[i]), outputs[i]), f"Error: primes1 outputs faulty value for input {i}!"
        assert np.array_equal(primes2(inputs[i]), outputs[i]), f"Error: primes2 outputs faulty value for input {i}!"
    
    print("All tests passed.")

if __name__ == "__main__":
    main()