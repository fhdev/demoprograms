
"""
Open a file in a specific VSCode window
"""
import os
import subprocess
import sys
import time
import win32gui
import win32con

def find_window(title):
    """
    Finds and returns the window handle of the window that has the specified title in its window title
    """
    def callback(hwnd, hwnd_list):
        if title in win32gui.GetWindowText(hwnd):
            hwnd_list.append(hwnd)
    hwnds = []
    win32gui.EnumWindows(callback, hwnds)
    return hwnds[0] if hwnds else None

def activate_window(hwnd):
    """
    Activates the window that has the specified window handle
    """
    win32gui.ShowWindow(hwnd, win32con.SW_SHOW)
    win32gui.SetForegroundWindow(hwnd)

def main() -> int:
    """
    Main function. Program execution originates here
    """
    # get arguments
    file_path = sys.argv[1]
    if not os.path.exists(file_path):
        return 1
    print(f"Opening '{file_path}' ...")
    # create fake workspace directory if needed
    home_dir = os.path.expanduser("~")
    dir_name = "__vscode_workspace__"
    workspace_dir_path = os.path.join(home_dir, dir_name)
    if os.path.exists(workspace_dir_path):
        print(f"Workspace directory '{workspace_dir_path}' already exists ...")
    else:
        print(f"Creating workspace directory '{workspace_dir_path}' ...")
        os.makedirs(workspace_dir_path, exist_ok=True)
    # assemble title
    window_title = f"{dir_name} - Visual Studio Code"
    # open vscode if not yet opened
    hwnd = find_window(window_title)
    if hwnd is None:
        print(f"Opening new VSCode instance at '{workspace_dir_path}' ...")
        args = ["code.cmd", "--new-window", workspace_dir_path]
        subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, check=False, creationflags=subprocess.CREATE_NO_WINDOW)
        time.sleep(1)
        hwnd = find_window(window_title)
    # activate window an open file
    if hwnd is not None:
        print(f"Activating existing VSCode instance at '{workspace_dir_path}' ...")
        activate_window(hwnd)
        time.sleep(0.1)
        args = ["code.cmd", "--reuse-window", file_path]
        subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True, check=False, creationflags=subprocess.CREATE_NO_WINDOW)
    else:
        print(f"No existing VSCode instance found at '{workspace_dir_path}' ...")
    return 0

if __name__ == "__main__":
    exit(main())
