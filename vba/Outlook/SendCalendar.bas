Sub SendCalendar()

Dim Namespace As Namespace
Dim Folder As Folder
Dim CalendarExporter As CalendarSharing
Dim MailItem As Outlook.MailItem
Dim Subject As String
Dim SentMailItem As Outlook.MailItem
Dim Answer As Integer

' Get default calendar folder and its exporter
Set Namespace = Application.GetNamespace("MAPI")
Set Folder = Namespace.GetDefaultFolder(olFolderCalendar)
Set CalendarExporter = Folder.GetCalendarExporter

' Set properties of export
CalendarExporter.CalendarDetail = olFreeBusyAndSubject
CalendarExporter.StartDate = Date
CalendarExporter.EndDate = DateAdd("d", 14, Date)
CalendarExporter.IncludeAttachments = False
CalendarExporter.RestrictToWorkingHours = False
CalendarExporter.IncludePrivateDetails = False

' Create mail
Set MailItem = CalendarExporter.ForwardAsICal(olCalendarMailFormatDailySchedule)
MailItem.To = "john.doe@mail.com"
MailItem.Subject = "Calendar of " & Environ("USERNAME") & " on " & Format(Now, "yyyy.MM.dd HH:mm:ss")

' Make sure user wants to send Calendar
Answer = MsgBox("Are You sure you want to send the calendar?", vbYesNo, "Confirmation")

If Answer = vbYes Then
    ' Send mail
    MailItem.Send
Else
    ' Delete mail
    MailItem.Delete
End If

End Sub