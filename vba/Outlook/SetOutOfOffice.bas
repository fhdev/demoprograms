Sub SetOutOfOffice()

'###### Get common variables #######################################################################
Set Namespace = Application.GetNamespace("MAPI")
Set Explorer = Application.ActiveExplorer
Set CurrentFolder = Explorer.CurrentFolder
Set CurrentView = Explorer.CurrentView
Set DefaultCalendar = Namespace.GetDefaultFolder(olFolderCalendar)
Set NavPane = Explorer.NavigationPane
Set NavModule = NavPane.Modules.GetNavigationModule(olModuleCalendar)
Set NavGroups = NavModule.NavigationGroups
Set SharedCalendarName = "MyCalendar"

'###### Find team calendar #########################################################################
TeamCalendar = Empty
' Search for team calendar among shared calendars in a language independent way
For Each NavGroup In NavModule.NavigationGroups
    If NavGroup.Name = "Megosztott naptárak" Or NavGroup.Name = "Shared Calendars" Or NavGroup.Name = "Freigegebene Kalender"  Then
        For Each NavFolder In NavGroup.NavigationFolders
            If InStr(NavFolder.DisplayName, SharedCalendarName) > 0 Then
                Set TeamCalendar = NavFolder.Folder
                Exit For
            End If
        Next
    End If
Next

' If the team calendar is not among shared calendars, search for it among own calendars
If TeamCalendar = Empty Then
    For Each SubCalendar In DefaultCalendar.Folders
        If SubCalendar.Name = SharedCalendarName Then
            Set TeamCalendar = SubCalendar
        End If
    Next
End If

' If team calendar is not found, give error message and exit
If TeamCalendar = Empty Then
    result = MsgBox("Team calendar could not be found! Exiting...", vbOKOnly, "Error")
    Exit Sub
End If

'###### Create appointment #########################################################################
' Create appointment only if we are in the default Calendar's view
If CurrentView.ViewType = olCalendarView And CurrentFolder = DefaultCalendar Then
    ' Set common settings
    Set DefaultAppointment = DefaultCalendar.Items.Add(olAppointmentItem)
    DefaultAppointment.MeetingStatus = olMeeting
    DefaultAppointment.Importance = olImportanceNormal
    DefaultAppointment.Start = CurrentView.SelectedStartTime
    DefaultAppointment.End = CurrentView.SelectedEndTime
    If (Hour(CurrentView.SelectedStartTime) = 0) And (Minute(CurrentView.SelectedStartTime) = 0) Then
        DefaultAppointment.AllDayEvent = True
    Else
        DefaultAppointment.AllDayEvent = False
    End If
    DefaultAppointment.ReminderSet = False
    DefaultAppointment.ResponseRequested = False
	
    ' Get user defined settings
    Subject = ""
    Body = ""
    BusyStatus = olFree
    UserOption = InputBox("Set absence type in the textbox below!" + Chr(13) + _
        "1 - Home office" + Chr(13) + _
        "2 - Holiday" + Chr(13) + _
        "3 - Training", "Please select absence type", "1")
    Select Case UserOption
        Case 1
            ' Home office
            Subject = "Home office"
            Body = InputBox("Set your availability in the textbox below!", _
                "Please set your availability", "Available: " + Chr(13) + "08:00-16:30")
            BusyStatus = olWorkingElsewhere
        Case 2
            ' Holiday
            Subject = "Holiday"
            Body = ""
            BusyStatus = olOutOfOffice
        Case 3
            ' Training
            Subject = "Training"
            Body = ""
            BusyStatus = olOutOfOffice
    End Select
    
    If Subject = "" Then
        DefaultAppointment.Delete
		Exit Sub
    End If
	
	' Apply user defined settings
	' Add user
	Subject = Subject + " (" + Namespace.CurrentUser + ")"
	' Finish setting of the appointment in the default calendar and save it
	DefaultAppointment.Subject = Subject
	DefaultAppointment.Body = Body
	DefaultAppointment.BusyStatus = BusyStatus
	DefaultAppointment.Save
	' Create item in team calendar
	Set TeamAppointment = DefaultAppointment.CopyTo(TeamCalendar, olCreateAppointment)
	TeamAppointment.Subject = Subject ' Must do this because Outlook overrides with Copy of ...
	TeamAppointment.BusyStatus = olFree
	TeamAppointment.Save

End If

End Sub

