use std::io::{self, Write};
fn main() {
    println!("Solving the equation: a.x^2 + b.x +c .");
    println!("Please enter the values of the coefficients.");
    let mut input = String::new(); 

    print!("a=");
    io::stdout().flush().expect("Failed to flush");
    io::stdin() .read_line(&mut input).expect("Failed to read line"); 
    let a : f64 = input.trim().parse().expect("Failed to convert string to double");
    input.clear();

    print!("b=");
    io::stdout().flush().expect("Failed to flush");
    io::stdin() .read_line(&mut input).expect("Failed to read line"); 
    let b : f64 = input.trim().parse().expect("Failed to convert string to double");
    input.clear();

    print!("c=");
    io::stdout().flush().expect("Failed to flush");
    io::stdin() .read_line(&mut input).expect("Failed to read line"); 
    let c : f64 = input.trim().parse().expect("Failed to convert string to double");
    input.clear();

    println!("{}.x^2 +{}.x + {} = 0", a, b, c);
    const TOL: f64 = 1e-9;
    if a == 0.0 {
        if b == 0.0 {
            if c == 0.0 {
                println!("0 = 0");
            }
            else {
                println!("?! 0 = {} ?!", c)
            }
        }
        else {
            let x = -c / b;
            println!("x = {}", x);
        }
    }
    else {
        let d = b * b - 4.0 * a * c;
        if d.abs() < TOL {
            let x12 = -b / (2.0 *a);
            println!("x12 = {}", x12);
        }
        else if d < 0.0 {
            let re = -b / (2.0 *a);
            let im = (-d).sqrt() / (2.0 *a);
            println!("x1 = {} + {}i, x2 = {} - {}i", re, im, re, im);
        }
        else {
            let x1 = (-b + d.sqrt()) / (2.0 * a);
            let x2 = (-b - d.sqrt()) / (2.0 * a);
            println!("x1 = {}, x2 = {}", x1, x2);
        }
    }

}
