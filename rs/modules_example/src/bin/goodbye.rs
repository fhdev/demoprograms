use modules_example::greet;
use modules_example::salute;

fn main() {
    println!("Goodbye, world!");
    modules_example::lib_fn();
    greet::greet_fn();
    salute::salute_fn();
}
