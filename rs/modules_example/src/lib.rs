pub mod greet;
pub mod salute;

pub fn lib_fn() {
    println!("Lib ...");
    greet::greet_fn();
    salute::salute_fn();
}
