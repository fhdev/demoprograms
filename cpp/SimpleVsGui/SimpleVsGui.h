#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_SimpleVsGui.h"

class SimpleVsGui : public QMainWindow
{
    Q_OBJECT

signals:
    void myTextFieldChanged(const QString& newText) const;

public:
    SimpleVsGui(QWidget *parent = Q_NULLPTR);
    void setMyTextField(const QString& text);

private slots:
    void buttonClicked();

private:
    Ui::SimpleVsGuiClass ui;
    QString myTextField;
};
