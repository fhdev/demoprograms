#include "SimpleVsGui.h"

SimpleVsGui::SimpleVsGui(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    // Click event handler
    connect(ui.leftButton, &QPushButton::clicked, this, &SimpleVsGui::buttonClicked);
    connect(ui.rightButton, &QPushButton::clicked, this, &SimpleVsGui::buttonClicked);

    // Simple data binding
    connect(this, &SimpleVsGui::myTextFieldChanged, ui.bindTextEdit, &QPlainTextEdit::setPlainText);

}

void SimpleVsGui::buttonClicked()
{
    const QString clickedStr = "Clicked!";
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    if (buttonSender->text() == clickedStr)
    {
        setMyTextField("Change the value of myTextField.");
    }
    else
    {
        buttonSender->setText(clickedStr);
        buttonSender->setStyleSheet("background-color:red");
    }
}

void SimpleVsGui::setMyTextField(const QString& value)
{
    if (myTextField != value)
    {
        myTextField = value;
        emit myTextFieldChanged(value);
    }
}
