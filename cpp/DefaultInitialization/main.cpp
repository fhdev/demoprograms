#include <iostream>
#include <stdexcept>
#include <vector>

class classDefault {
   public:
    // these are just declarations
    static int si;
    static float sf;
    static double sd;
    // no default-initialization for non-class types
    int i;
    float f;
    double d;
};
// these are the actual definitions, static variables are zero-initialized by default
int classDefault::si;
float classDefault::sf;
double classDefault::sd;

// https://en.cppreference.com/w/cpp/language/data_members
class classSimple {
   public:
    // these are just declarations
    static int si;
    static float sf;
    static double sd;
    // default member initialization happens only if the member is omitted from the member
    // initializer list of the constructor
    int i {0};
    // initialized by the member initializer list of the constructor
    float f;
    // no default-initialization for non-class type
    double d;

    classSimple() : f{0.0f} { 
        // i and f are initialized when execution reaches this line
        std::cout << ":: ctor of classSimple (" << i << ", " << f << ", " << d << ") ::\n";
        d = 0.0;
    }
    classSimple(const classSimple &other): i{other.i}, f{other.f}, d{other.d} {
        std::cout << ":: copy ctor of classSimple ::\n";
    }
    classSimple(classSimple &&other) noexcept: i{other.i}, f{other.f}, d{other.d} {
        std::cout << ":: move ctor of classSimple ::\n";
    }
    ~classSimple() {
        std::cout << ":: dtor of classSimple ::\n";
    }
    classSimple &operator=(const classSimple &other) {
        std::cout << ":: copy assignment operator of classSimple ::\n";
        i = other.i;
        f = other.f;
        d = other.d;
        return *this;
    }
};
// these are the actual definitions, static variables are zero-initialized by default
int classSimple::si;
float classSimple::sf;
double classSimple::sd;

// https://en.cppreference.com/w/cpp/language/default_initialization
class classMember {
   public:
    static classSimple sc;
    // c is default-initialized, its default constructor is called
    classSimple c;

    classMember() {
        // default constructor is called for c before entering the constructor
        std::cout << ":: ctor of classMember (" << c.i << ", " << c.f << ", " << c.d << ") ::\n";
    }

    // default move ctor and assigment operator is only declared if there is no dtor, comment this 
    // out so that c is moved automatically
    ~classMember() { std::cout << ":: dtor of classMember ::\n"; }

};
// this is the actual definition, the default constructor of sc is called
classSimple classMember::sc;

int main() {
    std::cout << ">> MAIN BEGINS ---------------------------------------------------------------\n";
    std::cout << ">> VARIABLE INITIALIZATION ---------------------------------------------------\n";
    { 
        // No default-initialization for non-class types
        int i;
        float f;
        double d;
        // Zero-initialization for static types
        static int si;
        static float sf;
        static double sd;
        std::cout << "i = " << i << "\n";
        std::cout << "f = " << f << "\n";
        std::cout << "d = " << d << "\n";
        std::cout << "si = " << si << "\n";
        std::cout << "sf = " << sf << "\n";
        std::cout << "sd = " << sd << "\n";
    }
    std::cout << ">> CLASS MEMBER DEFAULT INITIALIZATION (WITHOUT CTOR) ------------------------\n";
    {
        classDefault c;
        std::cout << "classDefault c.i = " << c.i << "\n";
        std::cout << "classDefault c.f = " << c.f << "\n";
        std::cout << "classDefault c.d = " << c.d << "\n";
        std::cout << "classDefault::si = " << classSimple::si << "\n";
        std::cout << "classDefault::sf = " << classSimple::sd << "\n";
        std::cout << "classDefault::sd = " << classSimple::sf << "\n";

    }
    std::cout << ">> CLASS MEMBER INITIALIZATION / COPY / MOVE WITH SELF-WRITTEN CTORS ---------\n";
    {
        classSimple c;
        std::cout << "classSimple c.i = " << c.i << "\n";
        std::cout << "classSimple c.f = " << c.f << "\n";
        std::cout << "classSimple c.d = " << c.d << "\n";
        std::cout << "classSimple::si = " << classSimple::si << "\n";
        std::cout << "classSimple::sf = " << classSimple::sf << "\n";
        std::cout << "classSimple::sd = " << classSimple::sd << "\n";
        classSimple d{c};
        classSimple e{std::move(c)};
        d = e;
    }
    std::cout << ">> CLASS MEMBER DEFAULT INITIALIZATION / COPY / MOVE  ------------------------\n";
    {
        classMember c;
        std::cout << "classMember c.c.i = " << c.c.i << "\n";
        std::cout << "classMember c.c.f = " << c.c.f << "\n";
        std::cout << "classMember c.c.d = " << c.c.d << "\n";
        std::cout << "classMember::sc.i = " << classMember::sc.i << "\n";
        std::cout << "classMember::sc.f = " << classMember::sc.f << "\n";
        std::cout << "classMember::sc.d = " << classMember::sc.d << "\n";
        classMember d{c};
        classMember e{std::move(c)};
        // at this point, we get a compiler error because a move ctor is declared, that means we must declare the copy / move assignment as well
        //d = e;
    }
    std::cout << ">> MAIN ENDS -----------------------------------------------------------------\n";

    return 0;
}