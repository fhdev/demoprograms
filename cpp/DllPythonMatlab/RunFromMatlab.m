libName = 'DllPythonMatlab';
headerPath = fullfile(pwd, [libName, '.hpp']);
if strcmpi(computer, 'glnxa64')
    libraryPath = fullfile(pwd, 'build', 'linux', 'Debug', [libName, '.so']);
elseif strcmpi(computer, 'pcwin64')
    libraryPath = fullfile(pwd, 'build', 'windows', 'Debug', [libName, '.dll']);
else
    error('Only implemented for Windows and Linux!');
end
loadlibrary(libraryPath, headerPath);

scalarIn = 1;
vectIn = randi(100, randi(10) + 5, 1);
vectInLen = length(vectIn);
vectInC = libpointer('doublePtr', vectIn);
vectOutC =  libpointer('doublePtr');
scalarOut = calllib(libName, 'myfunction', scalarIn, vectInC, vectInLen, vectOutC);
vectOutC.setdatatype('doublePtr', vectInLen);
vectOut = vectOutC.Value;
calllib(libName, 'freevector', vectOutC);

fprintf('scalar input\n');
fprintf('%s\n', mat2str(scalarIn));
fprintf('vector input\n');
fprintf('%s\n', mat2str(vectIn));
fprintf('scalar output\n');
fprintf('%s\n', mat2str(scalarOut));
fprintf('vector output\n');
fprintf('%s\n', mat2str(vectOut));

unloadlibrary(libName)