#ifndef DLLPYTONMATLAB_HPP
#define DLLPYTONMATLAB_HPP

#ifdef _WINDOWS
	#define DLLEXPORT __declspec(dllexport) 
    #define CDECL __cdecl
#else
	#define DLLEXPORT
    #define CDECL
#endif

static double *ptr = NULL;

#ifdef __cplusplus
    #define EXTERN_C extern "C"
#else
    #define EXTERN_C
#endif

EXTERN_C DLLEXPORT double CDECL myfunction(double scalarIn, const double * const vectIn, int len, double** vectOut);
EXTERN_C DLLEXPORT void CDECL freevector(double* vect);

#endif // DLLPYTONMATLAB_HPP
