#include <stdlib.h>
#include "DllPythonMatlab.hpp"

// See further examples in <matlabroot>>/R2019b/extern/examples/shrlib
EXTERN_C DLLEXPORT double CDECL myfunction(double scalarIn, const double * const vectIn, int len, double** vectOut)
{
	double scalarOut = 2 * scalarIn;
	(*vectOut) = (double *)calloc(len, sizeof(double));
	for (int i = 0; i < len; i++)
		(*vectOut)[i] = 2 * vectIn[i];
	return scalarOut;
}

EXTERN_C DLLEXPORT void CDECL freevector(double* vect)
{
	free(vect);
}