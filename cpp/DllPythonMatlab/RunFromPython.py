import ctypes as c
import numpy as np
import platform

x = platform.system()
if x.lower() == 'windows':
    libPath = './build/windows/Debug/DllPythonMatlab.dll'
elif x.lower() == 'linux':
    libPath = './build/linux/Debug/DllPythonMatlab.so'
else:
    raise Exception('Only implemented for Windows and Linux!')

lib = c.CDLL(libPath)
lib.myfunction.argtypes = [c.c_double, c.POINTER(c.c_double), c.c_int, c.POINTER(c.POINTER(c.c_double))]
lib.myfunction.restype = c.c_double
lib.freevector.argtypes = [c.POINTER(c.c_double)]
lib.freevector.restype = c.c_void_p

scalarIn = 1
vectIn = np.random.randint(1, 101, np.random.randint(5, 16, 1))
vectInLen = len(vectIn)
vectInC = (c.c_double * vectInLen)(*vectIn.tolist())
vectOutC = c.POINTER(c.c_double)()
scalarOut = lib.myfunction(scalarIn, vectInC, vectInLen, c.byref(vectOutC))
vectOut = np.ctypeslib.as_array(vectOutC, (vectInLen, ))
# seems this is not needed in python
#lib.freevector(vectOutC)

print("scalar input: ")
print(scalarIn)
print("vector input: ")
print(vectIn)
print("scalar output: ")
print(scalarOut)
print("vector output: ")
print(vectOut)


