#include <algorithm>
#include <iostream>

template<typename T>
class Vector {
private:
    uint64_t size_;
    T *elements_;
public:
    // default + parametrized ctor in one
    Vector(uint64_t size = 0u) : size_{size}, elements_{size > 0u ? new T[size] : nullptr} {
        std::cout << this << " :: ctor (" << size << ") ::\n";
        for (uint64_t i = 0u; i < size_; i++) { elements_[i] = T(); }
    };

    // initializer list ctor
    Vector(std::initializer_list<T> il) : Vector(il.size()) {
        std::cout << this << " :: ctor (initializer_list) ::\n";
        if (il.size() > 0u) { std::copy(il.begin(), il.end(), elements_); }
    }

    // copy ctor
    Vector(const Vector& other) : Vector(other.size_) {
        std::cout << this << " :: copy ctor ::\n";
        if (size_ > 0u) { std::copy(other.elements_, other.elements_ + other.size_, elements_); }
    }

    // move ctor
    Vector(Vector && other) : Vector() {
        std::cout << this << " :: move ctor ::\n";
        swap(*this, other);
    }

    // dtor
    ~Vector() {
        if (elements_ != nullptr ) { delete[] elements_; }
        std::cout << this << " :: dtor ::\n";
    }

    // copy and move assigmment in one
    Vector& operator=(Vector other) noexcept {
        std::cout << this << " :: copy and move assignment operator ::\n";
        swap(*this, other);
        return *this;
    }

    // subscript operator
    T& operator[](uint64_t i) {
        return elements_[i];
    }

    // swap function for copy & swap 
    friend void swap(Vector& first, Vector& second) noexcept {
        using std::swap;
        swap(first.size_, second.size_);
        swap(first.elements_, second.elements_);
    }

    // to print 
    friend std::ostream& operator<< (std::ostream& os, const Vector& vect) {
        os << &vect << " { size=" << vect.size_ << "; elements=" << vect.elements_ << "; *elements=";
        for (uint64_t i = 0u; i < vect.size_; i++) {
            os << vect.elements_[i];
            if (i < (vect.size_ - 1u)) {os << ","; }
        }
        os << " }\n";
        return os;
    }
};

class Point {
public:
    // default and parametrized constructor
    Point(double x=0.0, double y=0.0, double z=0.0): x_{x}, y_{y}, z_{z} {}

    // to print 
    friend std::ostream& operator<< (std::ostream& os, const Point& point) {
        os << "{" << point.x_ << "," << point.y_ << "," << point.z_ << "}";
        return os;
    }
private:
    double x_;
    double y_;
    double z_; 
};

void doubleVector() {
    std::cout << "Vector<double> r{}; ------------------------------------------------------------------------------\n";
    Vector<double> r{};
    std::cout << "r = " << r << "\n\n";

    std::cout << "Vector<double> s{r}; -----------------------------------------------------------------------------\n";
    Vector<double> s{r};
    std::cout << "s = " << s << "\n\n";

    std::cout << "Vector<double> t(5); -----------------------------------------------------------------------------\n";
    Vector<double> t(5);
    std::cout << "t = " << t << "\n\n";

    std::cout << "Vector<double> u{1.1, 2.2, 3.3}; -----------------------------------------------------------------\n";
    Vector<double> u{1.1, 2.2, 3.3};
    std::cout << "u = " << u << "\n\n";

    std::cout << "Vector<double> v{u}; -----------------------------------------------------------------------------\n";
    Vector<double> v{u};
    std::cout << "v = " << v << "\n\n";

    std::cout << "Vector<double> w{std::move(t)}; ------------------------------------------------------------------\n";
    Vector<double> w{std::move(t)};
    std::cout << "w = " << w << "\n\n";

    std::cout << "w = u; -------------------------------------------------------------------------------------------\n";
    w = u;
    std::cout << "w = " << w << "\n\n";

    std::cout << "w = std::move(v); --------------------------------------------------------------------------------\n";
    w = std::move(v);
    std::cout << "w = " << w << "\n\n";

    std::cout << "--------------------------------------------------------------------------------------------------\n";
}

void pointVector() {
    std::cout << "Vector<Point> r{}; -------------------------------------------------------------------------------\n";
    Vector<Point> r{};
    std::cout << "r = " << r << "\n\n";

    std::cout << "Vector<Point> s{r}; ------------------------------------------------------------------------------\n";
    Vector<Point> s{r};
    std::cout << "s = " << s << "\n\n";

    std::cout << "Vector<Point> t(5); ------------------------------------------------------------------------------\n";
    Vector<Point> t(5);
    std::cout << "t = " << t << "\n\n";

    std::cout << "Vector<Point> u{{1.1, 1.1, 1.1},{2.2, 2.2, 2.2}, {3.3, 3.3, 3.3}}; -------------------------------\n";
    Vector<Point> u{{1.1, 1.1, 1.1},{2.2, 2.2, 2.2}, {3.3, 3.3, 3.3}};
    std::cout << "u = " << u << "\n\n";

    std::cout << "Vector<Point> v{u}; ------------------------------------------------------------------------------\n";
    Vector<Point> v{u};
    std::cout << "v = " << v << "\n\n";

    std::cout << "Vector<Point> w{std::move(t)}; -------------------------------------------------------------------\n";
    Vector<Point> w{std::move(t)};
    std::cout << "w = " << w << "\n\n";

    std::cout << "w = u; -------------------------------------------------------------------------------------------\n";
    w = u;
    std::cout << "w = " << w << "\n\n";

    std::cout << "w = std::move(v); --------------------------------------------------------------------------------\n";
    w = std::move(v);
    std::cout << "w = " << w << "\n\n";

    std::cout << "--------------------------------------------------------------------------------------------------\n";
}

int main() {
    doubleVector();
    pointVector();
    return 0;
}