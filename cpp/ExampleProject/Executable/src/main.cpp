#include <iostream>
#include "staticlibrary.hpp"
#include "dynamiclibrary.hpp"

using namespace ExampleProject;

int main(int argc, char *argv[])
{
    std::cout << "Hello!" << std::endl;
    std::cout << add(5, 10) << std::endl;
    std::cout << multiply(5, 10) << std::endl;
    return 0;
}
