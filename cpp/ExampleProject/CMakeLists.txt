cmake_minimum_required(VERSION 3.0.0)
project(CMakeTest VERSION 0.1.0)

include(CTest)
enable_testing()




# Add project ######################################################################################
project(ExampleProject)

# Compiler definitions #############################################################################
if (UNIX)
    add_compile_definitions(EXE_COMPILER_DEFS _GNU_SOURCE NDEBUG)
endif (UNIX)
if (WIN32)
    add_compile_definitions(_CRT_SECURE_NO_DEPRECATE _SCL_SECURE_NO_DEPRECATE _SECURE_SCL=0 
        _USE_MATH_DEFINES NDEBUG WIN32)
endif (WIN32)

# Compiler options  ################################################################################
if (UNIX)
    add_compile_options(-march=native -fPIC -fexceptions -fno-omit-frame-pointer -pthread -fwrapv
        -std=c++11)
endif (UNIX)
if (WIN32)
    add_compile_options(/Zp8 /GR /W3 /EHs /Oy-)
endif (WIN32)

# Extensions #######################################################################################
if (UNIX)
    set(STATICLIBRARY_EXT ".a")
    set(DYNAMICLIBRARY_EXT ".so")
    set(EXECUTABLE_EXT "")
endif(UNIX)
if (WIN32)
    set(STATICLIBRARY_EXT ".lib")
    set(DYNAMICLIBRARY_EXT ".dll")
    set(EXECUTABLE_EXT ".exe")
endif(WIN32)

# Make runtime files build in the same directory ###################################################
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# Add subdirectory of source and include files #####################################################
add_subdirectory(DynamicLibrary)
add_subdirectory(StaticLibrary)
add_subdirectory(Executable)




set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
