cmake_minimum_required(VERSION 3.5.0)
project(CMakeTest VERSION 0.1.0)

include(CTest)
enable_testing()




# DynamicLibrary ###################################################################################
# Include directories
set(DYNAMICLIBRARY_INCLUDE_DIR_LIST "${CMAKE_CURRENT_SOURCE_DIR}/inc")
set(DYNAMICLIBRARY_INCLUDE_DIR_LIST ${DYNAMICLIBRARY_INCLUDE_DIR_LIST} PARENT_SCOPE)

# Add dynamic library target
add_library(DynamicLibrary
    SHARED 
    "${CMAKE_CURRENT_SOURCE_DIR}/src/dynamiclibrary.cpp"
)

# Add compiler definitions
target_compile_definitions(DynamicLibrary PRIVATE BUILD_SHARED_LIBRARY)

# Add include directories
target_include_directories(DynamicLibrary PUBLIC ${DYNAMICLIBRARY_INCLUDE_DIR_LIST})

# Add library dependencies
if(UNIX)
    target_link_libraries(DynamicLibrary m stdc++)
endif(UNIX)

# Set proper name for static library (lib prefix is not needed)
set_target_properties(DynamicLibrary PROPERTIES PREFIX "")
set_target_properties(DynamicLibrary PROPERTIES SUFFIX "${DYNAMICLIBRARY_EXT}")




set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)