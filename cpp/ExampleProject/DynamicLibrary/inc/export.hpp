# pragma once

#ifdef WIN32
    #ifdef BUILD_SHARED_LIBRARY
        // the dll exports
        #define LIBRARY_SHARE __declspec(dllexport)
    #else
        // the exe imports
        #define LIBRARY_SHARE __declspec(dllimport)
    #endif
#else
    #define LIBRARY_SHARE
#endif
