#pragma once

#include "export.hpp"

namespace ExampleProject
{
    LIBRARY_SHARE double multiply(double x, double y);
}