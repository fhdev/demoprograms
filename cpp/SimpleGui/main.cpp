#include "simplegui.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    SimpleGui w;
    w.show();
    return a.exec();
}
