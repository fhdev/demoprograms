#ifndef SIMPLEGUI_H
#define SIMPLEGUI_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class SimpleGui; }
QT_END_NAMESPACE

class SimpleGui : public QMainWindow
{
    Q_OBJECT

public:
    SimpleGui(QWidget *parent = nullptr);
    ~SimpleGui();

private slots:
    void on_leftButton_clicked();
    void rightButtonClicked();

private:
    Ui::SimpleGui *ui;
};
#endif // SIMPLEGUI_H
