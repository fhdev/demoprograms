#include "simplegui.h"
#include "ui_simplegui.h"

SimpleGui::SimpleGui(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::SimpleGui)
{
    ui->setupUi(this);
    //connect(ui->rightButton, &QPushButton::clicked, this, &SimpleGui::rightButtonClicked);
}

SimpleGui::~SimpleGui()
{
    delete ui;
}


void SimpleGui::on_leftButton_clicked()
{
    ui->leftButton->setText("Clicked.");
}

void SimpleGui::rightButtonClicked()
{
    ui->rightButton->setText("Clicked.");
}

