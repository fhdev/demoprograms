#pragma once

#include <QtWidgets/QMainWindow>
#include <string>
#include "ui_calculator.h"

class Calculator
{
private:
    double lhs_;
    double rhs_;
    unsigned int op_;

public:
    enum : unsigned int { NoOp = 0, Add = '+', Subtr = '-', Mult = '*', Div = '/', Sqrt = 'R', Sqr = 'S', Inv = 'I' };

    Calculator();

    const double& lhs() const;
    void setLhs(const double& value);

    const double& rhs() const;
    void setRhs(const double& value);

    const unsigned int& op() const;
    void setOp(const unsigned int& value);

    double eval(const unsigned int& unOp = NoOp);

    void reset();
};

class CalculatorWindow : public QMainWindow
{
private:
    Q_OBJECT
    Ui::CalculatorClass ui_;
    QString dispStr_;
    bool waitingForOperand_;
    bool waitingForOperator_;
    Calculator calc_;

    void setDispStr(const QString &str);
    void setDispStr(const double &num);

    void digitInput(const unsigned int &num);
    void binOpInput(const unsigned int &op);
    void unOpInput(const unsigned int &op);

    void reset();

protected:
    void keyPressEvent(QKeyEvent* event);

public:
    CalculatorWindow(QWidget *parent = Q_NULLPTR);
    QString dispStr();
    Q_PROPERTY(QString dispStr READ dispStr)



private:
    QString myProperty_;

public:
    QString myProperty() { return myProperty_; }
    void setMyProperty(const QString& value) { myProperty_ = value; }
    Q_PROPERTY(QString myProperty READ myProperty WRITE setMyProperty)


public slots:
    void buttonDigitClicked();
    void dotInput();
    void signInput();
    void bkspInput();
    void clrInput();
    void buttonBinOpClicked();
    void returnInput();
    void buttonUnOpClicked();

signals:
    void dispStrChanged(const QString& dispStr_);

};
