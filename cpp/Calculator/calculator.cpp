#include <QDoubleValidator>
#include <QKeyEvent>
#include <string>
#include "calculator.h"

constexpr char zero = '0';
constexpr char dot = '.';
constexpr char minus = '-';
constexpr int dispPrecision = 18;


Calculator::Calculator() : lhs_(0.0), rhs_(0.0), op_(NoOp) {

}

const double& Calculator::lhs() const {
    return lhs_;
}

void Calculator::setLhs(const double& value) {
    if (lhs_ != value) {
        lhs_ = value;
    }
}

const double& Calculator::rhs() const {
	return rhs_;
}

void Calculator::setRhs(const double& value) {
	if (rhs_ != value) {
		rhs_ = value;
	}
}

const unsigned int& Calculator::op() const
{
	return op_;
}

void Calculator::setOp(const unsigned int& value)
{
    if (op_ != value) {
        op_ = value;
    }
}

double Calculator::eval(const unsigned int& unOp)
{
    unsigned int op = unOp == NoOp ? op_ : unOp;
    switch (op) {
    case Calculator::Add: 
        lhs_ += rhs_;
        break;
    case Calculator::Subtr:
        lhs_ -= rhs_;
        break;
    case Calculator::Mult:
        lhs_ *= rhs_;
        break;
    case Calculator::Div:
        lhs_ /= rhs_;
        break;
    case Calculator::Sqrt:
        lhs_ = sqrt(lhs_);
        break;
    case Calculator::Sqr:
        lhs_ *= lhs_;
        break;
    case Calculator::Inv:
        lhs_ = 1.0 / lhs_;
        break;
    }
    return lhs_;

}

void Calculator::reset()
{
    lhs_ = 0.0;
    rhs_ = 0.0;
    op_ = NoOp;
}

CalculatorWindow::CalculatorWindow(QWidget *parent)
    : QMainWindow(parent)
{
    // Set up UI
    ui_.setupUi(this);
    this->grabKeyboard();

    // Connect signals and slots
    connect(ui_.pushButton0,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton1,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton2,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton3,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton4,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton5,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton6,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton7,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton8,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButton9,      &QPushButton::clicked,             this,         &CalculatorWindow::buttonDigitClicked);
    connect(ui_.pushButtonDot,    &QPushButton::clicked,             this,         &CalculatorWindow::dotInput);
    connect(ui_.pushButtonSign,   &QPushButton::clicked,             this,         &CalculatorWindow::signInput);
    connect(ui_.pushButtonBksp,   &QPushButton::clicked,             this,         &CalculatorWindow::bkspInput);
    connect(ui_.pushButtonClr,    &QPushButton::clicked,             this,         &CalculatorWindow::clrInput);
    connect(ui_.pushButtonPlus,   &QPushButton::clicked,             this,         &CalculatorWindow::buttonBinOpClicked);
    connect(ui_.pushButtonMinus,  &QPushButton::clicked,             this,         &CalculatorWindow::buttonBinOpClicked);
    connect(ui_.pushButtonDiv,    &QPushButton::clicked,             this,         &CalculatorWindow::buttonBinOpClicked);
    connect(ui_.pushButtonMult,   &QPushButton::clicked,             this,         &CalculatorWindow::buttonBinOpClicked);
    connect(ui_.pushButtonEquals, &QPushButton::clicked,             this,         &CalculatorWindow::returnInput);
    connect(ui_.pushButtonSqrt,   &QPushButton::clicked,             this,         &CalculatorWindow::buttonUnOpClicked);
    connect(ui_.pushButtonSqr,    &QPushButton::clicked,             this,         &CalculatorWindow::buttonUnOpClicked);
    connect(ui_.pushButtonInv,    &QPushButton::clicked,             this,         &CalculatorWindow::buttonUnOpClicked);
    connect(this,                 &CalculatorWindow::dispStrChanged, ui_.lineEdit, &QLineEdit::setText);
    
    // Custom initialization
    reset();
    
}

void CalculatorWindow::setDispStr(const QString& str) {
    if (dispStr_ != str) {
        dispStr_.clear();
        dispStr_.append(str);
        emit dispStrChanged(dispStr_);
    }
}

void CalculatorWindow::setDispStr(const double& num) {
    setDispStr(QString::number(num, 'g', dispPrecision));
}

void CalculatorWindow::digitInput(const unsigned int &num) {
    if ((dispStr_ == zero) && (num == zero)) {
        return;
    }
    if (waitingForOperand_) {
        dispStr_.clear();
        waitingForOperand_ = false;
    }
    dispStr_.append(num);
    emit dispStrChanged(dispStr_);
}

void CalculatorWindow::binOpInput(const unsigned int &op) {
    if (waitingForOperator_) {
        calc_.setLhs(dispStr_.toDouble());
    }
    else if (!waitingForOperand_) {
        calc_.setRhs(dispStr_.toDouble());
        double res = calc_.eval();
        setDispStr(res);
        waitingForOperand_ = true;
    }
    calc_.setOp(op);
    waitingForOperator_ = false;
    waitingForOperand_ = true;
}

void CalculatorWindow::unOpInput(const unsigned int &op) {
    calc_.setLhs(dispStr_.toDouble());
    double res = calc_.eval(op);
    setDispStr(res);
}


void CalculatorWindow::reset() {
    calc_.reset();
    waitingForOperand_ = true;
    waitingForOperator_ = true;
    setDispStr(0.0);
}

void CalculatorWindow::buttonDigitClicked()
{
	QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    QString buttonText = buttonSender->text();
    digitInput(buttonText[0].toLatin1());
}

QString CalculatorWindow::dispStr()
{
    return dispStr_;
}

void CalculatorWindow::dotInput()
{
    if (waitingForOperand_) {
        dispStr_.clear();
        dispStr_.append('0');
        waitingForOperand_ = false;
    }
    if (!dispStr_.contains(dot)) {
        dispStr_.append(dot);   
    }
    emit dispStrChanged(dispStr_);
}

void CalculatorWindow::signInput()
{
    if (!dispStr_.isEmpty() && (dispStr_[0] == minus)) {
        dispStr_.remove(0, 1);
    }
    else {
        dispStr_.prepend(minus);
    }
    emit dispStrChanged(dispStr_);
}

void CalculatorWindow::bkspInput()
{
    if (waitingForOperand_) {
        return;
    }
    dispStr_.chop(1);
    if (dispStr_.isEmpty()) {
        dispStr_.append('0');
        waitingForOperand_ = true;
    }
    emit dispStrChanged(dispStr_);
}

void CalculatorWindow::clrInput()
{
    reset();
}

void CalculatorWindow::buttonBinOpClicked() {
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    QString buttonText = buttonSender->text();
    binOpInput(buttonText[0].toLatin1());
}

void CalculatorWindow::keyPressEvent(QKeyEvent* event)
{
    int key = event->key();
    if ((Qt::Key_0 <= key) && (key <= Qt::Key_9)) {
        digitInput(key);
    }
    else if ((key == Qt::Key_Period) || (key == Qt::Key_Comma)) {
        dotInput();
    }
    else if (key == Qt::Key_M) {
        signInput();
    }
    else if (key == Qt::Key_Backspace) {
        bkspInput();
    }
    else if (key == Qt::Key_Delete) {
        clrInput();
    }
    else if ((key == Qt::Key_Plus) || (key == Qt::Key_Minus) || (key == Qt::Key_Asterisk) || (key == Qt::Key_Slash)) {
        binOpInput(key);
    }
    else if (key == Qt::Key_Enter) {
        returnInput();
    }
    else if ((key == Qt::Key_R) || (key == Qt::Key_S) || (key == Qt::Key_I)) {
        unOpInput(key);
    }
}

void CalculatorWindow::returnInput() {
    if (waitingForOperator_) {
        calc_.setLhs(dispStr_.toDouble());
    }
    else {
        calc_.setRhs(dispStr_.toDouble());
    }
    double res = calc_.eval();
    setDispStr(res);
    waitingForOperator_ = true;
    waitingForOperand_ = true;
}

void CalculatorWindow::buttonUnOpClicked() {
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    QString buttonText = buttonSender->text();
    unsigned int op = 0;
    if (buttonText == "sqrt(x)") {
        op = Calculator::Sqrt;
    }
    else if (buttonText == "x^2") {
        op = Calculator::Sqr;
    }
    else if (buttonText == "1/x") {
        op = Calculator::Inv;
    }
    unOpInput(op);
}


