#include <iostream>
#include <cstdint>
#include <limits>

using namespace std;

void function1(uint32_t arg1, uint32_t arg2) {
    cout << "function1: arg1=" << arg1 << "; arg2=" << arg2 << "." << endl;
}

uint16_t function2(uint16_t arg1, uint16_t arg2) {
    cout << "function2: arg1=" << arg1 << "; arg2=" << arg2 << "." << endl;
    return arg1 + arg2;
} 

typedef void (*ptr_to_function1_t)(uint32_t, uint32_t);
typedef uint16_t (*ptr_to_function2_t)(uint16_t, uint16_t);

int main() {
    ptr_to_function1_t ptr_to_function1 { function1 };
    ptr_to_function2_t ptr_to_function2 { function2 };

    ptr_to_function2_t ptr_to_funciton1_as_function2 { reinterpret_cast<ptr_to_function2_t>(ptr_to_function1) };
    ptr_to_function1_t ptr_to_funciton2_as_function1 { reinterpret_cast<ptr_to_function1_t>(ptr_to_function2) };

    {
        uint16_t arg1 { 1u };
        uint16_t arg2 { numeric_limits<uint16_t>::max() };
        cout << "before_call: arg1=" << arg1 << "; arg2=" << arg2 << endl;
        uint16_t out { ptr_to_funciton1_as_function2(arg1, arg2) };
        cout << "after_call: out=" << out << endl;
    }

    {
        uint32_t arg1 { 1u };
        uint32_t arg2 { numeric_limits<uint32_t>::max() };
        cout << "before_call: arg1=" << arg1 << "; arg2=" << arg2 << endl;
        ptr_to_funciton2_as_function1(arg1, arg2);
    }

    return 0;
}