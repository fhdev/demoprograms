#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_notepad.h"

class Notepad : public QMainWindow
{
    Q_OBJECT

public:
    Notepad(QWidget *parent = Q_NULLPTR);

private:
    Ui::NotepadClass ui;
    QString currentFilePath = "";

    void actionNewHandler();
    void actionOpenHandler();
    void actionSaveHandler();
    void actionPrintHandler();
    void actionExitHandler();
    void actionCopyHandler();
    void actionPasteHandler();
    void actionCutHandler();
    void actionUndoHandler();
    void actionRedoHandler();

};
