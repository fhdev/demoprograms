#include "notepad.h"

#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QtPrintSupport/QPrinter>
#include <QtPrintSupport/QPrintDialog>


Notepad::Notepad(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    ui.mainToolBar->setFixedHeight(24);

    connect(ui.actionNew, &QAction::triggered, this, &Notepad::actionNewHandler);
    connect(ui.actionOpen, &QAction::triggered, this, &Notepad::actionOpenHandler);
    connect(ui.actionSave, &QAction::triggered, this, &Notepad::actionSaveHandler);
    connect(ui.actionSaveAs, &QAction::triggered, this, &Notepad::actionSaveHandler);
    connect(ui.actionPrint, &QAction::triggered, this, &Notepad::actionPrintHandler);
    connect(ui.actionExit, &QAction::triggered, this, &Notepad::actionExitHandler);
    connect(ui.actionCopy, &QAction::triggered, this, &Notepad::actionCopyHandler);
    connect(ui.actionPaste, &QAction::triggered, this, &Notepad::actionPasteHandler);
    connect(ui.actionCut, &QAction::triggered, this, &Notepad::actionCutHandler);
    connect(ui.actionUndo, &QAction::triggered, this, &Notepad::actionUndoHandler);
    connect(ui.actionRedo, &QAction::triggered, this, &Notepad::actionRedoHandler);
}

void Notepad::actionNewHandler()
{
    setWindowTitle("Notepad");
    currentFilePath.clear();
    ui.textEdit->clear();
}

void Notepad::actionOpenHandler()
{
    QString filePath = QFileDialog::getOpenFileName(this, "Open");
    if (filePath.isEmpty()) return;
    currentFilePath = filePath;
    QFile file(currentFilePath);
    if (!file.open(QIODevice::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Error opening file", "The selected file can not be opened. Reason: " + file.errorString());
        return;
    }
    setWindowTitle(currentFilePath);
    QTextStream in(&file);
    QString text = in.readAll();
    ui.textEdit->setText(text);
    file.close();
}

void Notepad::actionSaveHandler()
{
    QAction *senderAction = qobject_cast<QAction*>(sender());
    if ((senderAction->objectName() == "actionSaveAs") || (currentFilePath == ""))
    {
        QString filePath = QFileDialog::getSaveFileName(this, "Save As");
        if (filePath.isEmpty()) return;
        currentFilePath = filePath;
    }
    QFile file(currentFilePath);
    if (!file.open(QFile::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Error saving file", "The selected file can not be saved. Reason: " + file.errorString());
        return;
    }
    setWindowTitle(currentFilePath);
    QTextStream out(&file);
    QString text = ui.textEdit->toPlainText();
    out << text;
    file.close();
}

void Notepad::actionPrintHandler()
{
    QPrinter printer;
    printer.setPrinterName("Microsoft Print to PDF");
    QPrintDialog pDialog(&printer, this);
    if (pDialog.exec() == QDialog::Rejected)
    {
        QMessageBox::warning(this, "Error printing file", "The file cannot be printed.");
        return;
    }
    ui.textEdit->print(&printer);
}

void Notepad::actionExitHandler()
{
    QApplication::quit();
}

void Notepad::actionCopyHandler()
{
    ui.textEdit->copy();
}

void Notepad::actionPasteHandler()
{
    ui.textEdit->paste();
}

void Notepad::actionCutHandler()
{
    ui.textEdit->cut();
}

void Notepad::actionUndoHandler()
{
    ui.textEdit->undo();
}

void Notepad::actionRedoHandler()
{
    ui.textEdit->redo();
}
