#include <module1.hpp>
#include <module2.hpp>


int main(int argc, char **argv)
{
    module1Function();
    module2Function();
    return 0;
}

void module2Function()
{
    std::cout << "This is module2!" << std::endl;
}
