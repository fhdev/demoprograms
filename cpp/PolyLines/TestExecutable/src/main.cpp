// Testing the calculation of point-line, line-line and polyline-polyline distances.
// Author: Ferenc Hegedus <ferenc.hegedus@outlook.com>
// Date:   2021.05.19.

#include <iostream>
#include <vector>
#include <tuple>
#include <cassert>
#include <algorithm>
#include "polylineslibrary.hpp"

using namespace PolyLines;

int main(int argc, char *argv[])
{

    // Test point to line distance calculation
    int fail = testDistancePoint2Line();
    if (fail == 0) {
        std::cout << "All tests passed for distancePoint2Line()." << std::endl;
    }
    else {
        std::cout << "At least one test failed for distancePoint2Line()." << std::endl;
    }

    // Test line to line distance calculation
    fail = testDistanceLine2Line();
    if (fail == 0) {
        std::cout << "All tests passed for distanceLine2Line()." << std::endl;
    }
    else {
        std::cout << "At least one test failed for distanceLine2Line()." << std::endl;
    }

    // Test polyline distance calculation
    fail = testDistancePolyLine2PolyLine();
    if (fail == 0) {
        std::cout << "All tests passed for testDistancePolyLine2PolyLine()." << std::endl;
    }
    else {
        std::cout << "At least one test failed for testDistancePolyLine2PolyLine()." << std::endl;
    }

    return 0;
}
