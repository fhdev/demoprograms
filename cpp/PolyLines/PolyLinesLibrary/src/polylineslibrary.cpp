// Calculation of point-line, line-line and polyline-polyline distances.
// Author: Ferenc Hegedus <ferenc.hegedus@outlook.com>
// Date:   2021.05.19.
// Remarks: 
//  The task can be solved with external libraries inestead of own implementation.
//  The boost geometry library seems to offer the required calculations:
//  https://www.boost.org/doc/libs/1_69_0/libs/geometry/doc/html/geometry/reference/algorithms/distance/distance_2.html
//  It would be necessary to check if the boost geometry "Linestring" or "MultiLinestring" concept is
//  equivalent to the polylines or not.

#include <iostream>
#include <cmath>
#include <algorithm>
#include "polylineslibrary.hpp"

namespace PolyLines
{
    // arePolylinesCloserThanThreshold
    // Calculates if polylines are closer to each other than 1.5 units
    // Remarks:
    //  This could be calculated faster with quitting immediately if the distance of a line pair is 
    //  smaller than the given threshold. The source code of distancePolyLine2PolyLine can be 
    //  extended with one additional check if the current value of minimalDistance is smaller than  
    //  the threshold.
    bool arePolylinesCloserThanThreshold(std::vector<sPoint2D>& polyline1, std::vector<sPoint2D>& polyline2)
    {
        float DISTANCE_THRESHOLD = 1.5F;
        float distance = distancePolyLine2PolyLine(polyline1, polyline2);
        bool closerThanThreshold = distance < DISTANCE_THRESHOLD;
        return closerThanThreshold;
    }

    // distancePolyLine2PolyLine
    // Calculates the distance of two polylines
    //
    // Principle:
    //  Calculate distance of every line pairs and choose the minimal one. Quit early of an 
    //  intersection is found.
    // 
    // Remarks:
    //  The algorithm has O(n^2) complexity. 
    float distancePolyLine2PolyLine(const std::vector<sPoint2D>& polyline1, const std::vector<sPoint2D>& polyline2)
    {
        // Check arguments -------------------------------------------------------------------------
        if (polyline1.size() < 2) {
            throw std::invalid_argument("Error: polyline1 must have at least 2 points.");
        }
        if (polyline2.size() < 2) {
            throw std::invalid_argument("Error: polyline2 must have at least 2 points.");
        }
        // Output variables ------------------------------------------------------------------------
        float minimalDistance = INFINITY;
        // Calculate distance ----------------------------------------------------------------------
        // Calculate distance for all line pairs (complexity is O(n^2))
        for (size_t i = 0; i < polyline1.size() - 1; i++)
        {
            for (size_t j = 0; j < polyline2.size() - 1; j++)
            {
                std::vector<sPoint2D> linePoints1(polyline1.begin() + i, polyline1.begin() + i + 2);
                std::vector<sPoint2D> linePoints2(polyline2.begin() + j, polyline2.begin() + j + 2);
                float distance = 0.0F;
                std::vector<sPoint2D> closestPoints {sPoint2D(0.0F, 0.0F), sPoint2D(0.0F, 0.0F)};
                std::tie(distance, closestPoints) = distanceLine2Line(linePoints1, linePoints2);
                if (distance < minimalDistance) {
                    minimalDistance = distance;
                }
                // Quit if polylines have an intersection
                if (minimalDistance == 0.0F) { break;}
            }
            // Quit if polylines have an intersection
            if (minimalDistance == 0.0F) { break;}
        }
        return minimalDistance;
    }
    // distancePoint2Line
    // Calculates the distance of a point from a line segment specified by a point pair.
    //
    // Sources:
    //   http://paulbourke.net/geometry/pointlineplane
    //
    // Principle:
    //   We have a line L specified by points l1 = [x1, y1] and l2 = [x2, y2].
    //   Points along L from l1 to l2 are given as
    //       l = l1 + u (l2 - l1)    [x1, y1] + u [x2 - x1, y2 - y1]
    //   The nearest point on the line is where the segment [l, p] is perpendicular to the line
    //       (p - l) . (l2 - l1) = 0
    //   which gives
    //       [p - l1 - u(l2 - l1)] . (l2 - l1) = 0
    //       u = (p - l1) . (l2 - l1) / (l2 - 1) . (l2 - 1)
    //       u = ((xp - x1)(x2 - x1) + (yp - y1)(y2 - y1)) / ((x2 - x1)^2 + (y2 - y1)^2)
    //
    // Extension:
    //   For extension to 3D only the z coordinate must be considered as well in the same equation.
    std::tuple<float, sPoint2D> distancePoint2Line(const sPoint2D &point, const std::vector<sPoint2D> &linePoints, 
        bool clamp /* = true */)
    {
        // Check arguments -------------------------------------------------------------------------
        if (linePoints.size() != 2) {
            throw std::invalid_argument("Error: linePoints must have 2 elements.");
        }
        // Output variables ------------------------------------------------------------------------
        float distance = 0.0F;
        sPoint2D closestLinePoint(0.0F, 0.0F);
        // Calculate distance ----------------------------------------------------------------------
        sPoint2D toPointFromLinePoint1 = directionVector(point, linePoints[0]);
        if (isEqual(linePoints[0], linePoints[1])) {
            // The two points of the line are identical, calculate point to point distance
            distance = std::sqrt(dotProduct(toPointFromLinePoint1, toPointFromLinePoint1));
            closestLinePoint = linePoints[0];
        }
        else {
            // The two points are not identical, calculate point to line distance
            sPoint2D lineDirVect = directionVector(linePoints[1], linePoints[0]);
            float u = dotProduct(toPointFromLinePoint1, lineDirVect) / dotProduct(lineDirVect, lineDirVect);
            if (clamp) {
                // Restrict to end points
                u = std::min(std::max(0.0F, u), 1.0F);
            }
            closestLinePoint = addVectors(linePoints[0], multiplyByScalar(u, lineDirVect));
            sPoint2D toPointFromClosestLinePoint = directionVector(point, closestLinePoint);
            distance = std::sqrt(dotProduct(toPointFromClosestLinePoint, toPointFromClosestLinePoint));
        }
        // Return output ---------------------------------------------------------------------------
        return std::make_tuple(distance, closestLinePoint);
    }

    // distanceLine2Line
    // Calculates the distance of two line segments, each specified by one point pair.
    //
    // Sources:
    //   https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
    //   https://mathworld.wolfram.com/Line-LineIntersection.html
    //
    // Principle:
    //  We have the first line L1 specified by points l11 = [x11, y11] and l12 = [x12, y12].
    //  Points along L1 from l11 to l12 are given as
    //      l1 = l11 + u (l12 - l11)    [x11, y11] + u [x12 - x11, y12 - y11]
    //  We have the second line L2 specified by points l21 = [x21, y21] and l22 = [x22, y22].
    //  Points along L2 from l21 to l22 are given as
    //      l2 = l21 + t (l22 - l21)    [x21, y21] + t [x22 - x21, y22 - y21]
    //  Intersection point is found in terms of values of u and t.
    // 
    // Extension:
    //  For 3D, the calculation of u and t must be extended, and distance needs to be found between
    //  the intersection points on both lines.
    std::tuple<float, std::vector<sPoint2D>> distanceLine2Line(const std::vector<sPoint2D> &linePoints1, 
        const std::vector<sPoint2D> &linePoints2, bool clamp /* = true */)
    {
        // Check arguments -------------------------------------------------------------------------
        if (linePoints1.size() != 2) {
            throw std::invalid_argument("Error: linePoints1 must have 2 elements.");
        }
        if (linePoints2.size() != 2) {
            throw std::invalid_argument("Error: linePoints2 must have 2 elements.");
        }
        // Output variables ------------------------------------------------------------------------
        float distance = 0.0F;
        std::vector<sPoint2D> closestPoints {sPoint2D(0.0F, 0.0F), sPoint2D(0.0F, 0.0F)};
        // Calculate distance ----------------------------------------------------------------------
        // Find intersection point
        float x = (((linePoints1[0].x - linePoints1[1].x) * (linePoints2[0].y - linePoints2[1].y)) -
            ((linePoints1[0].y - linePoints1[1].y) * (linePoints2[0].x - linePoints2[1].x)));
        float u = (((linePoints1[0].x - linePoints2[0].x) * (linePoints2[0].y - linePoints2[1].y)) -
            ((linePoints1[0].y - linePoints2[0].y) * (linePoints2[0].x - linePoints2[1].x))) / x;
        float t = (((linePoints1[1].x - linePoints1[0].x) * (linePoints1[0].y - linePoints2[0].y)) -
            ((linePoints1[1].y - linePoints1[0].y) * (linePoints1[0].x - linePoints2[0].x))) / x;
        if (((0 <= u) && (u <= 1) && (0 <= t) && (t <= 1)) ||
            (!clamp && !std::isinf(u) && !std::isinf(t) && !std::isnan(u) && !std::isnan(t))) {
            // [line segments are intersecting] or [lines are intersecting and clamping is switched off]
            distance = 0;
            sPoint2D lineDirVect1 = directionVector(linePoints1[1], linePoints1[0]);
            closestPoints[0] = addVectors(linePoints1[0], multiplyByScalar(u, lineDirVect1));
            sPoint2D lineDirVect2 = directionVector(linePoints2[1], linePoints2[0]);
            closestPoints[1] = addVectors(linePoints2[0], multiplyByScalar(t, lineDirVect2));
        }
        else {
            // [line segments are not intersecting]
            // Check distances between the end points of one segment and the line of the other segment
            std::vector<float> dists(4, 0.0F);
            std::vector<sPoint2D> clPts (4, sPoint2D(0.0F, 0.0F));
            std::tie(dists[0], clPts[0]) = distancePoint2Line(linePoints1[0], linePoints2, clamp);
            std::tie(dists[1], clPts[1]) = distancePoint2Line(linePoints1[1], linePoints2, clamp);
            std::tie(dists[2], clPts[2]) = distancePoint2Line(linePoints2[0], linePoints1, clamp);
            std::tie(dists[3], clPts[3]) = distancePoint2Line(linePoints2[1], linePoints1, clamp);
            std::vector<float>::iterator minDists = std::min_element(dists.begin(), dists.end());
            int64_t iMin = minDists - dists.begin();
            distance = *minDists;
            // Set closest point for both segments
            if (iMin < 2) {
                // Closest point is on the second line line
                closestPoints[0] = linePoints1[iMin];
                closestPoints[1] = clPts[iMin];
            }
            else {
                // Closest point is on the first line
                closestPoints[0] = clPts[iMin];
                closestPoints[1] = linePoints2[iMin - 2];
            }
        }
        // Return output ---------------------------------------------------------------------------
        return std::make_tuple(distance, closestPoints);
    }

    // isEqual
    // Calculates if two points are equal.
    bool isEqual(const sPoint2D &point1, const sPoint2D &point2)
    {
        return (point1.x == point2.x) && (point1.y == point2.y);
    }

    // dotProduct
    // Calculates the dot product of two 2D vectors
    float dotProduct(const sPoint2D &vect1, const sPoint2D &vect2)
    {
        return vect1.x * vect2.x + vect1.y * vect2.y;
    }

    // directionVector
    // Calculates direction vector to point [to] from point [from]
    sPoint2D directionVector(const sPoint2D &to, const sPoint2D &from)
    {
        return sPoint2D(to.x - from.x, to.y - from.y);
    }

    // addVectors
    // Adds two 2D vectors
    sPoint2D addVectors(const sPoint2D &vect1, const sPoint2D &vect2)
    {
        return sPoint2D(vect1.x + vect2.x, vect1.y + vect2.y);
    }
    
    // multiplyByScalar
    // Multiply vector by scalar number
    sPoint2D multiplyByScalar(float scalar, const sPoint2D &vect)
    {
        return sPoint2D(scalar * vect.x, scalar * vect.y);
    }

    // testDistancePoint2Line
    // Test function for point to line distance calculation
    int testDistancePoint2Line()
    {
        // point to line difference ----------------------------------------------------------------
        int fail = 0;
        // test inputs
        std::vector<sPoint2D> inputPoints {
            sPoint2D( 0.0F,  0.0F),   // all points at same location
            sPoint2D( 0.0F,  0.0F),   // point at segment end point
            sPoint2D( 0.0F,  5.0F),   // point on line
            sPoint2D( 5.0F,  5.0F),   // point is closest to line segment 1
            sPoint2D(-5.0F,  5.0F),   // point is closest to line segment 2
            sPoint2D( 0.0F, 10.0F),   // point is closest to line segment 3
            sPoint2D( 5.0F,  0.0F),   // point is closest to line segment 4
            sPoint2D( 5.0F, -5.0F),   // point is closest to line segment endpoint 1
            sPoint2D( 1.0F,  1.0F),   // point is closest to line segment endpoint 2
            sPoint2D( 1.0F,  4.0F),   // point is closest to line segment endpoint 3
        }; 
        std::vector<std::vector<sPoint2D>> inputLinePoints { 
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F,  0.0F)},  // all points at same location
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // point at segment end point
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // point on line
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // point is closest to line segment 1
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // point is closest to line segment 2
            {sPoint2D(0.0F, 0.0F), sPoint2D(10.0F, 10.0F)},  // point is closest to line segment 3
            {sPoint2D(0.0F, 0.0F), sPoint2D(10.0F, 10.0F)},  // point is closest to line segment 4
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // point is closest to line segment endpoint 1
            {sPoint2D(2.0F, 4.0F), sPoint2D( 6.0F,  6.0F)},  // point is closest to line segment endpoint 2
            {sPoint2D(5.0F, 2.0F), sPoint2D(11.0F,  4.0F)},  // point is closest to line segment endpoint 3
        }; 
        // required outputs
        std::vector<float> outputDistancesClamp { 
            0.0F,                     // all points at same location
            0.0F,                     // point at segment end point
            0.0F,                     // point on line
            5.0F,                     // point is closest to line segment 1
            5.0F,                     // point is closest to line segment 2
            5.0F * std::sqrt(2.0F),   // point is closest to line segment 3
            2.5F * std::sqrt(2.0F),   // point is closest to line segment 4
            5.0F * std::sqrt(2.0F),   // point is closest to line segment endpoint 1
            std::sqrt(10.0F),         // point is closest to line segment endpoint 2
            std::sqrt(20.0F),         // point is closest to line segment endpoint 3
        }; 
        std::vector<sPoint2D> outputClosestPointsClamp {
            sPoint2D(0.0F, 0.0F),   // all points at same location
            sPoint2D(0.0F, 0.0F),   // point at segment end point
            sPoint2D(0.0F, 5.0F),   // point on line
            sPoint2D(0.0F, 5.0F),   // point is closest to line segment 1
            sPoint2D(0.0F, 5.0F),   // point is closest to line segment 2
            sPoint2D(5.0F, 5.0F),   // point is closest to line segment 3
            sPoint2D(2.5F, 2.5F),   // point is closest to line segment 4
            sPoint2D(0.0F, 0.0F),   // point is closest to line segment endpoint 1
            sPoint2D(2.0F, 4.0F),   // point is closest to line segment endpoint 2
            sPoint2D(5.0F, 2.0F),   // point is closest to line segment endpoint 3
        }; 
        std::vector<float> outputDistancesNoClamp { 
            0.0F,                    // all points at same location
            0.0F,                    // point at segment end point
            0.0F,                    // point on line
            5.0F,                    // point is closest to line segment 1
            5.0F,                    // point is closest to line segment 2
            5.0F * std::sqrt(2.0F),  // point is closest to line segment 3
            2.5F * std::sqrt(2.0F),   // point is closest to line segment 4
            5.0F,                    // point is closest to line segment endpoint 1
            std::sqrt(5.0F),         // point is closest to line segment endpoint 2
            std::sqrt(10.0F),        // point is closest to line segment endpoint 3
        }; 
        std::vector<sPoint2D> outputClosestPointsNoClamp {
            sPoint2D(0.0F,  0.0F),   // all points at same location
            sPoint2D(0.0F,  0.0F),   // point at segment end point
            sPoint2D(0.0F,  5.0F),   // point on line
            sPoint2D(0.0F,  5.0F),   // point is closest to line segment 1
            sPoint2D(0.0F,  5.0F),   // point is closest to line segment 2
            sPoint2D(5.0F,  5.0F),   // point is closest to line segment 3
            sPoint2D(2.5F,  2.5F),   // point is closest to line segment 4
            sPoint2D(0.0F, -5.0F),   // point is closest to line segment endpoint 1
            sPoint2D(0.0F,  3.0F),   // point is closest to line segment endpoint 2
            sPoint2D(2.0F,  1.0F),   // point is closest to line segment endpoint 3
        }; 
        // Test loop
        for (size_t i = 0; i < inputPoints.size(); i++)
        {
            // Actual inputs
            sPoint2D point = inputPoints[i];
            std::vector<sPoint2D> linePoints = inputLinePoints[i];
            // Actual expected outputs
            float outputDistance = outputDistancesClamp[i];
            sPoint2D outputClosestPoint = outputClosestPointsClamp[i];
            // Run function distancePoint2Line with clamping
            float distance = 0.0F;
            sPoint2D closestPoint(0.0F, 0.0F);
            std::tie(distance, closestPoint) = distancePoint2Line(point, linePoints, true);
            // Test verdict with clamping
            if ((distance == outputDistance) && isEqual(closestPoint, outputClosestPoint)) {
                std::cout << "[P] Test " << i << " with clamping passed." << std::endl;
            } 
            else {
                std::cout << "[F] Test " << i << " with clamping FAILED." << std::endl;
                fail = 1;
            }
            // Actual expected outputs
            outputDistance = outputDistancesNoClamp[i];
            outputClosestPoint = outputClosestPointsNoClamp[i];
            // Run function distancePoint2Line without clamping
            std::tie(distance, closestPoint) = distancePoint2Line(point, linePoints, false);
            // Test verdict without clamping
            if ((distance == outputDistance) && isEqual(closestPoint, outputClosestPoint)) {
                std::cout << "[P] Test " << i << " without clamping passed." << std::endl;
            } 
            else {
                std::cout << "[F] Test " << i << " without clamping FAILED." << std::endl;
                fail = 1;
            }
        }
        return fail;
    }

    // testDistanceLine2Line
    // Test function for line to line distance calculation
    int testDistanceLine2Line()
    {
        // point to line distance ------------------------------------------------------------------
        int fail = 0;
        // test inputs
        std::vector<std::vector<sPoint2D>> inputLinePoints1 { 
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // overlapping 1
            {sPoint2D(0.0F, 1.0F), sPoint2D( 0.0F,  9.0F)},  // overlapping 2
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // parallel 1
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // parallel 2
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // colinear
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // intersecting 1
            {sPoint2D(0.0F, 0.0F), sPoint2D(10.0F, 10.0F)},  // intersecting 2
            {sPoint2D(0.0F, 0.0F), sPoint2D( 0.0F, 10.0F)},  // general 1
            {sPoint2D(3.0F, 3.0F), sPoint2D( 7.0F,  5.0F)},  // general 2
            {sPoint2D(1.0F, 1.0F), sPoint2D( 4.0F,  1.0F)},  // general 3
        }; 
        std::vector<std::vector<sPoint2D>> inputLinePoints2 { 
            {sPoint2D( 0.0F,  0.0F), sPoint2D( 0.0F, 10.0F)},  // overlapping 1
            {sPoint2D( 0.0F,  0.0F), sPoint2D( 0.0F, 10.0F)},  // overlapping 2
            {sPoint2D( 1.0F,  0.0F), sPoint2D( 1.0F, 10.0F)},  // parallel 1
            {sPoint2D( 1.0F,  1.0F), sPoint2D( 1.0F,  9.0F)},  // parallel 2
            {sPoint2D( 0.0F, 11.0F), sPoint2D( 0.0F, 21.0F)},  // colinear
            {sPoint2D(-5.0F,  5.0F), sPoint2D( 5.0F,  5.0F)},  // intersecting 1
            {sPoint2D(10.0F,  0.0F), sPoint2D( 0.0F, 10.0F)},  // intersecting 2
            {sPoint2D( 1.0F,  1.0F), sPoint2D(10.0F, 10.0F)},  // general 1
            {sPoint2D( 2.0F,  1.0F), sPoint2D( 5.0F, -2.0F)},  // general 2
            {sPoint2D( 3.0F,  2.0F), sPoint2D( 3.0F,  5.0F)},  // general 3
        }; 
        // required outputs
        std::vector<float> outputDistancesClamp { 
            0.0F,                     // overlapping 1
            0.0F,                     // overlapping 2
            1.0F,                     // parallel 1
            1.0F,                     // parallel 2
            1.0F,                     // colinear
            0.0F,                     // intersecting 1
            0.0F,                     // intersecting 2
            1.0F,                     // general 1
            std::sqrt(5.0F),          // general 2
            1.0F,                     // general 3
        }; 
        std::vector<std::vector<sPoint2D>> outputClosestPointsClamp { 
            {sPoint2D(0.0F,  0.0F), sPoint2D(0.0F,  0.0F)},  // overlapping 1
            {sPoint2D(0.0F,  1.0F), sPoint2D(0.0F,  1.0F)},  // overlapping 2
            {sPoint2D(0.0F,  0.0F), sPoint2D(1.0F,  0.0F)},  // parallel 1
            {sPoint2D(0.0F,  1.0F), sPoint2D(1.0F,  1.0F)},  // parallel 2
            {sPoint2D(0.0F, 10.0F), sPoint2D(0.0F, 11.0F)},  // colinear
            {sPoint2D(0.0F,  5.0F), sPoint2D(0.0F,  5.0F)},  // intersecting 1
            {sPoint2D(5.0F,  5.0F), sPoint2D(5.0F,  5.0F)},  // intersecting 2
            {sPoint2D(0.0F,  1.0F), sPoint2D(1.0F,  1.0F)},  // general 1
            {sPoint2D(3.0F,  3.0F), sPoint2D(2.0F,  1.0F)},  // general 2
            {sPoint2D(3.0F,  1.0F), sPoint2D(3.0F,  2.0F)},  // general 3
        };
        std::vector<float> outputDistancesNoClamp { 
            0.0F,                    // overlapping 1
            0.0F,                    // overlapping 2
            1.0F,                    // parallel 1
            1.0F,                    // parallel 2
            0.0F,                    // colinear
            0.0F,                    // intersecting 1
            0.0F,                    // intersecting 2
            0.0F,                    // general 1
            0.0F,                    // general 2
            0.0F,                    // general 3
        }; 
        std::vector<std::vector<sPoint2D>> outputClosestPointsNoClamp { 
            {sPoint2D(0.0F, 0.0F), sPoint2D(0.0F, 0.0F)},  // overlapping 1
            {sPoint2D(0.0F, 1.0F), sPoint2D(0.0F, 1.0F)},  // overlapping 2
            {sPoint2D(0.0F, 0.0F), sPoint2D(1.0F, 0.0F)},  // parallel 1
            {sPoint2D(0.0F, 0.0F), sPoint2D(1.0F, 0.0F)},  // parallel 2
            {sPoint2D(0.0F, 0.0F), sPoint2D(0.0F, 0.0F)},  // colinear
            {sPoint2D(0.0F, 5.0F), sPoint2D(0.0F, 5.0F)},  // intersecting 1
            {sPoint2D(5.0F, 5.0F), sPoint2D(5.0F, 5.0F)},  // intersecting 2
            {sPoint2D(0.0F, 0.0F), sPoint2D(0.0F, 0.0F)},  // general 1
            {sPoint2D(1.0F, 2.0F), sPoint2D(1.0F, 2.0F)},  // general 2
            {sPoint2D(3.0F, 1.0F), sPoint2D(3.0F, 1.0F)},  // general 3
        };
        // Test loop
        for (size_t i = 0; i < inputLinePoints1.size(); i++)
        {
            // Actual inputs
            std::vector<sPoint2D> linePoints1 = inputLinePoints1[i];
            std::vector<sPoint2D> linePoints2 = inputLinePoints2[i];
            // Actual expected outputs
            float outputDistance = outputDistancesClamp[i];
            std::vector<sPoint2D> outputClosestPoints = outputClosestPointsClamp[i];
            // Run function distancePoint2Line with clamping
            float distance = 0.0F;
            std::vector<sPoint2D> closestPoints {sPoint2D(0.0F, 0.0F), sPoint2D(0.0F, 0.0F)};
            std::tie(distance, closestPoints) = distanceLine2Line(linePoints1, linePoints2, true);
            // Test verdict with clamping
            if ((distance == outputDistance) && isEqual(closestPoints[0], outputClosestPoints[0]) &&
                isEqual(closestPoints[1], outputClosestPoints[1])) {
                std::cout << "[P] Test " << i << " with clamping passed." << std::endl;
            } 
            else {
                std::cout << "[F] Test " << i << " with clamping FAILED." << std::endl;
                fail = 1;
            }
            // Actual expected outputs
            outputDistance = outputDistancesNoClamp[i];
            outputClosestPoints = outputClosestPointsNoClamp[i];
            // Run function distancePoint2Line without clamping
            std::tie(distance, closestPoints) = distanceLine2Line(linePoints1, linePoints2, false);
            // Test verdict with clamping
            if ((distance == outputDistance) && isEqual(closestPoints[0], outputClosestPoints[0]) &&
                isEqual(closestPoints[1], outputClosestPoints[1])) {
                std::cout << "[P] Test " << i << " without clamping passed." << std::endl;
            } 
            else {
                std::cout << "[F] Test " << i << " without clamping FAILED." << std::endl;
                fail = 1;
            }
        }
        return fail;
    }

    // testDistancePolyLine2PolyLine
    // Test function for polyline to polyline distance calculation
    int testDistancePolyLine2PolyLine()
    {
        // point to line distance ------------------------------------------------------------------
        int fail = 0;
        // test inputs
        std::vector<std::vector<sPoint2D>> inputLinePoints1 { 
            { sPoint2D(2.0F, 3.0F), sPoint2D(3.0F, 4.0F), sPoint2D(2.0F, 6.0F) },                       // example 1
            { sPoint2D(1.0F, 1.0F), sPoint2D(2.0F, 2.0F), sPoint2D(4.0F, 2.0F), sPoint2D(6.0F, 4.0F) }, // example 2
            { sPoint2D(1.0F, 1.0F), sPoint2D(2.0F, 2.0F), sPoint2D(2.0F, 4.0F), sPoint2D(3.0F, 5.0F) }, // example 3
            { sPoint2D(1.0F, 1.0F), sPoint2D(3.0F, 3.0F), sPoint2D(3.0F, 5.0F) },                       // example 4
        };
        std::vector<std::vector<sPoint2D>> inputLinePoints2{
            { sPoint2D(5.0F, 6.0F), sPoint2D(5.0F, 4.0F), sPoint2D(7.0F, 4.0F), sPoint2D(7.0F, 2.0F) }, // example 1
            { sPoint2D(2.0F, 4.0F), sPoint2D(4.0F, 4.0F), sPoint2D(6.0F, 6.0F), sPoint2D(6.0F, 8.0F) }, // example 2
            { sPoint2D(4.0F, 2.0F), sPoint2D(3.0F, 3.0F), sPoint2D(4.0F, 4.0F), sPoint2D(4.0F, 6.0F) }, // example 3
            { sPoint2D(1.0F, 4.0F), sPoint2D(5.0F, 4.0F), sPoint2D(5.0F, 2.0F) },                       // example 4
        };
        // required outputs
        std::vector<float> outputDistances { 
            2.0F,               // example 1
            std::sqrt(2.0F),    // example 2
            1.0F,               // example 3
            0.0F,               // example 3
        };
        // Test loop
        for (size_t i = 0; i < inputLinePoints1.size(); i++)
        {
            // Actual inputs
            std::vector<sPoint2D> linePoints1 = inputLinePoints1[i];
            std::vector<sPoint2D> linePoints2 = inputLinePoints2[i];
            // Actual expected output
            float outputDistance = outputDistances[i];
            // Run function
            float distance = distancePolyLine2PolyLine(linePoints1, linePoints2);
            // Test verdict
            if (distance == outputDistance) {
                std::cout << "[P] Test " << i << " passed." << std::endl;
            } 
            else {
                std::cout << "[F] Test " << i << " FAILED." << std::endl;
                fail = 1;
            }
        }
        return fail;
    }
    
}