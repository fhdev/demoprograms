// Calculation of point-line, line-line and polyline-polyline distances.
// Author: Ferenc Hegedus <ferenc.hegedus@outlook.com>
// Date:   2021.05.19.
#pragma once

#include <tuple>
#include <vector>

namespace PolyLines
{
    struct sPoint2D
    {   
        sPoint2D(float xValue, float yValue)
        {
            x = xValue;
            y = yValue;
        }
        float x;
        float y;
    };

    bool arePolylinesCloserThanThreshold(std::vector<sPoint2D>& polyline1, std::vector<sPoint2D>& polyline2);

    float distancePolyLine2PolyLine(const std::vector<sPoint2D>& polyline1, const std::vector<sPoint2D>& polyline2);

    std::tuple<float, sPoint2D> distancePoint2Line(const sPoint2D &point, const std::vector<sPoint2D> &linePoints, bool clamp = true);

    std::tuple<float, std::vector<sPoint2D>> distanceLine2Line(const std::vector<sPoint2D> &linePoints1, const std::vector<sPoint2D> &linePoints2, bool clamp = true);

    bool isEqual(const sPoint2D &point1, const sPoint2D &point2);

    float dotProduct(const sPoint2D &vect1, const sPoint2D &vect2);

    sPoint2D directionVector(const sPoint2D &to, const sPoint2D &from);

    sPoint2D addVectors(const sPoint2D &vect1, const sPoint2D &vect2);

    sPoint2D multiplyByScalar(float scalar, const sPoint2D &vect);

    int testDistancePoint2Line();

    int testDistanceLine2Line();

    int testDistancePolyLine2PolyLine();

}