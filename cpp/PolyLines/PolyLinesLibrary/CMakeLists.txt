cmake_minimum_required(VERSION 3.0.0)
project(CMakeTest VERSION 0.1.0)

include(CTest)
enable_testing()




# PolyLinesLibrary ####################################################################################
# Include directories
set(POLYLINESLIBRARY_INCLUDE_DIR_LIST "${CMAKE_CURRENT_SOURCE_DIR}/inc")
set(POLYLINESLIBRARY_INCLUDE_DIR_LIST ${POLYLINESLIBRARY_INCLUDE_DIR_LIST} PARENT_SCOPE)

# Add static library target PolyLinesLibrary
add_library(PolyLinesLibrary
    STATIC 
    "${CMAKE_CURRENT_SOURCE_DIR}/src/polylineslibrary.cpp"
)

# Add include directories for PolyLinesLibrary
target_include_directories(PolyLinesLibrary PUBLIC ${POLYLINESLIBRARY_INCLUDE_DIR_LIST})

# Add library dependencies
if(UNIX)
    target_link_libraries(PolyLinesLibrary m stdc++)
endif(UNIX)

# Set proper name for static library (lib prefix is not needed)
set_target_properties(PolyLinesLibrary PROPERTIES PREFIX "")
set_target_properties(PolyLinesLibrary PROPERTIES SUFFIX "${STATICLIBRARY_EXT}")




set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)